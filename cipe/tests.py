from datetime import date, timedelta
from decimal import Decimal
from pathlib import Path

from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.core import mail
from django.core.exceptions import ObjectDoesNotExist
from django.core.files import File
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone
from django_countries.fields import Country
from freezegun import freeze_time

from archive.models import ArchiveCIPE
from croixrouge.export import openxml_contenttype
from croixrouge.models import Contact, JournalAcces, Personne, Region, Role, Utilisateur
from croixrouge.stat_utils import Month
from croixrouge.tests import TempMediaRootMixin

from .forms import PrestationForm, SendByEmailForm
from .models import (
    FamilleCIPE,
    GeneralDoc,
    GeneralDocCateg,
    PrestationCIPE,
    PrestationGen,
    PsychoItem,
    SuiviEnfant,
    ThemeDoc,
    ThemeSante,
)
from .percentile import GraphEmpty, GraphPercentilePoids


class InitialDataMixin:
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        Role.objects.bulk_create([
            Role(nom='Père', famille=True),
            Role(nom='Mère', famille=True),
            Role(nom='Enfant suivi', famille=True),
            Role(nom='Enfant non-suivi', famille=True),
            Role(nom='IPE', famille=False),
        ])
        Region.objects.bulk_create([
            Region(nom="Neuchâtel", rue="av. du Premier-Mars 2a", secteurs=["cipe"]),
            Region(nom="La Chaux-de-Fonds", rue="rue de la Paix 71", secteurs=["cipe"]),
            Region(nom="Le Locle", rue="rue Daniel-Jeanrichard 9", secteurs=["cipe"]),
            Region(nom='Boudry', secteurs=['cipe']),
            Region(nom='Cernier', secteurs=['cipe']),
            Region(nom='Couvet', secteurs=['cipe']),
            Region(nom='Fleurier', secteurs=['cipe']),
        ])

        cls.cipe_group = Group.objects.create(name='ipe')
        cls.cipe_admin_group = Group.objects.create(name='admin ipe')
        cls.cipe_group.permissions.add(
            *Permission.objects.filter(codename__in=[
                'add_famillecipe', 'view_famillecipe', 'change_famillecipe',
                'view_prestationcipe', 'add_prestationcipe', 'add_prestationgen',
                'add_themesante', 'view_generaldoc',
            ])
        )
        cls.cipe_admin_group.permissions.add(
            *Permission.objects.filter(codename__in=[
                'add_famillecipe', 'view_famillecipe', 'change_famillecipe', 'delete_famillecipe',
                'view_prestationcipe', 'add_prestationcipe', 'add_prestationgen',
                'add_themesante', 'view_generaldoc', 'can_archive',
            ])
        )
        cls.user_cipe = Utilisateur.objects.create_user(
            'user_cipe', 'user_cipe@example.org',
            nom='Ipe', prenom='Laure',
        )
        cls.user_cipe.groups.add(cls.cipe_group)
        cls.user_cipe.roles.add(Role.objects.get(nom='IPE'))
        cls.admin_user = Utilisateur.objects.create_user(
            'admin_cipe', 'admin_cipe@example.org', nom='Admin Ipe'
        )
        cls.admin_user.groups.add(cls.cipe_admin_group)

        cls.famillecipe_content_type = ContentType.objects.get(app_label="cipe", model="famillecipe")

    def _add_famille_avec_enfant(self, fam_kwargs={}, enf_kwargs={}):
        famille = FamilleCIPE.objects.create(
            nom='Martinet', npa='2000', localite='Neuchâtel', region=Region.objects.get(nom="Neuchâtel"),
            **fam_kwargs,
        )
        role_enfant = Role.objects.get(nom='Enfant suivi')
        role_mere = Role.objects.get(nom='Mère')
        Personne.objects.create(
            famille=famille, nom='Martinet', prenom='Greta', role=role_mere,
            email='greta@example.org', telephone='078 444 44 44',
        )
        enfant_kwargs = dict(
            famille=famille, nom='Martinet', prenom='Eva', npa=famille.npa, localite=famille.localite,
            date_naissance=date.today() - timedelta(days=160), role=role_enfant,
        ) | enf_kwargs
        enfant = Personne.objects.create_personne(**enfant_kwargs)
        return famille, enfant


class FamilleCIPETests(InitialDataMixin, TestCase):
    def test_create_famille(self):
        self.client.force_login(self.user_cipe)
        response = self.client.get(reverse('cipe:famille-add'))
        self.assertContains(response, '<label for="id_region">Centre :</label>')
        response = self.client.post(reverse('cipe:famille-add'), data={
            'nom': 'Martin',
            'rue': 'Rue des Fleurs 4',
            'city_auto': '2000 Neuchâtel',
        })
        famille = FamilleCIPE.objects.get(nom='Martin')
        self.assertRedirects(response, reverse('cipe:famille-edit', args=[famille.pk]))
        self.assertEqual(famille.localite, 'Neuchâtel')
        response = self.client.get(reverse('cipe:famille-edit', args=[famille.pk]))
        self.assertContains(response, "Changer l’adresse")

    def test_delete_famille(self):
        famille = FamilleCIPE.objects.create(nom='Martin', localite='Neuchâtel')
        self.client.force_login(self.user_cipe)
        response = self.client.post(reverse('cipe:famille-delete', args=[famille.pk]))
        self.assertEqual(response.status_code, 403)
        self.client.force_login(self.admin_user)
        response = self.client.post(reverse('cipe:famille-delete', args=[famille.pk]))
        self.assertEqual(response.status_code, 403)
        famille.archived_at = timezone.now()
        famille.save()
        response = self.client.post(reverse('cipe:famille-delete', args=[famille.pk]))
        self.assertRedirects(response, reverse('cipe:famille-list'))
        self.assertEqual(FamilleCIPE.objects.filter(pk=famille.pk).count(), 0)

    def test_famille_autocomplete(self):
        famille = FamilleCIPE.objects.create(nom='Martin', localite='Neuchâtel')
        self.client.force_login(self.user_cipe)
        response = self.client.get(reverse('cipe:famille-autocomplete') + '?q=mar')
        self.assertEqual(response.json(), {
            'pagination': {'more': False},
            'results': [{
                'id': str(famille.pk),
                'selected_text': 'Martin,  Neuchâtel',
                'text': 'Martin,  Neuchâtel',
            }]
        })

    def test_add_enfant_suivi(self):
        famille = FamilleCIPE.objects.create(nom='Martin', localite='Neuchâtel')
        role_enfant = Role.objects.get(nom='Enfant suivi')
        url = '{}?role={}'.format(
            reverse('cipe:personne-add', args=[famille.pk]), role_enfant.pk
        )
        self.client.force_login(self.user_cipe)
        response = self.client.post(url, data={
            'role': role_enfant.pk,
            'nom': 'Martinet',
            'prenom': 'Paul',
            'genre': 'M',
            'date_naissance': date.today() - timedelta(days=60),
            'prescripteur': 'M',
        })
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        enfant = famille.membres.get(prenom='Paul')
        self.assertRedirects(response, reverse('cipe:enfant-perinatal', args=[famille.pk, enfant.pk]))
        self.assertEqual(enfant.role, role_enfant)
        # A suivi model was automatically created.
        self.assertIsInstance(enfant.suivienfant, SuiviEnfant)
        self.assertEqual(enfant.suivienfant.get_prescripteur_display(), 'Maternité')

    def test_add_enfant_non_suivi(self):
        famille = FamilleCIPE.objects.create(nom='Martin', localite='Neuchâtel')
        role_enfant = Role.objects.get(nom='Enfant non-suivi')
        url = '{}?role={}'.format(
            reverse('cipe:personne-add', args=[famille.pk]), role_enfant.pk
        )
        self.client.force_login(self.user_cipe)
        response = self.client.post(url, data={
            'role': role_enfant.pk,
            'nom': 'Martinet',
            'prenom': 'Paul',
            'genre': 'M',
            'date_naissance': date.today() - timedelta(days=60),
        })
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        enfant = famille.membres.get(prenom='Paul')
        self.assertRedirects(response, reverse('cipe:famille-edit', args=[famille.pk]))
        self.assertEqual(enfant.role, role_enfant)
        # A suivi model was not created.
        with self.assertRaises(ObjectDoesNotExist):
            enfant.suivienfant

    def test_add_enfant_suivi_famille_archivee(self):
        famille = FamilleCIPE.objects.create(
            nom='Martin', localite='Neuchâtel', archived_at=timezone.now()
        )
        role_enfant = Role.objects.get(nom='Enfant suivi')
        url = reverse('cipe:personne-add', args=[famille.pk])
        self.client.force_login(self.user_cipe)
        response = self.client.post(url, data={
            'role': role_enfant.pk,
            'nom': 'Martinet',
            'prenom': 'Paul',
            'genre': 'M',
            'date_naissance': date.today() - timedelta(days=60),
            'prescripteur': 'M',
        }, follow=True)
        self.assertContains(response, "La famille a été sortie des archives")
        famille.refresh_from_db()
        self.assertIsNone(famille.archived_at)

    def test_print_pdf_coord(self):
        fam, _ = self._add_famille_avec_enfant()
        self.client.force_login(self.user_cipe)
        response = self.client.get(reverse('cipe:print-coord-famille', args=(fam.pk,)))
        self.assertEqual(
            response['content-disposition'],
            'attachment; filename="martinet_coordonnees.pdf"'
        )
        self.assertEqual(response['content-type'], 'application/pdf')
        self.assertGreater(len(response.getvalue()), 200)

    def test_suivi_premat(self):
        self.assertFalse(SuiviEnfant(age_gest=None).is_premat)
        self.assertFalse(SuiviEnfant(age_gest=37 * 7).is_premat)
        self.assertTrue(SuiviEnfant(age_gest=37 * 7 - 1).is_premat)

    def test_suivi_age_premat(self):
        _, enfant = self._add_famille_avec_enfant()
        enfant.date_naissance = date.today() - timedelta(days=90)
        enfant.suivienfant.age_gest = 40 * 7
        self.assertEqual(enfant.suivienfant.age_premat(), '')
        enfant.suivienfant.age_gest = 35 * 7 + 2
        self.assertEqual(enfant.suivienfant.age_premat(), '8 sem. 1 jour')
        self.assertEqual(enfant.suivienfant.age_premat(date_=date.today() - timedelta(days=7)), '7 sem. 1 jour')
        enfant.date_naissance = None
        self.assertEqual(enfant.suivienfant.age_premat(), '')

    def test_form_perinatal(self):
        famille, enfant = self._add_famille_avec_enfant()
        mere = famille.parents()[0]
        mere.allergies = 'Cacahuètes'
        mere.save()
        self.client.force_login(self.user_cipe)
        form_url = reverse('cipe:enfant-perinatal', args=[famille.pk, enfant.pk])
        response = self.client.get(form_url)
        self.assertContains(response, '<i>Mère :</i> Cacahuètes')
        response = self.client.post(form_url, data={
            'motif_demande': ['primip', 'psychosoc'],
            'poids_naiss': '3.27',
            'taille': '51',
            'age_gest_0': '38',
            'age_gest_1': '4',
        })
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        self.assertRedirects(response, form_url)
        enfant.refresh_from_db()
        self.assertEqual(enfant.suivienfant.motif_demande, ['primip', 'psychosoc'])
        self.assertEqual(enfant.suivienfant.poids_naiss, Decimal('3.270'))
        self.assertEqual(enfant.suivienfant.age_gest, 38 * 7 + 4)
        response = self.client.post(form_url, data={
            'motif_demande': ['primip', 'psychosoc'],
            'poids_naiss': '3.27',
            'taille': '51',
            'age_gest_0': '',
            'age_gest_1': '',
        })
        self.assertRedirects(response, form_url)

    def test_age_gestationnel(self):
        famille, enfant = self._add_famille_avec_enfant()
        enfant.date_naissance = date(year=2020, month=10, day=10)
        enfant.save()
        suivi = SuiviEnfant.objects.get(personne=enfant)
        self.assertEqual(suivi.age_gest_display(), '')
        suivi.age_gest = 271
        suivi.save()
        self.assertEqual(suivi.age_gest_display(), '38 + 5')

    def test_mere_gestite(self):
        famille, enfant = self._add_famille_avec_enfant()
        enfant.suivienfant.gestite = 2
        enfant.suivienfant.save()
        self.assertEqual(famille.parents()[0].gestite, 2)

    def test_psycho_list(self):
        famille, enfant = self._add_famille_avec_enfant()
        enfant.date_naissance = date.today() - timedelta(days=20)
        enfant.save()
        self.client.force_login(self.user_cipe)
        response = self.client.get(reverse('cipe:enfant-psycho', args=[famille.pk, enfant.pk]))
        self.assertContains(response, "L’enfant n’a pas encore un mois")
        enfant.date_naissance = None
        enfant.save()
        response = self.client.get(reverse('cipe:enfant-psycho', args=[famille.pk, enfant.pk]))
        self.assertContains(response, "La date de naissance de l’enfant est manquante.")

    def test_psycho_edition(self):
        famille, enfant = self._add_famille_avec_enfant()
        enfant.date_naissance = date.today() - timedelta(days=40)
        enfant.save()
        item = PsychoItem.objects.create(descriptif="Tonus normal, symétrique ", age_ref=30)

        self.client.force_login(self.user_cipe)
        response = self.client.get(reverse('cipe:enfant-psycho', args=[famille.pk, enfant.pk]))
        self.assertContains(response, "À évaluer")
        response = self.client.post(
            reverse('cipe:enfant-psychoedit', args=[famille.pk, enfant.pk]), data={
                'item': item.pk,
                'acquisition': 'nacq',
                'remarque': 'Pas du tout',
            }
        )
        self.assertTrue(response.content.decode().startswith('<tr>'))
        self.assertContains(response, '<td class="nacq">Non acquis</td>')
        self.assertEqual(enfant.suivienfant.psychoeval_set.count(), 1)
        peval = enfant.suivienfant.psychoeval_set.first()
        self.assertEqual(peval.auteur, self.user_cipe)
        self.assertTrue(peval.can_edit(self.user_cipe))
        response = self.client.post(
            reverse('cipe:item-psychoedit', args=[famille.pk, peval.pk]), data={
                'item': item.pk,
                'acquisition': 'nacq',
                'remarque': 'À moitié',
            }
        )
        peval.refresh_from_db()
        self.assertEqual(peval.remarque, 'À moitié')

    def test_avertissement_consultation(self):
        famille, enfant = self._add_famille_avec_enfant(
            enf_kwargs={'date_naissance': date.today() - timedelta(days=31 * 9 + 5)}
        )
        self.client.force_login(self.user_cipe)
        response = self.client.get(reverse('cipe:famille-list'))
        self.assertEqual(len(response.context['object_list']), 1)
        self.assertContains(response, "À contacter pour contrôle 9 mois")
        PrestationCIPE.objects.create(
            auteur=self.user_cipe, famille=famille, enfant=enfant,
            date=date.today(), duree='00:30', faits_courants='Consultation habituelle'
        )
        response = self.client.get(reverse('cipe:famille-list'))
        self.assertNotContains(response, "À contacter pour contrôle 9 mois")

    def test_famille_filtre(self):
        ntel = Region.objects.par_secteur('cipe').get(nom='Neuchâtel')
        cdf = Region.objects.par_secteur('cipe').get(nom='La Chaux-de-Fonds')
        familles = FamilleCIPE.objects.bulk_create([
            FamilleCIPE(nom='Valjean', typ=['cipe'], region=cdf),
            FamilleCIPE(nom='Haddock', typ=['cipe'], region=ntel),
            FamilleCIPE(nom='Smith', typ=['cipe'], region=cdf),
            FamilleCIPE(nom='Tournesol', typ=['cipe'], region=None),
        ])
        role_enfant = Role.objects.get(nom='Enfant suivi')
        role_mere = Role.objects.get(nom='Mère')
        # No bulk_create as suivi is auto-created in signal
        Personne.objects.create(
            famille=familles[0], nom='Valjean', prenom='Isa', role=role_enfant,
            date_naissance=date.today() - timedelta(weeks=6 * 4),  # 6 mois
        ),
        Personne.objects.create(
            famille=familles[0], nom='Valjean', prenom='Greg', role=role_enfant,
            date_naissance=date.today() - timedelta(weeks=36 * 4),  # 3 ans
        ),
        Personne.objects.create(
            famille=familles[0], nom='Autrenom', prenom='Claire', role=role_mere,
            date_naissance=date(1997, 3, 4), telephone='077 777 77 77',
        ),
        Personne.objects.create(
            famille=familles[1], nom='Haddock', prenom='Jules', role=role_enfant,
            date_naissance=date.today() - timedelta(weeks=24 * 4),  # 2 ans
        ),
        Personne.objects.create(
            famille=familles[1], nom='Haddock', prenom='Line', role=role_enfant,
            date_naissance=date.today() - timedelta(days=30 * 9 + 18),  # 9 mois
        ),
        PrestationCIPE.objects.bulk_create([
            PrestationCIPE(famille=familles[0], date=date(2021, 12, 4)),
            PrestationCIPE(famille=familles[0], date=date(2022, 3, 15)),
            PrestationCIPE(famille=familles[1], date=date(2020, 1, 25)),
            PrestationCIPE(famille=familles[1], date=date(2021, 12, 4), manque=True),
        ])

        self.client.force_login(self.user_cipe)
        list_url = reverse('cipe:famille-list')
        response = self.client.get(list_url)
        self.assertEqual(len(response.context['object_list']), 4)

        response = self.client.get(list_url + '?famille=addo')
        self.assertTrue(response.context['form'].is_valid())
        self.assertEqual(len(response.context['object_list']), 1)

        # Recherche aussi noms de famille autres membres.
        response = self.client.get(list_url + '?famille=aut')
        self.assertTrue(response.context['form'].is_valid())
        self.assertEqual(len(response.context['object_list']), 1)

        response = self.client.get(list_url + '?famille=p')
        self.assertTrue(response.context['form'].is_valid())
        self.assertEqual(len(response.context['object_list']), 0)

        response = self.client.get(list_url + '?region=%s' % cdf.pk)
        self.assertTrue(response.context['form'].is_valid())
        self.assertEqual(len(response.context['object_list']), 2)

        response = self.client.get(list_url + '?region=none')
        self.assertTrue(response.context['form'].is_valid())
        self.assertEqual(len(response.context['object_list']), 1)

        response = self.client.get(list_url + '?age_de=3')
        self.assertTrue(response.context['form'].is_valid())
        self.assertEqual(len(response.context['object_list']), 2)

        response = self.client.get(list_url + '?age_de=28')
        self.assertTrue(response.context['form'].is_valid())
        self.assertEqual(len(response.context['object_list']), 1)

        response = self.client.get(list_url + '?age_de=3&age_a=9')
        self.assertTrue(response.context['form'].is_valid())
        self.assertEqual(len(response.context['object_list']), 1)

        response = self.client.get(list_url + '?age_de=7&age_a=9')
        self.assertTrue(response.context['form'].is_valid())
        self.assertEqual(len(response.context['object_list']), 0)

        response = self.client.get(list_url + '?age_de=0&age_a=40')
        self.assertTrue(response.context['form'].is_valid())
        self.assertEqual(len(response.context['object_list']), 2)

        response = self.client.get(list_url + '?age_a=20')
        self.assertTrue(response.context['form'].is_valid())
        self.assertEqual(len(response.context['object_list']), 2)

        response = self.client.get(list_url + '?consult_de=2020-01-01')
        self.assertTrue(response.context['form'].is_valid())
        self.assertEqual(len(response.context['object_list']), 2)

        response = self.client.get(list_url + '?consult_de=2021-01-01&consult_a=2021-12-31')
        self.assertTrue(response.context['form'].is_valid())
        self.assertEqual(len(response.context['object_list']), 0)

        response = self.client.get(list_url + '?consult_a=2022-01-01')
        self.assertTrue(response.context['form'].is_valid())
        self.assertEqual(len(response.context['object_list']), 1)

        response = self.client.get(list_url + '?tel=7777')
        self.assertTrue(response.context['form'].is_valid())
        self.assertEqual(len(response.context['object_list']), 1)
        self.assertEqual(response.context['object_list'][0].nom, 'Valjean')

        response = self.client.get(list_url + '?a_contacter=contr9')
        self.assertTrue(response.context['form'].is_valid())
        self.assertEqual(len(response.context['object_list']), 1)
        self.assertEqual(response.context['object_list'][0].nom, 'Haddock')

        # Export
        response = self.client.get(list_url + '?age_de=1&export=1')
        self.assertEqual(response.headers['Content-Type'], openxml_contenttype)

    def test_liste_suivis_archives(self):
        arch = ArchiveCIPE.objects.create(
            nom_famille="Schmid",
            nom="Schmid",
            prenom="Lulu",
            npa="2000",
            localite="Neuchâtel",
        )
        self.client.force_login(self.admin_user)
        response = self.client.get(reverse('cipe:suivis-archives'))
        self.assertQuerySetEqual(response.context["object_list"], [])
        response = self.client.get(reverse('cipe:suivis-archives') + '?nom=schmi')
        self.assertQuerySetEqual(response.context["object_list"], [arch])

    def test_acces_famille_sap(self):
        """Une famille s@p n'est pas visible pour les utilisateurs non IPE."""
        famille, enfant = self._add_famille_avec_enfant()
        famille.sap = True
        famille.save()
        self.client.force_login(self.user_cipe)
        response = self.client.get(reverse('cipe:famille-list'))
        self.assertQuerySetEqual(response.context['object_list'], [famille])

        user_spe = Utilisateur.objects.create_user(
            'user_spe', 'user_spe@example.org', nom='Spe', prenom='Prénom',
        )
        user_spe.user_permissions.add(Permission.objects.get(codename='view_famillecipe'))
        self.assertFalse(famille.can_view(user_spe))
        self.client.force_login(user_spe)
        response = self.client.get(reverse('cipe:famille-list'))
        self.assertQuerySetEqual(response.context['object_list'], [])
        blocked_urls = [
            reverse('cipe:famille-edit', args=[famille.pk]),
            reverse('cipe:prestation-famille-list', args=[famille.pk]),
            reverse('cipe:prestation-enfant-list', args=[famille.pk, enfant.pk]),
        ]
        for url in blocked_urls:
            response = self.client.get(url)
            self.assertEqual(response.status_code, 403)


class PrestationsTests(InitialDataMixin, TempMediaRootMixin, TestCase):
    def test_prestations(self):
        famille, enfant = self._add_famille_avec_enfant()
        theme = ThemeSante.objects.create(nom='Santé de la mère')
        neuch = Region.objects.get(nom='Neuchâtel')
        self.client.force_login(self.user_cipe)
        add_url = reverse('cipe:prestation-enfant-add', args=[famille.pk, enfant.pk])
        response = self.client.get(add_url)
        # Default to famille.region
        self.assertContains(response, f'<option value="{neuch.pk}" selected>Neuchâtel</option>')
        self.assertContains(response, 'Martinet Eva (5 mois 1 sem.)')
        self.assertGreater(len(response.context['form']['type_consult'].field.choices), 2)
        response = self.client.post(
            add_url,
            data={
                'date': date.today(),
                'duree': '00:30',
                'type_consult': 'cons_crn',
                'lieu': neuch.pk,
                'faits_courants': 'Contrôle de routine',
                'faits_marquants': '',
                'themes': [theme.pk],
                'poids': '6.5',
            }
        )
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        self.assertRedirects(response, reverse('cipe:prestation-enfant-list', args=[famille.pk, enfant.pk]))
        self.assertEqual(enfant.prestationcipe_set.count(), 1)
        # edit
        edit_url = reverse('cipe:prestation-edit', args=[famille.pk, enfant.prestationcipe_set.first().pk])
        response = self.client.get(edit_url)
        self.assertContains(response, 'Santé de la mère (%s)' % date.today().strftime("%d.%m.%Y"))
        self.assertContains(response, f'<option value="{neuch.pk}" selected>Neuchâtel</option>')
        with (Path('.').parent / 'croixrouge' / 'test.pdf').open(mode='rb') as fh:
            response = self.client.post(
                edit_url,
                data={
                    'date': date.today(),
                    'duree': '00:30',
                    'lieu': '',
                    'type_consult': 'cons_dom',
                    'a_domicile_prest': 'handi',
                    'faits_courants': 'Contrôle de routine',
                    'faits_marquants': '',
                    'poids': '7',
                    'fichiers': [File(fh)],
                }
            )
        self.assertRedirects(response, reverse('cipe:prestation-enfant-list', args=[famille.pk, enfant.pk]))
        prest = PrestationCIPE.objects.get(enfant=enfant)
        self.assertEqual(prest.type_consult, 'cons_dom')
        self.assertEqual(prest.a_domicile_prest, 'handi')
        self.assertIsNone(prest.lieu)
        self.assertEqual(len(prest.fichiers), 1)
        response = self.client.get(edit_url)
        self.assertContains(response, '<option value="cons_dom" selected>Consultation à domicile</option>')
        # Par tél.
        response = self.client.post(
            edit_url,
            data={
                'date': date.today(),
                'duree': '00:30',
                'lieu': 'tel',
                'type_consult': 'cons_crn',
                'faits_courants': 'Contrôle de routine',
                'faits_marquants': '',
                'poids': '7',
            }
        )
        self.assertRedirects(response, reverse('cipe:prestation-enfant-list', args=[famille.pk, enfant.pk]))
        prest = PrestationCIPE.objects.get(enfant=enfant)
        self.assertEqual(prest.type_consult, 'cons_crn')
        self.assertEqual(prest.mode_consult, 'tel')
        self.assertIsNone(prest.lieu)
        response = self.client.get(edit_url)
        self.assertContains(response, '<option value="tel" selected>Par téléphone</option>')

    def test_prestation_add_by_user_spe(self):
        user_spe = Utilisateur.objects.create_user(
            'user_spe', 'user_spe@example.org', nom='Spe', prenom='Prénom',
        )
        user_spe.user_permissions.add(Permission.objects.get(codename='view_famillecipe'))
        user_spe.user_permissions.add(Permission.objects.get(codename='add_prestationcipe'))
        famille, enfant = self._add_famille_avec_enfant()
        self.client.force_login(user_spe)
        add_url = reverse('cipe:prestation-enfant-add', args=[famille.pk, enfant.pk])
        response = self.client.get(add_url)
        self.assertEqual(len(response.context['form']['type_consult'].field.choices), 1)
        response = self.client.post(
            add_url,
            data={
                'date': date.today(),
                'duree': '00:00',
                'type_consult': 'hors_stats',
                'lieu': '',
                'faits_courants': 'Note par SPE',
                'faits_marquants': '',
                'themes': [],
                'poids': '',
            }
        )
        self.assertRedirects(response, reverse('cipe:prestation-enfant-list', args=[famille.pk, enfant.pk]))

    def test_prestation_date(self):
        famille, enfant = self._add_famille_avec_enfant()
        self.client.force_login(self.user_cipe)
        add_url = reverse('cipe:prestation-enfant-add', args=[famille.pk, enfant.pk])
        response = self.client.post(add_url, data={
            'date': date.today() + timedelta(days=2),
            'faits_courants': 'Contrôle de routine',
        })
        self.assertContains(response, "Vous ne pouvez pas saisir de prestations dans le futur.")

    def test_prestation_validation(self):
        famille, enfant = self._add_famille_avec_enfant()
        neuch = Region.objects.get(nom='Neuchâtel')
        form = PrestationForm(user=self.user_cipe, famille=famille, instance=None, initial={}, data={
            'date': date.today(),
            'duree': '00:00',
            'type_consult': 'cons_crn',
            'lieu': '',
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {'lieu': ['Le lieu est obligatoire pour les consultations Croix-Rouge']}
        )
        form = PrestationForm(user=self.user_cipe, famille=famille, instance=None, initial={}, data={
            'date': date.today(),
            'duree': '00:00',
            'type_consult': 'res_fam',
            'lieu': neuch.pk,
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {'lieu': ['Le lieu ne doit pas être indiqué pour le type de consultation choisi.']}
        )

    def test_prestation_suppression(self):
        famille, enfant = self._add_famille_avec_enfant()
        prest = PrestationCIPE.objects.create(
            auteur=self.user_cipe, famille=famille, enfant=enfant,
            date=date.today(), duree='00:30', faits_courants='Consultation habituelle'
        )
        prest_fam = PrestationCIPE.objects.create(
            auteur=self.user_cipe, famille=famille, type_consult='res_fam',
            date=date.today(), duree='00:30', faits_courants='Consultation habituelle'
        )
        self.assertTrue(prest.can_delete(self.user_cipe))
        self.assertTrue(prest_fam.can_delete(self.user_cipe))
        other_user = Utilisateur.objects.create_user(
            'externe', 'externe@example.org', 'mepassword', prenom='Bruce', nom='Externe',
        )
        self.assertFalse(prest.can_delete(other_user))
        self.assertFalse(prest_fam.can_delete(other_user))
        with freeze_time(date.today() + timedelta(days=1)):
            self.assertFalse(prest.can_delete(self.user_cipe))
            self.assertFalse(prest_fam.can_delete(self.user_cipe))

        self.client.force_login(self.user_cipe)
        self.client.post(reverse('cipe:prestation-delete', args=[famille.pk, prest.pk]), data={})
        self.assertFalse(PrestationCIPE.objects.filter(pk=prest.pk).exists())
        self.client.post(reverse('cipe:prestation-delete', args=[famille.pk, prest_fam.pk]), data={})
        self.assertFalse(PrestationCIPE.objects.filter(pk=prest_fam.pk).exists())

    def test_prestation_reseau_familial(self):
        """Prestations liées à la famille mais à aucun enfant."""
        famille, enfant = self._add_famille_avec_enfant()
        self.client.force_login(self.user_cipe)
        add_url = reverse('cipe:prestation-enfant-add', args=[famille.pk, enfant.pk])
        response = self.client.post(
            add_url,
            data={
                'date': date.today(),
                'duree': '00:30',
                'lieu': '',
                'type_consult': 'res_fam',
                'faits_courants': 'Contrôle de routine',
                'faits_marquants': '',
            }
        )
        self.assertRedirects(response, reverse('cipe:prestation-enfant-list', args=[famille.pk, enfant.pk]))
        prest = PrestationCIPE.objects.get(famille=famille)
        self.assertIsNone(prest.enfant)
        # When re-editing, res_fam select option is selected
        edit_url = reverse('cipe:prestation-edit', args=[famille.pk, prest.pk])
        response = self.client.get(edit_url)
        self.assertEqual(response.context['form']['type_consult'].value(), 'res_fam')
        response = self.client.post(
            edit_url,
            data={
                'date': date.today(),
                'duree': '00:30',
                'lieu': '',
                'type_consult': 'res_fam',
                'faits_courants': 'Contrôle de routine',
                'faits_marquants': 'Ceci est marquant',
            }
        )
        self.assertRedirects(response, reverse('cipe:famille-edit', args=[famille.pk]))
        prest.refresh_from_db()
        self.assertEqual(prest.faits_marquants, 'Ceci est marquant')

    def test_prestation_reseau_vers_prest_fam(self):
        famille, enfant = self._add_famille_avec_enfant()
        res_fam = PrestationCIPE.objects.create(
            auteur=self.user_cipe, famille=famille, type_consult='res_fam',
            date=date.today(), duree='00:30', faits_courants='Réseau familial'
        )
        self.client.force_login(self.user_cipe)
        self.client.post(
            reverse('cipe:prestation-edit', args=[famille.pk, res_fam.pk]),
            data={
                'date': res_fam.date,
                'duree': res_fam.duree,
                'lieu': '',
                'type_consult': 'prest_fam',
                'faits_courants': res_fam.faits_courants,
                'faits_marquants': res_fam.faits_marquants,
                'referer': 'http://testserver' + reverse('cipe:prestation-enfant-list', args=[famille.pk, enfant.pk]),
            }
        )
        res_fam.refresh_from_db()
        self.assertEqual(res_fam.type_consult, 'prest_fam')
        self.assertEqual(res_fam.enfant, enfant)
        # Test with another referer
        res_fam.type_consult = 'res_fam'
        res_fam.enfant = None
        res_fam.save()
        self.client.post(
            reverse('cipe:prestation-edit', args=[famille.pk, res_fam.pk]),
            data={
                'date': res_fam.date,
                'duree': '00:30',
                'lieu': '',
                'type_consult': 'prest_fam',
                'faits_courants': res_fam.faits_courants,
                'faits_marquants': res_fam.faits_marquants,
                'referer': 'http://testserver' + reverse('cipe:prestations-utilisateur'),
            }
        )
        res_fam.refresh_from_db()
        self.assertEqual(res_fam.type_consult, 'prest_fam')
        self.assertEqual(res_fam.enfant, enfant)

    def test_prestations_list(self):
        famille, enfant = self._add_famille_avec_enfant()
        neuch = Region.objects.get(nom='Neuchâtel')
        PrestationCIPE.objects.create(
            auteur=self.user_cipe, famille=famille, enfant=enfant,
            date=date.today(), duree='00:30', lieu=neuch,
            faits_courants='Consultation habituelle'
        )
        PrestationCIPE.objects.create(
            auteur=self.user_cipe, famille=famille, enfant=enfant,
            date=date.today(), duree='00:30', lieu=None, type_consult='cons_dom',
            faits_courants='RAS', faits_marquants='Première fois',
            poids=8.43, taille=74.5
        )
        self.client.force_login(self.user_cipe)
        list_url = reverse('cipe:prestation-enfant-list', args=[famille.pk, enfant.pk])
        response = self.client.get(list_url)
        self.assertContains(response, 'Première fois')
        self.assertContains(response, '<td>8,430</td>')
        self.assertContains(response, '<td>74,5</td>')
        self.assertEqual(response.context['object_list'].count(), 2)
        # Seulement les faits marquants
        response = self.client.get(list_url + '?faits=marq')
        self.assertNotContains(response, 'Consultation habituelle')
        self.assertEqual(response.context['object_list'].count(), 1)

    def test_prestations_list_events(self):
        """Affichage des rendez-vous futurs sur la liste des prestations (journal)."""
        from agenda.models import Event, EventCategory

        famille, enfant = self._add_famille_avec_enfant()
        event_data = {
            'person': self.user_cipe,
            'famille': famille,
            'centre': Region.objects.get(nom='Neuchâtel'),
            'description': 'Rendez-vous',
            'category': EventCategory.objects.get(name='rendez-vous'),
        }
        yesterday = timezone.localtime(timezone.now() - timedelta(days=1)).replace(hour=14, minute=30)
        tomorrow = timezone.localtime(timezone.now() + timedelta(days=1)).replace(hour=14, minute=30)
        Event.objects.create(**{
            **event_data, 'start': yesterday, 'end': yesterday + timedelta(hours=1),
            'description': 'Hier',
        })
        Event.objects.create(**{
            **event_data, 'start': tomorrow, 'end': tomorrow + timedelta(hours=1),
            'description': 'Demain',
        })
        self.client.force_login(self.user_cipe)
        list_url = reverse('cipe:prestation-enfant-list', args=[famille.pk, enfant.pk])
        response = self.client.get(list_url)
        self.assertContains(response, "14:30 - 15:30 (Demain)")
        self.assertNotContains(response, "Hier")

    def test_prestation_generale(self):
        self.client.force_login(self.user_cipe)
        prest_data = {
            'date': date.today().strftime('%d.%m.%Y'),
            'participants': [self.user_cipe.pk],
            'type_prest': 'recif',
            'nb_adultes': '12',
            'remarques': 'Tout va bien',
        }
        response = self.client.get(reverse('cipe:prestation-gen-add'))
        self.assertContains(
            response,
            f'<input type="checkbox" name="participants" value="{self.user_cipe.pk}" '
            'id="id_participants_0" class="form-check-input" checked>',
            html=True
        )
        response = self.client.post(reverse('cipe:prestation-gen-add'), data=prest_data)
        self.assertEqual(response.context['form'].errors, {'duree': ['Ce champ est obligatoire.']})
        prest_data['duree'] = '00:30'
        response = self.client.post(reverse('cipe:prestation-gen-add'), data=prest_data)
        self.assertRedirects(response, reverse('cipe:prestations-utilisateur'))
        # Edit
        prest_data['nb_adultes'] = 9
        prestation = PrestationGen.objects.get(type_prest='recif')
        self.assertTrue(prestation.can_edit(self.user_cipe))
        response = self.client.post(reverse('cipe:prestation-gen-edit', args=[prestation.pk]), data=prest_data)
        prestation.refresh_from_db()
        self.assertEqual(prestation.nb_adultes, 9)
        self.assertFalse(
            PrestationGen(date=date.today() - timedelta(days=60), auteur=self.user_cipe).can_edit(self.user_cipe)
        )
        self.assertTrue(prestation.can_delete(self.user_cipe))
        other_user = Utilisateur.objects.create_user(
            'externe', 'externe@example.org', 'mepassword', prenom='Bruce', nom='Externe',
        )
        self.assertFalse(prestation.can_delete(other_user))

    def test_prestation_generale_list(self):
        famille, enfant = self._add_famille_avec_enfant()
        prests = PrestationGen.objects.bulk_create([
            PrestationGen(auteur=self.user_cipe, date=date.today(), duree='0:00', nb_sms=8),
            PrestationGen(auteur=self.user_cipe, date=date.today(), duree='2:15', type_prest='lecture', nb_adultes=5),
            PrestationGen(auteur=self.user_cipe, date=date.today() - timedelta(days=60), duree='0:00', nb_tel=2),
        ])
        for prest in prests:
            prest.participants.add(self.user_cipe)
        PrestationCIPE.objects.bulk_create([
            PrestationCIPE(
                auteur=self.user_cipe, date=date.today(), famille=famille, enfant=enfant, duree='1:00'
            ),
            PrestationCIPE(
                auteur=self.user_cipe, date=date.today() - timedelta(days=60),
                famille=famille, enfant=enfant, duree='1:00'
            ),
        ])
        self.client.force_login(self.user_cipe)
        response = self.client.get(reverse('cipe:prestations-utilisateur'))
        self.assertEqual(response.context['total_fam'], timedelta(hours=1))
        self.assertEqual(response.context['total_gen'], timedelta(hours=2, minutes=15))
        self.assertEqual(len(response.context['object_list']), 3)


class PercentileTests(InitialDataMixin, TestCase):
    def setUp(self):
        super().setUp()
        famille = FamilleCIPE.objects.create(nom='Martin', localite='Neuchâtel')
        role_enfant = Role.objects.get(nom='Enfant suivi')
        self.fille = Personne.objects.create(
            famille=famille, nom='Martinet', prenom='Greta', date_naissance='2020-10-10',
            role=role_enfant, genre='F'
        )
        self.garcon = Personne.objects.create(
            famille=famille, nom='Martinet', prenom='René', date_naissance='2019-07-24',
            role=role_enfant, genre='M'
        )

    def _test_percentiles(self, enfant):
        self.client.force_login(self.user_cipe)
        response = self.client.get(reverse('cipe:enfant-percentile-poids', args=[enfant.pk]))
        self.assertContains(response, 'Encore aucune donnée pour cet enfant.')

        PrestationCIPE.objects.create(
            famille=enfant.famille, enfant=enfant, date=date.today(), auteur=self.user_cipe,
            duree='00:15', poids=4.5, taille=64
        )
        response = self.client.get(reverse('cipe:enfant-percentile-poids', args=[enfant.pk]))
        self.assertContains(response, '<img src="data:image/png;base64,')
        response = self.client.get(reverse('cipe:enfant-percentile-taille', args=[enfant.pk]))
        self.assertContains(response, '<img src="data:image/png;base64,')
        # Avec les poids et taille de naissance
        suivi = enfant.suivienfant
        suivi.poids_naiss = Decimal('3.450')
        suivi.taille = Decimal('51.5')
        suivi.save()
        response = self.client.get(reverse('cipe:enfant-percentile-poids', args=[enfant.pk]))
        self.assertContains(response, '<img src="data:image/png;base64,')
        response = self.client.get(reverse('cipe:enfant-percentile-taille', args=[enfant.pk]))
        self.assertContains(response, '<img src="data:image/png;base64,')

    def test_percentiles_garcon(self):
        self._test_percentiles(self.garcon)

    def test_percentiles_fille(self):
        self._test_percentiles(self.fille)

    def test_percentile_sans_date_naissance(self):
        self.fille.date_naissance = None
        self.fille.save()
        with self.assertRaises(GraphEmpty):
            GraphPercentilePoids(self.fille)
        self.client.force_login(self.user_cipe)
        response = self.client.get(reverse('cipe:enfant-percentile-poids', args=[self.fille.pk]))
        self.assertRedirects(response, self.fille.edit_url)
        response = self.client.get(reverse('cipe:enfant-percentile-taille', args=[self.fille.pk]))
        self.assertRedirects(response, self.fille.edit_url)

    def test_percentiles_impression(self):
        PrestationCIPE.objects.create(
            famille=self.fille.famille, enfant=self.fille, date=date.today(), auteur=self.user_cipe,
            duree='00:15', poids=4.5, taille=64
        )
        self.client.force_login(self.user_cipe)
        response = self.client.get(reverse('cipe:print-percentile-poids', args=[self.fille.pk]))
        self.assertEqual(response['content-type'], 'application/pdf')
        response = self.client.get(reverse('cipe:print-percentile-taille', args=[self.fille.pk]))
        self.assertEqual(response['content-type'], 'application/pdf')

    def test_send_by_email(self):
        role_mere = Role.objects.get(nom='Mère')
        role_autre = Role.objects.create(nom='Oncle', famille=True)
        mere = Personne.objects.create(
            famille=self.fille.famille, nom='Martinet', prenom='Julie', date_naissance='1997-10-10',
            email='julie@example.org', role=role_mere, genre='F'
        )
        oncle = Personne.objects.create(
            famille=self.fille.famille, nom='Dupond', prenom='Ralph', email='', role=role_autre, genre='M'
        )
        PrestationCIPE.objects.create(
            famille=self.fille.famille, enfant=self.fille, date=date.today(), auteur=self.user_cipe,
            duree='00:15', poids=4.5, taille=64
        )

        url = reverse('cipe:enfant-percentile-email', args=[self.fille.pk])
        self.client.force_login(self.user_cipe)
        response = self.client.get(url)
        # Personne sans email est visible mais désactivée
        self.assertContains(
            response, '<input type="checkbox" name="recips" value="%d" id="id_recips_0" disabled>' % oncle.pk,
            html=True
        )
        self.assertContains(
            response,
            '<div><input type="checkbox" name="recips" value="%d" id="id_recips_1"> '
            '<label for="id_recips_1">Martinet Julie &lt;julie@example.org&gt;</label></div>' % mere.pk,
            html=True
        )

        response = self.client.post(url, data={'recips': [mere.pk]}, follow=True)
        self.assertRedirects(
            response,
            reverse('cipe:prestation-enfant-list', args=[self.fille.famille_id, self.fille.pk])
        )
        self.assertContains(response, "Le message a bien été envoyé à julie@example.org")
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(
            mail.outbox[0].body,
            "Madame, Monsieur,\n\n"
            "Veuillez trouver ci-joint les graphiques des courbes de poids et de taille pour Greta.\n\n"
            "Cordiales salutations.\n\n"
            "Ipe Laure\n"
        )
        self.assertEqual(len(mail.outbox[0].attachments), 2)

    def test_send_by_email_form(self):
        role_mere = Role.objects.get(nom='Mère')
        mere = Personne.objects.create(
            famille=self.fille.famille, nom='Martinet', prenom='Julie', date_naissance='1997-10-10',
            email='julie@example.org', role=role_mere, genre='F'
        )
        pediatre = Contact.objects.create(nom='Pédiatre', prenom='Dr', email='pediatre@example.org')
        self.garcon.suivienfant.pediatre = pediatre
        self.garcon.suivienfant.save()
        form = SendByEmailForm(enfant=self.garcon, data={'recips': [], 'recip_other': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.recipients(), [])
        form = SendByEmailForm(enfant=self.garcon, data={
            'recips': [mere.pk],
            'recip_other': 'jean@example.org, lucie@example.org',
            'pediatre': pediatre.email,
        })
        self.assertTrue(form.is_valid())
        self.assertEqual(
            form.recipients(),
            ['julie@example.org', 'jean@example.org', 'lucie@example.org', 'pediatre@example.org']
        )


class ThemesTests(InitialDataMixin, TempMediaRootMixin, TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        theme = ThemeSante.objects.create(nom='Allaitement')
        themedoc = ThemeDoc.objects.create(titre='Instructions all.', langue='fr')
        themedoc.themes.set([theme])

    def test_edit_themesante(self):
        theme = ThemeSante.objects.get(nom='Allaitement')
        self.client.force_login(self.user_cipe)
        response = self.client.get(reverse('cipe:themesante-edit', args=[theme.pk]))
        self.assertContains(response, 'Instructions all.')

    def test_doc_filter_form(self):
        theme = ThemeSante.objects.get(nom='Allaitement')
        self.client.force_login(self.user_cipe)
        response = self.client.get(reverse('cipe:themedoc-list') + '?theme=&langue=fr')
        self.assertEqual(len(response.context['object_list']), 1)
        response = self.client.get(reverse('cipe:themedoc-list') + '?theme=&langue=ar')
        self.assertEqual(len(response.context['object_list']), 0)
        response = self.client.get(reverse('cipe:themedoc-list') + f'?theme={theme.pk}&langue=')
        self.assertEqual(len(response.context['object_list']), 1)

    def test_doc_add_delete(self):
        self.user_cipe.user_permissions.add(
            Permission.objects.get(codename='add_themedoc'),
            Permission.objects.get(codename='delete_themedoc'),
        )
        self.client.force_login(self.user_cipe)
        with (Path('.').parent / 'croixrouge' / 'test.pdf').open(mode='rb') as fh:
            response = self.client.post(reverse('cipe:themedoc-add'), data={
                'fichier': File(fh),
                'titre': 'Fichier de test',
                'langue': 'pt',
                'themes': [ThemeSante.objects.get(nom='Allaitement').pk],
            })
        self.assertRedirects(response, reverse('cipe:themedoc-list'))
        doc = ThemeDoc.objects.get(titre='Fichier de test')
        # Now delete
        response = self.client.post(reverse('cipe:themedoc-delete', args=[doc.pk]), data={})
        self.assertRedirects(response, reverse('cipe:themedoc-list'))
        self.assertFalse(ThemeDoc.objects.filter(titre='Fichier de test').exists())


class GeneralDocTests(InitialDataMixin, TempMediaRootMixin, TestCase):
    def test_add_new_doc_new_categ(self):
        categ = GeneralDocCateg.objects.create(nom='Documents')
        self.user_cipe.user_permissions.add(
            Permission.objects.get(codename='add_generaldoc'),
        )
        self.client.force_login(self.user_cipe)
        with (Path('.').parent / 'croixrouge' / 'test.pdf').open(mode='rb') as fh:
            response = self.client.post(reverse('cipe:generaldoc-add'), data={
                'fichier': File(fh),
                'titre': 'Fichier de test',
                'categorie': str(categ.pk),
            })
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        self.assertRedirects(response, reverse('cipe:generaldoc-list'))
        self.assertEqual(GeneralDoc.objects.count(), 1)
        # Now the user has the right to create new categories
        self.user_cipe.user_permissions.add(
            Permission.objects.get(codename='add_generaldoccateg'),
        )
        self.client.force_login(self.user_cipe)
        response = self.client.get(reverse('cipe:generaldoc-add'))
        self.assertContains(response, '<datalist id="categorie_list">')
        with (Path('.').parent / 'croixrouge' / 'test.pdf').open(mode='rb') as fh:
            response = self.client.post(reverse('cipe:generaldoc-add'), data={
                'fichier': File(fh),
                'titre': 'Fichier de test',
                'categorie': 'PV',
            })
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        self.assertRedirects(response, reverse('cipe:generaldoc-list'))
        self.assertEqual(GeneralDoc.objects.count(), 2)


class JournalisationTests(InitialDataMixin, TestCase):
    def test_acces_famille_journalise(self):
        famille, enfant = self._add_famille_avec_enfant()
        famille_url = reverse('cipe:famille-edit', args=[famille.pk])
        enfant_url = reverse('cipe:prestation-enfant-list', args=[famille.pk, enfant.pk])
        self.client.force_login(self.user_cipe)
        self.client.get(famille_url)
        self.client.get(enfant_url)

        user_spe = Utilisateur.objects.create_user(
            'user_spe', 'user_spe@example.org', nom='Spe', prenom='Prénom',
        )
        user_spe.user_permissions.add(Permission.objects.get(codename='view_famillecipe'))
        self.client.force_login(user_spe)
        response = self.client.get(famille_url)
        self.assertTemplateUsed(response, 'croixrouge/acces_famille.html')
        response = self.client.get(famille_url + '?confirm=1')
        self.assertTemplateUsed(response, 'croixrouge/famille_edit.html')
        self.client.get(enfant_url + '?confirm=1')

        self.assertEqual(
            JournalAcces.objects.filter(utilisateur=self.user_cipe, ordinaire=True).count(), 2
        )
        self.assertEqual(
            JournalAcces.objects.filter(utilisateur=user_spe, ordinaire=False).count(), 2
        )


class StatistiquesTests(InitialDataMixin, TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.admin_user = Utilisateur.objects.create(username='admin')
        cls.admin_user.groups.add(cls.cipe_admin_group)

    def test_stats_familles(self):
        famille1, enfant1 = self._add_famille_avec_enfant()
        famille2, enfant2 = self._add_famille_avec_enfant(fam_kwargs={'besoins_part': True})
        last_year = date.today().year - 1
        neuch = Region.objects.get(nom="Neuchâtel")
        PrestationCIPE.objects.bulk_create([
            PrestationCIPE(
                auteur=self.user_cipe, date=date(last_year, 3, 12), famille=famille1, enfant=enfant1,
                lieu=neuch, duree='1:30'
            ),
            PrestationCIPE(
                auteur=self.user_cipe, date=date(last_year, 3, 15),
                famille=famille1, enfant=enfant1, duree='1:00'
            ),
            PrestationCIPE(
                auteur=self.user_cipe, date=date(last_year, 4, 22),
                famille=famille2, enfant=enfant2, lieu=neuch, duree='0:30',
            ),
            PrestationCIPE(
                auteur=self.user_cipe, date=date(last_year, 8, 20),
                famille=famille2, enfant=enfant2, lieu=neuch, duree='0:30',
            ),
        ])
        params = '&'.join(
            f'{key}={val}' for key, val in {
                'start_year': last_year, 'start_month': '3', 'end_year': last_year, 'end_month': '5'
            }.items()
        )
        self.client.force_login(self.admin_user)
        response = self.client.get(reverse('cipe:stats-familles') + f'?{params}')
        self.assertEqual(response.context['nb_fam_tot'], 2)
        self.assertEqual(response.context['nb_fam_besoins'], 1)
        self.assertEqual(response.context['enfants']['totals']['total'], 2)
        self.assertEqual(response.context['consults']['totals']['total'], 3)
        # Check export doesn't crash
        response = self.client.get(reverse('cipe:stats-familles') + f'?{params}&export=1')
        self.assertEqual(response.headers['Content-Type'], openxml_contenttype)

    def test_stats_interv(self):
        self.maxDiff = None
        famille1, enfant1 = self._add_famille_avec_enfant()
        famille2, enfant2 = self._add_famille_avec_enfant()
        last_year = date.today().year - 1
        neuch = Region.objects.get(nom="Neuchâtel")
        PrestationCIPE.objects.bulk_create([
            PrestationCIPE(
                auteur=self.user_cipe, date=date(last_year, 3, 12), famille=famille1, enfant=enfant1,
                lieu=neuch, duree='1:30'
            ),
            PrestationCIPE(
                auteur=self.user_cipe, date=date(last_year, 3, 15),
                famille=famille1, enfant=enfant1, duree='1:00'
            ),
            PrestationCIPE(
                auteur=self.user_cipe, date=date(last_year, 4, 19),
                famille=famille1, enfant=enfant1, duree=None
            ),
            PrestationCIPE(
                auteur=self.user_cipe, date=date(last_year, 4, 22),
                famille=famille1, enfant=enfant1, duree='0:07', type_consult='hors_stats'
            ),
        ])
        params = '&'.join(
            f'{key}={val}' for key, val in {
                'start_year': last_year, 'start_month': '3', 'end_year': last_year, 'end_month': '5'
            }.items()
        )
        self.client.force_login(self.admin_user)
        response = self.client.get(reverse('cipe:stats-interv') + f'?{params}')
        self.assertEqual(
            response.context['interv_stats_consult'],
            {
                self.user_cipe.pk: {
                    'nom': 'Ipe Laure',
                    Month(last_year, 3): {'nb': 2, 'duree': timedelta(minutes=150)},
                    Month(last_year, 4): {'nb': 1, 'duree': timedelta(0)},
                    Month(last_year, 5): {'nb': 0, 'duree': timedelta(0)},
                    'total': {'nb': 3, 'duree': timedelta(minutes=150)},
                }
            }
        )
        # Check export doesn't crash
        response = self.client.get(reverse('cipe:stats-interv') + f'?{params}&export=1')
        self.assertEqual(response.headers['Content-Type'], openxml_contenttype)

    def test_stats_lieux(self):
        self.maxDiff = None
        famille1, enfant1 = self._add_famille_avec_enfant()
        famille2, enfant2 = self._add_famille_avec_enfant()
        last_year = date.today().year - 1
        neuch = Region.objects.get(nom="Neuchâtel")
        PrestationCIPE.objects.bulk_create([
            PrestationCIPE(
                auteur=self.user_cipe, date=date(last_year, 3, 12), famille=famille1, enfant=enfant1,
                lieu=neuch, duree='1:00'
            ),
            PrestationCIPE(
                auteur=self.user_cipe, date=date(last_year, 4, 22),
                famille=famille1, enfant=enfant1, duree='0:07', type_consult='hors_stats'
            ),
            PrestationCIPE(
                auteur=self.user_cipe, date=date(last_year, 4, 22),
                famille=famille2, enfant=enfant2, lieu=neuch, duree='0:30',
            ),
            PrestationCIPE(
                auteur=self.user_cipe, date=date(last_year, 8, 20),
                famille=famille2, enfant=enfant2, lieu=neuch, duree='0:30',
            ),
        ])
        params = '&'.join(
            f'{key}={val}' for key, val in {
                'start_year': last_year, 'start_month': '3', 'end_year': last_year, 'end_month': '5'
            }.items()
        )
        self.client.force_login(self.admin_user)
        response = self.client.get(reverse('cipe:stats-lieux') + f'?{params}')
        self.assertEqual(
            response.context['total_stats'],
            {Month(last_year, 3): 1, Month(last_year, 4): 1, Month(last_year, month=5): 0, 'total': 2}
        )
        self.assertEqual(
            response.context['centres']['Neuchâtel'],
            {Month(last_year, 3): 1, Month(last_year, 4): 1, Month(last_year, month=5): 0, 'total': 2}
        )
        # Check export doesn't crash
        response = self.client.get(reverse('cipe:stats-lieux') + f'?{params}&export=1')
        self.assertEqual(response.headers['Content-Type'], openxml_contenttype)

    def test_stats_type_consult(self):
        self.maxDiff = None
        famille1, enfant1 = self._add_famille_avec_enfant()
        famille2, enfant2 = self._add_famille_avec_enfant()
        last_year = date.today().year - 1
        neuch = Region.objects.get(nom="Neuchâtel")
        PrestationCIPE.objects.bulk_create([
            PrestationCIPE(
                auteur=self.user_cipe, date=date(last_year, 3, 12), famille=famille1, enfant=enfant1,
                lieu=neuch, duree='1:00'
            ),
            PrestationCIPE(
                auteur=self.user_cipe, date=date(last_year, 3, 15),
                famille=famille1, enfant=enfant1, type_consult='cons_dom', a_domicile_prest='handi', duree='1:00'
            ),
            PrestationCIPE(
                auteur=self.user_cipe, date=date(last_year, 4, 22),
                famille=famille1, enfant=enfant1, type_consult='cons_dom', a_domicile_prest='psycho', duree='1:00'
            ),
            PrestationCIPE(
                auteur=self.user_cipe, date=date(last_year, 4, 22),
                famille=famille1, enfant=enfant1, duree='0:07', type_consult='hors_stats'
            ),
        ])
        params = '&'.join(
            f'{key}={val}' for key, val in {
                'start_year': last_year, 'start_month': '3', 'end_year': last_year, 'end_month': '5'
            }.items()
        )
        self.client.force_login(self.admin_user)
        response = self.client.get(reverse('cipe:stats-type') + f'?{params}')
        self.assertIn('psycho', response.context['stats'].keys())
        self.assertEqual(
            response.context['stats']['num_prest'],
            {Month(last_year, 3): 2, Month(last_year, 4): 1, Month(last_year, 5): 0, 'total': 3}
        )
        self.assertEqual(
            response.context['stats']['num_rzv'],
            {Month(last_year, 3): 2, Month(last_year, 4): 1, Month(last_year, 5): 0, 'total': 3}
        )
        self.assertEqual(
            response.context['stats']['handi'],
            {Month(last_year, 3): 1, Month(last_year, 4): 0, Month(last_year, 5): 0, 'total': 1}
        )
        self.assertEqual(
            response.context['stats']['domicile'],
            {Month(last_year, 3): 1, Month(last_year, 4): 1, Month(last_year, 5): 0, 'total': 2}
        )
        # Check export doesn't crash
        response = self.client.get(reverse('cipe:stats-type') + f'?{params}&export=1')
        self.assertEqual(response.headers['Content-Type'], openxml_contenttype)

    def test_stats_nationalite(self):
        self.maxDiff = None
        famille1, enfant1 = self._add_famille_avec_enfant()
        mere1 = famille1.parents()[0]
        mere1.pays_origine = Country(code='TN')
        mere1.save()
        famille2, enfant2 = self._add_famille_avec_enfant()
        mere2 = famille2.parents()[0]
        mere2.pays_origine = Country(code='CH')
        mere2.save()
        last_year = date.today().year - 1
        famille3, enfant3 = self._add_famille_avec_enfant()
        PrestationCIPE.objects.bulk_create([
            PrestationCIPE(
                auteur=self.user_cipe, date=date(last_year, 3, 12),
                famille=famille3, enfant=enfant3, duree='1:00'
            ),
            PrestationCIPE(
                auteur=self.user_cipe, date=date(last_year, 3, 15),
                famille=famille1, enfant=enfant1, duree='1:00'
            ),
            PrestationCIPE(
                auteur=self.user_cipe, date=date(last_year, 4, 22),
                famille=famille2, enfant=enfant2, duree='1:00'
            ),
            PrestationCIPE(
                auteur=self.user_cipe, date=date(last_year, 4, 25),
                famille=famille2, enfant=enfant2, duree='1:00', type_consult='hors_stats'
            ),
        ])
        params = '&'.join(
            f'{key}={val}' for key, val in {
                'start_year': last_year, 'start_month': '3', 'end_year': last_year, 'end_month': '5'
            }.items()
        )
        self.client.force_login(self.admin_user)
        response = self.client.get(reverse('cipe:stats-nationalite') + f'?{params}')
        self.assertEqual(
            response.context['stats'],
            {
                Country(code='TN'): {
                    Month(last_year, 3): 1, Month(last_year, 4): 0, Month(last_year, 5): 0, 'total': {mere1.pk}
                },
                Country(code='CH'): {
                    Month(last_year, 3): 0, Month(last_year, 4): 1, Month(last_year, 5): 0, 'total': {mere2.pk}
                },
                '': {
                    Month(last_year, 3): 1, Month(last_year, 4): 0, Month(last_year, 5): 0,
                    'total': {famille3.parents()[0].pk}
                },
            }
        )
        # Check export doesn't crash
        response = self.client.get(reverse('cipe:stats-nationalite') + f'?{params}&export=1')
        self.assertEqual(response.headers['Content-Type'], openxml_contenttype)

    def test_stats_nouvelles(self):
        famille1, enfant1 = self._add_famille_avec_enfant()
        enfant1.suivienfant.prescripteur = 'M'
        enfant1.suivienfant.save()
        famille2, enfant2 = self._add_famille_avec_enfant()
        enfant2.suivienfant.prescripteur = 'E'
        enfant2.suivienfant.save()
        last_year = date.today().year - 1
        PrestationCIPE.objects.bulk_create([
            PrestationCIPE(
                auteur=self.user_cipe, date=date(last_year, 2, 12),
                famille=famille1, enfant=enfant1, duree='0:07', type_consult='hors_stats'
            ),
            PrestationCIPE(
                auteur=self.user_cipe, date=date(last_year, 3, 12),
                famille=famille1, enfant=enfant1, duree='01:00',
            ),
            PrestationCIPE(
                auteur=self.user_cipe, date=date(last_year, 4, 22),
                famille=famille2, enfant=enfant2, duree='0:30',
            ),
        ])

        params = '&'.join(
            f'{key}={val}' for key, val in {
                'start_year': last_year, 'start_month': '3', 'end_year': last_year, 'end_month': '5'
            }.items()
        )
        self.client.force_login(self.admin_user)
        response = self.client.get(reverse('cipe:stats-nouvelles') + f'?{params}')
        self.assertEqual(
            response.context['nouvelles'],
            {Month(last_year, 3): 1, Month(last_year, 4): 1, Month(last_year, 5): 0, 'total': 2}
        )
        self.assertEqual(
            response.context['nouvelles_prescr']['Maternité'],
            {Month(last_year, 3): 1, Month(last_year, 4): 0, Month(last_year, 5): 0, 'total': 1}
        )
        # Check export doesn't crash
        response = self.client.get(reverse('cipe:stats-nouvelles') + f'?{params}&export=1')
        self.assertEqual(response.headers['Content-Type'], openxml_contenttype)

    def test_stats_gen(self):
        self.maxDiff = None
        last_year = date.today().year - 1
        PrestationGen.objects.bulk_create([
            PrestationGen(
                auteur=self.user_cipe, date=date(last_year, 3, 4), duree='0:00', nb_tel=4, nb_sms=1,
            ),
            PrestationGen(
                auteur=self.user_cipe, date=date(last_year, 4, 4), duree='1:00', type_prest='lecture',
                nb_adultes=7, nb_enfants=2
            ),
            PrestationGen(
                auteur=self.user_cipe, date=date(last_year, 4, 25), duree='0:45', type_prest='lecture',
                nb_adultes=3, nb_enfants=3
            ),
            PrestationGen(
                auteur=self.user_cipe, date=date(last_year, 4, 28), duree='1:45', type_prest='seance',
                remarques='Séance avec groupe de pédiatres'
            ),
        ])
        params = '&'.join(
            f'{key}={val}' for key, val in {
                'start_year': last_year, 'start_month': '3', 'end_year': last_year, 'end_month': '5'
            }.items()
        )
        self.client.force_login(self.admin_user)
        response = self.client.get(reverse('cipe:stats-gen') + f'?{params}')
        self.assertEqual(
            response.context['totals'],
            {
                'num_tel': 4,
                'num_sms': 1,
                'num_email': 0,
                'num_courr': 0,
                'num_colloque': 0,
                'num_fcont': 0,
                'num_lecture': 2,
                'num_patio': 0,
                'num_recif': 0,
                'num_sommeil': 0,
                'num_autre': 0,
                'num_seance': 1,
                'num_superv': 0,
                'num_interv': 0,
                'part_colloque': 0,
                'part_fcont': 0,
                'part_lecture': 10,
                'part_lecture_enf': 5,
                'part_patio': 0,
                'part_patio_enf': 0,
                'part_recif': 0,
                'part_sommeil': 0,
                'part_autre': 0,
                'part_autre_enf': 0,
                'part_seance': 0,
                'part_superv': 0,
                'part_interv': 0,
                'duree_colloque': timedelta(0),
                'duree_fcont': timedelta(0),
                'duree_lecture': timedelta(seconds=6300),
                'duree_patio': timedelta(0),
                'duree_recif': timedelta(0),
                'duree_autre': timedelta(0),
                'duree_seance': timedelta(seconds=6300),
                'duree_sommeil': timedelta(0),
                'duree_superv': timedelta(0),
                'duree_interv': timedelta(0),
            }
        )
        # Check export doesn't crash
        response = self.client.get(reverse('cipe:stats-gen') + f'?{params}&export=1')
        self.assertEqual(response.headers['Content-Type'], openxml_contenttype)

    def test_stats_exports(self):
        # Essentially check exports don't crash
        self.client.force_login(self.admin_user)
        response = self.client.post(reverse('cipe:stats-export-santene'), data={'year': date.today().year})
        self.assertEqual(response.headers['Content-Type'], openxml_contenttype)
        response = self.client.post(reverse('cipe:stats-export-ascpe'), data={'year': date.today().year})
        self.assertEqual(response.headers['Content-Type'], openxml_contenttype)
