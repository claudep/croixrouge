import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models

import cipe.models
import common.fields
import croixrouge.file_array_field


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('croixrouge', '0048_remove_region_cipe_spe_sifp'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='GeneralDocCateg',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='PsychoItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('descriptif', models.CharField(max_length=200, unique=True)),
                ('anomalie', models.CharField(blank=True, max_length=200)),
                ('age_ref', models.PositiveSmallIntegerField(choices=[(30, '1 mois'), (91, '3 mois'), (183, '6 mois'), (274, '9 mois'), (365, '12 mois')])),
                ('order', models.SmallIntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='ThemeSante',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(max_length=150)),
            ],
            options={
                'verbose_name': 'Thème de santé',
                'verbose_name_plural': 'Thèmes de santé',
            },
        ),
        migrations.CreateModel(
            name='FamilleCIPE',
            fields=[
            ],
            options={
                'proxy': True,
                'indexes': [],
                'constraints': [],
            },
            bases=('croixrouge.famille',),
        ),
        migrations.CreateModel(
            name='GeneralDoc',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fichier', models.FileField(upload_to='cipe_doc', verbose_name='Document')),
                ('titre', models.CharField(max_length=200)),
                ('categorie', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='documents', to='cipe.generaldoccateg', verbose_name='Catégorie')),
            ],
        ),
        migrations.CreateModel(
            name='PrestationGen',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(verbose_name='Date de la prestation')),
                ('duree', models.DurationField(verbose_name='Durée')),
                ('nb_adultes', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Nbre d’adultes')),
                ('nb_enfants', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Nbre d’enfants')),
                ('nb_tel', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Nbre de téléphones')),
                ('nb_sms', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Nbre de SMS')),
                ('nb_email', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Nbre de courriels')),
                ('nb_courr', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Nbre de courriers parents')),
                ('type_prest', models.CharField(blank=True, choices=[('patio', 'Rencontres parents-enfants'), ('recif', 'Animation Récif'), ('lecture', 'Animation lecture'), ('sommeil', 'Atelier sommeil'), ('autre', 'Autre animation'), ('colloque', 'Colloque'), ('superv', 'Supervision'), ('interv', 'Intervision'), ('fcont', 'Formation continue'), ('seance', 'Séance externe')], max_length=10, verbose_name='Type de prestation')),
                ('remarques', models.TextField(blank=True, verbose_name='Remarques')),
                ('auteur', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='Auteur')),
                ('lieu', models.ForeignKey(blank=True, limit_choices_to={'secteurs__contains': ['cipe']}, null=True, on_delete=django.db.models.deletion.PROTECT, to='croixrouge.region', verbose_name='Lieu')),
                ('participants', models.ManyToManyField(related_name='prestations_cipe', to=settings.AUTH_USER_MODEL, verbose_name='Participantes')),
            ],
            options={
                'verbose_name': 'Prestation générale',
                'verbose_name_plural': 'Prestations générales',
            },
            bases=(cipe.models.CheckEditableMixin, models.Model),
        ),
        migrations.CreateModel(
            name='SuiviEnfant',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('motif_demande', common.fields.ChoiceArrayField(base_field=models.CharField(choices=[('routine', 'Suivi de routine'), ('primip', 'Suivi primipare'), ('premat', 'Suivi prématuré'), ('psychosoc', 'Contexte psycho-social particulier'), ('integr', 'Soutien à l’intégration')], max_length=10), blank=True, null=True, size=None, verbose_name='Motif de la demande')),
                ('prescripteur', models.CharField(blank=True, choices=[('F', 'Fratrie déjà suivie'), ('M', 'Maternité'), ('P', 'Pédiatre'), ('S', 'SFIS'), ('O', 'OPE'), ('A', 'ASA'), ('I', 'SPE'), ('X', 'SOF'), ('E', 'Entourage'), ('C', 'Courrier Croix-Rouge'), ('D', 'Divers')], max_length=1, verbose_name='Prescripteur')),
                ('lieu_acc', models.CharField(blank=True, max_length=50, verbose_name='Lieu d’accouchement')),
                ('poids_naiss', models.DecimalField(blank=True, decimal_places=3, max_digits=4, null=True, verbose_name='Poids à la naissance')),
                ('taille', models.DecimalField(blank=True, decimal_places=1, max_digits=4, null=True, verbose_name='Taille')),
                ('poids_sortie', models.DecimalField(blank=True, decimal_places=3, max_digits=4, null=True, verbose_name='Poids de sortie')),
                ('accouchement', models.TextField(blank=True, verbose_name='Accouchement')),
                ('particularites', models.TextField(blank=True, verbose_name='Particularités')),
                ('gestite', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Gestité')),
                ('parite', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Parité')),
                ('age_gest', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Âge gestationnel')),
                ('allaitement', models.CharField(blank=True, choices=[('oui', 'Oui'), ('non', 'Non'), ('mix', 'Mixte')], max_length=3, verbose_name='Allaitement')),
                ('lait_subst', models.CharField(blank=True, max_length=80, verbose_name='Lait de substitution')),
                ('traitement', models.TextField(blank=True, verbose_name='Traitement')),
                ('allergies', models.TextField(blank=True, verbose_name='Allergies de l’enfant')),
                ('pediatre', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='croixrouge.contact', verbose_name='Pédiatre')),
                ('personne', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='croixrouge.personne')),
            ],
        ),
        migrations.CreateModel(
            name='PsychoEval',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(verbose_name='Date')),
                ('acquisition', models.CharField(choices=[('acq', 'Acquis'), ('part', 'Partiellement acquis'), ('nacq', 'Non acquis')], max_length=4)),
                ('remarque', models.TextField(blank=True, verbose_name='Remarques')),
                ('auteur', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='Auteur')),
                ('item', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='cipe.psychoitem')),
                ('suivi', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cipe.suivienfant', verbose_name='Suivi')),
            ],
        ),
        migrations.CreateModel(
            name='ThemeDoc',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fichier', models.FileField(upload_to='cipe_doc', verbose_name='Document')),
                ('titre', models.CharField(max_length=150)),
                ('langue', models.CharField(blank=True, choices=[('sq', 'Albanais'), ('de', 'Allemand'), ('en', 'Anglais'), ('ar', 'Arabe'), ('es', 'Espagnol'), ('fa', 'Farsi'), ('fr', 'Français'), ('it', 'Italien'), ('pt', 'Portugais'), ('ru', 'Russe'), ('hr', 'Serbo-croate'), ('so', 'Somali'), ('ta', 'Tamoul'), ('th', 'Thaï'), ('ti', 'Tigrigna'), ('tr', 'Turc')], max_length=5, verbose_name='Langue')),
                ('themes', models.ManyToManyField(blank=True, related_name='documents', to='cipe.themesante', verbose_name='Thèmes')),
            ],
        ),
        migrations.CreateModel(
            name='PrestationCIPE',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(verbose_name='Date de la consultation')),
                ('date_saisie', models.DateTimeField(auto_now_add=True, null=True)),
                ('type_consult', models.CharField(choices=[('cons_crn', 'Consultation'), ('cons_som', 'Consultation sommeil'), ('cons_dom', 'Consultation à domicile'), ('prest_fam', 'Prestation familiale'), ('res_fam', 'Réseau familial'), ('hors_stats', 'Autre (hors statistiques)')], default='cons_crn', max_length=12, verbose_name='Type de prestation')),
                ('mode_consult', models.CharField(choices=[('pres', 'En présentiel'), ('tel', 'Par téléphone'), ('email', 'Par courriel')], default='pres', max_length=5, verbose_name='Mode de prestation')),
                ('a_domicile_prest', models.CharField(blank=True, choices=[
                    ('psycho', 'suivi psychosocial'), ('nombr', 'famille nombreuse'),
                    ('handi', 'handicap du parent'), ('handi-enf', 'handicap de l’enfant'),
                    ('fatigue', 'fatigue maternelle'), ('eloign', 'éloignement'),
                    ('centre_req', "centre de requérants"),
                ], max_length=10, verbose_name='Prestation à domicile')),
                ('autre_lieu', models.BooleanField(default=False)),
                ('duree', models.DurationField(blank=True, null=True, verbose_name='Durée')),
                ('poids', models.DecimalField(blank=True, decimal_places=3, max_digits=5, null=True, verbose_name='Poids [kg]')),
                ('taille', models.DecimalField(blank=True, decimal_places=1, max_digits=4, null=True, verbose_name='Taille [cm]')),
                ('faits_courants', models.TextField(blank=True, verbose_name='Faits courants')),
                ('faits_marquants', models.TextField(blank=True, verbose_name='Faits marquants')),
                ('sans_rendezvous', models.BooleanField(default=False, verbose_name='Sans rendez-vous')),
                ('manque', models.BooleanField(default=False, verbose_name='Rendez-vous manqué')),
                ('fichiers', croixrouge.file_array_field.ArrayFileField(base_field=models.FileField(upload_to='prestations'), blank=True, null=True, size=None, verbose_name='Fichiers/images')),
                ('auteur', models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='Auteur')),
                ('enfant', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='croixrouge.personne', verbose_name='Enfant')),
                ('lieu', models.ForeignKey(blank=True, limit_choices_to={'secteurs__contains': ['cipe']}, null=True, on_delete=django.db.models.deletion.PROTECT, to='croixrouge.region', verbose_name='Lieu')),
                ('themes', models.ManyToManyField(blank=True, to='cipe.themesante', verbose_name='Thèmes de santé abordés')),
                ('famille', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cipe.famillecipe', verbose_name='Famille')),
            ],
            bases=(cipe.models.CheckEditableMixin, models.Model),
        ),
    ]
