from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cipe', '0001_squashed_0046_migrate_type_manque'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='famillecipe',
            options={'permissions': {('can_archive', 'Archiver les dossiers CIPE')}},
        ),
    ]
