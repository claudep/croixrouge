from django.utils import timezone


def maintenant():
    return timezone.now().astimezone(timezone.get_current_timezone())
