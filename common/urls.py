from django.contrib import admin
from django.contrib.auth.urls import urlpatterns as auth_patterns
from django.contrib.auth.views import LoginView, LogoutView
from django.urls import include, path
from django.views.decorators.cache import cache_page
from django.views.i18n import JavaScriptCatalog
from two_factor.urls import urlpatterns as tf_urls

from croixrouge import views

handler404 = 'croixrouge.views.page_not_found'

urlpatterns = [
    path('', views.HomeView.as_view(), name='home'),
    path('account/password_change/', views.PasswordChangeView.as_view(), name='password_change'),
    # Overriden 2FA path:
    path('account/two_factor/setup/', views.SetupView.as_view(), name='setup'),
    path('', include(tf_urls)),
    # Standard login is still permitted depending on IP origin
    path('login/', LoginView.as_view(template_name='login.html'), name='login'),
    path('logout/', LogoutView.as_view(next_page='/account/login'), name='logout'),
    path('jsi18n/', cache_page(86400, key_prefix='jsi18n-1')(JavaScriptCatalog.as_view()),
         name='javascript-catalog'),
    path("", include("city_ch_autocomplete.urls")),

    path('tinymce/', include('tinymce.urls')),
    path('admin/', admin.site.urls),
    path('', include('croixrouge.urls')),
    path('spe/', include('spe.urls')),
    path('amf/', include('amf.urls')),
    path('sof/', include('sof.urls')),
    path('cipe/', include('cipe.urls')),
    path('agenda/', include('agenda.urls')),
    path('archive/', include('archive.urls')),

    path('media/<path:path>', views.ServeMedia.as_view()),
]

# Include contrib.auth password reset URLs under the /account prefix
for pat in auth_patterns:
    if pat.name.startswith('password') and pat.name != 'password_change':
        pat.pattern._route = f'account/{pat.pattern._route}'
        urlpatterns.append(pat)
