import os
import shutil
import tempfile
from datetime import date, timedelta
from unittest.mock import patch

from django.conf.global_settings import PASSWORD_HASHERS
from django.contrib.auth.models import Group, Permission
from django.core.files import File
from django.db.utils import IntegrityError
from django.forms import Form
from django.test import TestCase
from django.test.utils import override_settings
from django.urls import reverse
from freezegun import freeze_time

from sof.models import FamilleSOF, PrestationSOF
from spe.models import FamilleSPE, PrestationSPE

from .forms import HMDurationField, PersonneForm, UtilisateurForm
from .models import (
    CercleScolaire,
    Contact,
    Famille,
    Formation,
    LibellePrestation,
    Personne,
    PrestationBase,
    Region,
    Role,
    Service,
    Utilisateur,
)
from .views import FacturationBase


class InitialDataMixin:
    @classmethod
    def setUpTestData(cls):
        s1, _ = Service.objects.get_or_create(sigle='CRXNE')
        Service.objects.bulk_create([
            Service(sigle='OPEN'),
            Service(sigle='SSE')
        ])

        CercleScolaire.objects.bulk_create([
            CercleScolaire(nom='EOREN-MAIL'),
            CercleScolaire(nom='EOCF-NUMA-DROZ')
        ])

        grp_spe = Group.objects.create(name='spe')
        grp_spe.permissions.add(*list(
            Permission.objects.filter(codename__in=[
                'view_famillespe', 'add_famillespe', 'add_prestationspe'
            ])
        ))

        Role.objects.bulk_create([
            Role(nom='Père', famille=True),
            Role(nom='Mère', famille=True),
            Role(nom='Beau-père', famille=True),
            Role(nom='Enfant suivi', famille=True),
            Role(nom='Enfant non-suivi', famille=True),
            Role(nom='Médecin', famille=False),
            Role(nom='Psy', famille=False, intervenant=True, editeur=True),
            Role(nom='Educ', famille=False, intervenant=True, editeur=True),
            Role(nom='IPE', famille=False, intervenant=True),
            Role(nom='ASE', famille=False, intervenant=True),
            Role(nom='Assistant-e social-e', famille=False, intervenant=True),
            Role(nom='Coach APA', famille=False, intervenant=True),
            Role(nom='Référent', famille=False),
        ])

        Contact.objects.bulk_create([
            Contact(nom='Sybarnez', prenom='Tina', tel_prof='032 886 88 88', service=s1),
            Contact(nom='Rathfeld', prenom='Christophe', service=Service.objects.get(sigle='OPEN')),
            Contact(nom='DrSpontz', prenom='Igor'),
        ])
        Contact.objects.get(nom='DrSpontz').roles.add(Role.objects.get(nom='Médecin'))

        Region.objects.bulk_create([
            # SPE, SOF
            Region(nom='Littoral Est', secteurs=['spe', 'sof']),
            Region(nom='Littoral Ouest', secteurs=['spe', 'sof']),
            Region(nom='Montagnes', secteurs=['spe', 'sof']),
            Region(nom='Val-de-Ruz', secteurs=['spe', 'sof']),
            Region(nom='Val-de-Travers', secteurs=['spe', 'sof']),
            # CIPE
            Region(nom='Neuchâtel', secteurs=['cipe']),
            Region(nom='La Chaux-de-Fonds', secteurs=['cipe']),
            Region(nom='Le Locle', secteurs=['cipe']),
            Region(nom='Boudry', secteurs=['cipe']),
            Region(nom='Cernier', secteurs=['cipe']),
            Region(nom='Couvet', secteurs=['cipe']),
            Region(nom='Fleurier', secteurs=['cipe']),
        ], ignore_conflicts=True)

        LibellePrestation.objects.bulk_create([
            LibellePrestation(unite='spe', code='spe01', nom='Évaluation SPE'),
            LibellePrestation(unite='spe', code='spe02', nom='Accompagnement SPE'),
            LibellePrestation(unite='spe', code='spe03', nom='Prestation gén. SPE'),
            LibellePrestation(unite='spe', code='spe04', nom='Activités ASE'),
        ])

        cls.user = Utilisateur.objects.create_user(
            'me', 'me@example.org', 'mepassword', prenom='Jean', nom='Valjean',
        )
        cls.user.groups.add(grp_spe)
        cls.user_externe = Utilisateur.objects.create_user(
            'externe', 'externe@example.org', 'mepassword', prenom='Bruce', nom='Externe',
        )
        cls.user_admin = Utilisateur.objects.create_user(
            'admin', 'admin@example.org', 'adminpw', nom='Zorro',
        )
        cls.user_admin.user_permissions.add(
            *list(Permission.objects.filter(codename__in=[
                'add_role', 'change_role', 'delete_role', 'add_service',
                'add_utilisateur', 'change_utilisateur', 'delete_utilisateur',
                'export_stats',
            ]))
        )

    def create_famille_spe(self, name='Haddock'):
        famille = FamilleSPE.objects.create_famille(
            nom=name, rue='Château1', npa=2000, localite='Moulinsart',
            equipe='montagnes', autorite_parentale='conjointe', statut_marital='divorce', monoparentale=False
        )
        pere = Role.objects.get(nom='Père')
        Personne.objects.create_personne(
            famille=famille, role=pere,
            nom=name, prenom='Archibald', genre='M', date_naissance=date(1956, 2, 16),
            rue='Château1', npa=2000, localite='Moulinsart',
        )
        famille.suivispe.ope_referent = Contact.objects.get(nom="Sybarnez")
        famille.suivispe.save()
        Personne.objects.create_personne(
            famille=famille, role=Role.objects.get(nom='Enfant suivi'),
            nom=name, prenom='Toto', genre='M', date_naissance=date(2010, 2, 16),
            rue='Château1', npa=2000, localite='Moulinsart',
        )
        return famille

    def create_famille_sof(self, name="Loiseau"):
        famille = FamilleSOF.objects.create_famille(
            nom=name, rue="Château 4", npa="2000", localite="Moulinsart",
        )
        pere = Role.objects.get(nom="Père")
        Personne.objects.create_personne(
            famille=famille, role=pere,
            nom=name, prenom="Archibald", genre="M", date_naissance=date(1956, 2, 16),
            rue="Château 4", npa=2000, localite="Moulinsart",
        )
        Personne.objects.create_personne(
            famille=famille, role=Role.objects.get(nom="Enfant suivi"),
            nom=name, prenom="Toto", genre="M", date_naissance=date(2010, 2, 16),
            rue="Château 4", npa=2000, localite="Moulinsart",
        )
        return famille


class TempMediaRootMixin:
    @classmethod
    def setUpClass(cls):
        cls._temp_media = tempfile.mkdtemp()
        cls._overridden_settings = cls._overridden_settings or {}
        cls._overridden_settings['MEDIA_ROOT'] = cls._temp_media
        super().setUpClass()

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(cls._temp_media)
        super().tearDownClass()


class FamilleTests(InitialDataMixin, TestCase):
    personne_data = {
        'nom': 'Dupont',
        'prenom': 'Jean',
        'date_naissance': '1950-03-30',
        'genre': 'M',
    }

    def setUp(self):
        self.user.groups.add(Group.objects.get(name='spe'))
        self.create_famille_spe()
        self.client.force_login(self.user)

    def test_acces(self):
        """
        Sans permission adaptée, un utilisateur ne peut pas accéder à certaines
        pages (création de famille, etc.).
        """
        user = Utilisateur.objects.create_user(
            'qqun', 'qqun@example.org', 'pwd', first_name='Qqun', last_name='Personne',
        )
        self.client.force_login(user)
        with self.assertLogs('django.request', level='WARNING'):
            response = self.client.get(reverse('spe-famille-add'))
        self.assertEqual(response.status_code, 403)

    def test_journal_acces(self):
        famille = FamilleSPE.objects.get(nom='Haddock')
        self.client.force_login(self.user)
        self.client.get(famille.suivi_url)
        self.client.force_login(self.user_admin)
        response = self.client.get(reverse("famille-journalacces", args=[famille.pk]))
        self.assertEqual(response.context["object_list"][0].utilisateur, self.user)

    def test_personne_creation(self):
        famille = FamilleSPE.objects.get(nom='Haddock')
        add_url = reverse('personne-add', args=[famille.pk])
        response = self.client.get(add_url)
        self.assertEqual(response.context['form'].initial['nom'], famille.nom)

        # Données minimales: nom, genre, rôle
        role = Role.objects.get(nom='Beau-père')
        response = self.client.post(add_url, data={'nom': 'Smith', 'genre': 'M', 'role': role.pk})
        self.assertRedirects(response, reverse('spe-famille-edit', args=[famille.pk]))
        response = self.client.post(
            add_url,
            data={**self.personne_data, 'role': Role.objects.get(nom='Beau-père').pk, 'localite': "bevaix"}
        )
        self.assertEqual(response.status_code, 302)
        self.assertQuerySetEqual(Personne.objects.all(), [
            '<Personne: Dupont Jean>',
            '<Personne: Haddock Archibald>',
            '<Personne: Haddock Toto>',
            '<Personne: Smith >',
        ], transform=repr)
        p1 = Personne.objects.get(nom='Dupont')
        self.assertEqual(p1.famille, famille)
        self.assertEqual(p1.localite, 'Bevaix')
        with self.assertRaises(Formation.DoesNotExist):
            p1.formation

        # Personne < 4 ans
        pers = Personne.objects.create_personne(
            famille=famille, prenom='Gaston', nom='Lagaffe',
            date_naissance=date.today() - timedelta(days=720),
            role=Role.objects.get(nom='Enfant suivi')
        )
        self.assertEqual(pers.formation.get_statut_display(), 'Pré-scolaire')
        self.assertTrue(famille.suivispe.demande_prioritaire)

    def test_label_profession_variable(self):
        famille = FamilleSPE.objects.get(nom='Haddock')
        response = self.client.get(
            '{}?role={}'.format(reverse('personne-add', args=[famille.pk]),
                                Role.objects.get(nom='Enfant suivi').pk)
        )
        self.assertContains(response, '<label for="id_profession">Profession/École :</label>', html=True)

        response = self.client.get(
            '{}?role={}'.format(reverse('personne-add', args=[famille.pk]),
                                Role.objects.get(nom='Père').pk)
        )
        self.assertContains(response, '<label for="id_profession">Profession :</label>', html=True)

    def test_get_membres(self):
        # Obtention des différents membres avec 2 requêtes: la famille, les membres (prefetch_related)
        with self.assertNumQueries(2):
            famille = FamilleSPE.objects.filter(nom='Haddock')[0]
            self.assertEqual(famille.membres_suivis()[0].prenom, 'Toto')
            self.assertEqual(famille.enfants_non_suivis(), [])
            self.assertEqual([p.nom_prenom for p in famille.parents()], ['Haddock Archibald'])
            self.assertEqual(famille.autres_parents(), [])

    def test_personne_age(self):
        with patch('croixrouge.models.date') as mock_date:
            mock_date.today.return_value = date(2019, 1, 16)
            self.assertEqual(Personne(date_naissance=date(1999, 11, 4)).age, 19.2)
            self.assertEqual(Personne(date_naissance=date(2000, 1, 1)).age, 19.0)
            self.assertEqual(Personne(date_naissance=date(2000, 1, 31)).age, 18.9)

    def test_personne_age_str(self):
        pers = Personne(prenom='Toto')
        pers.date_naissance = date.today() - timedelta(days=12)
        self.assertEqual(pers.age_str(), '12 jours')
        pers.date_naissance = date.today() - timedelta(days=33)
        self.assertEqual(pers.age_str(), '4 sem. 5 jours')
        pers.date_naissance = date.today() - timedelta(days=77)
        self.assertEqual(pers.age_str(), '2 mois 2 sem.')
        pers.date_naissance = date.today() - timedelta(days=689)
        self.assertEqual(pers.age_str(), '22 mois 4 sem.')
        pers.date_naissance = date.today() - timedelta(days=690)
        self.assertEqual(pers.age_str(), '1 an 10 mois')
        pers.date_naissance = date.today() - timedelta(days=691)
        self.assertEqual(pers.age_str(format_='jour'), '691 jours')

    def test_personne_edition(self):
        famille = FamilleSPE.objects.create_famille(nom='Dupont', equipe='montagnes')
        pers_data = dict(self.personne_data, famille=famille.pk, role=Role.objects.get(nom='Père').pk)
        form = PersonneForm(data=pers_data, famille=famille)
        self.assertTrue(form.is_valid(), msg=form.errors)
        pers = form.save()
        edit_url = reverse('personne-edit', args=[famille.pk, pers.pk])
        new_data = dict(pers_data, rue='Rue du Parc 12')
        response = self.client.post(edit_url, data=new_data)
        self.assertEqual(response.status_code, 302)
        pers.refresh_from_db()
        self.assertEqual(pers.rue, 'Rue du Parc 12')
        # Edition is refused if user is missing the permission
        user = Utilisateur.objects.create_user(
            'joe', 'joe@example.org', 'pwd', first_name='Joe', last_name='Cook',
        )
        self.client.force_login(user)
        with self.assertLogs('django.request', level='WARNING'):
            response = self.client.get(edit_url)
        self.assertEqual(response.status_code, 403)

    def test_personne_formation(self):
        famille = FamilleSPE.objects.get(nom='Haddock')
        pers = Personne.objects.create_personne(
            famille=famille, prenom='Gaston', nom='Lagaffe',
            role=Role.objects.get(nom='Enfant suivi')
        )
        self.assertTrue(famille.can_edit(self.user))
        form_url = reverse('formation', args=[pers.pk])
        response = self.client.get(form_url)
        self.assertFalse(response.context['form'].readonly)
        self.assertContains(response, "Enregistrer")
        response = self.client.post(form_url, data={
            'statut': 'cycle2',
            'cercle_scolaire': CercleScolaire.objects.first().pk,
            'college': 'École parfaite',
            'classe': '6H',
        })
        self.assertRedirects(response, reverse('spe-famille-edit', args=[famille.pk]))
        pers.refresh_from_db()
        self.assertEqual(pers.formation.classe, '6H')

    def test_delete_personne(self):
        famille = FamilleSPE.objects.first()
        pers = Personne.objects.create_personne(
            famille=famille, prenom='Gaston', nom='Lagaffe',
            role=Role.objects.get(nom='Père')
        )
        response = self.client.post(
            reverse('personne-delete', args=[famille.pk, pers.pk]), follow=True
        )
        self.assertRedirects(response, reverse('spe-famille-edit', args=[famille.pk]), status_code=302)

    def test_document_upload_delete(self):
        famille = FamilleSPE.objects.first()
        with open(__file__, mode='rb') as fh:
            response = self.client.post(reverse('famille-doc-upload', args=[famille.pk]), data={
                'famille': famille.pk,
                'fichier': File(fh),
                'titre': 'Titre1',
            })
        self.assertRedirects(response, famille.suivi_url)
        self.assertEqual(famille.documents.count(), 1)
        os.remove(famille.documents.first().fichier.path)
        # Adding second file with same title gives a validation error
        with open(__file__, mode='rb') as fh:
            response = self.client.post(reverse('famille-doc-upload', args=[famille.pk]), data={
                'famille': famille.pk,
                'fichier': File(fh),
                'titre': 'Titre1',
            })
        self.assertEqual(
            response.context['form'].errors['__all__'],
            ['Un objet Document avec ces champs Famille et Titre existe déjà.']
        )
        # Delete
        response = self.client.post(
            reverse('famille-doc-delete', args=[famille.pk, famille.documents.first().pk])
        )
        self.assertRedirects(response, famille.suivi_url)

    def test_ajoute_contact_a_enfant_suivi(self):
        famille = FamilleSPE.objects.get(nom='Haddock')
        pers = Personne.objects.create_personne(
            famille=famille, prenom='Gaston', nom='Lagaffe',
            role=Role.objects.get(nom='Enfant suivi')
        )
        new_contact = Contact.objects.get(nom='DrSpontz')
        self.client.force_login(self.user)
        add_contact_url = reverse('personne-reseau-add', args=[pers.pk])
        response = self.client.post(add_contact_url, data={'contacts[]': new_contact.pk})
        self.assertEqual(response.json(), {'is_valid': True})
        self.assertEqual(pers.reseaux.count(), 1)

    def test_creation_contact_possible_pour_educ(self):
        famille = FamilleSPE.objects.get(nom='Haddock')
        pers = Personne.objects.create_personne(
            famille=famille, prenom='Gaston', nom='Lagaffe',
            role=Role.objects.get(nom='Enfant suivi')
        )
        educ = Utilisateur.objects.create_user(
            'other', 'other@example.org', 'mepassword', prenom='Bruce', nom='Externe',
        )
        educ.groups.add(Group.objects.get(name='spe'))
        self.client.force_login(educ)
        self.client.force_login(self.user)
        add_contact_url = f"{reverse('contact-add')}?forpers={pers.pk}"
        new_contact = {
            'nom': "ContactNom",
            'prenom': 'ContactPrenom'
        }
        response = self.client.post(add_contact_url, data=new_contact)
        self.assertTrue(response.status_code, 200)
        response = self.client.get(reverse('personne-reseau-list', args=[pers.pk]))
        self.assertContains(response, "<td>ContactNom ContactPrenom</td>", html=True)
        self.assertEqual(pers.reseaux.count(), 1)

    def test_retirer_contact_du_reseau(self):
        famille = FamilleSPE.objects.get(nom='Haddock')
        pers = Personne.objects.create_personne(
            famille=famille, prenom='Gaston', nom='Lagaffe',
            role=Role.objects.get(nom='Enfant suivi')
        )
        new_contact = Contact.objects.get(nom='DrSpontz')
        pers.reseaux.add(new_contact)
        self.assertEqual(pers.reseaux.count(), 1)

        remove_url = reverse('personne-reseau-remove', args=[pers.pk, new_contact.pk])
        response = self.client.post(remove_url, data={})
        self.assertTrue(response.status_code, 302)
        pers.refresh_from_db()
        self.assertEqual(pers.reseaux.count(), 0)


class UtilisateurTests(InitialDataMixin, TestCase):

    def test_utilisateur_list(self):
        self.client.force_login(self.user_admin)
        response = self.client.get(reverse('utilisateur-list'))
        self.assertEqual(len(response.context['object_list']), 3)

    def test_create_utilisateur(self):
        self.client.force_login(self.user_admin)
        response = self.client.post(reverse('utilisateur-add'), data={
            'nom': 'Muller',
            'prenom': 'Hans',
            'username': 'MullerH',
            'sigle': 'HM',
            'groups': [Group.objects.create(name='grp1').pk, Group.objects.create(name='grp2').pk],
            'taux_activite': 100,
            'decharge': 2,
        })
        self.assertEqual(response.status_code, 302)
        user = Utilisateur.objects.get(nom='Muller')
        self.assertEqual(user.service.sigle, 'CRNE')
        self.assertEqual(user.username, 'MullerH')
        self.assertEqual(user.groups.count(), 2)
        self.assertEqual(user.charge_max, 30)

    def test_taux_activite(self):
        data = {
            'nom': 'Muller',
            'prenom': 'Hans',
            'username': 'MullerH',
            'sigle': 'HM',
            'taux_activite': 90,
        }
        form = UtilisateurForm(data=data)
        self.assertTrue(form.is_valid())

        form = UtilisateurForm(data={**data, 'taux_activite': 110})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['taux_activite'],
            ['Assurez-vous que cette valeur est inférieure ou égale à 100.']
        )

    def test_delete_utilisateur(self):
        user = Utilisateur.objects.create_user(username='user1', nom='Supprimer')
        self.client.force_login(self.user_admin)
        self.client.post(reverse('utilisateur-delete', args=[user.pk]))
        user.refresh_from_db()
        self.assertIsNotNone(user.date_desactivation)
        self.assertFalse(user.is_active)
        self.assertFalse(user.est_actif)

        response = self.client.get(reverse('utilisateur-autocomplete') + '?q=Sup')
        self.assertEqual(response.json()['results'], [])

    @override_settings(PASSWORD_HASHERS=PASSWORD_HASHERS)
    def test_reinit_password(self):
        Utilisateur.objects.create_superuser(
            'somebody', 'somebody@example.org', 'somebodypassword', prenom='Jean', nom='Valjean',
        )
        new_user = Utilisateur.objects.create_user(username='user')
        self.assertTrue(new_user.password.startswith('!'))  # Unusable password
        self.client.login(username='somebody', password='somebodypassword')
        self.client.post(reverse('utilisateur-password-reinit', args=[new_user.pk]))
        new_user.refresh_from_db()
        self.assertTrue(new_user.password.startswith('pbkdf2_sha256$'))  # Usable password

    def test_reinit_mobile(self):
        from django_otp.plugins.otp_totp.models import TOTPDevice
        user = Utilisateur.objects.create_user(username='user', nom="Spontz")
        TOTPDevice.objects.create(user=user, name='default')
        self.client.force_login(self.user_admin)
        response = self.client.post(reverse('utilisateur-otp-device-reinit', args=[user.pk]), follow=True)
        self.assertEqual(str(list(response.context['messages'])[0]), 'Le mobile de «Spontz » a été réinitialisé.')
        response = self.client.post(reverse('utilisateur-otp-device-reinit', args=[user.pk]), follow=True)
        self.assertEqual(
            str(list(response.context['messages'])[0]),
            'Aucune configuration mobile trouvée pour «Spontz »'
        )

    def test_liste_utilisateurs_actifs_inactifs(self):
        user1 = Utilisateur.objects.create_user(
            'User1P', 'user1@exemple.org', 'user1_password', prenom='Paul', nom='User1',
        )
        Utilisateur.objects.create_user(
            'User2M', 'user2@exemple.org', 'user2_password', prenom='Max', nom='User2',
        )
        self.client.force_login(self.user_admin)
        response = self.client.get(reverse('utilisateur-list'))
        self.assertContains(response, 'User1 Paul')
        self.assertContains(response, 'User2 Max')
        self.client.post(reverse('utilisateur-delete', args=[user1.pk]))
        response = self.client.get(reverse('utilisateur-list'))
        self.assertNotContains(response, 'User1 Paul')
        response = self.client.get(reverse('utilisateur-desactive-list'))
        self.assertContains(response, 'User1 Paul')

    def test_reactivation_utilisateur(self):
        user1 = Utilisateur.objects.create_user(
            'User1P', 'user1@exemple.org', 'user1_password', prenom='Paul', nom='User1',
        )
        Utilisateur.objects.create_user(
            'User2M', 'user2@exemple.org', 'user2_password', prenom='Max', nom='User2',
        )
        self.client.force_login(self.user_admin)
        response = self.client.post(reverse('utilisateur-delete', args=[user1.pk]), follow=True)
        self.assertNotContains(response, 'User1 Paul')
        user1.refresh_from_db()
        self.assertIsNotNone(user1.date_desactivation)
        response = self.client.post(reverse('utilisateur-reactiver', args=[user1.pk]), follow=True)
        self.assertContains(response, 'User1 Paul')
        user1.refresh_from_db()
        self.assertIsNone(user1.date_desactivation)


class OtherTests(InitialDataMixin, TestCase):
    def setUp(self):
        self.client.force_login(self.user)

    def test_contact_list(self):
        response = self.client.get(reverse('contact-list'))
        self.assertEqual(len(response.context['object_list']), 3)
        response = self.client.get(reverse('contact-list') + '?service=%s' % Service.objects.get(sigle='OPEN').pk)
        self.assertEqual(len(response.context['object_list']), 1)
        response = self.client.get(reverse('contact-list') + '?role=%s' % Role.objects.get(nom='Médecin').pk)
        self.assertEqual(len(response.context['object_list']), 1)
        response = self.client.get(reverse('contact-list') + '?texte=barn')
        self.assertEqual(len(response.context['object_list']), 1)

    def test_contact_autocomplete(self):
        medecin = Role.objects.get(nom='Médecin')
        ope_service = Service.objects.create(sigle="OPEC")
        other_service = Service.objects.get(sigle="SSE")
        contact_ope = Contact.objects.create(
            nom="Duplain", prenom="Irma", service=ope_service
        )
        contact_ope.roles.add(medecin)
        contact_other = Contact.objects.create(
            nom="Dupont", prenom="Paul", service=other_service
        )
        contact_other.roles.add(medecin)
        response = self.client.get(reverse('contact-autocomplete') + '?q=Dup')
        self.assertEqual(
            [res['text'] for res in response.json()['results']],
            ['Duplain Irma (OPEC)', 'Dupont Paul (SSE)']
        )
        # The OPE version
        response = self.client.get(reverse('contact-ope-autocomplete') + '?q=Dup')
        self.assertEqual(
            [res['text'] for res in response.json()['results']],
            ['Duplain Irma (OPEC)']
        )

    def test_supression_affichage_du_contact_desactive_dans_contact_autocomplete(self):
        medecin = Role.objects.get(nom='Médecin')
        ope_service = Service.objects.create(sigle="OPEC")
        other_service = Service.objects.get(sigle="SSE")
        contact_ope = Contact.objects.create(
            nom="Duplain", prenom="Irma", service=ope_service, est_actif=False
        )
        contact_ope.roles.add(medecin)
        contact_other = Contact.objects.create(
            nom="Dupont", prenom="Paul", service=other_service
        )
        contact_other.roles.add(medecin)
        response = self.client.get(reverse('contact-autocomplete') + '?q=Dup')
        self.assertEqual(
            [res['text'] for res in response.json()['results']],
            ['Dupont Paul (SSE)']
        )

    def test_suppression_affichage_contact_desactive_dans_list_contact(self):
        medecin = Role.objects.get(nom='Médecin')
        ope_service = Service.objects.create(sigle="OPEC")
        other_service = Service.objects.get(sigle="SSE")
        contact_ope = Contact.objects.create(
            nom="Duplain", prenom="Irma", service=ope_service, est_actif=False
        )
        contact_ope.roles.add(medecin)
        contact_other = Contact.objects.create(
            nom="Dupont", prenom="Paul", service=other_service
        )
        contact_other.roles.add(medecin)
        response = self.client.get(reverse('contact-list'))
        self.assertNotContains(response, 'Duplain')

    def test_controler_doublon_contact(self):
        medecin = Role.objects.get(nom='Médecin')
        contact1 = Contact.objects.create(
            nom="Duplain", prenom="Irma", est_actif=True
        )
        contact1.roles.add(medecin)
        doublon_url = reverse('contact-doublon')
        response = self.client.post(doublon_url, data={'nom': "Duplain", 'prenom': "Irma"})
        self.assertEqual(response.json(), [{'nom': "Duplain", 'prenom': "Irma"}])
        response = self.client.post(doublon_url, data={'nom': "Nouveau", 'prenom': "Contact"})
        # Réponse vide signifie pas de doublon détecté.
        self.assertEqual(response.json(), '')

    def test_service_creation(self):
        s1 = Service.objects.create(sigle='lower')  # transform code from lower to uppercase
        self.assertEqual(s1.sigle, 'LOWER')
        # By form
        self.client.force_login(self.user_admin)
        response = self.client.post(
            reverse('service-add'), data={'sigle': 'SERVICE', 'nom_complet': 'Super service'}
        )
        self.assertRedirects(response, reverse('service-list'))
        self.assertEqual(Service.objects.filter(sigle='SERVICE').count(), 1)

    def test_service_list(self):
        response = self.client.get(reverse('service-list'))
        self.assertEqual(len(response.context['object_list']), 3)

    def test_raise_unique_constraint_school_center_creation(self):
        sc = CercleScolaire(nom='EOREN-MAIL')
        self.assertRaises(IntegrityError, sc.save)

    def test_cerclescolaire_list(self):
        response = self.client.get(reverse('cercle-list'))
        self.assertEqual(len(response.context['object_list']), 2)

    def test_role_create(self):
        self.client.force_login(self.user_admin)
        response = self.client.post(reverse('role-add'), data={'nom': 'ROLE1', 'famille': False})
        self.assertRedirects(response, reverse('role-list'))
        self.assertEqual(Role.objects.filter(nom='ROLE1').count(), 1)

    def test_role_list(self):
        response = self.client.get(reverse('role-list'))
        self.assertGreater(len(response.context['object_list']), 10)
        # Les rôles "protégés" ne sont pas éditables.
        self.assertContains(response, '<td>Enfant suivi</td>')

    def test_delete_used_role(self):
        """A role with at least one attached Personne cannot be deleted."""
        role = Role.objects.get(nom="Père")
        Personne.objects.create_personne(
            famille=Famille.objects.create(nom='Schmurz', typ=['spe']), role=role,
            nom='Haddock', prenom='Archibald', genre='M', date_naissance=date(1956, 2, 16),
        )
        self.assertGreater(role.personne_set.count(), 0)
        self.client.force_login(self.user_admin)
        response = self.client.post(reverse('role-delete', args=[role.pk]), follow=True)
        self.assertContains(response, "Cannot delete")

    def test_facturation_interne(self):
        users = []
        for username in ['ipe1', 'ipe2', 'sof', 'ase', 'apa']:
            users.append(Utilisateur.objects.create_user(username, nom=username.title(), prenom="Madame"))
        ipe1, ipe2, sof, ase, apa = users
        role_ase, role_sof, role_apa, role_ipe = Role.objects.filter(
            nom__in=['ASE', 'Assistant-e social-e', 'Coach APA', 'IPE']
        ).order_by('nom')
        ipe1.roles.add(role_ipe)
        ipe2.roles.add(role_ipe)
        sof.roles.add(role_sof)
        ase.roles.add(role_ase)
        apa.roles.add(role_apa)
        famille = self.create_famille_spe()
        prests = PrestationSPE.objects.bulk_create([
            PrestationSPE(auteur=ipe1, date_prestation='2024-03-02', duree='00:40', famille=famille),
            PrestationSPE(auteur=sof, date_prestation='2024-03-05', duree='00:30', famille=famille),
            PrestationSPE(
                auteur=ase, date_prestation='2024-03-08', duree='00:20', famille=famille,
                lib_prestation=LibellePrestation.objects.get(code='spe03'),
            ),
            PrestationSPE(auteur=apa, date_prestation='2024-03-22', duree='00:10', famille=famille),
        ])
        for pers, prest in zip([ipe1, sof, ase, apa], prests):
            prest.intervenants.add(pers)
        prests[0].intervenants.add(ipe2)
        prests[1].intervenants.add(ipe1)
        famille_sof = self.create_famille_sof()
        prest = PrestationSOF.objects.create(
            auteur=ase, date_prestation="2024-03-05", duree="01:00", famille=famille_sof,
            lib_prestation=LibellePrestation.objects.get_or_create(unite="sof", code="sof04")[0],
        )
        prest.intervenants.add(ase)

        base = FacturationBase()
        base.mois = date(2024, 3, 1)
        base.typ = FacturationBase.typ_role_map['ipe-spe']
        lines, totals = base.prestations()
        self.maxDiff = None
        self.assertEqual(
            dict(totals),
            {ipe1: timedelta(minutes=70), ipe2: timedelta(minutes=40), 'total': timedelta(minutes=110)}
        )
        self.client.force_login(self.user_admin)
        for typ, expected in [
            ('ipe-spe', '00:40'), ('sof-spe', '00:30'), ('ase-spe', '00:20'), ('apa-spe', '00:10'),
            ('ase-sof', '01:00'),
        ]:
            response = self.client.get(reverse('facturation-pdf', args=[typ, '3', '2024']))
            self.assertEqual(response['content-type'], 'application/pdf')
            response = self.client.get(reverse('facturation-details', args=[typ, '3', '2024']))
            self.assertContains(response, f"<td>{expected}</td>")

    def test_truncate_with_more_ttag(self):
        from croixrouge.templatetags.my_tags import truncate_html_with_more
        txt = '<p>Ceci <b>est un très long</b> texte HTML.<br> Seconde ligne</p>'
        self.assertHTMLEqual(
            truncate_html_with_more(txt, 3),
            '<div class="long hidden"><p>Ceci <b>est un très long</b> texte HTML.<br>'
            ' Seconde ligne</p></div>'
            '<div class="short"><p>Ceci <b>est un…</b></p></div>'
            '<a class="read_more" href=".">Afficher la suite</a>'
        )

    def test_info_ope_ttag(self):
        from croixrouge.templatetags.my_tags import info_ope
        self.assertEqual(
            info_ope(Contact.objects.get(nom='Sybarnez')),
            '<span title="032 886 88 88">Sybarnez Tina</span>'
        )

    def test_check_date_allowed(self):
        with freeze_time("2021-01-20"):
            self.assertFalse(PrestationBase.check_date_allowed(self.user, date(2020, 1, 20)))
        with freeze_time("2021-01-04"):
            self.assertFalse(PrestationBase.check_date_allowed(self.user, date(2020, 11, 30)))
            self.assertTrue(PrestationBase.check_date_allowed(self.user, date(2020, 12, 2)))
            self.assertTrue(PrestationBase.check_date_allowed(self.user, date(2021, 1, 2)))

        with freeze_time("2020-11-02"):
            self.assertFalse(PrestationBase.check_date_allowed(self.user, date(2019, 10, 15)))
            self.assertTrue(PrestationBase.check_date_allowed(self.user, date(2020, 10, 15)))
            self.assertTrue(PrestationBase.check_date_allowed(self.user, date(2020, 11, 12)))

    def test_duration_field(self):
        class SomeForm(Form):
            duration = HMDurationField()

        form = SomeForm({'duration': timedelta(days=1, hours=2, minutes=21, seconds=4)})
        self.assertInHTML(
            '<input type="text" name="duration" value="26:21" placeholder="hh:mm" required id="id_duration">',
            form.as_div()
        )
