from datetime import date, timedelta
from pathlib import Path

from django.contrib.auth.models import AbstractUser, Group, UserManager
from django.contrib.postgres.fields import ArrayField
from django.core.validators import MaxValueValidator
from django.db import models, transaction
from django.db.models import Count, F, Func, Max, Q, Sum
from django.db.models.functions import ExtractMonth, ExtractYear
from django.urls import reverse
from django.utils.functional import cached_property, classproperty
from django_countries.fields import CountryField

from amf import choices as amf_choices
from common import choices
from common.fields import ChoiceArrayField

from .context_processors import current_app
from .utils import format_adresse, format_contact, format_d_m_Y, random_string_generator


class CercleScolaire(models.Model):
    """ Les 7 cercles scolaires du canton"""

    nom = models.CharField(max_length=50, unique=True)
    telephone = models.CharField('tél.', max_length=35, blank=True)

    class Meta:
        verbose_name = 'Cercle scolaire'
        verbose_name_plural = 'Cercles scolaires'
        ordering = ('nom',)

    def __str__(self):
        return self.nom


class Service(models.Model):
    """ Services sociaux en lien avec la Croix-Rouge """

    sigle = models.CharField('Sigle', max_length=20, unique=True)
    nom_complet = models.CharField('Nom complet', max_length=80, blank=True)

    class Meta:
        ordering = ('sigle',)

    def __str__(self):
        return self.sigle

    def save(self, *args, **kwargs):
        self.sigle = self.sigle.upper()
        super().save(*args, **kwargs)


class RoleManager(models.Manager):
    def get_by_natural_key(self, nom):
        return self.get(nom=nom)

    def get_enfant_suivi(self):
        return self.model.objects.get_or_create(
            nom='Enfant suivi',
            famille=True,
            intervenant=False,
            editeur=False,
        )[0]

    def get_enfant_non_suivi(self):
        return self.model.objects.get_or_create(
            nom='Enfant non-suivi',
            famille=True,
            intervenant=False,
            editeur=False,
        )[0]

    def get_parent_famille_accueil(self):
        return Role.objects.get_or_create(
            nom="Parent famille d'accueil",
            famille=True,
            intervenant=False,
            editeur=False
        )[0]


class Role(models.Model):
    NON_EDITABLE = ['Père', 'Mère', 'Enfant suivi', 'Enfant non-suivi']
    ROLES_PARENTS = ['Père', 'Mère']

    nom = models.CharField("Nom", max_length=50, unique=True)
    famille = models.BooleanField("Famille", default=False)
    intervenant = models.BooleanField("Intervenant", default=False)
    editeur = models.BooleanField("Éditeur", default=False, help_text=(
        "Un rôle éditeur donne le droit de modification des dossiers familles si "
        "la personne est intervenante."
    ))

    objects = RoleManager()

    class Meta:
        ordering = ('nom',)

    def __str__(self):
        return self.nom

    @property
    def editable(self):
        return self.nom not in self.NON_EDITABLE

    def natural_key(self):
        return (self.nom,)


class Contact(models.Model):
    prenom = models.CharField('Prénom', max_length=30)
    nom = models.CharField('Nom', max_length=30)
    rue = models.CharField('Rue', max_length=30, blank=True)
    npa = models.CharField('NPA', max_length=4, blank=True)
    localite = models.CharField('Localité', max_length=30, blank=True)
    tel_prive = models.CharField('Tél. privé', max_length=30, blank=True)
    tel_prof = models.CharField('Tél. prof.', max_length=30, blank=True)
    email = models.EmailField('Courriel', max_length=100, blank=True)
    service = models.ForeignKey(Service, null=True, blank=True, on_delete=models.PROTECT)
    roles = models.ManyToManyField(Role, related_name='contacts', blank=True, verbose_name='Rôles')
    profession = models.CharField('Activité/prof.', max_length=100, blank=True)
    remarque = models.TextField("Remarque", blank=True)
    est_actif = models.BooleanField('actif', default=True)

    class Meta:
        verbose_name = 'Contact'
        ordering = ('nom', 'prenom')
        unique_together = ('nom', 'prenom', 'service')

    def __str__(self):
        sigle = self.service.sigle if self.service else ''
        return '{} ({})'.format(self.nom_prenom, sigle) if self.service else self.nom_prenom

    @property
    def nom_prenom(self):
        return '{} {}'.format(self.nom, self.prenom)

    @property
    def adresse(self):
        return format_adresse(self.rue, self.npa, self.localite)

    @cached_property
    def all_roles(self):
        return self.roles.all()

    def has_role(self, roles=()):
        """Return True if user has at least one of the `roles` name."""
        return bool(set(r.nom for r in self.all_roles).intersection(set(roles)))

    def roles_str(self, sep=', '):
        return sep.join([role.nom for role in self.all_roles])

    @property
    def contact(self):
        return format_contact(self.tel_prof, self.email)

    @classmethod
    def membres_ope(cls):
        return cls.objects.filter(service__sigle__startswith='OPE', est_actif=True).select_related('service')


class EquipeChoices(models.TextChoices):
    MONTAGNES = 'montagnes', 'Montagnes et V-d-T'
    LITTORAL = 'littoral', 'Littoral et V-d-R'


class Utilisateur(Contact, AbstractUser):
    sigle = models.CharField(max_length=5, blank=True)
    signature = models.ImageField(upload_to="signatures/", blank=True, null=True)
    equipe = models.CharField("Équipe", max_length=10, choices=EquipeChoices.choices, blank=True)
    date_desactivation = models.DateField('Date désactivation', null=True, blank=True)
    taux_activite = models.PositiveSmallIntegerField(
        "Taux d’activité (en %)", blank=True, default=0, validators=[MaxValueValidator(100)]
    )
    decharge = models.PositiveSmallIntegerField("Heures de décharge", blank=True, null=True)

    objects = UserManager()

    def __str__(self):
        return self.nom_prenom

    @property
    def groupes(self):
        return [item.name for item in self.groups.all()]

    @property
    def is_psy_or_educ(self):
        return self.has_role({'Psy', 'Educ'})

    @property
    def is_responsable(self):
        return self.has_role({'Responsable/coordinateur'})

    @property
    def is_admin_ipe(self):
        return 'admin ipe' in self.groupes

    @property
    def initiales(self):
        return self.sigle if self.sigle else f'{self.prenom[0]}{self.nom[0]}'

    @property
    def charge_max(self):
        """
        Renvoie la charge maximale d'heures hebodmadaires de charge dossier en
        fonction du taux d'activité.
        """
        charge_map = {
            100: 32, 90: 28, 80: 24, 75: 22, 70: 20, 60: 16, 0: 0,
        }
        charge = charge_map.get(self.taux_activite, int(32 * self.taux_activite / 100))
        if self.decharge:
            charge -= self.decharge
        return charge

    def prestations(self, unite):
        """Renvoie toutes les prestations (liées ou générales) de l’utilisateur."""
        return getattr(self, f'prestations_{unite}').all()

    def prestations_gen(self, unite):
        """Renvoie les prestations générales de l’utilisateur."""
        return self.prestations(unite).filter(famille__isnull=True)

    def temps_total_prestations(self, unite):
        return self.prestations(unite).model.temps_total(self.prestations(unite), tous_utilisateurs=False)

    def total_mensuel(self, unite, month, year):
        prestations = self.prestations(unite).filter(
            date_prestation__year=year, date_prestation__month=month
        )
        return self.prestations(unite).model.temps_total(prestations, tous_utilisateurs=False)

    def totaux_mensuels(self, unite, year):
        return [self.total_mensuel(unite, m, year) for m in range(1, 13)]

    def total_annuel(self, unite, year):
        prestations = self.prestations(unite).filter(date_prestation__year=year)
        return self.prestations(unite).model.temps_total(prestations, tous_utilisateurs=False)

    @classmethod
    def _intervenants(cls, groupes=(), annee=None):
        if annee is None:
            # Utilisateurs actuellement actifs
            utils = Utilisateur.objects.filter(date_desactivation__isnull=True)
        else:
            utils = Utilisateur.objects.filter(
                Q(date_desactivation__isnull=True) | Q(date_desactivation__year__gte=annee)
            )
        return utils.filter(
            groups__name__in=groupes,
            is_superuser=False
        ).exclude(
            groups__name__in=['direction']
        ).distinct()

    @classmethod
    def intervenants_spe(cls, annee=None):
        return cls._intervenants(groupes=['spe'], annee=annee)

    @classmethod
    def intervenants_sof(cls, annee=None):
        return cls._intervenants(groupes=['sof'], annee=annee)

    @classmethod
    def intervenants_ipe(cls, annee=None):
        return cls._intervenants(groupes=['ipe'], annee=annee)


class GroupInfo(models.Model):
    group = models.OneToOneField(Group, on_delete=models.CASCADE)
    description = models.TextField(blank=True)

    def __str__(self):
        return f"Description of group {self.group.name}"


class RegionManager(models.Manager):
    def par_secteur(self, secteur):
        return self.filter(secteurs__contains=[secteur])


class Region(models.Model):
    nom = models.CharField(max_length=30, unique=True)
    secteurs = ChoiceArrayField(
        models.CharField(max_length=10, choices=choices.UNITE_CROIXROUGE_CHOICES),
        verbose_name="Secteurs", blank=True, null=True
    )
    rue = models.CharField("Rue", max_length=100, blank=True)

    objects = RegionManager()

    def __str__(self):
        return self.nom

    def __lt__(self, other):
        return self.nom < (other.nom if other else '')


class Famille(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    archived_at = models.DateTimeField('Archivée le', blank=True, null=True)
    typ = ArrayField(
        models.CharField(max_length=5, choices=choices.UNITE_CROIXROUGE_CHOICES)
    )
    nom = models.CharField('Nom de famille', max_length=40)
    rue = models.CharField('Rue', max_length=60, blank=True)
    npa = models.CharField('NPA', max_length=4, blank=True)
    localite = models.CharField('Localité', max_length=30, blank=True)
    region = models.ForeignKey(to=Region, null=True, blank=True, default=None, on_delete=models.SET_NULL)
    telephone = models.CharField('Tél.', max_length=60, blank=True)
    autorite_parentale = models.CharField("Autorité parentale", max_length=20,
                                          choices=choices.AUTORITE_PARENTALE_CHOICES, blank=True)
    monoparentale = models.BooleanField('Famille monoparent.', default=None, blank=True, null=True)
    statut_marital = models.CharField("Statut marital", max_length=20, choices=choices.STATUT_MARITAL_CHOICES,
                                      blank=True)
    connue = models.BooleanField("famille déjà suivie", default=False)
    accueil = models.BooleanField("famille d'accueil", default=False)
    interprete = models.BooleanField("interprète", default=False)
    # Pour CIPE
    besoins_part = models.BooleanField("famille à besoins particuliers", default=False)
    sap = models.BooleanField(default=False, verbose_name="famille s@p")
    garde = models.CharField(
        'Type de garde', max_length=20, choices=choices.TYPE_GARDE_CHOICES, blank=True
    )
    provenance = models.CharField(
        'Provenance', max_length=30, choices=choices.PROVENANCE_DESTINATION_CHOICES, blank=True)
    destination = models.CharField(
        'Destination', max_length=30, choices=choices.PROVENANCE_DESTINATION_CHOICES, blank=True)
    statut_financier = models.CharField(
        'Statut financier', max_length=30, choices=choices.STATUT_FINANCIER_CHOICES, blank=True)
    remarques = models.TextField(blank=True)

    default_typ = None
    suivi_name = None
    typ_register = {}

    class Meta:
        ordering = ('nom', 'npa')
        permissions = (
            ('export_stats', 'Exporter les statistiques'),
        )

    def __str__(self):
        return '{} - {}'.format(self.nom, self.adresse)

    def refresh_from_db(self):
        try:
            del self._types_fam
        except AttributeError:
            pass
        super().refresh_from_db()

    @classproperty
    def famille_map(cls):
        from amf.models import FamilleAMF
        from cipe.models import FamilleCIPE
        from sof.models import FamilleSOF
        from spe.models import FamilleSPE
        return {'spe': FamilleSPE, 'cipe': FamilleCIPE, 'amf': FamilleAMF, 'sof': FamilleSOF}

    @classmethod
    def get_from_request(cls, request, **kwargs):
        """Polymorphism based on Famille.typ or current request context."""
        famille = cls.objects.get(**kwargs)
        if famille.__class__ != Famille:
            # Already a subclass
            return famille
        if len(famille.typ) == 1:
            famille.__class__ = cls.famille_map[famille.typ[0]]
            return famille
        source = current_app(request)['source']
        if source in famille.typ:
            famille.__class__ = cls.famille_map[source]
            return famille
        raise Famille.DoesNotExist

    @classmethod
    def suivi_class(cls):
        return cls._meta.get_field(cls.suivi_name).remote_field.model

    @property
    def adresse(self):
        return format_adresse(self.rue, self.npa, self.localite)

    @property
    def suivi(self):
        return getattr(self, self.suivi_name) if self.suivi_name else None

    @property
    def suivi_url(self):
        typ = self.default_typ or self.typ[0]
        return reverse(f'{typ}-famille-suivi', args=[self.pk])

    @property
    def edit_url(self):
        typ = self.default_typ or self.typ[0]
        return reverse(f'{typ}-famille-edit', args=[self.pk])

    @property
    def add_person_url(self):
        return reverse('personne-add', args=[self.pk])

    def redirect_after_personne_creation(self, personne):
        return self.edit_url

    @property
    def print_coords_url(self):
        return reverse('%s-print-coord-famille' % self.default_typ, args=[self.pk])

    def membres_suivis(self):
        if 'membres' in getattr(self, '_prefetched_objects_cache', {}):
            return sorted([
                pers for pers in self._prefetched_objects_cache['membres'] if pers.role.nom == "Enfant suivi"
            ], key=lambda p: p.date_naissance or date(1950, 1, 1))
        else:
            return self.membres.filter(role__nom="Enfant suivi").order_by('date_naissance')

    def enfants_non_suivis(self):
        if 'membres' in getattr(self, '_prefetched_objects_cache', {}):
            return sorted([
                pers for pers in self._prefetched_objects_cache['membres'] if pers.role.nom == "Enfant non-suivi"
            ], key=lambda p: p.date_naissance or date(1950, 1, 1))
        else:
            return self.membres.filter(role__nom='Enfant non-suivi').order_by('date_naissance')

    def parents(self):
        if 'membres' in getattr(self, '_prefetched_objects_cache', {}):
            parents = [
                pers for pers in self._prefetched_objects_cache['membres']
                if pers.role.nom in Role.ROLES_PARENTS
            ]
        else:
            parents = [
                pers for pers in self.membres.filter(role__nom__in=Role.ROLES_PARENTS)
            ]
        return sorted(parents, key=lambda p: p.role.nom)  # Mère avant Père

    def autres_parents(self):
        excluded_roles = Role.ROLES_PARENTS + ['Enfant suivi', 'Enfant non-suivi']
        if 'membres' in getattr(self, '_prefetched_objects_cache', {}):
            return [
                pers for pers in self._prefetched_objects_cache['membres']
                if pers.role.nom not in excluded_roles
            ]
        else:
            return self.membres.exclude(role__nom__in=excluded_roles)

    @cached_property
    def _types_fam(self):
        return [self.typ_register[typ](self) for typ in self.typ]

    def access_ok(self, user):
        return any(tf.access_ok(user) for tf in self._types_fam)

    def can_view(self, user):
        return any(tf.can_view(user) for tf in self._types_fam)

    def can_edit(self, user):
        return any(tf.can_edit(user) for tf in self._types_fam)

    def can_be_deleted(self, user):
        return all(tf.can_be_deleted(user) for tf in self._types_fam)

    @property
    def prestations(self):
        raise NotImplementedError

    def temps_total_prestations(self, interv=None):
        """
        Temps total des prestations liées à la famille, quel que soit le nombre
        de membres suivis.
        Filtré facultativement par intervenant.
        """
        prest = self.prestations if interv is None else self.prestations.filter(intervenant=interv)
        return prest.annotate(num_util=Count('intervenants')).aggregate(
            total=Sum(F('duree') * F('num_util'), output_field=models.DurationField())
        )['total'] or timedelta()

    def temps_total_prestations_reparti(self):
        """
        Temps total des prestations liées à la famille, divisé par le nombre d'enfants suivis.
        """
        duree = self.temps_total_prestations()
        duree //= (len(self.membres_suivis()) or 1)
        return duree

    def total_mensuel(self, date_debut_mois):
        """
        Temps total de prestations sur un mois donné
        """
        return self.prestations.filter(
            date_prestation__month=date_debut_mois.month,
            date_prestation__year=date_debut_mois.year
        ).aggregate(total=Sum('duree'))['total'] or timedelta()

    def total_mensuel_par_prestation(self, prest_codes, date_debut_mois):
        """
        Temps total d'évaluation sur un mois donné par prestation.
        """
        return self.prestations.annotate(
            num_util=Count('intervenants')
        ).filter(
            lib_prestation__code__in=prest_codes,
            date_prestation__month=date_debut_mois.month,
            date_prestation__year=date_debut_mois.year
        ).aggregate(
            total=Sum(F('duree') * F('num_util'), output_field=models.DurationField())
        )['total'] or timedelta()

    def total_mensuel_evaluation(self, date_debut_mois):
        return self.total_mensuel_par_prestation([f'{self.default_typ}01'], date_debut_mois)

    def total_mensuel_suivi(self, date_debut_mois):
        raise NotImplementedError

    def prestations_historiques(self):
        return PrestationBase.prestations_historiques(self.prestations.all())

    def prestations_du_mois(self):
        """
        Retourne le détail des prestations pour cette famille
        à partir du mois courant
        """
        date_ref = date(date.today().year, date.today().month, 1)
        return self.prestations.filter(date_prestation__gte=date_ref)

    def can_be_reactivated(self, user):
        typ = self.default_typ
        return user.has_perm(f"{typ}.change_famille{typ}") and self.suivi.date_fin_suivi is not None


class PersonneQuerySet(models.QuerySet):
    def create_personne(self, **kwargs):
        kwargs.pop('_state', None)
        pers = self.create(**kwargs)
        return self.add_formation(pers)

    def add_formation(self, pers):
        if pers.role.nom == 'Enfant suivi' and not hasattr(pers, 'formation'):
            moins_de_4ans = bool(pers.age and pers.age < 4)
            Formation.objects.create(
                personne=pers, statut='pre_scol' if moins_de_4ans else ''
            )
            if moins_de_4ans:
                try:
                    pers.famille.suivi.demande_prioritaire = True
                    pers.famille.suivi.save()
                except AttributeError:
                    pass
        return pers

    def avec_age(self):
        return self.annotate(
            _age=Func(F("date_naissance"), function="age", output_field=models.DurationField())
        )


class Personne(models.Model):
    """ Classe de base des personnes """
    NIVEAU_FR_CHOICES = (
        ('bon', 'Bon'),
        ('moyen', 'Moyen'),
        ('faible', 'Faible'),
        ('non', 'Ne parle pas le français'),
    )
    created_at = models.DateTimeField(auto_now_add=True, editable=False, null=True)
    archived_at = models.DateTimeField('Archivée le', blank=True, null=True, default=None)

    nom = models.CharField('Nom', max_length=30)
    prenom = models.CharField('Prénom', max_length=30, blank=True)
    date_naissance = models.DateField('Date de naissance', null=True, blank=True)
    genre = models.CharField('Genre', max_length=1, choices=(('M', 'M'), ('F', 'F')), default='M')
    rue = models.CharField('Rue', max_length=60, blank=True)
    npa = models.CharField('NPA', max_length=5, blank=True)
    localite = models.CharField('Localité', max_length=30, blank=True)
    telephone = models.CharField('Tél.', max_length=60, blank=True)
    email = models.EmailField('Courriel', blank=True)
    pays_origine = CountryField('Nationalité', blank=True)
    remarque = models.TextField(blank=True)
    remarque_privee = models.TextField('Remarque privée', blank=True)
    famille = models.ForeignKey(Famille, on_delete=models.CASCADE, related_name='membres')
    role = models.ForeignKey('Role', on_delete=models.PROTECT)
    profession = models.CharField('Profession', max_length=50, blank=True)
    filiation = models.CharField('Filiation', max_length=80, blank=True)
    decedee = models.BooleanField('Cette personne est décédée', default=False)
    reseaux = models.ManyToManyField(Contact, blank=True)
    allergies = models.TextField('Allergies', blank=True)
    permis = models.CharField('Permis/séjour', max_length=30, blank=True)
    niveau_fr = models.CharField('Niveau de français', max_length=10, choices=NIVEAU_FR_CHOICES, blank=True)

    # Champs spécifiques AMF
    amf_debut_de_placement = models.DateField('Début de placement', blank=True, null=True)
    amf_fin_de_placement = models.DateField('Fin de placement', blank=True, null=True)
    amf_type_accueil = models.CharField(  # champ "Désignation familles" dans le fichier Excel SPAJ
        "Type d'accueil", max_length=20, blank=True, choices=amf_choices.TypeAccueilChoices
    )
    amf_provenance = models.CharField('Provenance', max_length=20, blank=True, choices=amf_choices.ProvenanceChoices)
    amf_provenance_details = models.CharField('Provenance (détails)', max_length=50, blank=True)

    objects = PersonneQuerySet.as_manager()

    class Meta:
        verbose_name = 'Personne'
        ordering = ('nom', 'prenom')

    def __str__(self):
        return '{} {}'.format(self.nom, self.prenom)

    @property
    def nom_prenom(self):
        return str(self)

    @property
    def adresse(self):
        return format_adresse(self.rue, self.npa, self.localite)

    @property
    def age(self):
        return self.age_a(date.today())

    @cached_property
    def est_enfant_suivi_amf(self):
        if not self.famille or "amf" not in self.famille.typ:
            return False
        if self.role.nom == "Enfant suivi":
            return True
        return any([
            self.amf_debut_de_placement,
            self.amf_fin_de_placement,
            self.amf_type_accueil,
            self.amf_provenance,
        ])

    def age_str(self, date_=None, format_='auto'):
        """Renvoie '2 ans, 4 mois', '5 ans', '8 mois'.
        Param:
            'auto': application du format
                - 'jour' si l'âge < 30 jours
                - 'sem_jour' si l'âge < 77 jours (11 semaines)
                - 'mois_sem' si l'âge < 690 jours (23 mois)
                - 'an_mois': autres cas
            'jour': âge en jours
            'sem_jour': âge en semaines et jours
            'mois_sem': âge en mois et semaines
        """
        if not self.date_naissance:
            # Pourrait disparaître si date_naissance devient non null
            return ''
        if format_ not in ['auto', 'jour', 'sem_jour', 'mois_sem', 'an_mois']:
            raise ValueError('Paramètre erroné')
        age_jours = self.age_jours(date_)

        if (age_jours < 30 and format_ == 'auto') or format_ == 'jour':
            age_str = f"{age_jours} jour{'s' if age_jours > 1 else ''}"
        elif (age_jours < 77 and format_ == 'auto') or format_ == 'sem_jour':
            sem, jours = age_jours // 7, age_jours % 7
            age_str = f"{sem} sem."
            age_str += f" {jours} jour{'s' if jours > 1 else ''}" if jours > 0 else ''
        elif (age_jours < 690 and format_ == 'auto') or format_ == 'mois_sem':
            mois, sem = age_jours // 30, int((age_jours % 30)/7)
            age_str = f"{mois} mois"
            age_str += f" {sem} sem." if sem > 0 else ''
        else:
            ans, mois = int(age_jours / 365.25), int((age_jours % 365.25) / 30)
            age_str = f"{ans} an{'s' if ans > 1 else ''}"
            age_str += " %d mois" % mois if mois > 0 else ''
        return age_str

    def age_a(self, date_):
        if not self.date_naissance:
            # Pourrait disparaître si date_naissance devient non null
            return None
        age = (date_ - self.date_naissance).days / 365.25
        return int(age * 10) / 10  # 1 décimale arrondi vers le bas

    def age_jours(self, date_=None):
        if date_ is None:
            if age_duration := getattr(self, "_age", None):  # Annot. en BD
                return age_duration.days
            date_ = date.today()
        if not self.date_naissance:
            return None
        return (date_ - self.date_naissance).days

    def age_mois(self, date_=None):
        """Âge en mois à la date_"""
        if date_ is None:
            date_ = date.today()
        if not self.date_naissance or date_ < self.date_naissance:
            return None
        return self.age_jours(date_) / (365 / 12)

    @transaction.atomic
    def anonymiser(self):
        self.nom = random_string_generator()
        self.prenom = random_string_generator()
        self.date_naissance = None
        self.rue = ''
        self.localite = ''
        self.telephone = ''
        self.pays_origine = ''
        self.remarque = ''
        self.remarque_privee = ''
        self.profession = ''
        self.allergies = ''
        self.permis = ''
        self.save()
        self.reseaux.clear()
        self.anonymiser_prestations_cipe()
        if hasattr(self, 'formation'):
            self.formation.delete()

    @transaction.atomic
    def anonymiser_prestations_cipe(self):
        # Pour une personne CIPE avec role="Enfant suivi"
        if "cipe" in self.famille.typ and self.role == Role.objects.get_enfant_suivi():
            [p.anonymiser() for p in self.prestationcipe_set.all()]

    @property
    def localite_display(self):
        return '{} {}'.format(self.npa, self.localite)

    @property
    def gestite(self):
        from cipe.models import SuiviEnfant
        if self.role.nom == 'Mère':
            return SuiviEnfant.objects.filter(
                personne__famille=self.famille, gestite__isnull=False
            ).aggregate(max_gest=Max('gestite'))['max_gest']
        return None

    @property
    def edit_url(self):
        return reverse('personne-edit', args=[self.famille_id, self.pk])

    def can_edit(self, user):
        return self.famille.can_edit(user)

    def can_be_deleted(self, user):
        return self.famille.can_be_deleted(user)


class Formation(models.Model):

    FORMATION_CHOICES = (
        ('pre_scol', 'Pré-scolaire'),
        ('cycle1', 'Cycle 1'),
        ('cycle2', 'Cycle 2'),
        ('cycle3', 'Cycle 3'),
        ('apprenti', 'Apprentissage'),
        ('etudiant', 'Etudiant'),
        ('en_emploi', 'En emploi'),
        ('sans_emploi', 'Sans emploi'),
        ('sans_occupation', 'Sans occupation'),
    )

    personne = models.OneToOneField(Personne, on_delete=models.CASCADE)

    statut = models.CharField('Scolarité', max_length=20, choices=FORMATION_CHOICES, blank=True)
    cercle_scolaire = models.ForeignKey(
        CercleScolaire, blank=True, null=True, on_delete=models.SET_NULL,
        related_name='+', verbose_name='Cercle scolaire'
    )
    college = models.CharField('Collège', max_length=50, blank=True)
    classe = models.CharField('Classe', max_length=50, blank=True)
    enseignant = models.CharField('Enseignant', max_length=50, blank=True)

    creche = models.CharField('Crèche', max_length=50, blank=True)
    creche_resp = models.CharField('Resp.crèche', max_length=50, blank=True)

    entreprise = models.CharField('Entreprise', max_length=50, blank=True)
    maitre_apprentissage = models.CharField("Maître d'appr.", max_length=50, blank=True)

    remarque = models.TextField(blank=True)

    class Meta:
        verbose_name = 'Scolarité'

    def __str__(self):
        return 'Scolarité de {}'.format(self.personne)

    def can_edit(self, user):
        return self.personne.famille.can_edit(user)

    @property
    def sse(self):
        return "Champ à définir"

    def pdf_data(self):
        if self.personne.formation.statut == 'pre_scol':
            data = [
                ['Crèche: {}'.format(self.creche), 'responsable: {}'.format(self.creche_resp)],
            ]
        elif self.personne.formation.statut in ['cycle1', 'cycle2', 'cycle3', 'etudiant']:
            data = [
                ['Cercle: {}'.format(self.cercle_scolaire), 'collège: {}'.format(self.college)],
                ['Classe: {}'.format(self.classe), 'Enseignant-e: {}'.format(self.enseignant)]
            ]

        elif self.personne.formation.statut == 'apprenti':
            data = [
                ['Employeur: {}'.format(self.entreprise), 'resp. apprenti-e: {}'.format(self.maitre_apprentissage)],
            ]
        else:
            data = [self.personne.formation.statut]
        return data

    def info_scol(self):
        if self.statut == 'pre_scol':
            creche = f"Crèche: {self.creche}" if self.creche else ''
            resp = f" - resp.: {self.creche_resp}" if self.creche_resp else ''
            data = ''.join([creche, resp])
        elif self.statut in ['cycle1', 'cycle2', 'cycle3', 'etudiant']:
            college = f"Collège: {self.college}" if self.college else ''
            classe = f" - classe: {self.classe}" if self.classe else ''
            ens = f" - ens.: {self.enseignant}" if self.enseignant else ''
            data = ''.join([college, classe, ens])
        elif self.statut == 'apprenti':
            emp = f"Employeur: {self.entreprise}" if self.entreprise else ''
            resp = f" - resp.: {self.maitre_apprentissage}" if self.maitre_apprentissage else ''
            data = ''.join([emp, resp])
        elif self.statut:
            data = self.get_statut_display() if self.statut else ''
        else:
            data = "Formation: aucune info. saisie"
        return data


class Document(models.Model):
    famille = models.ForeignKey(Famille, related_name='documents', on_delete=models.CASCADE)
    fichier = models.FileField("Nouveau fichier", upload_to='doc')
    titre = models.CharField(max_length=100)

    class Meta:
        unique_together = ('famille', 'titre')

    def __str__(self):
        return self.titre

    def delete(self):
        Path(self.fichier.path).unlink(missing_ok=True)
        return super().delete()

    def can_edit(self, user):
        return self.famille.can_edit(user)

    def can_be_deleted(self, user):
        return self.famille.can_be_deleted(user)


class Rapport(models.Model):
    """
    Rapport est l'appellation historique, Résumé étant l'appellation moderne (dès février 2023).
    Les champs «techniques» n'ont pas été renommés.
    """
    famille = models.ForeignKey(Famille, related_name='rapports', on_delete=models.CASCADE)
    date = models.DateField("Date du résumé")
    auteur = models.ForeignKey(Utilisateur, on_delete=models.PROTECT)
    situation = models.TextField("Situation / contexte familial", blank=True)
    evolutions = models.TextField("Évolutions et observations", blank=True)
    evaluation = models.TextField("Évaluation / Hypothèses", blank=True)
    observ_ipe = models.TextField("Observations IPE", blank=True)
    observ_sof = models.TextField("Observations SOF", blank=True)
    observ_apa = models.TextField("Observations APA", blank=True)
    projet = models.TextField("Perspectives d’avenir", blank=True)

    class Meta:
        abstract = True

    def __str__(self):
        return "Résumé du {} pour la famille {}".format(format_d_m_Y(self.date), self.famille)

    def get_absolute_url(self):
        return reverse('famille-rapport-view', args=[self.famille.pk, self.pk])

    def edit_url(self):
        return reverse('famille-rapport-edit', args=[self.famille.pk, self.pk])

    def get_print_url(self):
        return reverse('famille-rapport-print', args=[self.famille.pk, self.pk])

    def can_edit(self, user):
        return self.famille.can_edit(user) or user in self.famille.suivi.intervenants.all()

    def can_delete(self, user):
        return self.famille.can_edit(user)

    def title(self):
        return f"Résumé du {format_d_m_Y(self.date)}"

    def intervenants(self):
        return [i.intervenant for i in self.famille.interventions_actives(self.date)]


class LibellePrestation(models.Model):
    code = models.CharField('Code', max_length=6, unique=True)
    nom = models.CharField('Nom', max_length=30)
    unite = models.CharField('Unité', max_length=10, choices=choices.UNITE_CROIXROUGE_CHOICES)
    actes = models.TextField('Actes à prester', blank=True)

    class Meta:
        ordering = ('code',)

    def __str__(self):
        return self.nom


class PrestationBase(models.Model):
    # Redéfinir CURRENT_APP dans le modèle qui hérite PrestationBase pour gérer
    # les permissions 'edit_prest_prev_month'
    CURRENT_APP = "croixrouge"

    auteur = models.ForeignKey(Utilisateur, on_delete=models.PROTECT, verbose_name='auteur')
    date_prestation = models.DateField("date de l’intervention")
    duree = models.DurationField("durée")
    lib_prestation = models.ForeignKey(
        LibellePrestation, on_delete=models.SET_NULL, null=True, default=None,
        related_name='prestations_%(app_label)s'
    )
    # Nombre de familles actives au moment de la prestation, utilisé pour ventiler les heures
    # dans les statistiques.
    familles_actives = models.PositiveSmallIntegerField(blank=True, default=0)
    texte = models.TextField('Contenu', blank=True)
    manque = models.BooleanField('Rendez-vous manqué', default=False)
    fichier = models.FileField('Fichier/image', upload_to='prestations', blank=True)

    # Nbre de jours maximum après fin d'un mois où il est encore possible de saisir
    # des données du mois précédent.
    DELAI_SAISIE_SUPPL = 14

    class Meta:
        ordering = ('-date_prestation',)
        abstract = True
        permissions = (
            ('edit_prest_prev_month', 'Modifier prestations du mois précédent'),
        )

    def __str__(self):
        if self.famille:
            return f'Prestation pour la famille {self.famille} le {self.date_prestation} : {self.duree}'
        return f'Prestation générale le {self.date_prestation} : {self.duree}'

    def can_edit(self, user):
        return (
            (user == self.auteur or user.has_perm(f'{self.CURRENT_APP}.edit_prest_prev_month'))
            and self.check_date_allowed(user, self.date_prestation, self.CURRENT_APP)
        )

    @classmethod
    def check_date_allowed(cls, user, dt, app='spe'):
        """Contrôle si la date `dt` est située dans le mois courant + DELAI_SAISIE_SUPPL."""
        today = date.today()
        delai = cls.DELAI_SAISIE_SUPPL
        if user.has_perm(f'{app}.edit_prest_prev_month'):
            delai += 31
        if today.day <= delai:
            return dt >= (today - timedelta(days=delai + 1)).replace(day=1)
        else:
            return dt.year == today.year and dt.month == today.month

    @classmethod
    def prestations_historiques(cls, prestations):
        """
        Renvoie un queryset avec toutes les prestations regroupées par annee/mois
        et le total correspondant.
        """
        return prestations.annotate(
            annee=ExtractYear('date_prestation'),
            mois=ExtractMonth('date_prestation')
        ).values('annee', 'mois').order_by('-annee', '-mois').annotate(total=Sum('duree'))

    @classmethod
    def temps_total(cls, prestations, tous_utilisateurs=False):
        """
        Renvoie le temps total des `prestations` (QuerySet) sous forme de chaîne '12:30'.
        Si tous_utilisateurs = True, multiplie le temps de chaque prestation par son
        nombre d'intervenants.
        """
        if prestations.count() > 0:
            if tous_utilisateurs:
                duree = prestations.annotate(num_util=Count('intervenants')).aggregate(
                    total=Sum(F('duree') * F('num_util'), output_field=models.DurationField())
                )['total'] or timedelta()
            else:
                duree = prestations.aggregate(total=Sum('duree'))['total'] or timedelta()
        else:
            duree = timedelta()
        return duree

    @classmethod
    def temps_total_general(cls, annee, familles=True):
        """
        Renvoie le temps total des prestations (familiales ou générales selon
        familles) pour l'année civile `annee`.
        """
        prest_tot = cls.objects.filter(
            famille__isnull=not familles,
            date_prestation__year=annee
        ).aggregate(total=Sum('duree'))['total'] or timedelta()
        return prest_tot

    @classmethod
    def temps_total_general_fam_gen(cls, annee):
        """
        Renvoie le temps total des prestations familiales ET générales pour l'année civile `annee`.
        """
        prest_tot = cls.objects.filter(
            date_prestation__year=annee
        ).aggregate(total=Sum('duree'))['total'] or timedelta()
        return prest_tot

    @classmethod
    def temps_totaux_mensuels(cls, annee):
        """
        Renvoie une liste du total mensuel de toutes les prestations familiales
        (sans prestations générales) pour l'année en cours (de janv. à déc.).
        """
        data = []
        for month in range(1, 13):
            tot = cls.objects.filter(
                famille__isnull=False,
                date_prestation__year=annee, date_prestation__month=month,
            ).aggregate(total=Sum('duree'))['total'] or timedelta()
            data.append(tot)
        return data


class JournalAcces(models.Model):
    """Journalisation des accès aux familles."""
    famille = models.ForeignKey(Famille, on_delete=models.CASCADE, related_name='+')
    utilisateur = models.ForeignKey(Utilisateur, on_delete=models.CASCADE, related_name='+')
    ordinaire = models.BooleanField(default=True)
    quand = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"Accès de «{self.utilisateur}» à la famille «{self.famille}», le {self.quand}"


class EtapeBase:
    """
        1. Numéro d'ordre pour tri dans la vue
        2. Code
        3. Affichage dans tableau
        4. Nom visible de l'étape
        5. Code de l'étape suivante
        6. Délai jusqu'à l'échéance (en jours)
        7. Code de l'étape précédente
        8. Date obligatoire précédente
        9. Saisie obligatoire ou non (True / False)
    """
    suivi_model = None

    def __init__(self, num, code, abrev, nom, suivante, delai, precedente, preced_oblig, oblig):
        self.num = num
        self.code = code
        self.abrev = abrev
        self.nom = nom
        self.suivante = suivante
        self.delai = delai
        self.precedente = precedente
        self.preced_oblig = preced_oblig
        self.oblig = oblig

    def __str__(self):
        return self.nom

    def __repr__(self):
        return '<Etape - {}/{}>'.format(self.num, self.code)

    def date(self, suivi):
        return getattr(suivi, 'date_{}'.format(self.code))

    def delai_depuis(self, suivi, date_base):
        """Délai de cette étape à partir de la date date_base (en général date précédente étape)."""
        if not date_base:
            return None
        return date_base + timedelta(days=self.delai)

    def date_nom(self):
        return 'date_{}'.format(self.code)

    def etape_suivante(self, suivi):
        return self.suivi_model.WORKFLOW[self.suivante] if self.suivante else None

    def etape_precedente(self):
        return self.suivi_model.WORKFLOW[self.precedente] if self.precedente else None
