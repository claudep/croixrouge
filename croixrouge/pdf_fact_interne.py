from decimal import Decimal

from django.utils import numberformat
from django.utils.dates import MONTHS
from reportlab.lib import colors
from reportlab.lib.units import cm
from reportlab.platypus import Paragraph, Spacer, Table, TableStyle

from .pdf import BaseCroixrougePDF
from .utils import format_duree


class FacturationPDF(BaseCroixrougePDF):
    title = "Facturation interne CRNE"

    def __init__(self, *args, typ=None, mois=None, prestations=None, **kwargs):
        self.typ = typ
        self.role = self.typ['role']
        self.mois = mois
        self.prestations = prestations[1]  # seuls les totaux nous intéressent
        super().__init__(*args, **kwargs)

    def get_filename(self):
        return f"facturation_interne_{self.typ['nom'].lower()}_{self.mois.year}{self.mois.month:02}.pdf"

    def produce(self):
        tarif = self.typ['tarif']
        self.story.append(Spacer(0, 1.5 * cm))
        self.story.append(Paragraph(
            f"Facturation interne: service {self.typ['nom']} pour {self.typ['pour']}",
            self.style_title
        ))
        self.story.append(Spacer(0, 1 * cm))
        self.story.append(Paragraph(f"Tarif horaire: CHF {tarif}.-", self.style_normal))
        self.story.append(Paragraph(f"Période: {MONTHS[self.mois.month]} {self.mois.year}", self.style_normal))
        self.story.append(Spacer(0, 1 * cm))
        if not self.prestations:
            self.story.append(Paragraph("AUCUNE DONNÉE À FACTURER POUR CE MOIS", self.style_normal))
            self.doc.build(self.story, onFirstPage=self.draw_header_footer)
            return

        table_total = [["Intervenant·e", "Durée [min]", "Durée [cent.]", "Montant"]]
        total_cent = Decimal(0)
        for interv, total in self.prestations.items():
            if interv == 'total':
                continue
            duree_cent = format_duree(total, centiemes=True)
            total_cent += Decimal(duree_cent)
            table_total.append([
                interv.nom_prenom,
                format_duree(total),
                duree_cent,
                format_chf(Decimal(duree_cent) * tarif),
            ])
        montant_total = tarif * total_cent
        table_total.append([
            "Total",
            format_duree(self.prestations['total']),
            f'{total_cent:05.2f}',
            f'CHF {format_chf(montant_total)}',
        ])
        tbl = Table(table_total, colWidths=[5.7 * cm, 2 * cm, 2 * cm, 2.8 * cm])
        tbl.setStyle(TableStyle([
            ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
            ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
            ('BACKGROUND', (0, 0), (-1, 0), colors.lightgrey),
            ('FONTSIZE', (0, 0), (-1, 0), 8),
            ('ALIGN', (1, 0), (-1, -1), 'RIGHT'),
            ('FONTNAME', (0, -1), (-1, -1), 'Helvetica-Bold'),
        ]))
        self.story.append(tbl)
        self.story.append(Spacer(0, 0.7 * cm))
        self.story.append(
            Table([[
                f"Date et signature du/de la responsable {self.typ['nom']}",
                f"Date et signature du/de la responsable {self.typ['pour']}",
            ]], colWidths=[7.5 * cm, 7.5 * cm])
        )
        self.doc.build(self.story, onFirstPage=self.draw_header_footer)


def format_chf(value):
    return numberformat.format(value, '.', decimal_pos=2, grouping=3, thousand_sep=" ", force_grouping=True)
