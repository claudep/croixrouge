"""
Anonymiser les données de l'application SAP - SPE
"""
import sys
from datetime import date, timedelta

from django.core.management.base import BaseCommand
from django_otp.plugins.otp_static.models import StaticDevice
from django_otp.plugins.otp_totp.models import TOTPDevice

from croixrouge.models import Contact, Personne, Utilisateur
from spe.models import Bilan, FamilleSPE, PrestationSPE, RapportSPE, SuiviSPE

try:
    from faker import Faker
except ImportError:
    print("La commande anonymize exige la présence du paquet Python Faker (pip install faker)")
    sys.exit(1)


class Command(BaseCommand):
    def handle(self, *args, **options):
        fake = Faker('fr_CH')

        StaticDevice.objects.all().delete()
        TOTPDevice.objects.all().delete()

        # Contacts et Utilisateurs
        Contact.objects.filter(utilisateur__isnull=True, est_actif=False).delete()
        for contact in Contact.objects.all().select_related('utilisateur'):
            nom, prenom, service = fake.last_name(), fake.first_name(), contact.service
            while Contact.objects.filter(nom=nom, prenom=prenom, service=service).exists():
                nom, prenom, service = fake.last_name(), fake.first_name(), contact.service
            contact.prenom = prenom
            contact.nom = nom
            contact.rue = fake.street_address()[:30]
            contact.npa = fake.postcode()
            contact.localite = fake.city()
            if contact.tel_prive:
                contact.tel_prive = fake.phone_number()
            if contact.tel_prof:
                contact.tel_prof = fake.phone_number()
            if contact.email:
                contact.email = fake.email()
            try:
                util = contact.utilisateur
                util.first_name = contact.prenom
                util.last_name = contact.nom
                username = f'{util.last_name}{util.first_name[0]}'
                while Utilisateur.objects.filter(username=username).exists():
                    username += '0'
                util.username = username
                util.set_password(fake.password())
                util.save()
            except Utilisateur.DoesNotExist:
                pass
            contact.save()

        # Familles et Personnes
        FamilleSPE.objects.filter(archived_at__date__gt=date.today() - timedelta(days=360)).delete()
        for famille in FamilleSPE.objects.all():
            famille.nom = fake.last_name()
            famille.rue = fake.street_address()
            famille.npa = fake.postcode()
            famille.localite = fake.city()
            famille.telephone = fake.phone_number()
            if famille.remarques:
                famille.remarques = fake.text(max_nb_chars=100)
            famille.save()

        for personne in Personne.objects.all().select_related('famille'):
            personne.nom = personne.famille.nom
            personne.prenom = fake.first_name()
            if personne.date_naissance:
                personne.date_naissance = fake.date_between(
                    personne.date_naissance - timedelta(days=100),
                    personne.date_naissance + timedelta(days=100),
                )
            personne.rue = personne.famille.rue
            personne.npa = personne.famille.npa
            personne.localite = personne.famille.localite
            if personne.telephone:
                personne.telephone = fake.phone_number()
            if personne.email:
                personne.email = fake.email()
            if personne.remarque:
                personne.remarque = fake.text(max_nb_chars=100)
            if personne.remarque_privee:
                personne.remarque_privee = fake.text(max_nb_chars=100)
            personne.save()

        # Suivi
        for suivi in SuiviSPE.objects.all():
            for text_field in [
                'difficultes', 'aides', 'competences', 'disponibilites', 'collaboration',
                'ressource', 'crise', 'remarque', 'remarque_privee'
            ]:
                if getattr(suivi, text_field):
                    setattr(suivi, text_field, fake.text(max_nb_chars=200))
            for pres_field in ['pers_famille_presentes', 'ref_presents', 'autres_pers_presentes']:
                if getattr(suivi, pres_field):
                    mots = getattr(suivi, pres_field).split(" ")
                    mots_rempl = " ".join([
                        fake.first_name() if (len(mot) and mot[0].isupper()) else mot for mot in mots
                    ])
                    setattr(suivi, pres_field, mots_rempl[:100])
            suivi.save()

        # Rapports/Bilans
        for rapport in RapportSPE.objects.all():
            for text_field in [
                'situation', 'evolutions', 'evaluation', 'observ_ipe', 'observ_sof',
                'observ_apa', 'projet',
            ]:
                if getattr(rapport, text_field):
                    setattr(rapport, text_field, fake.text(max_nb_chars=200))
            rapport.save()
        for bilan in Bilan.objects.all():
            for text_field in ['objectifs', 'rythme']:
                if getattr(bilan, text_field):
                    setattr(bilan, text_field, fake.text(max_nb_chars=200))
            bilan.save()

        # Prestations
        for prest in PrestationSPE.objects.all():
            prest.texte = fake.text(max_nb_chars=200)
            prest.save(update_fields=['texte'])
