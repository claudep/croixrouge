from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('croixrouge', '0054_remove_personne_animaux_employeur_validite'),
    ]

    operations = [
        migrations.AddField(
            model_name='famille',
            name='interprete',
            field=models.BooleanField(default=False, verbose_name='interprète'),
        ),
        migrations.AddField(
            model_name='personne',
            name='niveau_fr',
            field=models.CharField(blank=True, choices=[('bon', 'Bon'), ('moyen', 'Moyen'), ('faible', 'Faible'), ('non', 'Ne parle pas le français')], max_length=10, verbose_name='Niveau de français'),
        ),
        migrations.AlterField(
            model_name='personne',
            name='amf_type_accueil',
            field=models.CharField(blank=True, choices=[('FAO', 'Famille d’accueil par opportunité'), ('Relais FA', 'Relais d’une famille d’accueil'), ('Intra', 'Intrafamilial'), ('Intra relais', 'Relais intrafamilial'), ('Relais IES', 'Relais d’une institution d’éducation spécialisée'), ('FA', 'Famille d’accueil'), ('SMI', 'Dossier transmis par le Service des Migrations'), ('Relais biologique', 'Relais d’une famille biologique')], max_length=20, verbose_name="Type d'accueil"),
        ),
    ]
