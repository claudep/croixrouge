from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('croixrouge', '0052_personne_amf_provenance_details_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='libelleprestation',
            name='unite',
            field=models.CharField(
                choices=[('amf', 'AMF'), ('cipe', 'IPE'), ('spe', 'SPE')],
                max_length=10, verbose_name='Unité'
            ),
        ),
    ]
