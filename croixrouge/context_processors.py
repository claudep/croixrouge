from urllib.parse import urlparse

from common.choices import UNITE_CROIXROUGE_CHOICES


def current_app(request):
    def find_source(path):
        src = path.strip('/').split('/')[0]
        if src in dict(UNITE_CROIXROUGE_CHOICES):
            return src
        return ''

    source = find_source(request.path)
    if source == '':
        source = find_source(urlparse(request.META.get('HTTP_REFERER', '')).path)
    if source == '' and request.session.get('current_app'):
        return {'source': request.session['current_app']}
    if source == '':
        source = request.GET.get('from', '')
    return {'source': source}
