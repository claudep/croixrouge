import calendar
from dataclasses import dataclass
from datetime import date

from django import forms
from django.utils.dates import MONTHS

from croixrouge.export import OpenXMLExport


class DateLimitForm(forms.Form):
    YEAR_CHOICES = tuple(
        (str(y), str(y))
        for y in range(2020, date.today().year + (1 if date.today().month < 12 else 2))
    )
    start_month = forms.ChoiceField(choices=[(str(m), MONTHS[m]) for m in range(1, 13)])
    start_year = forms.ChoiceField(choices=YEAR_CHOICES)
    end_month = forms.ChoiceField(choices=[(str(m), MONTHS[m]) for m in range(1, 13)])
    end_year = forms.ChoiceField(choices=YEAR_CHOICES)

    def __init__(self, data, **kwargs):
        if not data:
            today = date.today()
            data = {
                'start_year': today.year, 'start_month': today.month,
                'end_year': today.year if today.month < 12 else today.year + 1,
                'end_month': (today.month + 1) if today.month < 12 else 1,
            }
        super().__init__(data, **kwargs)

    def clean(self):
        cleaned_data = super().clean()
        if not self.errors and self.start > self.end:
            raise forms.ValidationError("Les dates ne sont pas dans l’ordre.")
        return cleaned_data

    @property
    def start(self):
        return date(int(self.cleaned_data['start_year']), int(self.cleaned_data['start_month']), 1)

    @property
    def end(self):
        return date(
            int(self.cleaned_data['end_year']),
            int(self.cleaned_data['end_month']),
            calendar.monthrange(int(self.cleaned_data['end_year']), int(self.cleaned_data['end_month']))[1]
        )


@dataclass
class Month:
    year: int
    month: int

    def __str__(self):
        return f'{self.month:0>2}.{self.year}'

    def __lt__(self, other):
        return (self.year, self.month) < (other.year, other.month)

    def __hash__(self):
        return hash((self.year, self.month))

    def next(self):
        if self.month == 12:
            return Month(self.year + 1, 1)
        else:
            return Month(self.year, self.month + 1)

    @classmethod
    def from_date(cls, dt):
        return Month(dt.year, dt.month)

    def is_future(self):
        return date(self.year, self.month, 1) > date.today()

    def get_first_of_the_month(self):
        return date(self.year, self.month, 1)

    def get_next(self):
        if self.month == 12:
            return self.__class__(self.year + 1, 1)
        return self.__class__(self.year, self.month + 1)


class ExportStatistique(OpenXMLExport):
    def __init__(self, *args, col_widths=None, **kwargs):
        self.col_widths = col_widths
        super().__init__(*args, **kwargs)

    def fill_data(self, generator):
        for row in generator:
            if row and row[0] == 'BOLD':
                self.write_line(row[1:], bold=True, col_widths=self.col_widths)
            else:
                self.write_line(row, col_widths=self.col_widths)


class StatsMixin:
    permission_required = 'croixrouge.export_stats'

    def get_months(self):
        """Return a list of tuples [(year, month), ...] from date_start to date_end."""
        months = [Month(self.date_start.year, self.date_start.month)]
        while True:
            next_m = months[-1].next()
            if next_m > Month(self.date_end.year, self.date_end.month):
                break
            months.append(next_m)
        return months

    def month_limits(self, month):
        """From (2020, 4), return (date(2020, 4, 1), date(2020, 5, 1))."""
        next_m = month.next()
        return (
            date(month.year, month.month, 1),
            date(next_m.year, next_m.month, 1)
        )

    @staticmethod
    def init_counters(names, months, default=0, total=int):
        """
        Create stat counters:
            {<counter_name>: {(2020, 3): 0, (2020, 4): 0, …, 'total': 0}, …}
        """
        counters = {}
        for count_name in names:
            counters[count_name] = {}
            for month in months:
                counters[count_name][month] = '-' if month.is_future() else (
                    default.copy() if hasattr(default, "copy") else default
                )
            counters[count_name]['total'] = total()
        return counters

    def get_stats(self, months):
        # Here subclasses produce stats to be merged in view context
        return {}

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        get_params = self.request.GET.copy()
        self.export_flag = get_params.pop('export', None)
        date_form = DateLimitForm(get_params)
        context['date_form'] = date_form
        if not date_form.is_valid():
            return context
        self.date_start = date_form.start
        self.date_end = date_form.end
        months = self.get_months()
        context.update({
            'date_form': date_form,
            'months': months,
        })
        context.update(self.get_stats(months))
        return context

    def export_as_openxml(self, context):
        export = ExportStatistique(col_widths=[50])
        export.fill_data(self.export_lines(context))
        return export.get_http_response(self.__class__.__name__.replace('View', '') + '.xlsx')

    def render_to_response(self, context, **response_kwargs):
        if self.export_flag:
            return self.export_as_openxml(context)
        else:
            return super().render_to_response(context, **response_kwargs)


def permute_niveaux_dict(dict1):
    """
    Pour un dictionnaire à deux niveaux, inverse les niveaux
    Exemple:
        {"janvier": {"garçons": 1, "filles": 2}}
        => {"garçons": {"janvier": 1}, {"filles": {"janvier": 2}}
    """
    dict2 = {}
    for key1, subdict in dict1.items():
        for key2, value in subdict.items():
            if key2 not in dict2:
                dict2[key2] = {}
            dict2[key2][key1] = value
    return dict2
