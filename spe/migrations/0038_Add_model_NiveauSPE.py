from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('spe', '0037_duosperdv'),
    ]

    operations = [
        migrations.CreateModel(
            name='NiveauSPE',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('niveau_interv', models.PositiveSmallIntegerField(choices=[(0, '0'), (1, '1'), (2, '2'), (3, '3')], verbose_name='Niveau d’intervention')),
                ('date_debut', models.DateField(blank=True, null=True, verbose_name='Date début')),
                ('date_fin', models.DateField(blank=True, null=True, verbose_name='Date fin')),
                ('famille', models.ForeignKey(on_delete=models.deletion.CASCADE, related_name='niveaux_spe', to='spe.famillespe', verbose_name='Famille')),
            ],
        ),
    ]
