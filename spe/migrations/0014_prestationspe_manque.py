from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('spe', '0013_Add_SPE_ressource_and_crise'),
    ]

    operations = [
        migrations.AddField(
            model_name='prestationspe',
            name='manque',
            field=models.BooleanField(default=False, verbose_name='Rendez-vous manqué'),
        ),
    ]
