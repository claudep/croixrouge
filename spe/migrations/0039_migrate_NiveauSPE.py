from django.db import migrations


def migrate_niveaux(apps, schema_editor):
    SuiviSPE = apps.get_model('spe', 'SuiviSPE')
    NiveauSPE = apps.get_model('spe', 'NiveauSPE')
    for suivi in SuiviSPE.objects.all().select_related('famille'):
        if suivi.niveau_interv:
            NiveauSPE.objects.create(
                famille=suivi.famille,
                niveau_interv=suivi.niveau_interv,
                date_debut=suivi.date_debut_suivi,
                date_fin=suivi.date_fin_suivi
            )


class Migration(migrations.Migration):

    dependencies = [
        ('spe', '0038_Add_model_NiveauSPE'),
    ]

    operations = [
        migrations.RunPython(migrate_niveaux),
    ]
