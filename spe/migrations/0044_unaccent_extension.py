from django.contrib.postgres.operations import UnaccentExtension
from django.db import migrations

# ref for this migration: https://stackoverflow.com/questions/47230566


class Migration(migrations.Migration):

    dependencies = [
        ('spe', '0043_rapportspe_pres_interv'),
    ]

    operations = [
        UnaccentExtension(),
        migrations.RunSQL("CREATE TEXT SEARCH CONFIGURATION french_unaccent(COPY = french);"),
        migrations.RunSQL(
            "ALTER TEXT SEARCH CONFIGURATION french_unaccent "
            "ALTER MAPPING FOR hword, hword_part, word "
            "WITH unaccent, french_stem;"
        ),
    ]
