from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('spe', '0003_bilan'),
    ]

    operations = [
        migrations.AddField(
            model_name='prestationspe',
            name='fichier',
            field=models.FileField(blank=True, upload_to='prestations', verbose_name='Fichier/image'),
        ),
    ]
