from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('spe', '0025_alter_famillespe_options'),
    ]

    operations = [
        migrations.AlterField(
            model_name='suivispe',
            name='ref_presents',
            field=models.CharField(blank=True, max_length=250, verbose_name='Intervenants présents'),
        ),
    ]
