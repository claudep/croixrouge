import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models

import common.fields
from common import choices


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('croixrouge', '__first__'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='FamilleSPE',
            fields=[
            ],
            options={
                'proxy': True,
                'indexes': [],
                'constraints': [],
            },
            bases=('croixrouge.famille',),
        ),
        migrations.CreateModel(
            name='SuiviSPE',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('equipe', models.CharField(choices=[('montagnes', 'SPE - Montagnes'), ('litt_est', 'SPE - Littoral Est'), ('litt_ouest', 'SPE - Littoral Ouest')], max_length=10, verbose_name='Équipe')),
                ('difficultes', models.TextField(blank=True, verbose_name='Difficultés')),
                ('aides', models.TextField(blank=True, verbose_name='Aides souhaitées')),
                ('autres_contacts', models.TextField(blank=True, verbose_name='Autres services contactés')),
                ('disponibilites', models.TextField(blank=True, verbose_name='Disponibilités')),
                ('remarque', models.TextField(blank=True)),
                ('remarque_privee', models.TextField(blank=True, verbose_name='Remarque privée')),
                ('service_orienteur', models.CharField(blank=True, choices=choices.SERVICE_ORIENTEUR_CHOICES, max_length=15, verbose_name="Orienté vers le SPE par")),
                ('detail_demande', models.CharField(blank=True, max_length=100, verbose_name='Détail de la demande')),
                ('motif_demande', common.fields.ChoiceArrayField(base_field=models.CharField(choices=choices.MOTIF_DEMANDE_CHOICES, max_length=60), blank=True, null=True, size=None, verbose_name='Motif de la demande')),
                ('mandat_ope', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=choices.MANDATS_OPE_CHOICES, max_length=65), blank=True, null=True, size=None, verbose_name='Mandat OPE')),
                ('referent_note', models.TextField(blank=True, verbose_name='Autres contacts')),
                ('date_demande_eventuelle', models.DateField(blank=True, default=None, null=True, verbose_name='Demande éventuelle le')),
                ('date_demande', models.DateField(blank=True, default=None, null=True, verbose_name='Demande déposée le')),
                ('date_analyse', models.DateField(blank=True, default=None, null=True, verbose_name='Analyse réalisée le')),
                ('date_confirmation', models.DateField(blank=True, default=None, null=True, verbose_name='Demande confirmée le')),
                ('date_debut_suivi', models.DateField(blank=True, default=None, null=True, verbose_name='Début du suivi le')),
                ('date_fin_suivi', models.DateField(blank=True, default=None, null=True, verbose_name='Fin du suivi le')),
                ('date_bilan_3mois', models.DateField(blank=True, default=None, null=True, verbose_name='Bilan 3 mois le')),
                ('date_bilan_annee', models.DateField(blank=True, default=None, null=True, verbose_name='Bilan 12 mois le')),
                ('date_bilan_final', models.DateField(blank=True, default=None, null=True, verbose_name='Bilan 18 mois/final le')),
                ('date_prolongation_1', models.DateField(blank=True, default=None, null=True, verbose_name='Début 1ère prolong.')),
                ('date_bilan_prolongation_1', models.DateField(blank=True, default=None, null=True, verbose_name='Bilan 1ère prolong.')),
                ('date_prolongation_2', models.DateField(blank=True, default=None, null=True, verbose_name='Début 2ème prolong.')),
                ('date_bilan_prolongation_2', models.DateField(blank=True, default=None, null=True, verbose_name='Bilan 2ème prolong.')),
                ('demande_prioritaire', models.BooleanField(default=False, verbose_name='Demande prioritaire')),
                ('demarche', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=[('volontaire', 'Volontaire'), ('contrainte', 'Contrainte'), ('post_placement', 'Post placement'), ('non_placement', 'Eviter placement')], max_length=60), blank=True, null=True, size=None, verbose_name='Démarche')),
                ('pers_famille_presentes', models.CharField(blank=True, max_length=200, verbose_name='Membres famille présents')),
                ('ref_presents', models.CharField(blank=True, max_length=40, verbose_name='Intervenants présents')),
                ('autres_pers_presentes', models.CharField(blank=True, max_length=100, verbose_name='Autres pers. présentes')),
                ('motif_fin_suivi', models.CharField(blank=True, choices=choices.MOTIFS_FIN_SUIVI_CHOICES, max_length=20, verbose_name='Motif de fin de suivi')),
                ('famille', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='spe.FamilleSPE')),
                ('ope_referent', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='croixrouge.Contact', verbose_name='as. OPE')),
                ('referent_1', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL, verbose_name='Educ. 1')),
                ('referent_2', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL, verbose_name='Educ. 2')),
                ('sse_referent', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='croixrouge.Contact', verbose_name='SSE')),
            ],
            options={
                'verbose_name': 'Suivi SPE',
                'verbose_name_plural': 'Suivis SPE',
            },
        ),
        migrations.CreateModel(
            name='PrestationSPE',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_prestation', models.DateField(verbose_name='date de l’intervention')),
                ('duree', models.DurationField(verbose_name='durée')),
                ('familles_actives', models.PositiveSmallIntegerField(blank=True, default=0)),
                ('texte', models.TextField(blank=True, verbose_name='Contenu')),
                ('auteur', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='auteur')),
                ('famille', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='prestations_spe', to='spe.FamilleSPE', verbose_name='Famille')),
                ('intervenants', models.ManyToManyField(related_name='prestations_spe', to=settings.AUTH_USER_MODEL)),
                ('lib_prestation', models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='prestations_spe', to='croixrouge.LibellePrestation')),
            ],
            options={
                'ordering': ('-date_prestation',),
                'abstract': False,
            },
        ),
    ]
