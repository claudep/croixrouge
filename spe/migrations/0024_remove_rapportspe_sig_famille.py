import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('croixrouge', '__first__'),
        ('spe', '0023_remove_suivispe_date_demande_eventuelle'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='rapportspe',
            name='sig_famille',
        ),
        migrations.AlterField(
            model_name='prestationspe',
            name='lib_prestation',
            field=models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='prestations_%(app_label)s', to='croixrouge.libelleprestation'),
        ),
        migrations.AlterField(
            model_name='rapportspe',
            name='sig_interv',
            field=models.ManyToManyField(blank=True, related_name='+', to=settings.AUTH_USER_MODEL, verbose_name='Signature des intervenants'),
        ),
    ]
