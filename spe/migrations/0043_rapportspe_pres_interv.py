from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('spe', '0042_alter_prestationspe_options'),
    ]

    operations = [
        migrations.AddField(
            model_name='rapportspe',
            name='pres_interv',
            field=models.ManyToManyField(blank=True, related_name='+', to=settings.AUTH_USER_MODEL, verbose_name='Intervenants cités dans le résumé'),
        ),
    ]
