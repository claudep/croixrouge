from django import template
from django.utils.html import format_html
from django.utils.safestring import SafeString

register = template.Library()


@register.filter
def stat_niveau(stats, month):
    if stats[month] == "-":
        return SafeString('<td class="num">-</td><td class="border-end"></td>')
    return format_html(
        '<td class="num{}">{}</td><td class="num border-end">{}</td>',
        ' total-vert' if month == "total" else '',
        stats[month]["familles"],
        stats[month]["enfants"]
    )
