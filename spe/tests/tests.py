from datetime import date, datetime, timedelta
from pathlib import Path
from unittest.mock import patch

from django.conf import settings
from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.contrib.messages import get_messages
from django.core.files import File
from django.core.files.uploadedfile import SimpleUploadedFile
from django.forms.models import model_to_dict
from django.test import RequestFactory, TestCase, tag
from django.urls import reverse

from croixrouge.export import ExportReporting, openxml_contenttype
from croixrouge.models import Contact, JournalAcces, LibellePrestation, Personne, Region, Role, Service, Utilisateur
from croixrouge.tests import InitialDataMixin
from croixrouge.utils import ANTICIPATION_POUR_DEBUT_SUIVI, format_d_m_Y, format_duree
from sof.models import FamilleSOF, PrestationSOF
from spe.forms import AgendaForm, FamilleCreateForm, NiveauSPEForm, PrestationSPEForm
from spe.models import Bilan, EtapeFin, FamilleSPE, IntervenantSPE, NiveauSPE, PrestationSPE, RapportSPE, SuiviSPE


class SPEDataMixin(InitialDataMixin):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        famille = FamilleSPE.objects.create_famille(
            nom='Haddock', rue='Château1', npa=2000, localite='Moulinsart',
            equipe='montagnes', autorite_parentale='conjointe', statut_marital='divorce', monoparentale=False
        )
        pere = Role.objects.get(nom='Père')
        resp = Role.objects.create(nom='Responsable/coordinateur')
        Personne.objects.create_personne(
            famille=famille, role=pere,
            nom='Haddock', prenom='Archibald', genre='M', date_naissance=date(1956, 2, 16),
            rue='Château1', npa=2000, localite='Moulinsart',
        )

        cls.user_spe = Utilisateur.objects.create_user(
            'user_spe', 'user_spe@example.org', nom='Spe', prenom='Prénom', sigle='SP', taux_activite=60,
        )
        cls.user_spe.roles.add(Role.objects.get(nom='Educ'))
        cls.user_spe2 = Utilisateur.objects.create_user(
            'user_spe2', 'user_spe2@example.org', nom='Spe2', prenom='Prénom', sigle='SP2'
        )
        cls.user_spe2.roles.add(Role.objects.get(nom='Psy'))
        cls.spe_group = Group.objects.get(name='spe')
        cls.user_spe.groups.add(cls.spe_group)
        cls.user_spe2.groups.add(cls.spe_group)

        grp_admin = Group.objects.create(name='admin')
        grp_admin.permissions.add(Permission.objects.get(codename='change_famillespe'))
        grp_admin.permissions.add(Permission.objects.get(codename='change_utilisateur'))
        grp_admin.permissions.add(Permission.objects.get(codename='export_stats'))
        cls.user_admin = Utilisateur.objects.create_user(
            'user_admin', 'user_admin@example.com', nom='Admin', prenom='Prénom', sigle='ADM'
        )
        cls.user_admin.groups.add(grp_admin)
        cls.user_admin.groups.add(cls.spe_group)
        cls.user_admin.roles.add(resp)

        cls.famillespe_content_type = ContentType.objects.get(app_label="spe", model="famillespe")


class FamilleSPETests(SPEDataMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.client.force_login(self.user_spe)

    famille_data = {
        'nom': 'Dupont',
        'rue': 'Rue du Moulins',
        'npa': '3000',
        'localite': 'Paris',
        'autorite_parentale': 'conjointe',
        'equipe': 'montagnes',
    }

    def test_famille_creation(self):
        response = self.client.get(reverse('spe-famille-add'))
        self.assertContains(response, '<option value="montagnes">Montagnes et V-d-T</option>', html=True)
        self.assertContains(
            response,
            '<button class="btn btn-sm btn-success" name="save" type="submit">Enregistrer</button>',
            html=True
        )
        self.assertContains(response, "id_motif_detail")
        response = self.client.post(reverse('spe-famille-add'), data={
            **self.famille_data, 'motif_detail': "Un test parmi d'autres"
        })
        famille = FamilleSPE.objects.get(nom='Dupont')
        self.assertRedirects(response, reverse('spe-famille-edit', args=[famille.pk]))
        famille = FamilleSPE.objects.get(nom='Dupont')
        self.assertEqual(famille.suivispe.equipe, 'montagnes')
        self.assertEqual(famille.suivispe.date_demande, date.today())
        self.assertEqual(famille.suivispe.motif_detail, "Un test parmi d'autres")

    def test_famille_edition(self):
        famille = FamilleSPE.objects.get(nom='Haddock')
        edit_url = reverse('spe-famille-edit', args=[famille.pk])
        response = self.client.get(edit_url)
        self.assertContains(response, '<select name="provenance" class="form-select" id="id_provenance">')
        self.assertContains(response, reverse('spe-print-coord-famille', args=[famille.pk]))
        self.assertContains(response, "Changer l’adresse")
        self.assertNotContains(response, "id_motif_detail")
        data = {**self.famille_data, 'localite': 'Monaco'}
        response = self.client.post(edit_url, data=data)
        self.assertRedirects(response, edit_url)
        famille.refresh_from_db()
        self.assertEqual(famille.localite, 'Monaco')

    def test_famille_edition_perm(self):
        famille = FamilleSPE.objects.get(nom='Haddock')
        user_1 = Utilisateur.objects.create(username='jean')
        user_1.groups.add(Group.objects.get(name='spe'))
        user_2 = Utilisateur.objects.create(username='ben')
        user_2.groups.add(Group.objects.get(name='spe'))
        manager = Utilisateur.objects.create(username='boss')
        manager.user_permissions.add(Permission.objects.get(codename='change_famillespe'))
        manager2 = Utilisateur.objects.create(username='boss2')
        manager2.roles.add(Role.objects.get(nom='Responsable/coordinateur'))
        # No referent set, any team user can edit
        self.assertTrue(famille.can_edit(user_1))
        interv = IntervenantSPE.objects.create(
            suivi=famille.suivispe, intervenant=user_2, role=Role.objects.get(nom='Educ'),
            date_debut=date.today() - timedelta(days=3)
        )
        # Only referent (or manager) can access
        self.assertFalse(famille.can_edit(user_1))
        self.assertTrue(famille.can_edit(user_2))
        self.assertTrue(famille.can_edit(manager))
        self.assertTrue(famille.can_edit(manager2))
        interv.date_fin = date.today() - timedelta(days=1)
        self.assertTrue(famille.can_edit(user_2))

    def test_famille_list(self):
        reg_montagnes = Region.objects.get_or_create(nom='Montagnes', defaults={'secteurs': ['spe']})[0]
        reg_litt_ouest = Region.objects.get_or_create(nom='Littoral Ouest', defaults={'secteurs': ['spe']})[0]
        fam1 = FamilleSPE.objects.get(nom='Haddock')
        fam2 = FamilleSPE.objects.create_famille(
            nom='Dupond', rue='Château 4', npa=2299, localite='Klow',
            region=reg_montagnes, equipe='montagnes',
        )
        fam2.suivispe.date_debut_evaluation = date.today() - timedelta(days=3)
        fam2.suivispe.save()
        fam2.suivispe.intervenants.add(self.user_spe, through_defaults={'role': Role.objects.get(nom='Educ')})
        NiveauSPE.objects.create(famille=fam2, niveau_interv=1, date_debut=date.today())
        # famille sans équipe
        FamilleSPE.objects.create_famille(
            nom='Sprutz', rue='Fleur 12', npa=2299, localite='Klow',
            region=reg_montagnes, equipe='montagnes'
        )
        FamilleSPE.objects.create_famille(
            nom='Tournesol', rue='Château1', npa=2000, localite='Moulinsart',
            region=reg_litt_ouest, equipe='littoral',
        )
        response = self.client.get(reverse('spe-famille-list'))
        self.assertEqual(len(response.context['object_list']), FamilleSPE.objects.count())
        self.assertContains(
            response,
            '<a href="%s" title="Suivi">Haddock</a>' % reverse('spe-famille-suivi', args=[fam1.pk]),
            html=True
        )
        # Apply filters
        response = self.client.get(
            reverse('spe-famille-list') + '?equipe=montagnes&niveau=1&nom=du'
        )
        self.assertEqual(response.context['form'].errors, {})
        self.assertQuerySetEqual(response.context['object_list'], [fam2])
        # All levels
        response = self.client.get(reverse('spe-famille-list') + '?niveau=')
        self.assertEqual(len(response.context['object_list']), 4)
        # Ressources
        hier = date.today() - timedelta(days=1)
        role_ase = Role.objects.get(nom='ASE')
        response = self.client.get(reverse('spe-famille-list') + f'?ressource={role_ase.pk}')
        self.assertNotContains(response, fam2.nom, html=False)

        IntervenantSPE.objects.create(suivi=fam2.suivispe, intervenant=self.user_spe2, role=role_ase, date_debut=hier)
        response = self.client.get(reverse('spe-famille-list') + f'?ressource={role_ase.pk}')
        self.assertContains(response, fam2.nom, html=False)

        # Liste d'attente
        response = self.client.get(reverse('spe-famille-attente'))
        self.assertEqual(len(response.context['object_list']), 4)
        self.assertContains(response, f'<td>{reg_montagnes.nom}</td>', html=True)
        self.assertContains(response, '<td class="red">À faire</td>', html=True)

        response = self.client.get(reverse('spe-famille-attente') + '?equipe=littoral')
        self.assertEqual(len(response.context['object_list']), 1)

        response = self.client.get(reverse('spe-famille-attente') + '?equipe=montagnes')
        self.assertEqual(len(response.context['object_list']), 3)

    def test_famille_list_filtre_intervenant(self):
        fam1 = FamilleSPE.objects.get(nom='Haddock')
        # Ajout intervention passée
        fam1.suivispe.intervenants.add(
            self.user_spe, through_defaults={
                'role': Role.objects.get(nom='Educ'), 'date_fin': date.today() - timedelta(days=3)
            }
        )
        fam2 = FamilleSPE.objects.create_famille(
            nom='Dupond', rue='Château 4', npa=2299, localite='Klow', equipe='montagnes',
        )
        fam2.suivispe.date_debut_evaluation = date.today() - timedelta(days=3)
        fam2.suivispe.save()
        fam2.suivispe.intervenants.add(
            self.user_spe, through_defaults={'role': Role.objects.get(nom='Educ')}
        )
        response = self.client.get(reverse('spe-famille-list') + f'?&interv={self.user_spe.pk}')
        self.assertEqual(response.context['form'].errors, {})
        self.assertQuerySetEqual(response.context['object_list'], [fam2])
        # Familles sans intervenant
        response = self.client.get(reverse('spe-famille-list') + '?interv=0')
        self.assertQuerySetEqual(response.context['object_list'], [fam1])
        self.assertEqual(response.context['object_list'][0].suivispe.intervenantspe_set.count(), 0)

    def test_famille_list_filtre_duo(self):
        fam2 = FamilleSPE.objects.create_famille(
            nom='Dupond', rue='Château 4', npa=2299, localite='Klow', equipe='montagnes',
        )
        fam2.suivispe.date_debut_evaluation = date.today() - timedelta(days=3)
        fam2.suivispe.save()
        fam2.suivispe.intervenants.add(self.user_spe, through_defaults={'role': Role.objects.get(nom='Educ')})
        psy = Utilisateur.objects.create_user(
            'user_spe3', 'user_spe3@example.org', nom='Spe3', prenom='Prénom', sigle='PS1'
        )
        fam2.suivispe.intervenants.add(psy, through_defaults={'role': Role.objects.get(nom='Psy')})
        response = self.client.get(reverse('spe-famille-list') + '?duos=PS1/SP')
        self.assertEqual(len(response.context['object_list']), 1)
        self.assertEqual(response.context['object_list'][0], fam2)
        self.assertContains(response, "Prochain RdV")
        self.client.force_login(self.user_admin)
        response = self.client.get(reverse('spe-famille-list') + '?duos=PS1/SP')
        self.assertContains(response, "Prochain RdV")
        self.client.force_login(self.user_spe2)
        response = self.client.get(reverse('spe-famille-list') + '?duos=PS1/SP')
        self.assertNotContains(response, "Prochain RdV")

    def test_duos_familles(self):
        fam1 = FamilleSPE.objects.get(nom='Haddock')
        fam1.suivispe.intervenants.add(self.user_spe, through_defaults={'role': Role.objects.get(nom='Educ')})
        fam1.suivispe.intervenants.add(self.user_spe2, through_defaults={'role': Role.objects.get(nom='Psy')})
        fam2 = FamilleSPE.objects.create_famille(
            nom='Dupond', rue='Château 4', npa=2299, localite='Klow',
        )
        fam2.suivispe.intervenants.add(self.user_spe, through_defaults={'role': Role.objects.get(nom='Educ')})
        fam2.suivispe.intervenants.add(
            self.user_spe2, through_defaults={'role': Role.objects.get(nom='Responsable/coordinateur')}
        )
        self.assertEqual(
            list(FamilleSPE.objects.all().with_duos().values_list('duo', flat=True)),
            ['SP/SP2', 'SP']
        )

    def test_affichage_dates_rdv_si_filtre_actif(self):
        famille = FamilleSPE.objects.get(nom='Haddock')
        famille.suivispe.intervenants.add(self.user_spe, through_defaults={'role': Role.objects.get(nom='Educ')})
        famille.suivispe.intervenants.add(self.user_spe2, through_defaults={'role': Role.objects.get(nom='Psy')})
        # sans duo sélectionné
        response = self.client.get(reverse('spe-famille-list'))
        self.assertNotContains(response, "Prochain RdV pour")
        # avec duo sélectionné
        self.user_spe.roles.add(Role.objects.get(nom='Responsable/coordinateur'))
        response = self.client.get(reverse('spe-famille-list') + "?duos=SP/SP2")
        self.assertContains(response, "Prochain RdV pour SP/SP2:")
        # édition date
        demain = date.today() + timedelta(days=1)
        response = self.client.post(
            reverse('duo-date-change', args=['SP/SP2']),
            data={'rendez_vous': demain}
        )
        self.assertRedirects(response, reverse('spe-famille-list'))
        response = self.client.get(reverse('spe-famille-list') + "?duos=SP/SP2")
        self.assertContains(
            response,
            f'<a href="/spe/duo/edit/SP/SP2/" class="js-edit">{demain.strftime("%d.%m.%Y")}</a>',
            html=True
        )

    def test_calcul_charge_liste_famille(self):
        fam1 = FamilleSPE.objects.get(nom='Haddock')
        fam1.suivispe.date_debut_suivi = date.today()
        fam1.suivispe.save()
        fam1.suivispe.intervenants.add(self.user_spe, through_defaults={'role': Role.objects.get(nom='Educ')})
        NiveauSPE.objects.create(famille=fam1, niveau_interv=2, date_debut=date.today())

        fam2 = FamilleSPE.objects.create_famille(
            nom='Dupond', rue='Château 4', npa=2299, localite='Klow',
        )
        fam2.suivispe.date_debut_evaluation = date.today() - timedelta(days=3)
        fam2.suivispe.heure_coord = True
        fam2.suivispe.save()
        fam2.suivispe.intervenants.add(self.user_spe, through_defaults={'role': Role.objects.get(nom='Educ')})
        NiveauSPE.objects.create(famille=fam2, niveau_interv=1, date_debut=date.today())

        fam3 = FamilleSPE.objects.create_famille(
            nom='Dupond2', rue='Château 4', npa=2299, localite='Klow',
        )
        fam3.suivispe.date_debut_evaluation = date.today() - timedelta(days=3)
        fam3.suivispe.save()
        fam3.suivispe.intervenants.add(self.user_spe, through_defaults={'role': Role.objects.get(nom='Educ')})
        NiveauSPE.objects.create(famille=fam3, niveau_interv=3, date_debut=date.today())

        # Ne doit pas compter car intervention terminée
        fam4 = FamilleSPE.objects.create_famille(
            nom='Dupond3', rue='Château 6', npa=2299, localite='Klow',
        )
        fam4.suivispe.date_debut_evaluation = date.today() - timedelta(days=3)
        fam4.suivispe.save()
        fam4.suivispe.intervenants.add(
            self.user_spe, through_defaults={
                'role': Role.objects.get(nom='Educ'), 'date_fin': date.today() - timedelta(days=1),
            }
        )
        NiveauSPE.objects.create(famille=fam4, niveau_interv=2, date_debut=date.today())

        response = self.client.get(reverse('spe-famille-list'))
        self.assertEqual(
            response.context['ma_charge'],
            {'heures': 9, 'nbre_eval': 2, 'nbre_suivi': 1, 'charge_diff': 7}
        )

        self.client.force_login(self.user_admin)
        response = self.client.get(reverse('charge-utilisateurs'))
        self.assertEqual(response.context['utilisateurs'], [self.user_spe, self.user_spe2])
        self.assertEqual(response.context['utilisateurs'][0].charge, 8)
        self.assertEqual(response.context['utilisateurs'][0].heures, 9)
        self.assertEqual(response.context['utilisateurs'][0].nbre_suivi, 1)
        self.assertEqual(response.context['utilisateurs'][0].nbre_eval, 2)
        self.assertEqual(response.context['utilisateurs'][1].charge, 0)
        self.assertEqual(response.context['utilisateurs'][1].heures, 0)
        self.assertEqual(response.context['utilisateurs'][1].nbre_suivi, 0)
        self.assertEqual(response.context['utilisateurs'][1].nbre_eval, 0)

    def test_delete_enfant_suivi(self):
        famille = FamilleSPE.objects.first()
        pers = Personne.objects.create_personne(
            famille=famille, prenom='Gaston', nom='Lagaffe',
            role=Role.objects.get(nom='Enfant suivi')
        )
        with self.assertLogs('django.request', level='WARNING'):
            response = self.client.post(reverse('personne-delete', args=[famille.pk, pers.pk]), follow=True)
        self.assertTrue(response.status_code, 403)

    def test_affichage_familles_libres_pour_tous_les_educs(self):
        famille = FamilleSPE.objects.create_famille(
            nom='Simpson', rue='Château1', npa=2000, localite='Moulinsart',
            equipe='montagnes'
        )
        self.assertEqual(famille.suivispe.intervenants.count(), 0)
        response = self.client.get(reverse('spe-famille-list'))
        self.assertContains(response, 'Simpson')

    def test_affichage_adresse(self):
        famille = FamilleSPE(nom='Simpson', rue='Château1', npa=2000, localite='Moulinsart')
        self.assertEqual(famille.adresse, 'Château1, 2000 Moulinsart')
        famille.rue = ''
        self.assertEqual(famille.adresse, '2000 Moulinsart')
        famille.npa = ''
        self.assertEqual(famille.adresse, 'Moulinsart')

    def test_acces_lecture_seule_famille_archivee(self):
        famille = FamilleSPE.objects.get(nom='Haddock')
        famille.suivi.date_fin_suivi = '2020-04-15'
        famille.suivi.save()
        response = self.client.get(reverse('spe-famille-suivi', args=[famille.pk]))
        self.assertFalse(response.context['can_edit'])
        response = self.client.get(reverse('spe-famille-edit', args=[famille.pk]))
        self.assertFalse(response.context['can_edit'])
        response = self.client.get(reverse('personne-edit', args=[famille.pk, famille.membres.first().pk]))
        self.assertTrue(response.context['form'].fields['nom'].disabled)
        # Changement adresse impossible
        response = self.client.get(reverse('famille-adresse-change', args=[famille.pk]))
        self.assertEqual(response.status_code, 403)

    def test_affichage_bouton_changement_adresse(self):
        famille = FamilleSPE.objects.get(nom='Haddock')
        response = self.client.get(reverse('spe-famille-edit', args=[famille.pk]))
        url = reverse('famille-adresse-change', args=[famille.pk])
        self.assertContains(
            response,
            f'<a class="btn btn-sm btn-outline-primary" href="{url}">Changer l’adresse</a>',
            html=True
        )

    def test_membres_famille_dans_form_changement_adresse(self):
        famille = FamilleSPE.objects.get(nom='Haddock')
        pers = Personne.objects.create_personne(
            famille=famille, prenom='Gaston', nom='Lagaffe', rue='Fleurs 44',
            role=Role.objects.get(nom='Enfant suivi')
        )
        edit_url = reverse('famille-adresse-change', args=[famille.pk])
        response = self.client.get(edit_url)
        self.assertContains(response, 'Lagaffe Gaston (Enfant suivi)')
        self.assertContains(response, 'Haddock Archibald (Père)')

        data = {'rue': 'Champs-Elysées 1', 'npa': '7500', 'localite': 'Paris', 'membres': [pers.pk]}
        response = self.client.post(edit_url, data=data)
        self.assertRedirects(response, reverse('spe-famille-edit', args=[famille.pk]))
        pers.refresh_from_db()
        self.assertEqual(pers.rue, "Champs-Elysées 1")
        for parent in famille.parents():
            self.assertEqual(parent.rue, 'Château1')
        # Test changement adresse sans changer pour les membres
        data = {'rue': 'Champs-Elysées 2', 'npa': '7500', 'localite': 'Paris', 'membres': []}
        response = self.client.post(edit_url, data=data)
        self.assertRedirects(response, reverse('spe-famille-edit', args=[famille.pk]))

    def test_nom_famille_formatage(self):
        form = FamilleCreateForm(data={'nom': 'dubois', 'equipe': 'montagnes'})
        self.assertTrue(form.is_valid())
        form.save()
        self.assertTrue(FamilleSPE.objects.filter(nom='Dubois').exists())

        form = FamilleCreateForm(data={'nom': 'de Gaule', 'equipe': 'montagnes'})
        self.assertTrue(form.is_valid())
        form.save()
        self.assertTrue(FamilleSPE.objects.filter(nom='de Gaule').exists())

    def test_nom_personne_formatage(self):
        famille = FamilleSPE.objects.get(nom='Haddock')
        pers = Personne.objects.get(nom='Haddock', prenom='Archibald')
        edit_url = reverse('personne-edit', args=[famille.pk, pers.pk])
        self.client.post(
            edit_url,
            data={'nom': 'doe', 'prenom': 'john', 'role': pers.role.pk, 'genre': 'M'},
            follow=True
        )
        doe = Personne.objects.get(nom='Doe', prenom='John')
        self.assertEqual(pers.pk, doe.pk)

    def test_interventions_actives(self):
        famille = FamilleSPE.objects.get(nom='Haddock')
        educ = Role.objects.get(nom='Educ')
        today = date.today()
        date_passe = today - timedelta(days=30)
        date_futur = today + timedelta(days=30)
        int1, int2, int3, int4 = IntervenantSPE.objects.bulk_create([
            IntervenantSPE(
                suivi=famille.suivi, intervenant=self.user_spe, role=educ,
                date_debut=debut, date_fin=fin
            ) for debut, fin in [
                (date_passe, date_passe + timedelta(days=5)),  # complètement passé
                (date_futur, None),  # futur
                (date_passe, date_futur),
                (date_passe, None),
            ]
        ])
        self.assertQuerySetEqual(famille.interventions_actives().order_by('pk'), [int3, int4])
        self.assertQuerySetEqual(
            famille.interventions_actives(aussi_futures=True).order_by('pk'),
            [int2, int3, int4]
        )
        self.assertQuerySetEqual(famille.interventions_actives(today - timedelta(days=31)), [])

    def test_ajout_intervenant(self):
        famille = FamilleSPE.objects.get(nom='Haddock')
        intervenant = self.user_spe
        role = Role.objects.get(nom='Educ')
        date_debut = date.today()
        response = self.client.post(
            reverse('spe-intervenant-add', args=[famille.pk]),
            data={'intervenant': intervenant.pk, 'role': role.pk, 'date_debut': date_debut}
        )
        self.assertRedirects(response, reverse('spe-famille-suivi', args=[famille.pk]))
        self.assertTrue(
            IntervenantSPE.objects.filter(
                suivi=famille.suivispe, intervenant=intervenant, role=role, date_debut=date_debut
            ).exists()
        )
        # Même intervenant, intervention toujours ouverte: refus
        response = self.client.post(
            reverse('spe-intervenant-add', args=[famille.pk]),
            data={'intervenant': intervenant.pk, 'role': role.pk, 'date_debut': date_debut}
        )
        self.assertEqual(
            response.context['form'].errors['__all__'],
            ['Cet intervenant est déjà actif sur ce suivi']
        )

    def test_modif_intervenant(self):
        famille = FamilleSPE.objects.get(nom='Haddock')
        intervenant = self.user_spe
        role = Role.objects.get(nom='Educ')
        date_debut = date.today()
        intervention = IntervenantSPE.objects.create(
            suivi=famille.suivi, intervenant=intervenant, role=role, date_debut=date_debut
        )
        suivi_url = reverse('spe-famille-suivi', args=[famille.pk])
        add_url = reverse("spe-intervenant-add", args=[famille.pk])
        edit_url = reverse("spe-intervenant-edit", args=[famille.pk, intervention.pk])
        response = self.client.get(suivi_url)
        # Test boutons ajouter/modifier
        self.assertContains(
            response,
            ('<a href="" class="btn btn-sm btn-mini btn-primary js-add" '
             f'data-url="{add_url}" role="button" title="Ajouter un intervenant">+</a>'),
            html=True
        )
        self.assertContains(
            response,
            ('<a href="" class="btn btn-sm btn-mini js-edit"'
             f'data-url="{edit_url}" role="button" title="Modifier une intervention">'
             '<img src="/static/admin/img/icon-changelink.svg"></a>'),
            html=True
        )
        response = self.client.post(
            reverse('spe-intervenant-edit', args=[famille.pk, intervention.pk]), data={
                'intervenant': intervenant.pk, 'role': role.pk,
                'date_debut': date_debut, 'date_fin': date.today()
            }
        )
        self.assertRedirects(response, reverse('spe-famille-suivi', args=[famille.pk]))
        intervention.refresh_from_db()
        self.assertEqual(intervention.date_fin, date.today())


class SuiviTests(SPEDataMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.client.force_login(self.user_spe)

    def test_suivi_dossier(self):
        famille = FamilleSPE.objects.get(nom='Haddock')
        suivi_url = reverse('spe-famille-suivi', args=[famille.pk])
        response = self.client.get(suivi_url)
        self.assertContains(
            response,
            '<button class="btn btn-sm btn-success" name="save" type="submit">Enregistrer</button>',
            html=True
        )
        response = self.client.post(suivi_url, data={
            'equipe': 'montagnes',
            'ope_referent': Contact.objects.get(nom='Rathfeld').pk,
            'date_demande': '2018-10-24',
            'etape_suivi': 'demande',
        })
        self.assertRedirects(response, suivi_url)
        suivi = famille.suivispe
        self.assertEqual(suivi.equipe, 'montagnes')

    def test_suivi_lecture_seule(self):
        user_other = Utilisateur.objects.create_user(
            'user_other', 'user_other@example.org', nom='Spe2', prenom='Prénom', sigle='SP2'
        )
        user_other.groups.add(Group.objects.get(name='spe'))
        famille = FamilleSPE.objects.get(nom='Haddock')
        famille.suivi.intervenants.add(
            self.user_spe, through_defaults={'role': Role.objects.get(nom='Educ')}
        )
        self.client.force_login(user_other)
        suivi_url = reverse('spe-famille-suivi', args=[famille.pk])
        response = self.client.get(suivi_url + '?confirm=1')
        self.assertFalse(response.context['can_edit'])
        self.assertNotContains(response, 'Enregistrer')

    # Temporairement désactivé en attendant la finalisation de l'archivage.
    def xxtest_effacement_famille_impossible_en_janvier(self):
        self.user_spe.user_permissions.add(Permission.objects.get(codename='delete_famillespe'))
        # Recharger pour réinitialiser cache des permissions
        user_spe = Utilisateur.objects.get(pk=self.user_spe.pk)
        self.client.force_login(user_spe)
        famille = FamilleSPE.objects.get(nom='Haddock')
        famille.date_fin_suivi = date(2017, 2, 1)
        famille.motif_fin_suivi = 'autres'
        famille.date_bilan_final = date(2015, 1, 1)
        famille.save()

        response = self.client.get(reverse('spe-suivis-termines'))
        self.assertContains(response, '<th>Suppr.</th>', html=True)

        with patch('spe.models.date') as mock_date:
            mock_date.today.return_value = date(2019, 1, 15)
            response = self.client.get(reverse('spe-suivis-termines'))
            self.assertNotIn('<th>Suppr.</th>', response)

    def test_permission_effacement_definitif_saisie_par_erreur(self):

        famille = FamilleSPE.objects.create_famille(
            nom='Simpson', rue='Château1', npa=2000, localite='Moulinsart',
            equipe='montagnes'
        )
        famille.suivispe.date_fin_suivi = date.today() - timedelta(days=500)
        famille.suivispe.save()
        self.assertFalse(famille.can_be_deleted(self.user_spe))

        self.user_spe.user_permissions.add(Permission.objects.get(codename='change_famillespe'))
        self.user_spe.user_permissions.add(
            Permission.objects.get(
                codename='can_archive',
                content_type=self.famillespe_content_type,
            )
        )
        # Recharger pour réinitialiser cache des permissions
        user_mont = Utilisateur.objects.get(pk=self.user_spe.pk)
        self.client.force_login(user_mont)
        self.assertTrue(famille.can_be_archived(user_mont))

        famille.suivispe.date_fin_suivi = None
        famille.suivispe.save()
        response = self.client.post(
            reverse('spe-famille-agenda', args=[famille.pk]),
            data={'date_demande': date.today(),
                  'date_debut_evaluation': date.today(),
                  'date_fin_evaluation': date.today(),
                  'motif_fin_suivi': 'erreur'}
        )
        # Lorsque le dossier est fermé, la redirection se fait sur la liste des familles
        self.assertRedirects(response, reverse('spe-famille-list'))

        famille.refresh_from_db()
        self.assertEqual(famille.suivispe.date_fin_suivi, date.today())
        self.assertEqual(famille.suivispe.motif_fin_suivi, 'erreur')
        self.assertTrue(famille.can_be_deleted(user_mont))

    def test_date_suivante_bilan(self):
        famille = FamilleSPE.objects.create_famille(
            nom='Simpson', rue='Château1', npa=2000, localite='Moulinsart',
            equipe='montagnes'
        )
        famille.suivispe.date_debut_suivi = date.today() - timedelta(days=90)
        famille.suivispe.save()
        self.assertEqual(famille.suivispe.etape.code, 'debut_suivi')
        self.assertEqual(famille.suivispe.date_suivante(), date.today())
        Bilan.objects.create(
            date=date.today() - timedelta(days=5),
            auteur=self.user,
            famille=famille,
        )
        self.assertEqual(famille.suivispe.date_suivante(), date.today() + timedelta(days=90 - 5))

    def test_date_fin_suivi_prevue(self):
        famille = FamilleSPE.objects.create_famille(
            nom='Simpson', rue='Château1', npa=2000, localite='Moulinsart',
            equipe='montagnes'
        )
        famille.suivispe.date_debut_suivi = date.today() - timedelta(days=90)
        famille.suivispe.save()
        NiveauSPE.objects.create(
            famille=famille, niveau_interv=2, date_debut=famille.suivispe.date_debut_suivi
        )
        self.assertEqual(
            SuiviSPE.WORKFLOW['fin_suivi'].delai_depuis(famille.suivispe),
            famille.suivispe.date_debut_suivi + timedelta(days=EtapeFin.delai_standard)
        )
        NiveauSPE.objects.create(
            famille=famille, niveau_interv=3, date_debut=date.today() - timedelta(days=5)
        )
        famille = FamilleSPE.objects.get(pk=famille.pk)
        self.assertEqual(famille.niveau_actuel(), 3)
        self.assertEqual(
            SuiviSPE.WORKFLOW['fin_suivi'].delai_depuis(famille.suivispe),
            date.today() - timedelta(days=5) + timedelta(days=EtapeFin.delai_niveau3)
        )

    def test_bilan_form(self):
        famille = FamilleSPE.objects.create_famille(
            nom='Simpson', rue='Château1', npa=2000, localite='Moulinsart',
            equipe='montagnes'
        )
        famille.suivispe.intervenants.add(
            self.user_spe, through_defaults={'role': Role.objects.get(nom='Educ')}
        )
        famille.suivispe.intervenants.add(
            self.user_spe2, through_defaults={
                'role': Role.objects.get(nom='Educ'), 'date_fin': date.today() - timedelta(days=3)
            }
        )

        self.client.force_login(self.user_spe)
        response = self.client.get(reverse('famille-bilanspe-add', args=[famille.pk]))
        # Seule l'intervention active est visible
        self.assertQuerySetEqual(response.context['form']['sig_interv'].field.queryset, [self.user_spe])

        response = self.client.post(reverse('famille-bilanspe-add', args=[famille.pk]), data={
            'date': '2020-08-30',
            'objectifs': '<p class="msoNormal"><span>Objectifs</span></p>',
            'rythme': '<p class="msoNormal"><span>Rythme</span></p>',
        })
        self.assertRedirects(response, reverse('spe-famille-agenda', args=[famille.pk]))
        bilan = famille.bilans_spe.first()
        self.assertEqual(bilan.objectifs, '<p>Objectifs</p>')
        self.assertEqual(bilan.rythme, '<p>Rythme</p>')


class AgendaTests(SPEDataMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.client.force_login(self.user_spe)
        self.request = RequestFactory().get('/')
        self.request.user = self.user_spe

    def test_affichage_agenda(self):
        famille = FamilleSPE.objects.get(nom='Haddock')
        self.client.force_login(self.user_spe)
        suivi_url = reverse('spe-famille-agenda', args=[famille.pk])
        response = self.client.get(suivi_url)
        self.assertContains(
            response,
            '<input type="text" name="date_demande" value="{}" '
            'class="vDateField vDateField-rounded" size="10" id="id_date_demande">'.format(format_d_m_Y(date.today())),
            html=True
        )

    def test_dates_obligatoires(self):
        today = date.today()
        form_data = {
            field_name: today for field_name in AgendaForm._meta.fields
            if field_name not in ['date_fin_suivi', 'motif_fin_suivi', 'destination']
        }
        for field, etape in SuiviSPE.WORKFLOW.items():
            data2 = dict(form_data)
            if etape.oblig is True:
                etape_preced_oblig = SuiviSPE.WORKFLOW[etape.preced_oblig]
                data2[etape_preced_oblig.date_nom()] = ''
                if etape.code == 'fin_suivi':
                    data2['date_fin_suivi'] = today
                    data2['motif_fin_suivi'] = 'placement'
                    data2['destination'] = 'fah'
                form = AgendaForm(data2, request=self.request)
                self.assertFalse(form.is_valid())
                self.assertEqual(form.errors, {'__all__': ['La date «{}» est obligatoire'.format(etape_preced_oblig)]})
            else:
                form = AgendaForm(data2, request=self.request)
                self.assertTrue(form.is_valid())

    def test_fin_suivi(self):
        famille = FamilleSPE.objects.create_famille(
            nom='Loiseau', rue='Château1', npa=2000, localite='Moulinsart', equipe='montagnes',
        )
        today = date.today()
        suivi = famille.suivispe
        form_data = {}
        suivi.date_demande = form_data['date_demande'] = today - timedelta(days=360)
        suivi.date_debut_evaluation = form_data['date_debut_evaluation'] = today - timedelta(days=290)
        suivi.date_fin_evaluation = form_data['date_fin_evaluation'] = today - timedelta(days=250)
        suivi.date_debut_suivi = form_data['date_debut_suivi'] = today - timedelta(days=120)
        suivi.save()
        form = AgendaForm(
            data={**form_data, 'date_fin_suivi': today - timedelta(days=5),
                  'motif_fin_suivi': 'evol_positive', 'destination': 'fah'},
            instance=suivi,
            request=self.request,
        )
        self.assertTrue(form.is_valid(), msg=form.errors)
        suivi = form.save()
        self.assertEqual(suivi.date_fin_suivi, today - timedelta(days=5))

    def test_motif_fin_suivi_sans_date(self):
        data = {'date_demande': date.today(),
                'date_debut_evaluation': date.today(),
                'date_fin_evaluation': date.today(),
                'date_debut_suivi': date.today(),
                'motif_fin_suivi': 'evol_positive'}
        form = AgendaForm(data, request=self.request)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {'__all__': ["Les champs «Fin de l'accompagnement», «Motif de fin» et «Destination» "
                         "sont obligatoires pour fermer le dossier."]}
        )
        # Test avec date_fin_suivi non valide
        data['date_fin_suivi'] = '2019-01-32'
        form = AgendaForm(data, request=self.request)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {'date_fin_suivi': ['Saisissez une date valide.'],
             '__all__': ["Les champs «Fin de l'accompagnement», «Motif de fin» et «Destination» "
                         "sont obligatoires pour fermer le dossier."]
             }
        )

    def test_dates_non_chronologiques(self):
        data = {
            field_name: '2019-01-15' for field_name in AgendaForm._meta.fields
            if field_name not in ['date_fin_suivi', 'motif_fin_suivi', 'destination']
        }
        date_field_preced = None
        for field_name in AgendaForm._meta.fields:
            if field_name in ['date_demande', 'motif_fin_suivi', 'destination']:
                date_field_preced = field_name
                continue
            elif field_name == 'date_fin_suivi':
                data = dict(
                    data, date_fin_suivi='2019-01-15', motif_fin_suivi='placement', destination='famille'
                )
            form = AgendaForm(dict(data, **{date_field_preced: '2019-01-16'}), request=self.request)
            self.assertFalse(form.is_valid())
            self.assertEqual(
                form.errors,
                {'__all__':
                    ["La date «{}» ne respecte pas l’ordre chronologique!".format(form.fields[field_name].label)]}
            )
            date_field_preced = field_name
        # Test avec trou dans les valeurs de dates
        data = {
            'date_demande': '2019-01-15',
            'date_fin_evaluation': '2019-01-01',  # Ordre chronologique non respecté
            'date_debut_suivi': '2019-02-01'
        }
        form = AgendaForm(data, request=self.request)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {'__all__': ["La date «Fin de l’évaluation le» ne respecte pas l’ordre chronologique!"]}
        )

    def test_saisie_date_invalide(self):
        form = AgendaForm({
            'date_demande': '20.8',
        }, request=self.request)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {'date_demande': ['Saisissez une date valide.']})

    def test_saisie_par_erreur_OK(self):
        data = {'motif_fin_suivi': 'erreur'}
        form = AgendaForm(data, request=self.request)
        self.assertTrue(form.is_valid())
        self.assertEqual(form.cleaned_data['date_fin_suivi'], date.today())

    def test_demande_non_aboutie(self):
        data = {'motif_fin_suivi': 'non_aboutie'}
        form = AgendaForm(data, request=self.request)
        self.assertTrue(form.is_valid())
        self.assertEqual(form.cleaned_data['date_fin_suivi'], date.today())

    def test_abandon_durant_evaluation_1(self):
        data = {'date_demande': date.today(), 'motif_fin_suivi': 'placement',
                'destination': 'famille'}
        form = AgendaForm(data, request=self.request)
        self.assertTrue(form.is_valid(), form.errors)

    def test_abandon_durant_evaluation_2(self):
        data = {'date_demande': date.today(), 'motif_fin_suivi': 'placement', 'destination': 'famille'}
        form = AgendaForm(data, request=self.request)
        self.assertTrue(form.is_valid())

    def test_dates_tardives(self):
        date_passee = date.today() - timedelta(days=31 + PrestationSPE.DELAI_SAISIE_SUPPL)
        form_data = {}
        suivi = SuiviSPE()
        for date_field in AgendaForm._meta.fields:
            if not date_field.startswith('date'):
                continue
            if date_field == 'date_fin_suivi':
                form_data['motif_fin_suivi'] = 'placement'
                form_data['destination'] = 'famille'
            form_data[date_field] = date_passee
            form = AgendaForm(data=form_data, instance=suivi, request=self.request)
            self.assertFalse(form.is_valid())
            if date_field == 'date_fin_suivi':
                self.assertEqual(
                    form.errors,
                    {date_field: ["La saisie de dates pour le mois précédent n’est pas permise !"],
                     '__all__': ["Les champs «Fin de l'accompagnement», «Motif de fin» et «Destination» "
                                 "sont obligatoires pour fermer le dossier."]}
                )
            else:
                self.assertEqual(
                    form.errors,
                    {date_field: ["La saisie de dates pour le mois précédent n’est pas permise !"]}
                )
            setattr(suivi, date_field, date.today() - timedelta(days=90))
            form_data[date_field] = date.today() - timedelta(days=90)

    def test_date_demande_anticipee(self):
        date_demande = date.today() + timedelta(days=1)
        data = {'date_demande': date_demande}
        form = AgendaForm(data, request=self.request)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {'date_demande': ["La saisie anticipée est impossible !"]}
        )

    def test_sans_destination_erreur(self):
        data = {'date_demande': date.today(),
                'date_debut_evaluation': date.today(),
                'date_fin_evaluation': date.today(),
                'date_debut_suivi': date.today(),
                'date_fin_suivi': date.today(),
                'motif_fin_suivi': 'evol_positive'}
        form = AgendaForm(data, request=self.request)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {'__all__': ["Les champs «Fin de l'accompagnement», «Motif de fin» et «Destination» "
                         "sont obligatoires pour fermer le dossier."]}
        )

        data['destination'] = ''
        form = AgendaForm(data, request=self.request)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {'__all__': ["Les champs «Fin de l'accompagnement», «Motif de fin» et «Destination» "
                         "sont obligatoires pour fermer le dossier."]}
        )

    def test_date_debut_suivi_anticipe_ok(self):
        data = {'date_demande': date.today(),
                'date_debut_evaluation': date.today() + timedelta(days=ANTICIPATION_POUR_DEBUT_SUIVI),
                'date_fin_evaluation': date.today() + timedelta(days=ANTICIPATION_POUR_DEBUT_SUIVI),
                'date_debut_suivi': date.today() + timedelta(days=ANTICIPATION_POUR_DEBUT_SUIVI)}
        form = AgendaForm(data, request=self.request)
        self.assertTrue(form.is_valid())

    def test_date_debut_suivi_anticipe_error(self):
        data = {'date_demande': date.today(),
                'date_debut_evaluation': date.today() + timedelta(days=ANTICIPATION_POUR_DEBUT_SUIVI + 1),
                'date_fin_evaluation': date.today() + timedelta(days=ANTICIPATION_POUR_DEBUT_SUIVI + 1),
                'date_debut_suivi': date.today() + timedelta(days=ANTICIPATION_POUR_DEBUT_SUIVI + 1)}
        form = AgendaForm(data, request=self.request)
        self.assertFalse(form.is_valid())

    def test_date_anticipee_pour_groupe_admin(self):
        famille = FamilleSPE.objects.get(nom='Haddock')
        self.client.force_login(self.user_admin)
        response = self.client.post(
            reverse('spe-famille-agenda', args=[famille.pk]),
            data={'date_demande': '2022-09-01'},
            follow=True
        )
        self.assertContains(
            response,
            '<li class="alert alert-warning">Les dates saisies peuvent affecter les statistiques '
            'déjà communiquées !</li>',
            html=True
        )
        self.assertContains(
            response,
            '<li class="alert alert-success">Les modifications ont été enregistrées avec succès</li>',
            html=True
        )

    def test_affichage_intervention_temporaire(self):
        famille = FamilleSPE.objects.get(nom='Haddock')
        intervenant = self.user_spe
        role = Role.objects.get(nom='Educ')
        date_debut = date.today()
        intervention = IntervenantSPE.objects.create(
            suivi=famille.suivi, intervenant=intervenant, role=role, date_debut=date_debut, date_fin=date.today()
        )
        self.client.force_login(self.user_spe)
        suivi_url = reverse('spe-famille-agenda', args=[famille.pk])
        response = self.client.get(suivi_url)
        self.assertContains(
            response,
            (
                f"<div>Spe Prénom (Educ) ({format_d_m_Y(intervention.date_debut)} - "
                f"{format_d_m_Y(intervention.date_fin)})</div>"
            ),
            html=True
        )


class JournalisationTests(SPEDataMixin, TestCase):
    def test_acces_famille_journalise(self):
        famille = FamilleSPE.objects.get(nom='Haddock')
        IntervenantSPE.objects.create(
            suivi=famille.suivi, intervenant=self.user_spe,
            role=Role.objects.get(nom='Educ'),
            date_debut=date.today(),
        )
        IntervenantSPE.objects.create(
            suivi=famille.suivi, intervenant=self.user_spe2,
            role=Role.objects.get(nom='Educ'),
            date_debut=date.today() - timedelta(days=3),
            date_fin=date.today() - timedelta(days=1),
        )
        self.client.force_login(self.user_spe)
        suivi_url = reverse('spe-famille-suivi', args=[famille.pk])
        response = self.client.get(suivi_url)
        self.assertTemplateUsed(response, 'spe/suivi_edit.html')

        self.client.force_login(self.user_spe2)
        response = self.client.get(suivi_url)
        self.assertTemplateUsed(response, 'croixrouge/acces_famille.html')
        response = self.client.get(suivi_url + '?confirm=1')
        self.assertTemplateUsed(response, 'spe/suivi_edit.html')
        line = JournalAcces.objects.get(utilisateur=self.user_spe)
        self.assertTrue(line.ordinaire)
        line = JournalAcces.objects.get(utilisateur=self.user_spe2)
        self.assertFalse(line.ordinaire)


@tag('pdf')
class PdfTests(SPEDataMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.client.force_login(self.user_spe)

    def _test_print_pdf(self, url, filename):
        self.client.force_login(self.user_spe)
        response = self.client.get(url)
        self.assertEqual(
            response['content-disposition'],
            'attachment; filename="{}"'.format(filename)
        )
        self.assertEqual(response['content-type'], 'application/pdf')
        self.assertGreater(len(response.getvalue()), 200)

    def test_print_evaluation(self):
        fam = FamilleSPE.objects.get(nom='Haddock')
        self._test_print_pdf(
            reverse('spe-print-evaluation', args=(fam.pk,)),
            'haddock_spe_evaluation.pdf'
        )
        # Avec genogramme en PDF
        with (Path('.').parent / 'croixrouge' / 'test.pdf').open(mode='rb') as fh:
            fam.genogramme = File(fh)
            fam.save()
        self._test_print_pdf(
            reverse('spe-print-evaluation', args=(fam.pk,)),
            'haddock_spe_evaluation.pdf'
        )

    def test_print_bilan(self):
        fam = FamilleSPE.objects.get(nom='Haddock')
        bilan = Bilan.objects.create(
            date=date(2020, 11, 3),
            auteur=self.user,
            famille=fam,
            objectifs="<p>Para 1<br><br>Para 2</p>",
            rythme="<p>Para 1<br><br>Para 2</p>",
        )
        self._test_print_pdf(
            reverse('spe-print-bilan', args=(bilan.pk,)),
            'haddock_bilan_20201103.pdf'
        )

    def test_print_coord_famille_un_enfant_un_parent(self):
        fam = FamilleSPE.objects.get(nom='Haddock')
        Personne.objects.create_personne(
            famille=fam, prenom='Gaston', nom='Lagaffe',
            role=Role.objects.get(nom='Enfant suivi')
        )
        self._test_print_pdf(
            reverse('spe-print-coord-famille', args=(fam.pk,)),
            'haddock_coordonnees.pdf'
        )

    def test_print_coord_un_enfant_deux_parents(self):
        fam = FamilleSPE.objects.get(nom='Haddock')
        Personne.objects.create_personne(
            famille=fam, prenom='Gaston', nom='Lagaffe',
            role=Role.objects.get(nom='Enfant suivi')
        )
        Personne.objects.create_personne(
            famille=fam, prenom='Maude', nom='Zarella',
            role=Role.objects.get(nom='Mère')
        )
        self._test_print_pdf(
            reverse('spe-print-coord-famille', args=(fam.pk,)),
            'haddock_coordonnees.pdf'
        )

    def test_print_coord_pere_mere_beaupere(self):
        fam = FamilleSPE.objects.get(nom='Haddock')
        Personne.objects.create_personne(
            famille=fam, prenom='Gaston', nom='Lagaffe',
            role=Role.objects.get(nom='Enfant suivi')
        )
        Personne.objects.create_personne(
            famille=fam, prenom='Maude', nom='Zarella',
            role=Role.objects.get(nom='Mère')
        )
        Personne.objects.create_personne(
            famille=fam, prenom='Aloïs', nom='Silalune',
            role=Role.objects.get(nom='Beau-père')
        )
        self._test_print_pdf(
            reverse('spe-print-coord-famille', args=(fam.pk,)),
            'haddock_coordonnees.pdf'
        )

    def test_print_sans_famille(self):
        famille = FamilleSPE.objects.create_famille(
            nom='Haddock', rue='Château1', npa=2000, localite='Moulinsart',
            equipe='montagnes', autorite_parentale='conjointe', statut_marital='divorce',
            monoparentale=False
        )
        Personne.objects.create_personne(
            famille=famille, prenom='Gaston', nom='Lagaffe',
            role=Role.objects.get(nom='Enfant suivi')
        )
        self._test_print_pdf(
            reverse('spe-print-coord-famille', args=(famille.pk,)),
            'haddock_coordonnees.pdf'
        )


class PrestationTests(SPEDataMixin, TestCase):

    def setUp(self):
        super().setUp()
        self.famille = FamilleSPE.objects.get(nom='Haddock')
        Personne.objects.create_personne(
            famille=self.famille, role=Role.objects.get(nom='Enfant suivi'),
            nom='Haddock', prenom='Paulet', genre='M', date_naissance=date(1956, 2, 16),
            rue='Château1', npa=2000, localite='Moulinsart',
        )
        self.famille.suivispe.intervenants.add(
            self.user_spe, through_defaults={'role': Role.objects.get(nom='Educ')}
        )
        self.prest_fam = LibellePrestation.objects.get(code='spe01')  # Évaluation
        self.prest_gen = LibellePrestation.objects.get(code='spe03')

    def create_prestation_fam(self, texte=""):
        prest = PrestationSPE.objects.create(
            auteur=self.user_spe, date_prestation=date.today(), duree=timedelta(hours=8),
            famille=self.famille, lib_prestation=self.prest_fam, texte=texte
        )
        prest.intervenants.add(self.user_spe, self.user_spe2)
        return prest

    def create_prestation_gen(self):
        data = dict(
            auteur=self.user_spe, date_prestation=date.today(), duree=timedelta(hours=8),
            famille=None, lib_prestation=self.prest_gen
        )
        prest = PrestationSPE.objects.create(**data)
        prest.intervenants.add(self.user_spe)
        return prest

    def test_prestation_texte_clean(self):
        """Leading and trailing empty paragraphs are stripped."""
        texte = (
            '<p></p>\n'
            '<p>Echanges mail fin de suivi, au revoir à la famille<br></p>\n'
            '<p> </p>'
        )
        data = dict(
            duree='3:40', date_prestation=date.today(), lib_prestation=self.prest_fam.pk,
            intervenants=[self.user_spe.pk], texte=texte
        )
        form = PrestationSPEForm(famille=self.famille, user=self.user_spe, data=data)
        self.assertTrue(form.is_valid())
        self.assertEqual(
            form.cleaned_data['texte'],
            '<p>Echanges mail fin de suivi, au revoir à la famille<br></p>'
        )

    def test_prestation_date_prestation_dans_mois_courant(self):
        data = dict(
            duree='3:40', date_prestation=date.today(), lib_prestation=self.prest_fam.pk,
            intervenants=[self.user_spe.pk]
        )
        form = PrestationSPEForm(famille=self.famille, user=self.user_spe, data=data)
        self.assertTrue(form.is_valid(), msg=form.errors)

    def test_prestation_saisie_anticipee(self):
        data = dict(
            duree='3:40', date_prestation=date.today() + timedelta(days=1),
            lib_prestation=self.prest_fam.pk, intervenants=[self.user_spe.pk]
        )
        form = PrestationSPEForm(famille=self.famille, user=self.user_spe, data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {'date_prestation': ['La saisie anticipée est impossible !']})

    def test_prestation_saisie_tardive(self):
        data = dict(
            duree='3:40', date_prestation=date.today() - timedelta(days=31 + PrestationSPE.DELAI_SAISIE_SUPPL),
            lib_prestation=self.prest_fam.pk, intervenants=[self.user_spe.pk]
        )
        form = PrestationSPEForm(famille=self.famille, user=self.user_spe, data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {'date_prestation': ['La saisie des prestations des mois précédents est close !']}
        )

    def test_prestation_edit_perm_speciale(self):
        prest = self.create_prestation_fam()
        prest.date_prestation = date.today() - timedelta(days=31 + PrestationSPE.DELAI_SAISIE_SUPPL)
        prest.save()
        self.user_admin.user_permissions.add(
            Permission.objects.get(codename='edit_prest_prev_month', content_type__app_label='spe')
        )
        self.client.force_login(self.user_admin)
        self.client.post(reverse('spe-prestation-edit', args=[self.famille.pk, prest.pk]), data={
            'date_prestation': prest.date_prestation,
            'duree': '00:45',
            'intervenants': [self.user_spe.pk],
            'texte': 'foo',
        })
        prest.refresh_from_db()
        self.assertEqual(prest.texte, 'foo')

    def test_ase_duree_toujours_0(self):
        user_ase = Utilisateur.objects.create_user(
            'user_ase', 'user_ase@example.org',
            nom='Ase1'
        )
        user_ase.roles.add(Role.objects.get(nom='ASE'))
        user_ase.groups.add(Group.objects.get(name='spe'))
        IntervenantSPE.objects.create(
            suivi=self.famille.suivispe, intervenant=user_ase, role=Role.objects.get(nom="Educ")
        )
        self.client.force_login(user_ase)
        response = self.client.get(reverse('spe-prestation-famille-add', args=[self.famille.pk]))
        self.assertNotContains(response, "Durée")
        response = self.client.post(reverse('spe-prestation-famille-add', args=[self.famille.pk]), data={
            'date_prestation': date.today(),
            'intervenants': [user_ase.pk],
            'texte': "Un commentaire",
        })
        self.assertRedirects(response, reverse('spe-journal-list', args=[self.famille.pk]))
        prestation = user_ase.prestations('spe')[0]
        self.assertEqual(prestation.duree, timedelta())
        self.assertEqual(prestation.lib_prestation.code, 'spe04')

    def test_add_prestation_fam(self):
        add_url = reverse('spe-prestation-famille-add', args=[self.famille.pk])
        data = dict(
            duree='3:40', date_prestation=date.today() - timedelta(days=3), famille=self.famille.pk,
            intervenants=[self.user_spe.pk]
        )
        self.client.force_login(self.user_spe)
        response = self.client.post(add_url, data)
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        self.assertRedirects(response, reverse('spe-journal-list', args=[self.famille.pk]))
        prestation = self.famille.prestations.get(duree="3:40")
        # Évaluation SPE choisie auto car pas de date de début de suivi
        self.assertEqual(prestation.lib_prestation.code, 'spe01')

        # Le type de prestation dépend de la date exacte de début de suivi.
        self.famille.suivi.date_debut_suivi = date.today() - timedelta(days=1)
        self.famille.suivi.save()
        data.update({'date_prestation': date.today(), 'duree': '1:00'})
        response = self.client.post(add_url, data)
        prestation = self.famille.prestations.get(duree="1:00")
        self.assertEqual(prestation.lib_prestation.code, 'spe02')
        data.update({'date_prestation': date.today() - timedelta(days=2), 'duree': '1:30'})
        response = self.client.post(add_url, data)
        prestation = self.famille.prestations.get(duree="1:30")
        self.assertEqual(prestation.lib_prestation.code, 'spe01')

    def test_affichage_prestation_menu_fam(self):
        self.create_prestation_fam()
        menu_url = reverse('spe-prestation-menu')
        self.client.force_login(self.user_spe)
        response = self.client.get(menu_url)
        self.assertContains(
            response,
            '<td class="total">16:00</td>',
            html=True
        )
        self.assertContains(
            response,
            '<td class="mesprest">08:00</td>',
            html=True
        )

    def test_add_prestation_gen(self):
        add_url = reverse('spe-prestation-gen-add')
        data = dict(
            duree='3:40', famille='',
            date_prestation=date.today() - timedelta(days=360),  # Too old!
            intervenants=[self.user_spe.pk]
        )
        self.client.force_login(self.user_spe)
        response = self.client.post(add_url, data)
        self.assertEqual(
            response.context['form'].errors,
            {'date_prestation': ['La saisie des prestations des mois précédents est close !']}
        )
        data['date_prestation'] = date.today()
        response = self.client.post(add_url, data)
        self.assertRedirects(response, reverse('spe-prestation-gen-list'))
        self.assertEqual(self.user_spe.prestations_spe.first().lib_prestation.code, 'spe03')

    def test_update_prestation_fam(self):
        prest = self.create_prestation_fam()
        url = reverse('spe-prestation-edit', args=[self.famille.pk, prest.pk])
        self.client.force_login(self.user_spe)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        data = model_to_dict(prest)
        del data['medlink_date']
        data['fichier'] = ''
        data['intervenants'] = [str(interv.pk) for interv in data['intervenants']]
        data['duree'] = '12:00'
        response = self.client.post(url, data=data)
        prest.refresh_from_db()
        self.assertEqual(prest.duree, timedelta(hours=12))

    def test_correction_prestation_debut_suivi_passe(self):
        """
        Quand un début de suivi est saisi dans le passé, les prestations depuis
        cette date sont passées en accompagnement.
        """
        prest = self.create_prestation_fam()
        self.assertEqual(prest.lib_prestation.code, 'spe01')
        self.assertIsNone(self.famille.suivi.date_debut_suivi)
        self.famille.suivispe.intervenants.add(
            self.user, through_defaults={'role': Role.objects.get(nom='Educ')}
        )
        self.client.force_login(self.user)
        response = self.client.post(
            reverse('spe-famille-agenda', args=[self.famille.pk]),
            data={
                'date_demande': date.today() - timedelta(days=4),
                'date_debut_evaluation': date.today() - timedelta(days=4),
                'date_fin_evaluation': date.today() - timedelta(days=2),
                'date_debut_suivi': date.today() - timedelta(days=2),
            },
        )
        self.assertEqual(response.status_code, 302)
        self.famille.refresh_from_db()
        self.assertIsNotNone(self.famille.suivi.date_debut_suivi)
        prest.refresh_from_db()
        self.assertEqual(prest.lib_prestation.code, 'spe02')

    def test_droit_modification_prestation_familiale(self):
        prest = PrestationSPE.objects.create(
            famille=self.famille,
            auteur=self.user_admin,
            lib_prestation=LibellePrestation.objects.get(code='spe01'),
            date_prestation=date.today(),
            duree='1:10'
        )
        prest.intervenants.set([self.user_spe])

        self.client.force_login(self.user_spe2)
        with self.assertLogs('django.request', level='WARNING'):
            response = self.client.get(reverse('spe-prestation-edit', args=[self.famille.pk, prest.pk]))
            self.assertEqual(response.status_code, 403)
        self.assertTrue(prest.can_edit(self.user_spe))

        self.client.force_login(self.user_admin)
        response = self.client.get(reverse('spe-prestation-edit', args=[self.famille.pk, prest.pk]))
        self.assertEqual(response.status_code, 200)

    def test_affichage_prestation_personnelle(self):
        self.create_prestation_fam()
        self.create_prestation_gen()
        self.client.force_login(self.user_spe)
        response = self.client.get(reverse('prestation-personnelle', args=['spe']))
        self.assertContains(
            response,
            '<tr><th>Total prestations spe01</th><td align="right">08:00</td></tr>',
            html=True
        )
        self.assertContains(
            response,
            '<tr><th>Total prestations spe03</th><td align="right">08:00</td></tr>',
            html=True
        )
        self.assertContains(
            response,
            '<tr class="border-top"><th>Total</th><th class ="text-end">16:00</th></tr>',
            html=True
        )

    def test_affichage_prestation_generale(self):
        self.create_prestation_fam()
        self.create_prestation_gen()
        self.client.force_login(self.user_spe)
        response = self.client.get(reverse('prestation-generale', args=['spe']))
        self.assertContains(
            response,
            '<td>08:00</td>',
            html=True,
            count=1
        )

    def test_suppression_prestation_famille(self):
        prest_fam = self.create_prestation_fam()
        self.client.force_login(self.user_spe)
        response = self.client.post(reverse('spe-prestation-delete', args=[prest_fam.famille_id, prest_fam.pk]))
        self.assertRedirects(response, reverse('spe-journal-list', args=[prest_fam.famille_id]))
        self.assertFalse(PrestationSPE.objects.filter(pk=prest_fam.pk).exists())

    def test_recherche_prestations(self):
        texte1 = "La famille Lucky va bien"
        texte2 = "En été, Luke tire bien plus vite que son ombre!"
        prest1 = self.create_prestation_fam(texte1)
        prest2 = self.create_prestation_fam(texte2)
        self.client.force_login(self.user_spe)
        url = reverse('spe-journal-list', args=[self.famille.pk])

        # Note: les chaînes à rechercher sont en majuscule pour s'assurer que
        # la recherche n'est pas sensible à la casse

        # mot contenu dans les deux enregistrements
        response = self.client.get(url, {"recherche": "BIEN"})
        self.assertQuerySetEqual(response.context["object_list"], [prest1, prest2])

        # mot contenu dans le premier enregistrement
        response = self.client.get(url, {"recherche": "FAMILLE"})
        self.assertQuerySetEqual(response.context["object_list"], [prest1])

        # mot contenu dans le deuxième enregistrement
        response = self.client.get(url, {"recherche": "OMBRE"})
        self.assertQuerySetEqual(response.context["object_list"], [prest2])

        # mot contenu dans aucun enregistrement
        response = self.client.get(url, {"recherche": "TINTIN"})
        self.assertQuerySetEqual(response.context["object_list"], [])

        # deux mots contenus chacun dans un enregistrement différent
        response = self.client.get(url, {"recherche": "FAMILLE OMBRE"})
        self.assertQuerySetEqual(response.context["object_list"], [])

        # deux mots contenus dans le même enregistrement
        response = self.client.get(url, {"recherche": "BIEN OMBRE"})
        self.assertQuerySetEqual(response.context["object_list"], [prest2])

        # Recherche d'un mot avec accent (mot clé sans accent)
        response = self.client.get(url, {"recherche": "ETE"})
        self.assertQuerySetEqual(response.context["object_list"], [prest2])

        # Recherche d'un mot sans accent (mot clé avec accent)
        response = self.client.get(url, {"recherche": "FÂMÌLLÉ"})
        self.assertQuerySetEqual(response.context["object_list"], [prest1])

    def test_message_recherche_prestations_sans_resultat(self):
        pas_de_prestation = "Aucune prestation saisie"
        recherche_vide = "Pas de résultat pour votre recherche."
        self.client.force_login(self.user_spe)

        # Message pour contenu vide sans recherche
        response = self.client.get(reverse('spe-journal-list', args=[self.famille.pk]))
        self.assertContains(response, pas_de_prestation)
        self.assertNotContains(response, recherche_vide)

        # Message pour contenu vide lors d'une recherche
        response = self.client.get(reverse('spe-journal-list', args=[self.famille.pk]), {"recherche": "VIDE"})
        self.assertContains(response, recherche_vide)
        self.assertNotContains(response, pas_de_prestation)


class MedlinkImportTests(SPEDataMixin, TestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        ase1 = Utilisateur.objects.create_user(
            'smith', 'smith@example.org', nom='Smith', prenom='Gil'
        )
        ase1.roles.add(Role.objects.get(nom='ASE'))
        ase1.groups.add(cls.spe_group)
        ase2 = Utilisateur.objects.create_user(
            'deruns', 'deruns@example.org', nom='Déruns', prenom='Léa'
        )
        ase2.roles.add(Role.objects.get(nom='ASE'))
        ase2.groups.add(cls.spe_group)
        Utilisateur.objects.create_user(
            'derunsz', 'derunsz@example.org', nom='Déruns', prenom='Zoé'
        )

        Personne.objects.create_personne(
            famille=FamilleSPE.objects.get(nom='Haddock'),
            role=Role.objects.get(nom='Enfant suivi'),
            nom='Hergé', prenom='Léonard', genre='M', date_naissance=date(2010, 7, 16),
            rue='Château1', npa=2000, localite='Moulinsart',
        )

        famille2 = FamilleSPE.objects.create_famille(
            nom='Loiseau', rue='Fleurs 12', npa=2000, localite='Moulinsart',
        )
        Personne.objects.create_personne(
            famille=famille2, role=Role.objects.get(nom='Enfant suivi'),
            nom='Loiseau', prenom='Marie', genre='F', date_naissance=date(2015, 11, 2),
            rue='Fleurs 12', npa=2000, localite='Moulinsart',
        )

        famille_arch = FamilleSPE.objects.create_famille(
            nom='Loiseau', rue='Fleurs 12', npa=2000, localite='Moulinsart',
        )
        suivi = famille_arch.suivi
        suivi.date_debut_suivi = date(2020, 7, 16)
        suivi.date_fin_suivi = date(2020, 7, 25)
        suivi.save()
        Personne.objects.create_personne(
            famille=famille_arch, role=Role.objects.get(nom='Enfant suivi'),
            nom='Loiseau', prenom='Marie', genre='F', date_naissance=date(2015, 11, 2),
            rue='Fleurs 12', npa=2000, localite='Moulinsart',
        )

        famille_sof = FamilleSOF.objects.create_famille(
            nom="Djedje", rue="Grand-Rue 2", npa=2000, localite="Moulinsart",
        )
        Personne.objects.create_personne(
            famille=famille_sof, role=Role.objects.get(nom="Enfant suivi"),
            nom="Djedje", prenom="Julien", genre="M", date_naissance=date(2021, 11, 2),
            rue="Grand-Rue 2", npa=2000, localite="Moulinsart",
        )

        LibellePrestation.objects.create(unite="sof", code="sof03", nom="Évaluation SOF")
        LibellePrestation.objects.create(unite="sof", code="sof04", nom="Activités ASE")

        cls.test_medlink_file_xlsx = Path(__file__).parent / 'SPE Heures janvier 2023.xlsx'
        cls.test_medlink_file_csv = Path(__file__).parent / 'SPE Heures janvier 2023.CSV'

    def test_access_forbidden(self):
        user_ipe = Utilisateur.objects.create_user('user_ipe', 'user_ipe@example.org', nom='Ipe1')
        user_ipe.roles.add(Role.objects.get(nom='IPE'))
        user_ipe.groups.add(self.spe_group)
        self.client.force_login(user_ipe)
        with self.assertLogs('django.request', level='WARNING'):
            response = self.client.get(reverse('medlink-import'))
        self.assertEqual(response.status_code, 403)

    def test_access_allowed(self):
        self.client.force_login(self.user_admin)
        response = self.client.get(reverse('medlink-import'))
        self.assertEqual(response.status_code, 200)

    def _test_import_hours(self, file_):
        self.client.force_login(self.user_admin)
        with file_.open(mode='rb') as fh:
            response = self.client.post(reverse('medlink-import'), data={'fichier': File(fh)})
        messages = list(get_messages(response.wsgi_request))
        self.assertEqual(str(messages[0]), "L’importation s'est déroulée avec succès.")
        famille = FamilleSPE.objects.get(nom='Loiseau', suivispe__date_fin_suivi=None)
        prests = PrestationSPE.objects.filter(famille=famille)
        self.assertEqual(len(prests), 2)
        self.assertEqual(prests[0].date_prestation, date(2023, 1, 12))
        self.assertEqual(
            list(prests[0].intervenants.all()),
            [Utilisateur.objects.get(nom='Déruns', roles__nom='ASE')]
        )
        self.assertEqual(sum([p.duree.seconds for p in prests]), 4500)
        self.assertEqual(PrestationSPE.objects.filter(intervenants__nom='Smith').count(), 2)
        # Prestations SOF
        self.assertEqual(PrestationSOF.objects.filter(lib_prestation__code="sof03").count(), 1)
        self.assertEqual(PrestationSOF.objects.filter(lib_prestation__code="sof04").count(), 2)

    def test_import_hours_xlsx(self):
        self._test_import_hours(self.test_medlink_file_xlsx)

    def test_import_hours_csv(self):
        self._test_import_hours(self.test_medlink_file_csv)

    def test_intervenant_inexistant(self):
        Utilisateur.objects.filter(nom='Déruns').delete()
        self.client.force_login(self.user_admin)
        with self.test_medlink_file_xlsx.open(mode='rb') as fh:
            response = self.client.post(reverse('medlink-import'), data={'fichier': File(fh)})
        self.assertContains(response, 'Impossible de trouver les intervenant-e-s suivant-e-s')
        self.assertContains(response, 'DERUNS-ASE Léa Ecaterina')

    def test_famille_inexistante(self):
        FamilleSPE.objects.filter(nom='Loiseau').delete()
        self.client.force_login(self.user_admin)
        with self.test_medlink_file_xlsx.open(mode='rb') as fh:
            response = self.client.post(reverse('medlink-import'), data={'fichier': File(fh)})
        self.assertContains(response, "Impossible de trouver les personnes suivantes")
        self.assertContains(response, 'LOISEAU Marie')

    def test_historique(self):
        self.client.force_login(self.user_admin)
        with self.test_medlink_file_xlsx.open(mode='rb') as fh:
            response = self.client.post(reverse('medlink-import'), data={'fichier': File(fh)})
        messages = list(get_messages(response.wsgi_request))
        self.assertEqual(str(messages[0]), "L’importation s'est déroulée avec succès.")

        response = self.client.get(reverse('medlink-historique'))
        self.assertEqual(
            response.context['object_list'][0]['medlink_date'].strftime("%d.%m.%Y"),
            datetime.now().strftime("%d.%m.%Y")
        )

    def test_detail(self):
        # la ligne sans durée et celle sans SPE dans la prestation ne sont pas importées
        lignes_importees = 5
        self.client.force_login(self.user_admin)
        with self.test_medlink_file_xlsx.open(mode='rb') as fh:
            response = self.client.post(reverse('medlink-import'), data={'fichier': File(fh)})
        self.assertEqual(PrestationSPE.objects.count(), lignes_importees)
        prest = PrestationSPE.objects.first()

        url = f"{reverse('medlink-detail')}?ts={int(datetime.timestamp(prest.medlink_date))}"
        response = self.client.get(url)
        self.assertEqual(len(response.context["spe"]["object_list"]), lignes_importees)


class RapportSpeTests(SPEDataMixin, TestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.famille = FamilleSPE.objects.create_famille(nom='Doe', typ=['spe'])
        cls.educ = Utilisateur.objects.create_user('Educ', 'educ@exemple.org', '123')
        cls.educ.user_permissions.add(Permission.objects.get(codename='change_famillespe'))

    def setUp(self) -> None:
        self.create_kwargs = {
            'famille': self.famille,
            'auteur': self.educ,
            'date': date.today(),
            'situation': 'Situation initiale',
            'evolutions': 'Evolutions',
            'evaluation': 'Evaluation',
            'observations': 'Observation'
        }

    def test_create_model(self):
        rapport = RapportSPE.objects.create(**self.create_kwargs)
        self.assertIsInstance(rapport, RapportSPE)
        self.assertEqual(str(rapport), f"Résumé du {format_d_m_Y(date.today())} pour la famille Doe - ")

    def test_ajout_rapport(self):
        famille = FamilleSPE.objects.first()
        user = Utilisateur.objects.get(nom='Valjean')
        other_user = Utilisateur.objects.create_user(
            'other', 'other@example.org', 'mepassword', prenom='Bruce', nom='Externe',
        )
        other_user.groups.add(Group.objects.get(name='spe'))
        with (settings.BASE_DIR / "croixrouge" / "static" / "img" / "journal.jpg").open('rb') as fh:
            other_user.signature = SimpleUploadedFile('img.jpg', fh.read(), content_type='image/jpeg')
            other_user.save()
        famille.suivispe.intervenants.add(other_user, through_defaults={'role': Role.objects.get(nom='Psy')})
        famille.suivispe.intervenants.add(user, through_defaults={
            'role': Role.objects.get(nom='Psy'), 'date_fin': date.today() - timedelta(days=3)
        })
        self.client.force_login(self.user)
        response = self.client.get(reverse('famille-rapport-add', args=[famille.pk]))
        # Seule l'intervention active est visible
        self.assertQuerySetEqual(response.context['form']['sig_interv'].field.queryset, [other_user])

        rapport_data = {
            'date': date.today(),
            'situation': '<p>Assez bonne</p>',
            'evolutions': '<p>1<br>2<br>3</p>',
            'evaluation': '<p>Ha!</p>',
            'projet': '<p>Bon</p>',
            'sig_interv': [other_user.pk],
        }
        response = self.client.post(
            reverse('famille-rapport-add', args=[famille.pk]), data=rapport_data
        )
        self.assertRedirects(response, reverse('spe-famille-agenda', args=[famille.pk]))
        rapport = famille.rapports_spe.first()
        self.assertTrue(rapport.can_edit(user))
        self.assertTrue(rapport.can_edit(other_user))
        # Test édition
        rapport_data.update({'date': date.today(), 'projet': '<p>Un autre</p>'})
        response = self.client.post(
            reverse('famille-rapport-edit', args=[famille.pk, rapport.pk]), data=rapport_data
        )
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        rapport.refresh_from_db()
        self.assertEqual(rapport.date, date.today())
        # Test vue/impression
        response = self.client.get(reverse('famille-rapport-view', args=[famille.pk, rapport.pk]))
        self.assertContains(response, "<p>Assez bonne</p>")
        response = self.client.get(reverse('famille-rapport-print', args=[famille.pk, rapport.pk]))
        self.assertEqual(response['content-type'], 'application/pdf')

    def test_create_view_with_observations(self):
        # New rapports always with observations field
        self.client.force_login(self.educ)
        response = self.client.get(reverse('famille-rapport-add', args=[self.famille.pk]))
        self.assertNotContains(response, "Observations IPE")
        self.assertNotContains(response, "Observations SOF")
        self.assertNotContains(response, "Observations APA")
        self.assertContains(
            response,
            '<label for="id_observations">Observations, évolution et hypothèses :</label>',
            html=True
        )
        self.assertNotIn('Évaluation / Hypothèses', response)
        self.assertNotIn('Évolutions et observations', response)

    def test_display_old_rapport_without_observations(self):
        # evolutions and evaluation not empty
        rapport = RapportSPE.objects.create(**self.create_kwargs)
        self.client.force_login(self.educ)
        response = self.client.get(reverse('famille-rapport-view', args=[self.famille.pk, rapport.pk]))

        self.assertContains(response, '<h3>Évaluation / Hypothèses</h3>', html=True)
        self.assertContains(response, '<h3>Évolutions et observations</h3>', html=True)
        self.assertNotIn('Observations', response)

    def test_display_new_rapport_with_observations(self):
        # evolutions and evaluation are empty
        self.create_kwargs['evolutions'] = ''
        self.create_kwargs['evaluation'] = ''
        rapport = RapportSPE.objects.create(**self.create_kwargs)
        self.client.force_login(self.educ)
        response = self.client.get(reverse('famille-rapport-view', args=[self.famille.pk, rapport.pk]))

        self.assertNotContains(response, '<h3>Évaluation / Hypothèses</h3>', html=True)
        self.assertNotContains(response, '<h3>Évolutions et observations</h3>', html=True)
        self.assertContains(response, '<h3>Observations, évolution et hypothèses</h3>', html=True)

    def test_edit_rapport_by_ipe(self):
        rapport = RapportSPE.objects.create(**self.create_kwargs)
        user_ipe = Utilisateur.objects.create_user('IPE', 'ipe@exemple.org', '123')
        user_ipe.roles.add(Role.objects.get(nom='IPE'))
        user_ipe.groups.add(Group.objects.get(name='spe'))
        self.famille.suivispe.intervenants.add(self.educ, through_defaults={'role': Role.objects.get(nom='Educ')})
        self.famille.suivispe.intervenants.add(user_ipe, through_defaults={'role': Role.objects.get(nom='IPE')})
        self.assertFalse(rapport.can_delete(user_ipe))
        self.client.force_login(user_ipe)
        response = self.client.get(reverse('famille-rapport-view', args=[self.famille.pk, rapport.pk]))
        self.assertContains(response, "Modifier")
        self.assertNotContains(response, "Supprimer")
        url = reverse('famille-rapport-edit', args=[self.famille.pk, rapport.pk])
        response = self.client.get(url)
        self.assertTrue(response.context['form'].fields['situation'].disabled)
        ipe_content = "<p>Des observations de la part d’un-e IPE</p>"
        response = self.client.post(url, data={
            'observ_ipe': ipe_content,
        })
        self.assertRedirects(response, reverse('spe-famille-agenda', args=[self.famille.pk]))
        rapport.refresh_from_db()
        self.assertEqual(rapport.observ_ipe, ipe_content)

    def test_edit_rapport_by_sof(self):
        self.famille.suivispe.intervenants.add(self.educ, through_defaults={'role': Role.objects.get(nom='Educ')})
        rapport = RapportSPE.objects.create(**self.create_kwargs)
        rapport.pres_interv.set([self.educ])
        rapport.sig_interv.set([self.educ])
        user_sof = Utilisateur.objects.create_user('IPE', 'ipe@exemple.org', '123')
        user_sof.roles.add(Role.objects.get(nom='Assistant-e social-e'))
        user_sof.groups.add(Group.objects.get(name='spe'))
        self.client.force_login(user_sof)
        url = reverse('famille-rapport-edit', args=[self.famille.pk, rapport.pk])
        response = self.client.get(url)
        self.assertTrue(response.context['form'].fields['situation'].disabled)
        sof_content = "<p>Des observations de la part d’un-e assistant-e social-e</p>"
        response = self.client.post(url, data={
            'observ_sof': sof_content,
        })
        self.assertRedirects(response, reverse('spe-famille-agenda', args=[self.famille.pk]))
        rapport.refresh_from_db()
        self.assertEqual(rapport.observ_sof, sof_content)

    def test_suppression_rapport(self):
        famille = FamilleSPE.objects.first()
        rapp = RapportSPE.objects.create(
            famille=famille, date=date(2020, 4, 18), auteur=self.user, situation="La situation"
        )
        self.client.force_login(self.user)
        response = self.client.post(reverse('famille-rapport-delete', args=[famille.pk, rapp.pk]))
        self.assertRedirects(response, reverse('spe-famille-agenda', args=[famille.pk]))


class NiveauSPETests(SPEDataMixin, TestCase):

    def setUp(self) -> None:
        self.famille = FamilleSPE.objects.get(nom='Haddock')
        self.client.force_login(self.user_spe)

    def test_niveauspe_model(self):
        niv = NiveauSPE.objects.create(
            famille=self.famille,
            niveau_interv=2,
            date_debut=date.today()
        )
        self.assertEqual(niv.famille, self.famille)
        self.assertEqual(niv.niveau_interv, 2)
        self.assertEqual(niv.date_debut, date.today())
        self.assertEqual(niv.date_fin, None)

    def test_niveauspe_add_view(self):
        auj = date.today()
        demain = auj + timedelta(days=1)
        response = self.client.post(
            path=reverse('spe-niveau-add', args=[self.famille.pk]),
            data={'niveau_interv': 3, 'date_debut': demain},
            follow=True
        )
        self.assertContains(response, f"<td>{format_d_m_Y(demain)}</td><td>---</td><td>3</td>", html=True)

    def test_niveauspe_add_form(self):
        auj = date.today()
        demain = auj + timedelta(days=1)
        form = NiveauSPEForm(famille=self.famille, data={'niveau_interv': 3, 'date_debut': demain})
        self.assertTrue(form.is_valid())

    def test_niveauspe_add_second_enregistrement(self):
        auj = date.today()
        demain = auj + timedelta(days=1)
        niv = NiveauSPE.objects.create(famille=self.famille, niveau_interv=2, date_debut=auj, date_fin=None)
        self.client.post(
            path=reverse('spe-niveau-add', args=[self.famille.pk]),
            data={'niveau_interv': 3, 'date_debut': demain},
            follow=True
        )
        # Mise à jour dernier enreg.
        niv.refresh_from_db()
        self.assertEqual(niv.date_fin, auj)

        # Test nouvel enreg.
        self.assertEqual(self.famille.niveaux_spe.count(), 2)
        der_niv = self.famille.niveaux_spe.last()
        self.assertEqual(der_niv.famille, self.famille)
        self.assertEqual(der_niv.niveau_interv, 3)
        self.assertEqual(der_niv.date_debut, demain)
        self.assertEqual(der_niv.date_fin, None)

    def test_niveauspe_edit_view(self):
        auj = date.today()
        demain = auj + timedelta(days=1)
        niv = NiveauSPE.objects.create(famille=self.famille, niveau_interv=2, date_debut=auj, date_fin=None)
        self.client.post(
            path=reverse('spe-niveau-edit', args=[self.famille.pk, niv.pk]),
            data={'niveau_interv': 3, 'date_debut': demain},
            follow=True
        )
        niv.refresh_from_db()
        self.assertEqual(niv.niveau_interv, 3)

    def test_niveauspe_edit_form(self):
        auj = date.today()
        demain = auj + timedelta(days=1)
        niv = NiveauSPE.objects.create(famille=self.famille, niveau_interv=2, date_debut=auj, date_fin=None)
        form = NiveauSPEForm(famille=self.famille, instance=niv, data={'niveau_interv': 3, 'date_debut': demain})
        self.assertTrue(form.is_valid())
        form.save()
        niv.refresh_from_db()
        self.assertEqual(niv.niveau_interv, 3)

    def test_niveauspe_delete(self):
        auj = date.today()
        niv = NiveauSPE.objects.create(famille=self.famille, niveau_interv=2, date_debut=auj, date_fin=None)
        response = self.client.post(
            path=reverse('spe-niveau-delete', args=[self.famille.pk, niv.pk]),
            follow=True
        )
        self.assertEqual(len(response.context['niveaux']), 0)

    def test_niveauspe_affichage_dans_agenda(self):
        auj = date.today()
        NiveauSPE.objects.create(
            famille=self.famille, niveau_interv=2, date_debut=auj - timedelta(days=10), date_fin=None
        )
        agenda_url = reverse('spe-famille-agenda', args=[self.famille.pk])
        response = self.client.get(agenda_url)
        self.assertContains(response, '<span class="fw-bold">Niv. d’intervention 2</span>', html=True)
        # Fin niveau affiché au plus tard à fin du suivi.
        self.famille.suivi.date_fin_suivi = auj
        self.famille.suivi.save()
        response = self.client.get(agenda_url)
        self.assertEqual(response.context['niveaux'][0].date_fin_calc, auj)

    def test_debut_suivi_selon_niveau(self):
        """Début du suivi peut varier selon changement de niveau 2/3."""
        auj = date.today()
        self.famille.suivi.date_debut_suivi = auj - timedelta(days=20)
        self.famille.suivi.save()
        NiveauSPE.objects.create(
            famille=self.famille, niveau_interv=2,
            date_debut=auj - timedelta(days=20), date_fin=auj - timedelta(days=10)
        )
        NiveauSPE.objects.create(
            famille=self.famille, niveau_interv=3,
            date_debut=auj - timedelta(days=10), date_fin=None
        )
        self.assertEqual(self.famille.suivi.debut_suivi_selon_niveau, auj - timedelta(days=10))


class TestExporter(ExportReporting):
    """A fake exporter class that just collect data in lists to be able to assert the contents."""
    def __init__(self):
        super().__init__()
        self.sheets = {}
        self._current_sh = None

    def __call__(self):
        # This allows to provide an instance to mock
        return self

    def setup_sheet(self, title):
        self.sheets[title] = []
        self._current_sh = self.sheets[title]

    def write_line(self, values, **kwargs):
        self._current_sh.append(values)


class ExportTests(InitialDataMixin, TestCase):
    year = date.today().year - 1

    def setUp(self):
        self.user.user_permissions.add(Permission.objects.get(codename='export_stats'))
        self.client.force_login(self.user)
        self.cfg_date = date(self.year, 10, 3)
        self.cfg_sheet = 'En_cours_{}.{}'.format(self.cfg_date.month, self.cfg_date.year)
        self.cfg_export_month = {'mois': self.cfg_date.month, 'annee': self.cfg_date.year}

        self.entetes_communs = [
            'Institution', 'Prestation', 'Nom', 'Prenom', 'Genre', 'Date de naissance', 'Adresse', 'NPA', 'Localité',
            'Canton', 'OPE', 'Nom mère', 'Prénom mère', 'Nom père', 'Prénom père', 'Autorité parentale',
            'Statut marital', 'Statut financier', 'Fam. monopar.', 'Nbre enfants',
            'Date demande', 'Provenance', 'Motif demande', 'Début suivi', 'Niveau interv.',
        ]

        self.entetes_nouveaux = self.entetes_communs
        self.entetes_en_cours = self.entetes_communs + ['H. Évaluation', 'H. Suivi', 'H. Prest. gén.']
        self.entetes_termines = (self.entetes_communs +
                                 ['Date fin suivi', 'Motif fin suivi', 'Destination', 'Total heures'])

        self.enf_suivi = Role.objects.get(nom='Enfant suivi', famille=True)
        self.role_pere = Role.objects.get(nom='Père', famille=True)
        self.role_mere = Role.objects.get(nom='Mère', famille=True)

    def test_entete_nouveux_suivis(self):
        exp = TestExporter()
        with patch('spe.views.ExportReporting', new=exp):
            self.client.post(reverse('export-prestation'), {'mois': '10', 'annee': self.year})
        sheet_name = f"Nouveaux_10.{self.year}"
        self.assertEqual(len(exp.sheets[sheet_name]), 1)
        self.assertEqual(exp.sheets[sheet_name][0], self.entetes_nouveaux)

    def test_entete_suivis_en_cours(self):
        exp = TestExporter()
        with patch('spe.views.ExportReporting', new=exp):
            self.client.post(reverse('export-prestation'), {'mois': '10', 'annee': self.year})
        sheet_name = f"En_cours_10.{self.year}"
        self.assertEqual(len(exp.sheets[sheet_name]), 1)
        self.assertEqual(exp.sheets[sheet_name][0], self.entetes_en_cours)

    def test_entete_suivis_termines(self):
        exp = TestExporter()
        with patch('spe.views.ExportReporting', new=exp):
            self.client.post(reverse('export-prestation'), {'mois': '10', 'annee': self.year})
        sheet_name = f"Terminés_10.{self.year}"
        self.assertEqual(len(exp.sheets[sheet_name]), 1)
        self.assertEqual(exp.sheets[sheet_name][0], self.entetes_termines)

    def test_suivis_en_cours(self):
        from ..views import ExportPrestationView
        dates = [
            # date_demande, date_debut_eval, date_debut_suivi, suivi en cours entre 1-28.2 ?
            (None, None, None, False),
            (date(2020, 2, 15), None, None, True),
            (None, date(2020, 2, 15), None, True),
            (None, None, date(2020, 2, 15), True),
            (None, None, date(2020, 3, 3), False),
        ]
        famille = self.create_famille_spe(name='Haddock')
        for date_demande, date_debut_evaluation, date_debut_suivi, en_cours in dates:
            famille.suivispe.date_demande = date_demande
            famille.suivispe.date_debut_evaluation = date_debut_evaluation
            famille.suivispe.date_debut_suivi = date_debut_suivi
            famille.suivispe.save()
            familles = ExportPrestationView().suivis_en_cours(date(2020, 2, 1), date(2020, 2, 28))
            if en_cours:
                self.assertIn(famille, familles)
            else:
                self.assertNotIn(famille, familles)

    def test_export_prestations_decembre(self):
        self.user.user_permissions.add(Permission.objects.get(codename='export_stats'))
        self.client.force_login(self.user)
        response = self.client.post(reverse('export-prestation'), {'mois': '12', 'annee': self.year})
        self.assertEqual(response.status_code, 200)

    def test_date_nouveau_suivi(self):
        fam1 = self.create_famille_spe(name='Haddock')
        fam1.suivispe.date_demande = f"{self.year}-09-20"
        fam1.suivispe.save()

        fam2 = self.create_famille_spe(name='Tournesol')
        fam2.suivispe.date_demande = f"{self.year}-10-20"
        fam2.suivispe.save()

        fam3 = self.create_famille_spe(name='Castafiore')
        fam3.suivispe.date_demande = f"{self.year}-11-20"
        fam3.suivispe.save()

        exp = TestExporter()
        with patch('spe.views.ExportReporting', new=exp):
            self.client.post(reverse('export-prestation'), {'mois': '10', 'annee': self.year})
        sheet_name = f"Nouveaux_10.{self.year}"
        self.assertEqual(len(exp.sheets[sheet_name]), 2)
        self.assertEqual([line[2] for line in exp.sheets[sheet_name]], ['Nom', 'Tournesol'])

    def test_date_suivi_en_cours(self):
        fam1 = self.create_famille_spe(name='Haddock')
        fam1.suivispe.date_demande = f"{self.year}-09-20"
        fam1.suivispe.save()

        fam2 = self.create_famille_spe(name='Tournesol')
        fam2.suivispe.date_demande = f"{self.year}-10-20"
        fam2.suivispe.save()

        fam3 = self.create_famille_spe(name='Castafiore')
        fam3.suivispe.date_demande = f"{self.year}-11-20"
        fam3.suivispe.save()

        self.benef1 = dict(nom='Haddock', date_admission=f"{self.year}-10-02")
        self.benef2 = dict(nom='Tournesol', date_admission=f"{self.year}-09-30")

    def test_date_suivi_termine(self):
        fam1 = self.create_famille_spe(name='Haddock')
        fam1.suivispe.date_demande = f'{self.year}-09-20'
        fam1.suivispe.date_fin_suivi = f'{self.year}-10-31'
        fam1.suivispe.save()

        fam2 = self.create_famille_spe(name='Tournesol')
        fam2.suivispe.date_demande = f'{self.year}-10-20'
        fam2.suivispe.save()

        fam3 = self.create_famille_spe(name='Castafiore')
        fam3.suivispe.date_demande = f'{self.year}-11-20'
        fam3.suivispe.date_fin_suivi = f'{self.year}-12-31'
        fam3.suivispe.save()

        exp = TestExporter()
        with patch('spe.views.ExportReporting', new=exp):
            self.client.post(reverse('export-prestation'), {'mois': '10', 'annee': self.year})
        sheet_name = f"Terminés_10.{self.year}"
        self.assertEqual(len(exp.sheets[sheet_name]), 2)
        self.assertEqual([line[2] for line in exp.sheets[sheet_name]], ['Nom', 'Haddock'])

    def test_un_enfant_par_ligne(self):
        fam = self.create_famille_spe()
        fam.suivispe.date_demande = f"{self.year}-10-20"
        fam.suivispe.save()
        Personne.objects.create_personne(
            famille=fam, role=Role.objects.get(nom='Enfant suivi'),
            nom='Haddock', prenom='Ursule', genre='M', date_naissance=date(2008, 11, 6),
        )

        exp = TestExporter()
        with patch('spe.views.ExportReporting', new=exp):
            self.client.post(reverse('export-prestation'), {'mois': '10', 'annee': self.year})
        sheet_name = f"Nouveaux_10.{self.year}"
        self.assertEqual(len(exp.sheets[sheet_name]), 3)
        self.assertEqual(
            [(line[2], line[3]) for line in exp.sheets[f"Nouveaux_10.{self.year}"]],
            [('Nom', 'Prenom'), ('Haddock', 'Ursule'), ('Haddock', 'Toto')]
        )

    def test_repartition_temps_total_prestation_mensuel_entre_enfants(self):
        fam = self.create_famille_spe()
        fam.suivispe.date_demande = f'{self.year}-10-20'
        fam.suivispe.save()
        Personne.objects.create_personne(
            famille=fam, role=Role.objects.get(nom='Enfant suivi'),
            nom='Haddock', prenom='Ursule', genre='M', date_naissance=date(2008, 11, 6),
        )
        spe01 = LibellePrestation.objects.get(code='spe01')
        PrestationSPE.objects.bulk_create([
            PrestationSPE(
                auteur=self.user, famille=fam,
                date_prestation=date(self.year, 10, 21), lib_prestation=spe01, duree='3:40'
            ),
            PrestationSPE(
                auteur=self.user, famille=fam,
                date_prestation=date(self.year, 10, 22), lib_prestation=spe01, duree='6:40'
            ),
            PrestationSPE(
                auteur=self.user, famille=fam,
                date_prestation=date(self.year, 10, 30), lib_prestation=spe01, duree='2:00'
            ),
        ])
        for prest in PrestationSPE.objects.all():
            prest.intervenants.add(self.user)

        exp = TestExporter()
        with patch('spe.views.ExportReporting', new=exp):
            self.client.post(reverse('export-prestation'), {'mois': '10', 'annee': self.year})
        sheet_name = f'En_cours_10.{self.year}'
        self.assertEqual(len(exp.sheets[sheet_name]), 3)
        col_idx = len(self.entetes_communs)
        self.assertEqual(
            [(line[2], line[3], line[col_idx]) for line in exp.sheets[sheet_name]],
            [('Nom', 'Prenom', 'H. Évaluation'),
             ('Haddock', 'Ursule', timedelta(hours=6, minutes=10)),
             ('Haddock', 'Toto', timedelta(hours=6, minutes=10)),
             ]
        )

    def test_export_nouvelle_famille(self):
        fam = self.create_famille_spe()
        fam.suivispe.date_demande = f'{self.year}-10-20'
        fam.suivispe.date_debut_suivi = f'{self.year}-10-21'
        fam.suivispe.motif_demande = ['integration']
        fam.suivispe.save()
        PrestationSPE.objects.bulk_create([
            PrestationSPE(auteur=self.user, famille=fam, date_prestation=date(self.year, 10, 21), duree='3:40'),
            PrestationSPE(auteur=self.user, famille=fam, date_prestation=date(self.year, 10, 22), duree='6:40'),
            # Not included in this month
            PrestationSPE(auteur=self.user, famille=fam, date_prestation=date(self.year, 11, 1), duree='2:00'),
        ])
        self.assertEqual(PrestationSPE.objects.filter(famille=fam).count(), 3)
        for p in PrestationSPE.objects.filter(famille=fam):
            p.intervenants.set([self.user])
        NiveauSPE.objects.create(famille=fam, niveau_interv=2, date_debut=date.today())

        exp = TestExporter()
        with patch('spe.views.ExportReporting', new=exp):
            self.client.post(reverse('export-prestation'), {'mois': '10', 'annee': self.year})
        sheet_name = f'Nouveaux_10.{self.year}'
        self.assertEqual(len(exp.sheets[sheet_name]), 2)
        self.assertEqual(
            exp.sheets[sheet_name][1],
            ['Croix-Rouge', 'SPE', 'Haddock', 'Toto', 'M', '16.02.2010', 'Château1', '2000', 'Moulinsart', 'NE',
             'Sybarnez Tina', '', '', 'Haddock', 'Archibald', 'Conjointe', 'Divorcé', '', 'NON', 1,
             f'20.10.{self.year}', '', 'Aide à l’intégration', f'21.10.{self.year}', 2]
        )

    def test_export_total_prestation_mois_courant(self):
        fam = self.create_famille_spe()
        fam.suivispe.date_demande = f'{self.year}-10-20'
        fam.suivispe.save()
        spe01 = LibellePrestation.objects.get(code='spe01')
        PrestationSPE.objects.bulk_create([
            PrestationSPE(
                auteur=self.user, famille=fam,
                date_prestation=date(self.year, 10, 21), lib_prestation=spe01, duree='3:40'
            ),
            PrestationSPE(
                auteur=self.user, famille=fam,
                date_prestation=date(self.year, 10, 22), lib_prestation=spe01, duree='6:40'
            ),
            # Not included in this month
            PrestationSPE(
                auteur=self.user, famille=fam,
                date_prestation=date(self.year, 11, 1), lib_prestation=spe01, duree='2:00'
            ),
        ])
        for p in PrestationSPE.objects.filter(famille=fam):
            p.intervenants.set([self.user])

        exp = TestExporter()
        with patch('spe.views.ExportReporting', new=exp):
            self.client.post(reverse('export-prestation'), {'mois': '10', 'annee': self.year})
        self.assertEqual(len(exp.sheets[f'En_cours_10.{self.year}']), 2)
        col_idx = len(self.entetes_communs)
        self.assertEqual(exp.sheets[f'En_cours_10.{self.year}'][1][col_idx], timedelta(hours=10, minutes=20))

    def test_export_prest_gen_spe(self):
        familles = [self.create_famille_spe('Fam_{}'.format(i)) for i in range(5)]
        for famille in familles:
            famille.suivispe.date_demande = date(self.year, 10, 1)
            famille.suivispe.save()
        prestation_data = {
            'auteur': self.user, 'famille': None, 'date_prestation': date(self.year, 10, 21),
            'lib_prestation': LibellePrestation.objects.get(code='spe03')
        }
        PrestationSPE.objects.create(**{**prestation_data, 'duree': '6:50'})
        PrestationSPE.objects.create(**{**prestation_data, 'duree': '3:50'})

        for prest in PrestationSPE.objects.all():
            prest.intervenants.add(self.user)

        exp = TestExporter()
        with patch('spe.views.ExportReporting', new=exp):
            self.client.post(reverse('export-prestation'), self.cfg_export_month)
        self.assertEqual(len(exp.sheets[self.cfg_sheet]), 6)
        self.assertEqual(format_duree(exp._total_spe['gen']), '10:40')
        # Les 10h40 sont divisées par le nombre d'enfants suivis (= 5 * 2h08):
        col_idx = len(self.entetes_communs) + 2
        self.assertEqual(exp.sheets[self.cfg_sheet][1][col_idx], timedelta(hours=2, minutes=8))
        self.assertEqual(exp.sheets[self.cfg_sheet][2][col_idx], timedelta(hours=2, minutes=8))
        self.assertEqual(exp.sheets[self.cfg_sheet][3][col_idx], timedelta(hours=2, minutes=8))
        self.assertEqual(exp.sheets[self.cfg_sheet][4][col_idx], timedelta(hours=2, minutes=8))
        self.assertEqual(exp.sheets[self.cfg_sheet][5][col_idx], timedelta(hours=2, minutes=8))

    def test_export_evaluation_spe(self):
        familles = [self.create_famille_spe('Fam_{}'.format(i)) for i in range(3)]
        for famille in familles:
            famille.suivispe.date_demande = date(self.year, 10, 1)
            famille.suivispe.save()
        prestation_data = {
            'auteur': self.user, 'date_prestation': date(self.year, 10, 21),
            'lib_prestation': LibellePrestation.objects.get(code='spe01'),
        }
        PrestationSPE.objects.bulk_create([
            PrestationSPE(**{**prestation_data, 'famille': familles[0], 'duree': '6:50'}),
            PrestationSPE(**{**prestation_data, 'famille': familles[0], 'duree': '1:00'}),
            PrestationSPE(**{**prestation_data, 'famille': familles[1], 'duree': '1:25'}),
            PrestationSPE(**{**prestation_data, 'famille': familles[2], 'duree': '4:40'}),
        ])
        for prest in PrestationSPE.objects.all():
            prest.intervenants.add(self.user)
        exp = TestExporter()
        with patch('spe.views.ExportReporting', new=exp):
            self.client.post(reverse('export-prestation'), self.cfg_export_month)
        sheet_name = exp.sheets[self.cfg_sheet]
        self.assertEqual(len(sheet_name), 4)
        col_idx = len(self.entetes_communs)
        self.assertEqual(sheet_name[1][col_idx], timedelta(hours=7, minutes=50))
        self.assertEqual(sheet_name[2][col_idx], timedelta(hours=1, minutes=25))
        self.assertEqual(sheet_name[3][col_idx], timedelta(hours=4, minutes=40))
        self.assertEqual(format_duree(exp._total_spe['eval']), '13:55')

    def test_export_suivi_spe(self):
        familles = [self.create_famille_spe('Fam_{}'.format(i)) for i in range(3)]
        for famille in familles:
            famille.suivispe.date_demande = date(self.year, 10, 1)
            famille.suivispe.save()
        prestation_data = dict(
            {'auteur': self.user, 'date_prestation': date(self.year, 10, 21),
             'lib_prestation': LibellePrestation.objects.get(code='spe02')}
        )
        PrestationSPE.objects.bulk_create([
            PrestationSPE(**{**prestation_data, 'famille': familles[0], 'duree': '1:10'}),
            PrestationSPE(**{**prestation_data, 'famille': familles[0], 'duree': '2:20'}),
            PrestationSPE(**{**prestation_data, 'famille': familles[1], 'duree': '3:30'}),
            PrestationSPE(**{**prestation_data, 'famille': familles[2], 'duree': '4:45'}),
        ])
        for prest in PrestationSPE.objects.all():
            prest.intervenants.add(self.user)
        exp = TestExporter()
        with patch('spe.views.ExportReporting', new=exp):
            self.client.post(reverse('export-prestation'), self.cfg_export_month)
        sheet_name = exp.sheets[self.cfg_sheet]
        self.assertEqual(len(sheet_name), 4)
        col_idx = len(self.entetes_communs) + 1
        self.assertEqual(sheet_name[1][col_idx], timedelta(hours=3, minutes=30))
        self.assertEqual(sheet_name[2][col_idx], timedelta(hours=3, minutes=30))
        self.assertEqual(sheet_name[3][col_idx], timedelta(hours=4, minutes=45))
        self.assertEqual(format_duree(exp._total_spe['suivi']), '11:45')

    def test_export_authorization(self):
        self.client.force_login(self.user_externe)
        # Test denied access without export_stats permission
        with self.assertLogs('django.request', level='WARNING'):
            response = self.client.get(reverse('export-prestation'))
        self.assertEqual(response.status_code, 403)

        self.user_externe.user_permissions.add(Permission.objects.get(codename='export_stats'))
        # Test default selected options in export form
        with patch('spe.views.date') as mock_date:
            mock_date.today.return_value = date(self.year, 7, 3)
            response = self.client.get(reverse('export-prestation'))
            self.assertContains(
                response,
                '<option value="6" selected>juin</option>'
            )
            self.assertContains(
                response,
                f'<option value="{self.year}" selected>{self.year}</option>'
            )

            mock_date.today.return_value = date(self.year, 1, 30)
            response = self.client.get(reverse('export-prestation'))
            self.assertContains(
                response,
                '<option value="12" selected>décembre</option>'
            )
            self.assertContains(
                response,
                f'<option value="{self.year - 1}" selected>{self.year - 1}</option>'
            )

        response = self.client.post(reverse('export-prestation'), data={'mois': '2', 'annee': self.year})
        self.assertEqual(response['Content-Type'], openxml_contenttype)
        self.assertEqual(
            response['Content-Disposition'],
            f'attachment; filename="croixrouge_reporting_02_{self.year}.xlsx"'
        )

    def test_openxml_export_sheets(self):
        # "Nouvelle" famille
        famille = FamilleSPE.objects.create_famille(
            equipe='littoral',
            nom='NouvelleFamille', rue='Château 4', npa=2000, localite='Moulinsart',
        )
        famille.suivispe.date_demande = f'{self.year}-02-10'
        # famille.suivispe.date_fin_suivi = '2019-05-30'
        famille.suivispe.save()
        Personne.objects.create_personne(
            famille=famille, role=Role.objects.get(nom='Enfant suivi'),
            nom='NouvelleFamille', prenom='Hégésipe', genre='M', date_naissance=date(1996, 2, 16)
        )

        # Famille "en cours"
        famille = FamilleSPE.objects.create_famille(
            equipe='littoral',
            nom='FamilleEnCours', rue='Château 5', npa=2000, localite='Moulinsart',
            autorite_parentale='conjointe', statut_marital='divorce', monoparentale=False,
        )
        famille.suivispe.date_demande = f'{self.year}-01-01'
        famille.suivispe.date_debut_suivi = f'{self.year}-01-28'
        ope_service = Service.objects.create(sigle="OPEC")
        famille.suivispe.ope_referent = Contact.objects.create(
            nom="Duplain", prenom="Irma", service=ope_service
        )
        famille.suivispe.save()
        Personne.objects.create_personne(
            famille=famille, role=Role.objects.get(nom='Enfant suivi'),
            nom='FamilleEnCours', prenom='Alain', genre='M', date_naissance=date(2003, 4, 23),
            localite='Moulinsart',
        )
        Personne.objects.create_personne(
            famille=famille, role=self.role_mere,
            nom='FamilleEnCours', prenom='Judith', genre='F', date_naissance=date(1974, 11, 2)
        )
        Personne.objects.create_personne(
            famille=famille, role=self.role_pere,
            nom='NomDuPere', prenom='Hans', genre='M', date_naissance=date(1968, 3, 13)
        )

        # Famille "terminée"
        famille = FamilleSPE.objects.create_famille(
            nom='FamilleTerminée', rue='Château 6', npa=2000, localite='Moulinsart',
        )
        famille.suivispe.date_demande = f'{self.year - 1}-10-02'
        famille.suivispe.date_debut_suivi = f'{self.year - 1}-11-11'
        famille.suivispe.date_fin_suivi = f'{self.year}-02-20'
        famille.suivispe.motif_fin_suivi = 'relai'
        famille.suivispe.save()
        Personne.objects.create_personne(
            famille=famille, role=self.enf_suivi,
            nom='FamilleTerminée', prenom='Jeanne', genre='F', date_naissance=date(1998, 12, 14)
        )

        self.user.user_permissions.add(Permission.objects.get(codename='export_stats'))
        self.client.force_login(self.user)
        exp = TestExporter()
        with patch('spe.views.ExportReporting', new=exp):
            self.client.post(reverse('export-prestation'), {'mois': '2', 'annee': self.year})

        self.assertEqual(len(exp.sheets[f'En_cours_02.{self.year}']), 4)
        self.assertEqual(exp.sheets[f'En_cours_02.{self.year}'][0], self.entetes_en_cours)
        self.assertEqual(
            exp.sheets[f'En_cours_02.{self.year}'][1],
            ['Croix-Rouge', 'SPE', 'FamilleEnCours', 'Alain', 'M', '23.04.2003', '', '', 'Moulinsart', 'NE',
             'Duplain Irma', 'FamilleEnCours', 'Judith', 'NomDuPere', 'Hans', 'Conjointe', 'Divorcé', '', 'NON', 1,
             f'01.01.{self.year}', '', '', f'28.01.{self.year}', '',
             timedelta(0), timedelta(0), timedelta(0)]
        )
        self.assertEqual(
            [(line[2], line[3]) for line in exp.sheets[f'En_cours_02.{self.year}'][1:]],
            [('FamilleEnCours', 'Alain'), ('FamilleTerminée', 'Jeanne'), ('NouvelleFamille', 'Hégésipe')]
        )

        self.assertEqual(len(exp.sheets[f'Nouveaux_02.{self.year}']), 2)
        self.assertEqual(exp.sheets[f'Nouveaux_02.{self.year}'][0], self.entetes_nouveaux)
        self.assertEqual(
            exp.sheets[f'Nouveaux_02.{self.year}'][1],
            ['Croix-Rouge', 'SPE', 'NouvelleFamille', 'Hégésipe', 'M', '16.02.1996', '', '', '', 'NE',
             '', '', '', '', '', '', '', '', '', 1,
             f'10.02.{self.year}', '', '', '', ''
             ]
        )
        self.assertEqual(len(exp.sheets[f'Terminés_02.{self.year}']), 2)
        self.assertEqual(exp.sheets[f'Terminés_02.{self.year}'][0], self.entetes_termines)
        self.assertEqual(
            exp.sheets[f'Terminés_02.{self.year}'][1],
            ['Croix-Rouge', 'SPE', 'FamilleTerminée', 'Jeanne', 'F', '14.12.1998', '', '', '', 'NE',
             '', '', '', '', '', '', '', '', '', 1,
             f'02.10.{self.year - 1}', '', '', f'11.11.{self.year - 1}', '',
             f'20.02.{self.year}', 'Relai vers autre service', '', timedelta(0)
             ]
        )

        famille = FamilleSPE.objects.get(nom='NouvelleFamille')
        famille.suivispe.motif_fin_suivi = 'erreur'
        famille.suivispe.date_fin_suivi = date(self.year, 2, 17)
        famille.suivispe.save()

        famille = FamilleSPE.objects.get(nom='FamilleTerminée')
        famille.suivispe.motif_fin_suivi = 'erreur'
        famille.suivispe.date_fin_suivi = date(self.year, 2, 17)
        famille.suivispe.save()

        exp = TestExporter()
        with patch('spe.views.ExportReporting', new=exp):
            self.client.post(reverse('export-prestation'), {'mois': '2', 'annee': self.year})
        # Plus de famille, puisque les motifs de fin de suivi "erreur" ne sont pas pris en compte
        self.assertEqual(len(exp.sheets[f'Nouveaux_02.{self.year}']), 1)
        self.assertEqual(len(exp.sheets[f'Terminés_02.{self.year}']), 1)
        # 'NouvelleFamille' plus exportée
        self.assertEqual(len(exp.sheets[f'En_cours_02.{self.year}']), 2)

    def test_total_prestations_spe(self):
        famille = self.create_famille_spe(name='Haddock')
        famille.suivispe.date_demande = f'{self.year}-01-01'
        famille.suivispe.date_debut_suivi = f'{self.year}-01-28'
        famille.suivispe.save()
        user = Utilisateur.objects.create(username='ld', prenom='Lise', nom="Duhaut")
        my_group = Group.objects.get(name='spe')
        my_group.user_set.add(user)
        prestation_data = {
            'auteur': self.user, 'famille': famille, 'date_prestation': date(self.year, 3, 1),
            'lib_prestation': LibellePrestation.objects.get(code='spe01'),
        }
        PrestationSPE.objects.bulk_create([
            PrestationSPE(**{**prestation_data, 'duree': '3:40'}),
            PrestationSPE(**{**prestation_data, 'duree': '6:40'}),
            # Not included in this month
            PrestationSPE(**{**prestation_data, 'date_prestation': date(self.year, 4, 1), 'duree': '2:00'}),
        ])
        for p in PrestationSPE.objects.filter(famille=famille):
            p.intervenants.set([user])

        self.assertEqual(format_duree(user.total_mensuel('spe', 3, self.year)), '10:20')
        self.assertEqual(format_duree(user.totaux_mensuels('spe', self.year)[3]), '02:00')
        self.assertEqual(format_duree(user.total_annuel('spe', self.year)), '12:20')
        totaux_annee = PrestationSPE.temps_totaux_mensuels(self.year)
        self.assertEqual(totaux_annee[0], timedelta())
        self.assertEqual(format_duree(totaux_annee[2]), '10:20')
        self.assertEqual(format_duree(totaux_annee[3]), '02:00')
        self.assertEqual(format_duree(PrestationSPE.temps_total_general(self.year)), '12:20')

        user.user_permissions.add(Permission.objects.get(codename='export_stats'))
        self.client.force_login(user)
        exp = TestExporter()
        with patch('spe.views.ExportReporting', new=exp):
            self.client.post(reverse('export-prestation'), {'mois': '3', 'annee': self.year})
        self.assertEqual(format_duree(exp._total_spe['eval']), '10:20')
