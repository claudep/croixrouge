from datetime import date, timedelta
from unittest import skip

from dateutil.relativedelta import relativedelta
from django.contrib.auth.models import Group, Permission
from django.test import TestCase
from django.urls import reverse
from freezegun import freeze_time

from croixrouge.models import LibellePrestation, Personne, Role, Utilisateur
from croixrouge.stat_utils import Month
from croixrouge.utils import format_duree

from ..models import FamilleSPE, NiveauSPE, PrestationSPE
from ..views_stats import StatistiquesView


class StatTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        Group.objects.create(name='spe')
        cls.user = Utilisateur.objects.create(
            username='me', first_name='Jean', last_name='Valjean',
        )
        cls.user_admin = Utilisateur.objects.create(username='admin')
        cls.user_admin.user_permissions.add(Permission.objects.get(codename='export_stats'))
        user_haut = Utilisateur.objects.create(username='ld', prenom='Lise', nom="Duhaut")
        user_haut.groups.add(Group.objects.get(name='spe'))
        user_bas = Utilisateur.objects.create(username='jd', prenom='Jean', nom="Dubas")
        user_bas.groups.add(Group.objects.get(name='spe'))

        role_referent = Role.objects.create(nom='Référent', famille=False)
        cls.enf_suivi = Role.objects.create(nom='Enfant suivi', famille=True)
        cls.role_pere = Role.objects.create(nom='Père', famille=True)
        cls.role_mere = Role.objects.create(nom='Mère', famille=True)
        cls.famille_litt = FamilleSPE.objects.create_famille(
            equipe='littoral',
            nom='Haddock', rue='Château1', npa=2000, localite='Moulinsart',
        )
        Personne.objects.create_personne(
            famille=cls.famille_litt, role=cls.enf_suivi,
            nom='Haddock', prenom='Archibald', genre='M', date_naissance=date(1996, 2, 16)
        )
        Personne.objects.create_personne(
            famille=cls.famille_litt, role=cls.enf_suivi,
            nom='Haddock', prenom='Honorine', genre='F', date_naissance=date(1999, 11, 2)
        )
        cls.famille_litt.suivispe.date_demande = '2019-01-01'
        cls.famille_litt.suivispe.intervenants.add(user_bas, through_defaults={'role': role_referent})
        cls.famille_litt.suivispe.save()

        cls.famille_mont = FamilleSPE.objects.create_famille(
            equipe='montagnes',
            nom='Tournesol', rue='Château1', npa=2000, localite='Moulinsart',
        )
        Personne.objects.create_personne(
            famille=cls.famille_mont, role=cls.enf_suivi,
            nom='Tournesol', prenom='Tryphon', genre='M', date_naissance=date(1991, 1, 3)
        )
        cls.famille_mont.suivispe.date_demande = '2019-01-01'
        cls.famille_mont.suivispe.intervenants.add(user_haut, through_defaults={'role': role_referent})
        cls.famille_mont.suivispe.save()
        LibellePrestation.objects.bulk_create([
            LibellePrestation(unite='spe', code='spe01', nom='Évaluation SPE'),
            LibellePrestation(unite='spe', code='spe04', nom='Activités ASE'),
        ])

    def test_accueil_statistiques(self):
        self.client.force_login(self.user_admin)
        response = self.client.get(reverse('stats'))
        self.assertContains(response, 'Statistiques du 1<sup>er</sup>')

    @freeze_time("2020-12-04")
    def test_accueil_statistiques_decembre(self):
        self.client.force_login(self.user_admin)
        response = self.client.get(reverse('stats'))
        self.assertEqual(response.context['date_form'].data['end_month'], 1)
        self.assertEqual(response.context['date_form'].data['end_year'], 2021)

    def test_temps_total_prestations(self):
        """
        Test Famille.temps_total_prestations()/temps_total_prestations_reparti(),
        Test Utilisateur.temps_total_prestations()
        """
        my_group = Group.objects.get(name='spe')
        my_group.user_set.add(self.user)
        self.assertEqual(self.famille_litt.temps_total_prestations(), timedelta(0))
        auj = date.today()
        mois_sui1 = date(auj.year, auj.month, 3)
        mois_sui2 = date(auj.year, auj.month, 4)
        user2 = Utilisateur.objects.create(
            username='you', first_name='Hans', last_name='Zwei',
        )
        my_group.user_set.add(user2)
        p1 = PrestationSPE.objects.create(
            auteur=self.user,
            famille=self.famille_litt,
            date_prestation=auj,
            duree='0:45'
        )
        p1.intervenants.set([self.user])
        p2 = PrestationSPE.objects.create(
            auteur=self.user,
            famille=self.famille_litt,
            date_prestation=mois_sui1,
            duree='1:0'
        )
        #  Chaque intervenant saisit ses propres prestations
        p2.intervenants.set([self.user])
        p3 = PrestationSPE.objects.create(
            auteur=self.user,
            famille=self.famille_litt,
            date_prestation=mois_sui2,
            duree='1:05'
        )
        p3.intervenants.set([user2])

        # Avec ce processus, la même prestation peut avoir des durées différentes!!!

        # p1 (00:45) + 2 interv. x p2 (1:00) >> p1(00:45) + p2(1:00) + p3(1:05) = 2:50
        self.assertEqual(self.famille_litt.temps_total_prestations(), timedelta(hours=2, minutes=50))
        self.assertEqual(self.famille_litt.temps_total_prestations_reparti(), timedelta(hours=1, minutes=25))
        # self.user = p1 (00:45) + p2 (1:00) = 1:45
        # user2 = p3(1:05)
        self.assertEqual(format_duree(self.user.temps_total_prestations('spe')), '01:45')
        self.assertEqual(format_duree(user2.temps_total_prestations('spe')), '01:05')

    def test_spe_stats(self):
        auj = date.today()
        mois_sui1 = date(auj.year, auj.month, 3)
        user_bas = Utilisateur.objects.get(nom='Dubas')
        p = PrestationSPE.objects.create(
            auteur=self.user,
            famille=self.famille_litt,
            date_prestation=auj,
            duree='0:45'
        )
        p.intervenants.set([user_bas])
        p = PrestationSPE.objects.create(
            auteur=self.user,
            famille=self.famille_litt,
            date_prestation=mois_sui1,
            duree='1:00'
        )
        p.intervenants.set([user_bas])
        p = PrestationSPE.objects.create(
            auteur=self.user,
            famille=self.famille_litt,
            date_prestation=auj,
            duree='0:00', manque=True,
        )
        # Cette famille est du littoral, mais n'a pas Lise Dubas comme référente
        FamilleSPE.objects.create_famille(
            equipe='littoral',
            nom='Tintin', rue='Château1', npa=2000, localite='Moulinsart',
        )
        months = [Month(date.today().year, date.today().month)]
        stats = StatistiquesView(
            date_start=date.today().replace(day=1),
            date_end=(date.today().replace(day=1) + timedelta(days=35)).replace(day=1),
        ).get_stats(months)
        self.assertEqual(stats['familles_spe']['familles_evaluees']['total'], 3)
        self.assertEqual(stats['familles_spe']['enfants_evalues']['total'], 3)
        self.assertEqual(stats['familles_spe']['rdv_manques']['total'], 1)

    def test_total_mensuel(self):
        """Test Famille.total_mensuel() method."""
        famille = FamilleSPE.objects.get(nom='Haddock')
        auj = date.today()
        mois_suiv = auj + timedelta(days=31)
        self.assertEqual(famille.total_mensuel(auj), timedelta(0))
        PrestationSPE.objects.bulk_create([
            PrestationSPE(
                auteur=self.user, famille=famille, date_prestation=date(auj.year, auj.month, 1), duree='1:30'
            ),
            PrestationSPE(
                auteur=self.user, famille=famille, date_prestation=date(auj.year, auj.month, 25), duree='0:15'
            ),
            # Not included in this month
            PrestationSPE(
                auteur=self.user, famille=famille, date_prestation=date(mois_suiv.year, mois_suiv.month, 1),
                duree='0:15'
            ),
        ])
        self.assertEqual(famille.total_mensuel(auj), timedelta(minutes=105))

    @freeze_time("2020-12-14")
    def test_affichage_prestation_spe(self):
        famille = FamilleSPE.objects.get(nom='Tournesol')
        auj = date.today()
        self.assertEqual(famille.total_mensuel(auj), timedelta(0))
        user_haut = Utilisateur.objects.get(nom='Duhaut')
        user_haut.taux_activite = 100.0
        user_haut.save()
        PrestationSPE.objects.bulk_create([
            PrestationSPE(auteur=user_haut, famille=famille,
                          date_prestation=date(auj.year, 3, 1), duree='1:45'),
            PrestationSPE(auteur=user_haut, famille=famille,
                          date_prestation=date(auj.year, 5, 25), duree='0:15'),
            # Not included in this month
            PrestationSPE(auteur=user_haut, famille=famille,
                          date_prestation=date(auj.year, 7, 1), duree='0:15'),
        ])
        for p in PrestationSPE.objects.filter(famille=famille):
            p.intervenants.set([user_haut])

        self.user.user_permissions.add(Permission.objects.get(codename='export_stats'))
        self.client.force_login(self.user)
        response = self.client.get(reverse('stats-prestation-spe'))
        data = response.context['intervenants'][user_haut]
        self.assertEqual(format_duree(data['heures_prestees'][2]), '01:45')
        self.assertEqual(format_duree(data['heures_prestees'][4]), '00:15')
        self.assertEqual(format_duree(data['heures_prestees'][6]), '00:15')

        self.assertEqual(format_duree(data['tot_prestees']), '02:15')
        self.assertEqual(format_duree(response.context['totaux_prest_mensuels'][Month(auj.year, 3)]['total']), '01:45')
        self.assertEqual(format_duree(response.context['total_gen']), '02:15')

    @skip("This statistic is currently deactivated")
    def test_stats_interv(self):
        last_year = date.today().year - 1
        famille = FamilleSPE.objects.get(nom='Haddock')
        famille.suivi.date_debut_suivi = date(last_year, 2, 1)
        famille.suivi.save()
        user_bas = Utilisateur.objects.get(nom='Dubas')
        self.client.force_login(self.user_admin)
        params = '&'.join(
            f'{key}={val}' for key, val in {
                'start_year': last_year, 'start_month': '3', 'end_year': last_year, 'end_month': '5'
            }.items()
        )
        response = self.client.get(reverse('stats-interv') + '?' + params)
        self.assertEqual(response.context['interv_spe'][user_bas]['num_familles']['total'], 1)

    def test_stats_par_ressources(self):
        auj = date.today()
        NiveauSPE.objects.create(
            famille=self.famille_litt, niveau_interv=2, date_debut=auj - timedelta(days=1)
        )
        # Niveau obsolète
        NiveauSPE.objects.create(
            famille=self.famille_litt, niveau_interv=1,
            date_debut=auj - timedelta(days=360), date_fin=auj - timedelta(days=100)
        )
        PrestationSPE.objects.create(
            famille=self.famille_litt, auteur=self.user, date_prestation=auj,
            lib_prestation=LibellePrestation.objects.get(code='spe04'),
            duree=timedelta(minutes=90)
        )

        ilya2mois = auj - timedelta(days=60)
        params = '&'.join(
            f'{key}={val}' for key, val in {
                'start_year': ilya2mois.year, 'start_month': ilya2mois.month,
                'end_year': auj.year, 'end_month': auj.month,
            }.items()
        )

        self.client.force_login(self.user_admin)
        response = self.client.get(reverse('stats-ressources') + '?' + params)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.context['stats'][2]['spe04'][Month.from_date(auj)], timedelta(minutes=90)
        )
        self.assertEqual(
            response.context['stats'][2]['spe04']['total'], timedelta(minutes=90)
        )

    def test_stats_par_localite(self):
        auj = date.today()
        ilya2mois = auj - relativedelta(months=2)
        params = '&'.join(
            f'{key}={val}' for key, val in {
                'start_year': ilya2mois.year, 'start_month': ilya2mois.month,
                'end_year': auj.year, 'end_month': auj.month,
            }.items()
        )
        self.client.force_login(self.user_admin)
        response = self.client.get(reverse('stats-localite') + '?' + params)
        self.assertEqual(
            response.context["localites_spe"]["2000 Moulinsart"][Month.from_date(auj)],
            3
        )

    def test_stats_par_niveaux(self):
        auj = date.today()
        ilya2mois = auj - relativedelta(months=2)
        params = '&'.join(
            f'{key}={val}' for key, val in {
                'start_year': ilya2mois.year, 'start_month': ilya2mois.month,
                'end_year': auj.year, 'end_month': auj.month,
            }.items()
        )
        self.client.force_login(self.user_admin)
        response = self.client.get(reverse('stats-niveaux') + '?' + params)
        self.assertEqual(
            list(response.context['niveaux_spe']['Non défini'].values()),
            [{"familles": 2, "enfants": 3}] * 4
        )
