import math
from datetime import time

from django import template
from django.template.defaultfilters import linebreaksbr
from django.templatetags.static import static
from django.utils.html import escape, format_html
from django.utils.safestring import mark_safe
from django.utils.timezone import localtime

register = template.Library()


@register.simple_tag
def mult(*args):
    return math.prod(args)


@register.filter
def get_events(events, day_div):
    return events['events'].get(day_div)


@register.filter
def is_hour(time_):
    return time_.minute == 0


@register.filter
def is_meal(time_):
    return (time(12, 0) <= time_ < time(13, 30)) or (time(18, 30) <= time_ < time(19, 30))


@register.filter
def as_hour_min(dtime):
    return str(localtime(dtime).time())[:5]


@register.filter
def before_icons(event):
    icons = []
    if event.category and 'tel' in event.category.name:
        icons.append('<span title="Rendez-vous par téléphone">📞</span>')
    elif event.category and 'dom' in event.category.name:
        icons.append('<span title="Rendez-vous à domicile">🏠</span>')
    if event.online_form_data:
        icon = static("img/globe-filled.svg") if event.famille else static("img/globe.svg")
        icons.append('<img src="%s" class="icon" title="Rendez-vous pris sur le Web">' % icon)
    return mark_safe(' '.join(icons))


@register.filter
def event_details(event):
    descr = linebreaksbr(escape(event.description))
    if event.famille:
        descr = format_html(
            '<br><i>Fam: </i><a href="{}">{}</a><br>{}',
            event.famille.suivi_url, event.famille, descr
        )
    elif event.online_form_data:
        data = event.online_form_data
        descr = format_html(
            '<div>{descr}</div>'
            '<div>[Web] Enfant: {nom} {prenom} ({ddn})</div>'
            '<div>{tel_email}</div>'
            '<div>{raison}</div>',
            descr=descr, nom=data.get('nom', '-'), prenom=data.get('prenom', '-'),
            ddn=data.get('date_naissance', '-'),
            tel_email=", ".join([txt for txt in [
                f"Téléphone: {data.get('telephone')}" if data.get('telephone') else '',
                f"Courriel: {data.get('email')}" if data.get('email') else '',
            ] if txt]),
            raison=f"Raison: {data.get('raison')}" if data.get('raison') else '',
        )
    return descr
