from django.urls import path, re_path

from . import views, views_public

urlpatterns = [
    # Public part
    path('rendez-vous/', views_public.AppointmentWizard.as_view(), name='agenda-public'),
    path('rendez-vous/<int:pk>/termine/', views_public.AppointmentDoneView.as_view(), name='agenda-public-termine'),
    path('rendez-vous/annuler/<signature>/', views_public.AppointmentCancelView.as_view(),
         name='agenda-public-annuler'),

    # Authenticated part
    path('', views.AgendaChoiceView.as_view(), name='agenda-choice'),
    re_path(r'^ipe/(?:(?P<year>[0-9]+)/(?P<week>[0-9]+)/)?$', views.IPEAgendasView.as_view(), name='agendas-ipe'),
    re_path(r'^centre/(?P<centre>[^/]+)/(?:(?P<year>[0-9]+)/(?P<week>[0-9]+)/)?$', views.CentreAgendaView.as_view(),
            name='agenda-centre'),
    path('jour/<int:year>/<int:month>/<int:day>/', views.DayAgendaView.as_view(), name='agenda-day'),
    path('<username>/add/<slug:typ>/', views.PersonAgendaEditView.as_view(), name='person-agenda-add'),
    path('<username>/<int:pk>/edit/', views.PersonAgendaEditView.as_view(), name='person-agenda-edit'),
    path('<username>/<int:pk>/delete/', views.PersonAgendaDeleteView.as_view(), name='person-agenda-delete'),
    path('<username>/<int:pk>/toggle/', views.PersonAgendaToggleOccurrence.as_view(), name='person-agenda-toggleocc'),
    path('centre/<centre_pk>/add/<slug:typ>/', views.CentreAgendaEditView.as_view(), name='centre-agenda-add'),
    # Must come late, otherwise optional year/week catched too early.
    re_path(r'^(?P<username>[^/]+)/(?:(?P<year>[0-9]+)/(?P<week>[0-9]+)/)?', views.PersonAgendaView.as_view(),
            name='person-agenda'),
]
