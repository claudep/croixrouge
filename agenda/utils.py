from contextlib import contextmanager

from django.core.mail import EmailMessage
from django.db import transaction
from django.db.transaction import get_connection
from django.urls import reverse
from django.utils.dateformat import format as django_format
from django.utils.timezone import localtime

from croixrouge.utils import send_sms

# PostgreSQL's LOCK Documentation:
# https://www.postgresql.org/docs/current/sql-lock.html
PG_LOCK_MODES = (
    'ACCESS SHARE',
    'ROW SHARE',
    'ROW EXCLUSIVE',
    'SHARE UPDATE EXCLUSIVE',
    'SHARE',
    'SHARE ROW EXCLUSIVE',
    'EXCLUSIVE',
    'ACCESS EXCLUSIVE',
)


@contextmanager
def lock_table(model, mode):
    if mode not in PG_LOCK_MODES:
        raise ValueError(f'{mode} is not a PostgreSQL supported lock mode.')
    with transaction.atomic():
        cursor = get_connection().cursor()
        cursor.execute(f'LOCK TABLE {model._meta.db_table} IN {mode} MODE')
        try:
            yield
        finally:
            cursor.close()


def send_confirmation_sms(event, num_tel):
    """Return False if the sms could not be sent."""
    # Try to remain below 160 chars
    start = django_format(localtime(event.start), 'l j F à H:i')
    if event.category.name == 'rendez-vous tel':
        text = f"Rendez-vous téléphonique Croix-Rouge confirmé: le {start}."
    elif event.category.name == 'rendez-vous dom':
        text = f"Rendez-vous Croix-Rouge confirmé: le {start} à votre domicile."
    else:
        centre = event.centre.nom
        if event.centre.rue:
            centre = f'{centre}, {event.centre.rue}'
        text = f"Rendez-vous Croix-Rouge confirmé: le {start} à {centre}."
    text += " Pour annuler: https://sap.croix-rouge-ne.ch" + reverse('agenda-public-annuler', args=[event.cancel_hash])
    return send_sms(
        text, num_tel,
        f"Une erreur est survenue lors de l’envoi du SMS pour le rendez-vous ID {event.pk}."
    )


def send_confirmation_email(event):
    """Return False if the email could not be sent."""
    centre = event.centre.nom
    if event.centre.rue:
        centre = f'{centre}, {event.centre.rue}'
    start = django_format(localtime(event.start), 'l j F à H:i')
    tel = ' téléphonique' if event.category.name == 'rendez-vous tel' else ''
    cancel_url = reverse('agenda-public-annuler', args=[event.cancel_hash])
    text = (
        f"Nous avons le plaisir de vous confirmer votre rendez-vous pour une consultation{tel} "
        f"petite enfance Croix-Rouge :\n"
        f"- le {start} à {centre}.\n"
        f"Lien d’annulation : https://sap.croix-rouge-ne.ch{cancel_url}\n\n"
        "À bientôt!"
    )
    email = EmailMessage(
        "Confirmation rendez-vous Croix-Rouge",
        text,
        to=[event.online_form_data['email']],
        reply_to=['contact@croix-rouge-ne.ch'],
    )
    try:
        email.send()
        return True
    except OSError:
        return False
