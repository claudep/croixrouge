from django.contrib import admin
from django.utils.safestring import mark_safe

from . import models
from .forms import EventCategoryForm


@admin.register(models.EventCategory)
class EventCategoryAdmin(admin.ModelAdmin):
    form = EventCategoryForm
    list_display = ['name', 'color']

    def color(self, obj):
        txt_color = 'white' if obj.default_color and models.color_is_dark(obj.default_color) else 'black'
        return mark_safe(
            f'<div style="background: {obj.default_color}; color: {txt_color};">{obj.default_color}</div>'
        )
    color.short_description = "Couleur par défaut"


@admin.register(models.Event)
class EventAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'person', 'category', 'par_web']
    list_filter = ['person', 'category']
    readonly_fields = ['last_modif']

    @admin.display(boolean=True)
    def par_web(self, obj):
        return obj.online_form_data is not None


admin.site.register(models.Occurrence)
admin.site.register(models.Rule)
