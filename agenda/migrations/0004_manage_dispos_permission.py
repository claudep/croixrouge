from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agenda', '0003_add_last_author_modif'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='eventcategory',
            options={
                'ordering': ['name'],
                'permissions': [('manage_dispos', 'Autorisé à gérer les événements de type disponibilité')],
                'verbose_name': 'Catégorie d’évènement',
                'verbose_name_plural': 'Catégories d’évènement',
            },
        ),
    ]
