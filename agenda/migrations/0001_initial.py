import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('croixrouge', '__first__'),
        ('cipe', '__first__'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='EventCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, verbose_name='Nom')),
                ('default_color', models.CharField(blank=True, max_length=7, verbose_name='Couleur par défaut')),
            ],
            options={
                'verbose_name': 'Catégorie d’évènement',
                'verbose_name_plural': 'Catégories d’évènement',
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='Rule',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=40, verbose_name='Nom')),
                ('description', models.TextField(verbose_name='Description')),
                ('frequency', models.CharField(choices=[('YEARLY', 'Chaque année'), ('MONTHLY', 'Chaque mois'), ('WEEKLY', 'Chaque semaine'), ('DAILY', 'Chaque jour'), ('HOURLY', 'Chaque heure'), ('MINUTELY', 'Chaque minute'), ('SECONDLY', 'Chaque seconde')], max_length=10, verbose_name='Fréquence')),
                ('params', models.TextField(blank=True, verbose_name='Params')),
            ],
            options={
                'verbose_name': 'Règle',
                'verbose_name_plural': 'Règles',
            },
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start', models.DateTimeField(db_index=True, verbose_name='Début')),
                ('end', models.DateTimeField(blank=True, db_index=True, null=True, verbose_name='Fin')),
                ('title', models.CharField(max_length=255, verbose_name='Titre')),
                ('description', models.TextField(blank=True, verbose_name='Description')),
                ('color', models.CharField(blank=True, max_length=7, verbose_name='Couleur')),
                ('rule', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='agenda.rule', verbose_name='Règle')),
                ('end_recurring_period', models.DateTimeField(blank=True, db_index=True, null=True, verbose_name='Dernière occurrence')),
                ('centre', models.ForeignKey(
                    blank=True, limit_choices_to={'cipe': True}, null=True, on_delete=django.db.models.deletion.PROTECT,
                    to='croixrouge.region', verbose_name='Centre'
                )),
                ('category', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='agenda.eventcategory', verbose_name='Catégorie')),
                ('person', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='events', to=settings.AUTH_USER_MODEL)),
                ('famille', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='cipe.famillecipe')),
            ],
            options={
                'verbose_name': 'Évènement',
                'verbose_name_plural': 'Évènements',
            },
        ),
        migrations.CreateModel(
            name='Occurrence',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('orig_start', models.DateTimeField(verbose_name='Début d’origine')),
                ('cancelled', models.BooleanField(default=False)),
                ('event', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='exceptions', to='agenda.event')),
            ],
        ),
        migrations.CreateModel(
            name='MemOccurrence',
            fields=[
            ],
            options={
                'proxy': True,
                'indexes': [],
                'constraints': [],
            },
            bases=('agenda.event',),
        ),
    ]
