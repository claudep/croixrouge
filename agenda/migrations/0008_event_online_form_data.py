from django.core.serializers.json import DjangoJSONEncoder
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agenda', '0007_dom_tel_categories'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='online_form_data',
            field=models.JSONField(blank=True, encoder=DjangoJSONEncoder, null=True),
        ),
    ]
