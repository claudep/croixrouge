from ipaddress import IPv4Network
from pathlib import Path

from django.contrib.messages import constants as messages

BASE_DIR = Path(__file__).resolve().parent.parent

DEBUG = False

ALLOWED_HOSTS = ['localhost']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'croixrouge',
    }
}
DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

INSTALLED_APPS = [
    'dal',  # dal: django-auto-complete
    'dal_select2',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.postgres',

    'city_ch_autocomplete',
    'django_countries',
    'django_otp',
    'django_otp.plugins.otp_totp',
    'django_otp.plugins.otp_static',
    'two_factor',

    'tinymce',

    'croixrouge',
    'spe',
    'sof',
    'cipe',
    'amf',
    'agenda',
    'archive',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.http.ConditionalGetMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django_otp.middleware.OTPMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'common.middleware.LoginRequiredMiddleware',
]

ROOT_URLCONF = 'common.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            BASE_DIR / 'templates',
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.request',
                'croixrouge.context_processors.current_app',
            ],
            'libraries': {
                'my_tags': 'croixrouge.templatetags.my_tags',
            },
            'builtins': [
                'croixrouge.templatetags.my_tags', 'django.templatetags.static',
            ],
        },

    },
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'NAME': 'django-text',
        'APP_DIRS': True,
        'OPTIONS': {
            'autoescape': False,
        },
    },
]

SESSION_ENGINE = 'django.contrib.sessions.backends.signed_cookies'

WSGI_APPLICATION = 'common.wsgi.application'


# Password validation
# https://docs.djangoproject.com/en/2.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

AUTH_USER_MODEL = 'croixrouge.Utilisateur'

LOGIN_URL = 'two_factor:login'
LOGIN_REDIRECT_URL = '/'
EXEMPT_2FA_NETWORKS = [IPv4Network('127.0.0.0/30')]
# These users should not have TOTP devices configured.
EXEMPT_2FA_USERS = []

# Internationalization
# https://docs.djangoproject.com/en/2.1/topics/i18n/

LANGUAGE_CODE = 'fr'
LANGUAGES = [('fr', 'Français')]

TIME_ZONE = 'Europe/Zurich'

USE_I18N = True

USE_TZ = True

FORMAT_MODULE_PATH = ['common.formats']

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.1/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = BASE_DIR / 'static'

MEDIA_URL = '/media/'
MEDIA_ROOT = BASE_DIR / 'media'

SESSION_EXPIRE_AT_BROWSER_CLOSE = True

DEFAULT_FROM_EMAIL = 'contact@croix-rouge-ne.ch'

# For django-countries
COUNTRIES_FIRST = ['CH']
# https://github.com/SmileyChris/django-countries/issues/314
COUNTRIES_OVERRIDE = {
    "XK": {"name": "Kosovo", "ioc_code": "KOS"},
}

TINYMCE_DEFAULT_CONFIG = {
    'autosave_interval': '10s',
    'autosave_retention': '240m',
    'height': 360,
    'width': '100%',
    'cleanup_on_startup': True,
    'entity_encoding': 'raw',
    'custom_undo_redo_levels': 20,
    'browser_spellcheck': True,
    'theme': 'silver',
    'plugins': 'autosave,lists',
    'toolbar1': 'preview bold italic underline bullist | undo redo restoredraft',
    'menubar': False,
    'statusbar': True,
    # Modern way is to use text-decoration: underline, however it is harder to clean
    # with sanitizer than simpler <u>.
    'formats': {
        'underline': {'inline': 'u', 'exact': True}
    },
}

MESSAGE_TAGS = {
    messages.DEBUG: 'alert-info',
    messages.INFO: 'alert-info',
    messages.SUCCESS: 'alert-success',
    messages.WARNING: 'alert-warning',
    messages.ERROR: 'alert-danger',
}

SMSUP_SEND_URL = "https://api.smsup.ch/send"
SMSUP_API_TOKEN = ''
CRNE_RSA_PUBLIC_KEY = ''


# Les valeurs "POST_API_*" doivent être renseignées dans settings/__init__.py (non suivi dans le dépôt de code)
POST_API_USER = ""
POST_API_PASSWORD = ""


ENVOI_EMAIL_TEST = []  # Liste d'emails à utiliser pour des envois tests
ENVOI_SMS_NUMEROS_TEST = []  # Liste de numéro à utiliser pour des envois tests

AMF_EMAIL = "amf@example.com"
AMF_TELEPHONE = "032 000 00 00"
