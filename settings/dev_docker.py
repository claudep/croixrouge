import os
from ipaddress import IPv4Network

import dj_database_url

from settings.croixrouge import *  # NOQA

ALLOWED_HOSTS = os.environ["ALLOWED_HOSTS"].split(",")
DATABASES = {"default": dj_database_url.parse(os.environ["DATABASE_URL"])}
DEBUG = True
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = "mailhog"
EMAIL_PORT = 1025
EXEMPT_2FA_NETWORKS = [IPv4Network('0.0.0.0/0')]
SECRET_KEY = "no secret here"
