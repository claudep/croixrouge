from settings.croixrouge import *  # noqa

SECRET_KEY = 'ForAutomatedTestsC9g'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'ci_test',
        'USER': 'runner',
        'PASSWORD': '',
        'HOST': 'postgres',
        'PORT': '5432',
    }
}

ENVOI_EMAIL_TEST = ["test@test.com"]
ENVOI_SMS_NUMEROS_TEST = ["079 000 00 00"]
