from datetime import timedelta

from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.core.management.base import BaseCommand
from django.template.loader import render_to_string

from common.utils import maintenant
from croixrouge.models import Personne, Role
from croixrouge.utils import send_sms

from ...models import FamilleAMF, Formation


class Command(BaseCommand):
    DECLENCHEMENT_JOURS = 2  # notifications envoyées X jours avant la formation

    help = "Envoie un rappel aux familles inscrites pour les formations dans X jours"

    def add_arguments(self, parser):
        parser.add_argument("--envoi-reel", action="store_true", help="Si omis, envoie à des numéros tests")

    def get_telephones_famille(self, famille):
        tel = list(set(  # "set" évite les doublons
            p.telephone for p in Personne.objects.filter(
                famille__in=FamilleAMF.objects.filter(suiviamf__date_fin_accueil__isnull=True),
                role=Role.objects.get_parent_famille_accueil(),
            ).distinct() if p.telephone
        ))
        if famille.telephone and famille.telephone not in tel:
            tel.append(famille.telephone)
        return tel

    def handle(self, *args, **options):
        mode_test = not options["envoi_reel"] if "envoi_reel" in options else False
        if mode_test and not settings.ENVOI_SMS_NUMEROS_TEST:
            raise ImproperlyConfigured("Valeur manquante dans les settings: ENVOI_SMS_NUMEROS_TEST")

        formations = Formation.objects.filter(
            date=(maintenant() + timedelta(days=self.DECLENCHEMENT_JOURS)).date(),
        ).order_by("date")
        if not formations:
            self.stdout.write(f"Pas de formation dans {self.DECLENCHEMENT_JOURS} jours")
            return

        for formation in formations:
            telephones = list(set(  # "set" évite les doublons
                [t for p in formation.participants.all() for t in self.get_telephones_famille(p)]
            )) if not mode_test else settings.ENVOI_SMS_NUMEROS_TEST
            message = render_to_string("amf/sms/notification_sms.txt", context={"formation": formation})
            sent = 0
            for tel in telephones:
                msg = (
                    f"Erreur d’envoi de la notification SMS pour « {tel} » concernant "
                    f"la formation « {formation} ({formation.id}) »."
                )
                sent += 1 if send_sms(message, tel, msg) else 0
            self.stdout.write(
                self.style.SUCCESS(f"{sent}/{len(telephones)} envoyés pour la formation « {formation} »")
            )
