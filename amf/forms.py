from city_ch_autocomplete.forms import CityChField, CityChMixin
from dal import autocomplete
from django import forms
from django.urls import reverse
from tinymce.widgets import TinyMCE

from croixrouge.forms import (
    BootstrapMixin,
    FamilleFormBase,
    PersonneForm,
    PickDateWidget,
    PrestationFormBase,
    ReadOnlyableMixin,
)
from croixrouge.models import LibellePrestation, Personne
from croixrouge.utils import XLSXFile

from .models import FamilleAMF, Formation, PrestationAMF, SuiviAMF


class FamilleFilterForm(forms.Form):
    nom = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Nom de famille…', 'autocomplete': 'off',
                   'class': 'form-control form-control-sm inline'}),
        required=False
    )

    def filter(self, familles):
        if self.cleaned_data['nom']:
            familles = familles.filter(nom__istartswith=self.cleaned_data['nom'])
        return familles


class FamilleUpdateForm(FamilleFormBase):
    class Meta:
        model = FamilleAMF
        fields = ['nom', 'rue', 'npa', 'localite', 'telephone', 'region', 'autorite_parentale',
                  'monoparentale', 'statut_marital', 'garde', 'statut_financier', 'remarques']


class FamilleCreateForm(CityChMixin, FamilleUpdateForm):
    typ = 'amf'
    city_auto = CityChField(required=False)
    postal_code_model_field = 'npa'
    city_model_field = 'localite'
    field_order = ['nom', 'rue', 'city_auto']

    class Meta(FamilleUpdateForm.Meta):
        exclude = ['typ', 'destination']

    def save(self, **kwargs):
        famille = self._meta.model.objects.create_famille(**self.instance.__dict__)
        return famille


class EnfantAmfForm(PersonneForm):
    class Meta:
        model = Personne
        fields = (
            'role', 'nom', 'prenom', 'date_naissance', 'genre',
            'rue', 'npa', 'localite', 'telephone',
            'email', 'pays_origine', 'decedee',
            'allergies', 'remarque', 'remarque_privee',
            'amf_debut_de_placement', 'amf_fin_de_placement',
            'amf_type_accueil', 'amf_provenance', 'amf_provenance_details'
        )
        widgets = {
            'date_naissance': PickDateWidget,
            'amf_debut_de_placement': PickDateWidget,
            'amf_fin_de_placement': PickDateWidget,
        }
        labels = {
            'remarque_privee': 'Remarque privée (pas imprimée)',
        }

class PrestationAMFForm(PrestationFormBase):
    class Meta(PrestationFormBase.Meta):
        model = PrestationAMF
        fields = ['lib_prestation', 'date_prestation', 'duree', 'texte', 'fichier']
        widgets = {
            'date_prestation': PickDateWidget,
            'lib_prestation': forms.Select,
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields.values():
            if field.widget.__class__ not in [TinyMCE, forms.ClearableFileInput]:
                field.widget.attrs["class"] += " w-auto d-inline-block"

        self.fields["lib_prestation"].choices = [
            (lib.id, lib.nom) for lib in LibellePrestation.objects.filter(unite='amf')
        ]


class SuiviAMFForm(ReadOnlyableMixin, BootstrapMixin, forms.ModelForm):
    class Meta:
        model = SuiviAMF
        fields = [
            'date_contact1', 'date_entretien1', 'date_demande_spaj', 'autorisation',
            'date_autorisation', 'date_fin_accueil', 'referent_famille',
            'places_accueil_dispo', 'enfants_souhaites',
        ]
        widgets = {
            'date_contact1': PickDateWidget,
            'date_entretien1': PickDateWidget,
            'date_demande_spaj': PickDateWidget,
            'date_autorisation': PickDateWidget,
            'date_fin_accueil': PickDateWidget,
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields.values():
            field.widget.attrs["class"] += " w-auto"


class ImportationSPAJForm(forms.Form):
    CLEAN_VALUE_EXCEPT_FIELDS = [
        "Famille d'accueil",
        "Prénoms de la famille",
        "Téléphone de la famille",
        "Courriel de la famille",
    ]
    MANDATORY_HEADERS = [
        "Nom de l'enfant", "Prénom de l'enfant", "NPA Localité", "Téléphone de la famille",
        "Courriel de la famille", "Famille d'accueil", "Adresse de la famille",
        "Prénoms de la famille", "Statut de la famille", "CE",
        "Places d'accueil disponibles", "Enfants souhaités", "Année de naissance",
        "Début de placement", "Fin de placement", "Désignation", "Provenance",
        "AS OPE", "Remarques",
    ]

    fichier = forms.FileField(
        label="Fichier à importer",
        widget=forms.FileInput(attrs={'class': 'form-control'}),
    )

    def clean(self):
        cleaned_data = super().clean()
        filename = cleaned_data['fichier']
        if filename.name[-5:].lower() != '.xlsx':
            self.add_error('fichier', "Le fichier ne possède pas l'extension .xlsx")
        cleaned_data['fichier'] = XLSXFile(filename)
        self.headers = [clean_value(val) for val in next(cleaned_data["fichier"].lines)[:23]]
        if missing := set(self.MANDATORY_HEADERS) - set(self.headers):
            self.add_error(
                "fichier", f"Les en-têtes suivants manquent dans le fichier: {', '.join(sorted(missing))}"
            )
        return cleaned_data

    def famille_lines(self):
        for idx, line in enumerate(self.cleaned_data['fichier'].lines):
            if idx == 0:
                continue
            if all(val is None for val in line[:len(self.headers)]):
                # Ligne complètement vide, arrêter.
                return
            data = dict(zip(self.headers, line[:len(self.headers)]))
            for key, val in data.items():
                if key not in self.CLEAN_VALUE_EXCEPT_FIELDS:
                    data[key] = clean_value(val)
            data["_num_ligne"] = idx + 1
            yield data


class FormationEditForm(BootstrapMixin, forms.ModelForm):
    class Meta:
        fields = [
            'nom',
            'description',
            'date',
            'heure_debut',
            'heure_fin',
            'information_formateurs',
            'lieu',
            'information_delais_inscription',
        ]
        widgets = {
            'date': PickDateWidget,
            'information_formateurs': forms.TextInput(
                attrs={"placeholder": "exemple: avec Pierre Robert, formateur spécialisé"}
            ),
            'information_delais_inscription': forms.TextInput(
                attrs={"placeholder": "exemple: Inscription obligatoire jusqu'au ..."}
            ),
            'lieu': forms.TextInput(
                attrs={
                    "placeholder": (
                        "exemple: CRNE, Avenue du Premier-Mars 2a, 2000 Neuchâtel, salle de formation du 1er étage"
                    ),
                }
            ),
        }
        model = Formation


class FormationInscriptionForm(FormationEditForm):
    class Meta:
        fields = ['participants']
        model = Formation
        widgets = {
            'participants': autocomplete.ModelSelect2Multiple(
                url='',  # Doit être renseigné dans __init__ pour avoir l'url avec le pk de la formation
                attrs={
                    'data-minimum-input-length': 3,
                    'data-allow-clear': 'false',
                    'data-width': '100%',
                },
            ),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["participants"].widget.url = reverse('amf-inscription-autocomplete', args=[self.instance.pk])
        self.fields["participants"].queryset = self.instance.participants_potentiels


class FormationsFilterForm(forms.Form):
    nom = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'placeholder': 'Formation',
                'autocomplete': 'off',
                'class': 'form-control form-control-sm inline'
            }
        ),
        required=False
    )

    def filter(self, formations):
        if self.cleaned_data['nom']:
            formations = formations.filter(nom__icontains=self.cleaned_data['nom'])
        return formations


def clean_value(val):
    if isinstance(val, str):
        return val.replace("\n", " ").strip()
    return val
