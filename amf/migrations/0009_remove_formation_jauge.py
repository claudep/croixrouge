from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('amf', '0008_alter_formation_options_remove_formation_heure_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='formation',
            name='jauge',
        ),
    ]
