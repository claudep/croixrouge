from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('croixrouge', '0050_utilisateur_signature'),
    ]

    operations = [
        migrations.CreateModel(
            name='FamilleAMF',
            fields=[
            ],
            options={
                'permissions': {('can_archiveamf', 'Archiver les dossiers AMF')},
                'proxy': True,
                'indexes': [],
                'constraints': [],
            },
            bases=('croixrouge.famille',),
        ),
        migrations.CreateModel(
            name='SuiviAMF',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_entretien1', models.DateField(blank=True, null=True, verbose_name='Date premier entretien')),
                ('date_demande_spaj', models.DateField(blank=True, null=True, verbose_name='Date dépôt de demande au SPAJ')),
                ('autorisation', models.CharField(blank=True, choices=[('encours', 'En cours'), ('ok', 'Approuvée'), ('refus', 'Refusée')], max_length=10, verbose_name='Statut d’autorisation')),
                ('date_autorisation', models.DateField(blank=True, null=True, verbose_name='Date d’autorisation')),
                ('date_fin_accueil', models.DateField(blank=True, null=True, verbose_name='Date de fin d’accueil')),
                ('famille', models.OneToOneField(on_delete=models.deletion.CASCADE, to='amf.familleamf')),
            ],
        ),
    ]
