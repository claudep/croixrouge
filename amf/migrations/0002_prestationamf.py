import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('amf', '0001_initial'),
        ('croixrouge', '0050_utilisateur_signature'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='PrestationAMF',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_prestation', models.DateField(verbose_name='date de l’intervention')),
                ('duree', models.DurationField(verbose_name='durée')),
                ('familles_actives', models.PositiveSmallIntegerField(blank=True, default=0)),
                ('texte', models.TextField(blank=True, verbose_name='Contenu')),
                ('manque', models.BooleanField(default=False, verbose_name='Rendez-vous manqué')),
                ('fichier', models.FileField(blank=True, upload_to='prestations', verbose_name='Fichier/image')),
                ('auteur', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='auteur')),
                ('famille', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='prestations_amf', to='amf.familleamf', verbose_name='Famille')),
                ('lib_prestation', models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='prestations_%(app_label)s', to='croixrouge.libelleprestation')),
            ],
            options={
                'ordering': ('-date_prestation',),
                'permissions': (('edit_prest_prev_month', 'Modifier prestations du mois précédent'),),
                'abstract': False,
            },
        ),
    ]
