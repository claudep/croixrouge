from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('amf', '0002_prestationamf'),
    ]

    operations = [
        migrations.AlterField(
            model_name='suiviamf',
            name='autorisation',
            field=models.CharField(blank=True, choices=[('en_cours', 'En cours'), ('autorisee', 'Autorisée'), ('rec_smi', 'Recours SMI'), ('cessation', 'Cessation')], max_length=10, verbose_name='Statut d’autorisation'),
        ),
    ]
