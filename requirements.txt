city-ch-autocomplete>=0.3
Django>=5.1,<5.2
django-autocomplete-light==3.9.4
psycopg
reportlab
openpyxl==3.1.5
django-countries==7.6
django-tinymce==3.7.1
django-otp>=1.0.2
phonenumberslite
django-two-factor-auth==1.17.0
django-formtools  # required for public agenda form
workalendar==17.0.0
requests==2.31.0
nh3
html5lib
beautifulsoup4
pypdf==4.3.1
freezegun  # for tests
matplotlib==3.9.0
extract_msg==0.41.2
