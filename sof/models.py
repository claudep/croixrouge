from collections import OrderedDict
from datetime import date, timedelta

from django.db import models
from django.db.models import Q
from django.urls import reverse
from django.utils import timezone
from django.utils.functional import cached_property

from common import choices
from common.fields import ChoiceArrayField
from croixrouge.models import (
    Contact,
    EtapeBase,
    Famille,
    Personne,
    PrestationBase,
    Rapport,
    Role,
    Utilisateur,
)
from croixrouge.utils import format_d_m_Y, random_string_generator

SOF_MOTIFS_FIN_SUIVI_CHOICES = (
    ("desengagement", "Désengagement"),
    ("evol_positive", "Autonomie familiale"),
    ("non_aboutie", "Demande non aboutie"),
    ("non_dispo", "Pas de disponibilités/place"),
    ("erreur", "Erreur de saisie"),
    ("curatelle", "Demande de curatelle"),
    ("spe", "Suivi poursuivi sous SPE"),
    ("autre_svc", "Dirigé vers un autre service"),
)


class FamilleSOFManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(
            suivisof__isnull=False
        ).prefetch_related(models.Prefetch('membres', queryset=Personne.objects.select_related('role')))

    def create_famille(self, **kwargs):
        kwargs.pop('_state', None)
        kwargs['typ'] = ['sof']
        famille = self.create(**kwargs)
        SuiviSOF.objects.create(
            famille=famille,
            date_demande=date.today(),
        )
        return famille


class FamilleSOF(Famille):
    class Meta:
        proxy = True
        permissions = {
            ("can_archivesof", "Archiver les dossiers SOF"),
        }

    default_typ = 'sof'
    suivi_name = 'suivisof'
    objects = FamilleSOFManager()

    @classmethod
    def actives(cls, date=None):
        qs = FamilleSOF.objects.filter(suivisof__isnull=False).order_by('nom', 'npa')
        if date is not None:
            return qs.filter(suivisof__date_demande__lte=date).filter(
                models.Q(suivisof__date_fin_suivi__isnull=True) |
                models.Q(suivisof__date_fin_suivi__gte=date)
            )
        else:
            return qs.filter(suivisof__date_fin_suivi__isnull=True)

    @property
    def prestations(self):
        return self.prestations_sof

    def interventions_actives(self, dt=None):
        dt = dt or date.today()
        return self.suivi.intervenantsof_set.filter(
            Q(date_debut__lte=dt) & (
                Q(date_fin__isnull=True) | Q(date_fin__gte=dt)
            )
        ).select_related('intervenant', 'role')

    @property
    def print_coords_url(self):
        # May be removed once we implement a 'sof-print-coord-famille' url
        return reverse('sof-print-coord-famille', args=[self.pk])

    @property
    def suivi_url(self):
        return reverse('sof-famille-suivi', args=[self.pk])

    @property
    def edit_url(self):
        return reverse('sof-famille-edit', args=[self.pk])

    def can_be_archived(self, user):
        if self.archived_at:
            return False
        closed_since = date.today() - self.suivi.date_fin_suivi
        return (
            user.has_perm('sof.can_archivesof') and
            self.suivi.date_fin_suivi is not None and
            closed_since.days > 180 and
            self.suivi.date_fin_suivi.year < date.today().year and
            (date.today().month > 1 or closed_since.days > 400)
        )

    def total_mensuel_suivi(self, date_debut_mois):
        return self.total_mensuel_par_prestation(['sof02'], date_debut_mois)

    def anonymiser(self):
        # Famille
        self.nom = random_string_generator()
        self.rue = ''
        self.telephone = ''
        self.remarques = ''
        self.archived_at = timezone.now()
        self.save()

        # Personne
        for personne in self.membres.all().select_related('role'):
            if personne.role.nom == 'Enfant suivi':
                personne.nom = random_string_generator()
                personne.prenom = random_string_generator()
                personne.validite = None
                personne.contractant = False
                fields = ['rue', 'npa', 'localite', 'telephone', 'email', 'remarque', 'remarque_privee',
                          'profession', 'filiation', 'allergies', 'employeur', 'permis', 'animaux']
                [setattr(personne, field, '') for field in fields]
                personne.save()
                personne.reseaux.clear()
                if hasattr(personne, 'formation'):
                    personne.formation.cercle_scolaire = None
                    fields = ['college', 'classe', 'enseignant', 'creche', 'creche_resp', 'entreprise',
                              'maitre_apprentissage', 'remarque']
                    [setattr(personne.formation, field, '') for field in fields]
                    personne.formation.save()
            else:
                personne.delete()
        # Suivi
        fields = ['difficultes', 'aides', 'competences', 'autres_contacts', 'disponibilites',
                  'remarque_privee', 'motif_demande', 'referent_note',
                  'pers_famille_presentes', 'ref_presents', 'autres_pers_presentes']
        [setattr(self.suivi, field, '') for field in fields]
        self.suivi.save()

        # Related
        for doc in self.documents.all():
            doc.delete()
        for bilan in self.bilans_sof.all():
            bilan.delete()
        for rapport in self.rapports_sof.all():
            rapport.delete()
        self.prestations_sof.all().update(texte='')
        for prestation in self.prestations_sof.exclude(fichier=''):
            prestation.fichier.delete()
            prestation.save()


class _FamilleSOF:
    def __init__(self, famille):
        self.famille = famille
        self.famille.__class__ = FamilleSOF  # Maybe removed at some point?
        self.suivi = famille.suivisof

    def access_ok(self, user):
        if user.has_perm('sof.change_famillesof') or user.is_responsable:
            return True
        intervs = self.famille.interventions_actives()
        return not intervs or user in [interv.intervenant for interv in intervs]

    def can_view(self, user):
        return user.has_perm('sof.view_famillesof')

    def can_edit(self, user):
        if user.has_perm('sof.change_famillesof') or user.is_responsable:
            return True
        if not self.suivi.date_fin_suivi or self.suivi.date_fin_suivi >= date.today():
            intervs = self.famille.interventions_actives()
            if (
                self.can_view(user) and
                (not intervs or user in [
                    interv.intervenant for interv in intervs if interv.role.editeur
                ])
            ):
                return True
        return False

    def can_be_deleted(self, user):
        if not user.has_perm('sof.change_famillesof'):
            return False
        if self.suivi.motif_fin_suivi == 'erreur':
            return True
        return False

Famille.typ_register['sof'] = _FamilleSOF


class Bilan(models.Model):
    famille = models.ForeignKey(FamilleSOF, related_name='bilans_sof', on_delete=models.CASCADE)
    date = models.DateField("Date du bilan")
    auteur = models.ForeignKey(
        Utilisateur, related_name='bilans_sof', on_delete=models.PROTECT, null=True,
    )
    objectifs = models.TextField("Objectifs")
    rythme = models.TextField("Rythme et fréquence")
    sig_famille = models.BooleanField("Apposer signature de la famille", default=True)
    sig_interv = models.ManyToManyField(
        Utilisateur, blank=True, related_name='+', verbose_name="Signature des intervenants"
    )
    fichier = models.FileField("Fichier/image", blank=True, upload_to='bilans')

    delete_urlname = 'famille-bilansof-delete'

    def __str__(self):
        return "Bilan du {} pour la famille {}".format(self.date, self.famille)

    def get_absolute_url(self):
        return reverse('famille-bilansof-view', args=[self.famille_id, self.pk])

    def get_print_url(self):
        return reverse('sof-print-bilan', args=[self.pk])

    def edit_url(self):
        return reverse('famille-bilansof-edit', args=[self.famille_id, self.pk])

    def can_edit(self, user):
        return self.famille.can_edit(user) or user in self.famille.suivi.intervenants.all()

    def title(self):
        return f"Bilan du {format_d_m_Y(self.date)}"


class Etape(EtapeBase):
    pass


class EtapeBilan(Etape):
    delai_standard = 3 * 30  # 3 mois

    def date(self, suivi):
        """Date du dernier bilan"""
        if self.etape_suivante(suivi).code == 'bilan_suivant':
            return None
        return suivi.date_dernier_bilan()

    def etape_suivante(self, suivi):
        # Soit bilan suivant, soit rapport
        if suivi.date_dernier_rapport():
            return self.suivi_model.WORKFLOW['resume']
        date_bilan = suivi.date_dernier_bilan()
        prochain_rapport = suivi.date_prochain_rapport()
        if date_bilan and prochain_rapport and (prochain_rapport - date_bilan) < timedelta(days=4 * 30):
            return self.suivi_model.WORKFLOW['resume']
        else:
            return self.suivi_model.WORKFLOW['bilan_suivant']

    def delai_depuis(self, suivi, date_base):
        """Délai de cette étape à partir de la date date_base (en général date précédente étape)."""
        if not date_base:
            return None
        base = suivi.date_dernier_bilan() or date_base
        return base + timedelta(days=self.delai_standard)


class EtapeResume(Etape):
    def date(self, suivi):
        """Date du dernier rapport"""
        return suivi.date_dernier_rapport()


class EtapeFin(Etape):
    delai_standard = 365 + 180  # 18 mois

    def delai_depuis(self, suivi, *args):
        if not suivi.date_debut_suivi:
            return None
        return suivi.date_debut_suivi + timedelta(days=self.delai_standard)


class SuiviSOF(models.Model):
    WORKFLOW = OrderedDict([
        ('demande', Etape(
            1, 'demande', 'dem', 'Demande déposée', 'debut_evaluation', 0, 'demande', 'demande', True
        )),
        ('debut_evaluation', Etape(
            2, 'debut_evaluation', 'deb_eva', "Début de l’évaluation", 'fin_evaluation', 40, 'demande', 'demande', True
        )),
        ('fin_evaluation', Etape(
            3, 'fin_evaluation', "fin_eva", "Fin de l’évaluation", 'debut_suivi', 40,
            'debut_evaluation', 'debut_evaluation', True
        )),
        ('debut_suivi', Etape(4, 'debut_suivi', "deb", 'Début du suivi', 'bilan_suivant', 12, 'fin_evaluation',
                              'fin_evaluation', False)),
        ('bilan_suivant',  EtapeBilan(
            5, 'bilan_suivant', "bil", 'Bilan suivant', 'resume', 90, 'debut_suivi', 'fin_evaluation', False
        )),
        ('resume', EtapeResume(
            6, 'resume', 'rés', 'Résumé', 'fin_suivi', 90, 'bilan_suivant', 'fin_evaluation', False
        )),
        ('fin_suivi', EtapeFin(
            7, 'fin_suivi', "fsuiv", 'Fin du suivi', 'archivage', 365 + 180, 'resume', 'fin_evaluation', True
        )),
        ('archivage', Etape(
            8, 'arch_dossier', 'arch', 'Archivage du dossier', None, 0, 'fin_suivi', 'fin_suivi', False
        )),
    ])
    SERVICE_ANNONCEUR = (
        ("contact", "Formulaire de contact"),
        ("collab_crne", "Collaborateurice CRNE"),
        ("serv_soc", "Service social"),
        ("sagefemme", "Sage-femme"),
        ("connaiss", "Connaissance"),
        ("autre", "Autre (préciser)"),
    )
    MOTIF_DEMANDE_CHOICES = (
        ("soutien_adm", "Soutien administratif"),
        ("integr_enf", "Intégration sociale des enfants"),
        ("integr_par", "Intégration sociale des parents"),
        ("linguist", "Développement des compétences linguistiques"),
        ("soutien_edu", "Soutien éducatif"),
        ("soutien_par", "Soutien parental"),
        ("violence", "Violence/maltraitance"),
    )

    famille = models.OneToOneField(FamilleSOF, on_delete=models.CASCADE)
    heure_coord = models.BooleanField("Heure de coordination", default=False)
    remarque_privee = models.TextField('Remarque privée', blank=True)
    service_orienteur = models.CharField(
        "Orienté vers le SOF par", max_length=15, choices=choices.SERVICE_ORIENTEUR_CHOICES, blank=True
    )
    annonceur = models.CharField("Service annonceur", max_length=15, choices=SERVICE_ANNONCEUR, blank=True)
    annonceur_txt = models.CharField('Service annonceur', max_length=60, blank=True)
    motif_demande = ChoiceArrayField(
        models.CharField(max_length=60, choices=MOTIF_DEMANDE_CHOICES),
        verbose_name="Motif de la demande", blank=True, null=True
    )

    # Référents
    intervenants = models.ManyToManyField(
        Utilisateur, through='IntervenantSOF', related_name='interventions_sof', blank=True
    )
    sse_referent = models.ForeignKey(Contact, blank=True, null=True, related_name='+',
                                     on_delete=models.SET_NULL, verbose_name='SSE')
    referent_note = models.TextField('Autres contacts', blank=True)

    date_demande = models.DateField("Demande déposée le", blank=True, null=True, default=None)
    date_debut_evaluation = models.DateField("Début de l’évaluation le", blank=True, null=True, default=None)
    date_fin_evaluation = models.DateField("Fin de l’évaluation le", blank=True, null=True, default=None)
    date_debut_suivi = models.DateField("Début du suivi le", blank=True, null=True, default=None)
    date_fin_suivi = models.DateField("Fin du suivi le", blank=True, null=True, default=None)

    demande_prioritaire = models.BooleanField("Demande prioritaire", default=False)
    demarche = ChoiceArrayField(models.CharField(max_length=60, choices=choices.DEMARCHE_CHOICES, blank=True),
                                verbose_name="Démarche", blank=True, null=True)

    motif_fin_suivi = models.CharField('Motif de fin de suivi', max_length=20,
                                       choices=SOF_MOTIFS_FIN_SUIVI_CHOICES, blank=True)
    unite = 'sof'

    class Meta:
        verbose_name = "Suivi SOF"
        verbose_name_plural = "Suivis SOF"

    def __str__(self):
        return 'Suivi SOF pour la famille {} '.format(self.famille)

    @property
    def date_fin_theorique(self):
        if self.date_fin_suivi:
            return self.date_fin_suivi
        if self.date_debut_suivi is None:
            return None
        return self.date_debut_suivi + timedelta(days=548)  # env. 18 mois

    def date_prochain_rapport(self):
        if self.date_debut_suivi is None or self.date_fin_suivi is not None:
            return None
        date_dernier_rapport = self.date_dernier_rapport()
        if date_dernier_rapport is None:
            # Premier à 18 mois
            delai = 365 + 182
            return self.date_debut_suivi + timedelta(days=delai)
        else:
            # Suivants tous les 12 mois
            delai = 365
            return date_dernier_rapport + timedelta(days=delai)

    def date_dernier_rapport(self):
        # Using rapports_sof.all() to leverage prefetchs
        debut_suivi = self.date_debut_suivi
        return max([
            rapp.date for rapp in self.famille.rapports_sof.all() if rapp.date > debut_suivi
        ], default=None)

    def date_dernier_bilan(self):
        debut_suivi = self.date_debut_suivi
        # Using bilans_sof.all() to leverage prefetchs
        return max([
            bilan.date for bilan in self.famille.bilans_sof.all() if bilan.date > debut_suivi
        ], default=None)

    @cached_property
    def etape(self):
        """L’étape *terminée* du suivi."""
        for key, etape in reversed(self.WORKFLOW.items()):
            if key != 'archivage':
                if etape.date(self):
                    return etape

    @cached_property
    def etape_suivante(self):
        return self.etape.etape_suivante(self) if self.etape else None

    def date_suivante(self):
        etape_date = self.etape.date(self)
        if not self.etape_suivante or not etape_date:
            return None
        return self.etape_suivante.delai_depuis(self, etape_date)

    def get_motif_demande_display(self):
        dic = dict(SuiviSOF.MOTIF_DEMANDE_CHOICES)
        return '; '.join([dic[value] for value in self.motif_demande]) if self.motif_demande else ''


# Avoid circular import
Etape.suivi_model = SuiviSOF


class PrestationSOF(PrestationBase):
    famille = models.ForeignKey(FamilleSOF, related_name='prestations_sof', null=True, blank=True,
                                on_delete=models.SET_NULL, verbose_name="Famille")
    intervenants = models.ManyToManyField(Utilisateur, related_name='prestations_sof')
    medlink_date = models.DateTimeField('Date import. Medlink', blank=True, null=True)
    avec_famille = models.BooleanField("Rendez-vous avec famille", default=True)
    CURRENT_APP = "sof"

    def save(self, *args, **kwargs):
        if self.famille is None:
            self.familles_actives = FamilleSOF.actives(self.date_prestation).count()
        super().save(*args, **kwargs)

    def can_edit(self, user):
        return (
            (user == self.auteur or user in self.intervenants.all() or
             user.has_perm(f'{self.CURRENT_APP}.edit_prest_prev_month')
            ) and self.check_date_allowed(user, self.date_prestation, app=self.CURRENT_APP)
        )

    def edit_url(self):
        return reverse('sof-prestation-edit', args=[self.famille.pk if self.famille else 0, self.pk])


class IntervenantSOFManager(models.Manager):
    def actifs(self, _date=None):
        return self.filter(Q(date_fin__isnull=True) | Q(date_fin__gt=_date or date.today()))


class IntervenantSOF(models.Model):
    """
    Modèle M2M entre SuiviSOF et Utilisateur (utilisé par through de SuiviSOF.intervenants).
    """
    suivi = models.ForeignKey(SuiviSOF, on_delete=models.CASCADE)
    intervenant = models.ForeignKey(Utilisateur, on_delete=models.CASCADE)
    role = models.ForeignKey(Role, on_delete=models.CASCADE)
    date_debut = models.DateField('Date début', default=timezone.now)
    # date_fin est utilisé pour les interventions s'arrêtant avant la fin du suivi.
    date_fin = models.DateField('Date fin', null=True, blank=True)

    objects = IntervenantSOFManager()

    def __str__(self):
        return '{}, {} pour {}'.format(self.intervenant, self.role, self.suivi)

    def can_edit(self, user):
        return self.suivi.famille.can_edit(user)


class RapportSOF(Rapport):
    famille = models.ForeignKey(FamilleSOF, related_name='rapports_sof', on_delete=models.CASCADE)
    pres_interv = models.ManyToManyField(
        Utilisateur, blank=True, verbose_name="Intervenants cités dans le résumé", related_name='+'
    )
    sig_interv = models.ManyToManyField(
        Utilisateur, blank=True, verbose_name="Signature des intervenants", related_name='+'
    )
    # Remplace les anciens champs evolutions et evaluation (ces derniers pourront être
    # supprimés lorsque tous les rapports ayant du contenu pour ces champs auront été archivés).
    observations = models.TextField("Observations, évolution et hypothèses", blank=True)

    def is_observations_enable(self):
        return self.evolutions == '' and self.evaluation == ''

    def intervenants(self):
        if self.pres_interv.all():
            return list(self.pres_interv.all())
        return super().intervenants()
