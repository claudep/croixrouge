from django.contrib import admin

from croixrouge.admin import FamilleAdminBase, TypePrestationFilter

from .models import Bilan, FamilleSOF, IntervenantSOF, PrestationSOF, RapportSOF, SuiviSOF


@admin.register(FamilleSOF)
class FamilleSOFAdmin(FamilleAdminBase):
    def get_queryset(self, request):
        return FamilleSOF.objects.filter(typ='sof')


@admin.register(SuiviSOF)
class SuiviSOFAdmin(admin.ModelAdmin):
    list_display = ('famille', 'etape')
    ordering = ('famille__nom',)
    search_fields = ('famille__nom',)


@admin.register(IntervenantSOF)
class IntervenantSOFAdmin(admin.ModelAdmin):
    list_display = ('intervenant', 'famille', 'role', 'date_debut', 'date_fin', 'fin_suivi')
    list_filter = ('role',)

    def famille(self, obj):
        return obj.suivi.famille

    def fin_suivi(self, obj):
        return obj.suivi.date_fin_suivi


@admin.register(PrestationSOF)
class PrestationSOFAdmin(admin.ModelAdmin):
    list_display = ('lib_prestation', 'date_prestation', 'duree', 'auteur')
    list_filter = (TypePrestationFilter,)
    search_fields = ('famille__nom', 'texte')


admin.site.register(Bilan)
admin.site.register(RapportSOF)
