from datetime import date, timedelta
from operator import attrgetter

from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.postgres.aggregates import ArrayAgg
from django.core.exceptions import PermissionDenied
from django.db.models import Count, Prefetch, Q, Sum, Value
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.generic import CreateView, DeleteView, ListView, UpdateView, View

from croixrouge.models import LibellePrestation
from croixrouge.pdf import JournalPdf
from croixrouge.utils import format_d_m_Y
from croixrouge.views import BasePDFView, BaseSuiviView, FamilleUpdateViewBase, JournalAccesMixin

from .forms import (
    AgendaForm,
    FamilleCreateForm,
    FamilleFilterForm,
    FamilleForm,
    IntervenantSOFEditForm,
    IntervenantSOFForm,
    JournalAuteurFilterForm,
    PrestationSOFForm,
    SuiviSOFForm,
)
from .models import Bilan, FamilleSOF, IntervenantSOF, PrestationSOF, SuiviSOF
from .pdf import BilanPdf, CoordonneesFamillePdf

MSG_READ_ONLY = "Vous n'avez pas les droits nécessaires pour modifier cette page"
MSG_ACCESS_DENIED = "Vous n’avez pas la permission d’accéder à cette page."


class AccessRequiredMixin:
    def dispatch(self, request, *args, **kwargs):
        self.famille = get_object_or_404(FamilleSOF, pk=kwargs['pk'])
        self.check_access(request)
        return super().dispatch(request, *args, **kwargs)

    def check_access(self, request):
        if not self.famille.can_view(request.user):
            raise PermissionDenied(MSG_ACCESS_DENIED)
        self.readonly = not self.famille.can_edit(request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.readonly:
            messages.info(self.request, MSG_READ_ONLY)
        context['can_edit'] = not self.readonly
        context['famille'] = self.famille
        return context


class CheckCanEditMixin:
    def dispatch(self, request, *args, **kwargs):
        if not self.get_object().can_edit(request.user):
            raise PermissionDenied(MSG_ACCESS_DENIED)
        return super().dispatch(request, *args, **kwargs)


class FamilleCreateView(CreateView):
    template_name = 'croixrouge/famille_edit.html'
    model = FamilleSOF
    form_class = FamilleCreateForm
    action = 'Nouvelle famille'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm('sof.add_famillesof'):
            raise PermissionDenied(MSG_ACCESS_DENIED)
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        famille = form.save()
        return HttpResponseRedirect(reverse('sof-famille-edit', args=[famille.pk]))


class FamilleUpdateView(AccessRequiredMixin, SuccessMessageMixin, FamilleUpdateViewBase):
    template_name = 'croixrouge/famille_edit.html'
    model = FamilleSOF
    famille_model = FamilleSOF
    form_class = FamilleForm
    success_message = 'La famille %(nom)s a bien été modifiée.'

    def get_form_kwargs(self):
        return {**super().get_form_kwargs(), 'readonly': self.readonly}


class FamilleListView(ListView):
    template_name = 'sof/famille_list.html'
    model = FamilleSOF
    paginate_by = 20
    mode = 'normal'
    normal_cols = [
        ('Nom', 'nom'), ('Adresse', 'localite'), ('Réf. SOF', 'referents'),
        ('Statut', 'suivi'), ('Prior.', 'prioritaire'),
    ]
    attente_cols = [
        ('Nom', 'nom'), ('Adresse', 'localite'), ('Région', 'region'),
        ('Réf. SOF', 'referents'), ('Prior.', 'prioritaire'),
        ('Demande', 'date_demande'), ('Évaluation', 'evaluation'),
    ]

    def get(self, request, *args, **kwargs):
        request.session['current_app'] = 'sof'
        if self.mode == 'attente':
            self.paginate_by = None
        form_class = FamilleFilterForm
        self.filter_form = form_class(data=self.request.GET or None)
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        familles = super().get_queryset().filter(
            suivisof__isnull=False, suivisof__date_fin_suivi__isnull=True
        ).prefetch_related(
            Prefetch(
                'suivisof__intervenantsof_set',
                queryset=IntervenantSOF.objects.actifs().select_related('intervenant', 'role')
            ),
            'suivisof__intervenants', 'bilans_sof', 'rapports_sof'
        ).order_by('nom', 'npa')

        if self.mode == 'attente':
            familles = familles.filter(
                suivisof__date_debut_suivi__isnull=True
            ).order_by(
                '-suivisof__demande_prioritaire', 'suivisof__date_demande',
            )
        if self.filter_form.is_bound and self.filter_form.is_valid():
            familles = self.filter_form.filter(familles)
        return familles

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.mode == 'attente':
            cols = self.attente_cols
        else:
            cols = self.normal_cols
        # cf requete similaire dans UtilisateurChargeDossierView
        current_user = self.request.user
        ma_charge = current_user.interventions_sof.filter(
            Q(intervenantsof__date_fin__isnull=True) | Q(intervenantsof__date_fin__gt=date.today())
        ).aggregate(
            charge=Sum(Value(1), filter=Q(date_fin_suivi__isnull=True)),
            nbre_coord=Count('heure_coord', filter=Q(heure_coord=True, date_fin_suivi__isnull=True)),
            nbre_eval=Count('id', filter=(
                Q(date_fin_suivi__isnull=True) & Q(date_debut_suivi__isnull=True)
            )),
            nbre_suivi=Count('id', filter=(
                Q(date_fin_suivi__isnull=True) & Q(date_debut_suivi__isnull=False)
            )),
        )

        context.update({
            'labels': [c[0] for c in cols],
            'col_keys': [c[1] for c in cols],
            'form': self.filter_form,
            'ma_charge': {
                'heures': ma_charge['charge'] + ma_charge['nbre_coord'],
                'nbre_eval': ma_charge['nbre_eval'],
                'nbre_suivi': ma_charge['nbre_suivi'],
                'charge_diff': current_user.charge_max - (ma_charge['charge'] + ma_charge['nbre_coord']),
            } if ma_charge['charge'] is not None else '',
        })
        return context


class SOFSuiviView(AccessRequiredMixin, JournalAccesMixin, BaseSuiviView):
    template_name = 'sof/suivi_edit.html'
    model = SuiviSOF
    famille_model = FamilleSOF
    form_class = SuiviSOFForm

    def get_form_kwargs(self):
        return {**super().get_form_kwargs(), 'readonly': self.readonly}

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['intervenants'] = self.object.famille.interventions_actives().order_by('role')
        return context


class SuiviIntervenantCreate(AccessRequiredMixin, CreateView):
    model = IntervenantSOF
    form_class = IntervenantSOFForm
    template_name = 'croixrouge/form_in_popup.html'
    titre_page = "Ajout d’un intervenant"
    titre_formulaire = "Intervenant"

    def form_valid(self, form):
        form.instance.suivi = SuiviSOF.objects.get(famille__pk=self.kwargs['pk'])
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('sof-famille-suivi', args=[self.kwargs['pk']])


class SuiviIntervenantUpdateView(AccessRequiredMixin, UpdateView):
    model = IntervenantSOF
    form_class = IntervenantSOFEditForm
    template_name = 'croixrouge/form_in_popup.html'
    titre_page = "Modification d’une intervention"
    titre_formulaire = "Intervenant"

    pk_url_kwarg = 'obj_pk'

    def get_success_url(self):
        return reverse('sof-famille-suivi', args=[self.kwargs['pk']])


class SuivisTerminesListView(FamilleListView):
    template_name = 'sof/suivis_termines_list.html'

    def get_queryset(self):
        suivi_name = self.model.suivi_name
        familles = self.model.objects.filter(**{
            f'{suivi_name}__date_fin_suivi__isnull': False,
            'archived_at__isnull': True
        }).select_related(suivi_name)

        if self.filter_form.is_bound and self.filter_form.is_valid():
            familles = self.filter_form.filter(familles)
        return familles


class AgendaSuiviView(AccessRequiredMixin, SuccessMessageMixin, UpdateView):
    template_name = 'sof/agenda_suivi.html'
    model = SuiviSOF
    famille_model = FamilleSOF
    form_class = AgendaForm
    success_message = 'Les modifications ont été enregistrées avec succès'

    def get_object(self, queryset=None):
        return get_object_or_404(self.model, famille__pk=self.kwargs['pk'])

    def get_form_kwargs(self):
        return {**super().get_form_kwargs(),
                'readonly': self.readonly,
                'destination': self.get_object().famille.destination,
                'request': self.request}

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        dfin = self.object.date_fin_suivi
        ddebut = self.object.date_debut_suivi
        context.update({
            'bilans_et_rapports': sorted(
                list(self.famille.bilans_sof.all()) + list(self.famille.rapports_sof.all()),
                key=attrgetter('date'),
            ),
            'mode': 'reactivation' if dfin else ('evaluation' if ddebut is None else 'suivi'),
            'interv_temporaires': self.object.intervenantsof_set.filter(date_fin__isnull=False),
        })
        return context

    def form_valid(self, form):
        response = super().form_valid(form)
        if (
            'date_debut_suivi' in form.changed_data and form.cleaned_data['date_debut_suivi'] and
            form.cleaned_data['date_debut_suivi'] < date.today()
        ):
            # Contrôle attribution des prestations (accompagnement) depuis début suivi.
            self.famille.prestations.filter(
                date_prestation__gte=form.cleaned_data['date_debut_suivi'], lib_prestation__code='sof01'
            ).update(lib_prestation=LibellePrestation.objects.get(code='sof02'))
        return response

    def get_success_url(self):
        if self.object.date_fin_suivi:
            return reverse('sof-famille-list')
        return reverse('sof-famille-agenda', args=[self.object.famille.pk])


class CoordonneesPDFView(BasePDFView):
    obj_class = FamilleSOF
    pdf_class = CoordonneesFamillePdf


class JournalPDFView(BasePDFView):
    obj_class = FamilleSOF
    pdf_class = JournalPdf


class PrestationMenu(ListView):
    template_name = 'sof/prestation_menu.html'
    model = PrestationSOF
    paginate_by = 15
    context_object_name = 'familles'

    def get_queryset(self):
        user = self.request.user
        typ = FamilleSOF.default_typ
        rdv_manques = dict(
            FamilleSOF.actives().annotate(
                rdv_manques=ArrayAgg(
                    'prestations_sof__date_prestation',
                    filter=Q(prestations_sof__manque=True),
                    ordering='prestations_sof__date_prestation',
                    default=Value([])
                )
            ).values_list('pk', 'rdv_manques')
        )
        familles = FamilleSOF.actives(
        ).annotate(
            user_prest=Sum(f'prestations_{typ}__duree',
                           filter=Q(**{f'prestations_{typ}__intervenants': user}))
        ).annotate(
            sof1=Sum(f'prestations_{typ}__duree',
                     filter=(Q(**{f'prestations_{typ}__lib_prestation__code': 'sof01'}))
                     ) or None
        ).annotate(
            sof2=Sum(f'prestations_{typ}__duree',
                     filter=(Q(**{f'prestations_{typ}__lib_prestation__code': 'sof02'}))
                     ) or None
        )
        for famille in familles:
            famille.rdv_manques = [format_d_m_Y(rdv) for rdv in rdv_manques[famille.pk]]
        return familles

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'annee': date.today().year
        })
        return context


class PrestationBaseView:
    model = PrestationSOF
    famille_model = FamilleSOF
    form_class = PrestationSOFForm
    template_name = 'sof/prestation_edit.html'

    def dispatch(self, *args, **kwargs):
        self.famille = get_object_or_404(self.famille_model, pk=self.kwargs['pk']) if self.kwargs.get('pk') else None
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        return {**super().get_context_data(**kwargs), 'famille': self.famille}

    def get_success_url(self):
        if self.famille:
            return reverse('sof-journal-list', args=[self.famille.pk])
        return reverse('sof-prestation-gen-list')


class PrestationListView(PrestationBaseView, ListView):
    template_name = 'sof/prestation_list.html'
    model = PrestationSOF
    paginate_by = 15
    context_object_name = 'prestations'

    def get(self, request, **kwargs):
        self.filter_form = JournalAuteurFilterForm(famille=self.famille, data=request.GET or None)
        return super().get(request, **kwargs)

    def get_queryset(self):
        if self.famille:
            prestations = self.famille.prestations.all().order_by('-date_prestation')
            if self.filter_form.is_bound and self.filter_form.is_valid():
                prestations = self.filter_form.filter(prestations)
            return prestations
        return self.request.user.prestations_sof.filter(famille=None)

    def get_context_data(self, **kwargs):
        return super().get_context_data(**kwargs, filter_form=self.filter_form)


class PrestationCreateView(PrestationBaseView, CreateView):
    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm('sof.add_prestationsof'):
            raise PermissionDenied("Vous n'avez pas les droits pour ajouter une prestation.")
        return super().dispatch(request, *args, **kwargs)

    def get_initial(self):
        initial_text = (
            "<p><b>Observations:\n"
            "(relations et interactions dans la famille / disponibilité des parents / "
            "réponses données aux besoins des enfants / …)</b></p><p></p>"
            "<p><b>Discussion (thème-s abordé-s) / activités:</b></p><p></p>"
            "<p><b>Particularités en termes de ressources et/ou de limites:</b></p><p></p>"
            "<p><b>Ressentis de l’intervenant-e:</b></p><p></p>"
            "<p><b>Objectif-s traité-s:</b></p><p></p>"
            "<p><b>Objectif-s à suivre:</b></p><p></p>"
        )
        initial = {
            **super().get_initial(),
            'intervenants': [self.request.user.pk],
            'texte': initial_text if self.famille else '',
            'avec_famille': True,
        }
        return initial

    def get_form_kwargs(self):
        return {**super().get_form_kwargs(), 'famille': self.famille, 'user': self.request.user}

    def get_lib_prestation(self, date_prest):
        """
        Renvoie la prestation en fonction de la famille et du rôle de l'utilisateur connecté.
        """
        if self.famille is None:
            code = 'sof03'  # Prestation générale
        elif self.famille.suivi.date_debut_suivi and date_prest >= self.famille.suivi.date_debut_suivi:
            code = 'sof02'  # Accompagnement
        else:
            code = 'sof01'  # Évaluation
        return LibellePrestation.objects.get(code=code)

    def form_valid(self, form):
        if self.famille:
            form.instance.famille = self.famille
        form.instance.auteur = self.request.user
        form.instance.lib_prestation = self.get_lib_prestation(form.cleaned_data['date_prestation'])
        if 'duree' not in form.cleaned_data:
            form.instance.duree = timedelta()
        return super().form_valid(form)


class PrestationUpdateView(CheckCanEditMixin, PrestationBaseView, UpdateView):
    pk_url_kwarg = 'obj_pk'

    def get_form_kwargs(self):
        return {**super().get_form_kwargs(), 'famille': self.famille, 'user': self.request.user}

    def delete_url(self):
        fam_id = self.famille.pk if self.famille else 0
        return reverse('sof-prestation-delete', args=[fam_id, self.object.pk])


class PrestationDeleteView(PrestationBaseView, DeleteView):
    template_name = 'croixrouge/object_confirm_delete.html'
    pk_url_kwarg = 'obj_pk'
    form_class = DeleteView.form_class


class BilanPDFView(BasePDFView):
    obj_class = Bilan
    pdf_class = BilanPdf


class FamilleArchivableListe(View):
    """Return all family ids which are archivable by the current user."""

    def get(self, request, *args, **kwargs):
        data = [
            famille.pk for famille in FamilleSOF.objects.filter(
                archived_at__isnull=True, suivisof__date_fin_suivi__isnull=False
            )
            if famille.can_be_archived(request.user)
        ]
        return JsonResponse(data, safe=False)
