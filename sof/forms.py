from datetime import date

from city_ch_autocomplete.forms import CityChField, CityChMixin
from django import forms
from django.contrib.postgres.search import SearchQuery, SearchVector
from django.db.models import Count, Q

from croixrouge.forms import (
    AgendaFormBase,
    BootstrapMixin,
    BSCheckboxSelectMultiple,
    FamilleFormBase,
    PickDateWidget,
    PrestationFormBase,
    ReadOnlyableMixin,
    RichTextField,
)
from croixrouge.models import Role, Utilisateur

from .models import Bilan, FamilleSOF, IntervenantSOF, PrestationSOF, SuiviSOF


class FamilleFilterForm(forms.Form):
    ressource = forms.ChoiceField(
        label='Ressources',
        required=False,
        widget=forms.Select(attrs={'class': 'form-select form-select-sm immediate-submit'})
    )
    interv = forms.ChoiceField(
        label="Intervenant-e",
        required=False,
        widget=forms.Select(attrs={'class': 'form-select form-select-sm immediate-submit'})
    )
    nom = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Nom de famille…', 'autocomplete': 'off',
                   'class': 'form-control form-control-sm inline'}),
        required=False
    )

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        intervenants = Utilisateur.objects.filter(is_active=True, groups__name='sof')
        self.fields['interv'].choices = [
            ('', 'Tout-e-s'), ('0', 'Aucun')] + [
            (user.id, user.nom_prenom) for user in intervenants
        ]
        self.fields['ressource'].choices = [
            ('', 'Toutes')
        ] + [(el.pk, el.nom) for el in Role.objects.filter(
                nom__in=['ASE', 'IPE', 'Coach APA', 'Assistant-e social-e']
            )]

    def filter(self, familles):
        if self.cleaned_data['interv']:
            if self.cleaned_data['interv'] == '0':
                familles = familles.annotate(
                    num_interv=Count('suivisof__intervenants', filter=(
                        Q(suivisof__intervenantsof__date_fin__isnull=True) |
                        Q(suivisof__intervenantsof__date_fin__gt=date.today())
                    ))
                ).filter(num_interv=0)
            else:
                familles = familles.filter(
                    Q(suivisof__intervenantsof__intervenant=self.cleaned_data['interv']) & (
                        Q(suivisof__intervenantsof__date_fin__isnull=True) |
                        Q(suivisof__intervenantsof__date_fin__gt=date.today())
                    )
                )

        if self.cleaned_data['nom']:
            familles = familles.filter(nom__istartswith=self.cleaned_data['nom'])

        if self.cleaned_data['ressource']:
            ress = IntervenantSOF.objects.actifs().filter(
                role=self.cleaned_data['ressource'],
            ).values_list('intervenant', flat=True)
            familles = familles.filter(suivisof__intervenants__in=ress).distinct()

        return familles


class JournalAuteurFilterForm(forms.Form):
    recherche = forms.CharField(
        widget=forms.TextInput(attrs={
            'class': 'search-form-fields form-control d-inline-block',
            'placeholder': 'recherche',
        }),
        required=False,
    )
    auteur = forms.ModelChoiceField(
        queryset=Utilisateur.objects.all(),
        widget=forms.Select(attrs={'class': 'search-form-fields form-select d-inline-block immediate-submit'}),
        required=False,
    )

    def __init__(self, famille=None, **kwargs):
        super().__init__(**kwargs)
        self.fields['auteur'].queryset = Utilisateur.objects.filter(prestations_sof__famille=famille).distinct()

    def filter(self, prestations):
        if self.cleaned_data['auteur']:
            prestations = prestations.filter(auteur=self.cleaned_data['auteur'])
        if self.cleaned_data['recherche']:
            prestations = prestations.annotate(
                search=SearchVector("texte", config="french_unaccent")
            ).filter(
                search=SearchQuery(self.cleaned_data['recherche'], config="french_unaccent")
            )
        return prestations


class FamilleForm(FamilleFormBase):
    # Concernant 'npa' & 'location', ils sont en lecture seule ici => pas besoin de faire de l'autocomplete

    class Meta:
        model = FamilleSOF
        fields = [
            'nom', 'rue', 'npa', 'localite', 'telephone', 'region', 'autorite_parentale',
            'monoparentale', 'statut_marital', 'garde', 'statut_financier', 'interprete'
        ]


class FamilleCreateForm(CityChMixin, FamilleForm):
    motif_demande = forms.MultipleChoiceField(
        label="Motif de la demande", choices=SuiviSOF.MOTIF_DEMANDE_CHOICES,
        widget=BSCheckboxSelectMultiple,
        required=False
    )
    typ = 'sof'
    city_auto = CityChField(required=False)
    postal_code_model_field = 'npa'
    city_model_field = 'localite'
    field_order = ['nom', 'rue', 'city_auto']

    class Meta(FamilleForm.Meta):
        exclude = ['typ', 'destination']

    def save(self, **kwargs):
        famille = self._meta.model.objects.create_famille(**self.instance.__dict__)
        famille.suivi.motif_demande = self.cleaned_data["motif_demande"]
        famille.suivi.save()
        return famille


class SuiviSOFForm(BootstrapMixin, ReadOnlyableMixin, forms.ModelForm):

    class Meta:
        model = SuiviSOF
        fields = [
            'service_orienteur', 'annonceur', 'annonceur_txt', 'motif_demande',
            'demande_prioritaire', 'demarche', 'remarque_privee',
        ]
        widgets = {
            'motif_demande': BSCheckboxSelectMultiple,
        }

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data.get("annonceur") == "autre":
            if not cleaned_data.get("annonceur_txt"):
                self.add_error("annonceur_txt", "Vous devez indiquer une précision sur l’annonceur")
        else:
            if cleaned_data.get("annonceur_txt"):
                cleaned_data["annonceur_txt"] = ""
        return cleaned_data


class IntervenantSOFForm(BootstrapMixin, forms.ModelForm):
    intervenant = forms.ModelChoiceField(
        queryset=Utilisateur.objects.filter(groups__name__startswith='sof').distinct().order_by('nom')
    )
    date_debut = forms.DateField(
        label='Date de début', initial=date.today(), widget=PickDateWidget()
    )

    class Meta:
        model = IntervenantSOF
        fields = ['intervenant', 'role', 'date_debut']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['role'].queryset = Role.objects.filter(intervenant=True)


class IntervenantSOFEditForm(BootstrapMixin, forms.ModelForm):

    class Meta:
        model = IntervenantSOF
        fields = ['intervenant', 'role', 'date_debut', 'date_fin']
        widgets = {
            'date_debut': PickDateWidget(),
            'date_fin': PickDateWidget(),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['intervenant'].disabled = True
        self.fields['role'].disabled = True


class AgendaForm(ReadOnlyableMixin, AgendaFormBase):
    destination = None
    change_famille_perm = 'sof.change_famillesof'

    class Meta(AgendaFormBase.Meta):
        model = SuiviSOF
        fields = [
            'date_demande', 'date_debut_evaluation', 'date_fin_evaluation',
            'date_debut_suivi', 'date_fin_suivi', 'motif_fin_suivi',
        ]


class PrestationSOFForm(PrestationFormBase):
    class Meta(PrestationFormBase.Meta):
        model = PrestationSOF
        fields = [
            "date_prestation", "duree", "texte", "avec_famille", "manque",
            "fichier", "intervenants",
        ]

    def defaults_intervenants(self):
        return Utilisateur.intervenants_sof()


class BilanForm(BootstrapMixin, forms.ModelForm):
    class Meta:
        model = Bilan
        exclude = ['famille', 'auteur']
        field_classes = {
            'objectifs': RichTextField,
            'rythme': RichTextField,
        }
        widgets = {
            'famille': forms.HiddenInput,
            'date': PickDateWidget,
            'sig_interv': BSCheckboxSelectMultiple,
        }
        labels = {'fichier': ''}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['sig_interv'].queryset = Utilisateur.objects.filter(
            pk__in=kwargs['initial']['famille'].suivisof.intervenantsof_set.actifs(
                self.instance.date or date.today()
            ).values_list('intervenant', flat=True)
        )
