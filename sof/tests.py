from datetime import date, timedelta
from unittest import expectedFailure
from unittest.mock import patch

from django.contrib.auth.models import Group, Permission
from django.forms.models import model_to_dict
from django.test import RequestFactory, TestCase, tag
from django.urls import reverse

from croixrouge.models import Contact, JournalAcces, LibellePrestation, Personne, Region, Role, Utilisateur
from croixrouge.tests import InitialDataMixin
from croixrouge.utils import ANTICIPATION_POUR_DEBUT_SUIVI, format_d_m_Y
from sof.forms import AgendaForm, FamilleCreateForm, PrestationSOFForm
from sof.models import Bilan, EtapeFin, FamilleSOF, IntervenantSOF, PrestationSOF, RapportSOF, SuiviSOF


class SOFDataMixin(InitialDataMixin):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        famille = FamilleSOF.objects.create_famille(
            nom='Haddock', rue='Château1', npa=2000, localite='Moulinsart',
            autorite_parentale='conjointe', statut_marital='divorce', monoparentale=False
        )
        pere = Role.objects.get(nom='Père')
        resp = Role.objects.create(nom='Responsable/coordinateur')
        Personne.objects.create_personne(
            famille=famille, role=pere,
            nom='Haddock', prenom='Archibald', genre='M', date_naissance=date(1956, 2, 16),
            rue='Château1', npa=2000, localite='Moulinsart',
        )
        LibellePrestation.objects.bulk_create([
            LibellePrestation(unite='sof', code='sof01', nom='Évaluation SOF'),
            LibellePrestation(unite='sof', code='sof02', nom='Accompagnement SOF'),
            LibellePrestation(unite='sof', code='sof03', nom='Prestation gén. SOF'),
        ])
        cls.user_sof = Utilisateur.objects.create_user(
            'user_sof', 'user_sof@example.org', nom='Sof', prenom='Prénom', sigle='SP', taux_activite=60,
        )
        cls.user_sof.roles.add(Role.objects.get(nom='Educ'))
        cls.user_sof2 = Utilisateur.objects.create_user(
            'user_sof2', 'user_sof2@example.org', nom='Sof2', prenom='Prénom', sigle='SP2'
        )
        cls.user_sof2.roles.add(Role.objects.get(nom='Psy'))
        cls.sof_group = Group.objects.create(name='sof')
        cls.sof_group.permissions.add(*list(
            Permission.objects.filter(codename__in=[
                'view_famillesof', 'add_famillesof', 'add_prestationsof'
            ])
        ))
        cls.user_sof.groups.add(cls.sof_group)
        cls.user_sof2.groups.add(cls.sof_group)

        grp_admin = Group.objects.create(name='admin')
        grp_admin.permissions.add(Permission.objects.get(codename='change_famillesof'))
        grp_admin.permissions.add(Permission.objects.get(codename='change_utilisateur'))
        grp_admin.permissions.add(Permission.objects.get(codename='export_stats'))
        cls.user_admin = Utilisateur.objects.create_user(
            'user_admin', 'user_admin@example.com', nom='Admin', prenom='Prénom', sigle='ADM'
        )
        cls.user_admin.groups.add(grp_admin)
        cls.user_admin.groups.add(cls.sof_group)
        cls.user_admin.roles.add(resp)


class FamilleSOFTests(SOFDataMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.client.force_login(self.user_sof)

    famille_data = {
        'nom': 'Dupont',
        'rue': 'Rue du Moulins',
        'npa': '3000',
        'localite': 'Paris',
        'autorite_parentale': 'conjointe',
    }

    def test_famille_creation(self):
        response = self.client.get(reverse('sof-famille-add'))
        self.assertContains(
            response, f'<option value="{Region.objects.get(nom="Montagnes").pk}">Montagnes</option>',
            html=True
        )
        self.assertContains(
            response,
            '<button class="btn btn-sm btn-success" name="save" type="submit">Enregistrer</button>',
            html=True
        )
        self.assertContains(response, "id_motif_demande")
        response = self.client.post(reverse('sof-famille-add'), data={
            **self.famille_data, "motif_demande": ["soutien_edu"],
        })
        famille = FamilleSOF.objects.get(nom='Dupont')
        self.assertRedirects(response, reverse('sof-famille-edit', args=[famille.pk]))
        famille = FamilleSOF.objects.get(nom='Dupont')
        self.assertEqual(famille.suivisof.date_demande, date.today())
        self.assertEqual(famille.suivisof.get_motif_demande_display(), "Soutien éducatif")

    def test_famille_edition(self):
        famille = FamilleSOF.objects.get(nom='Haddock')
        edit_url = reverse('sof-famille-edit', args=[famille.pk])
        response = self.client.get(edit_url)
        self.assertContains(response, '<select name="statut_financier" class="form-select" id="id_statut_financier">')
        self.assertContains(response, reverse('sof-print-coord-famille', args=[famille.pk]))
        self.assertContains(response, "Changer l’adresse")
        self.assertNotContains(response, "id_motif_demande")
        data = {**self.famille_data, 'localite': 'Monaco'}
        response = self.client.post(edit_url, data=data)
        self.assertRedirects(response, edit_url)
        famille.refresh_from_db()
        self.assertEqual(famille.localite, 'Monaco')

    def test_famille_edition_perm(self):
        famille = FamilleSOF.objects.get(nom='Haddock')
        user_1 = Utilisateur.objects.create(username='jean')
        user_1.groups.add(Group.objects.get(name='sof'))
        user_2 = Utilisateur.objects.create(username='ben')
        user_2.groups.add(Group.objects.get(name='sof'))
        manager = Utilisateur.objects.create(username='boss')
        manager.user_permissions.add(Permission.objects.get(codename='change_famillesof'))
        manager2 = Utilisateur.objects.create(username='boss2')
        manager2.roles.add(Role.objects.get(nom='Responsable/coordinateur'))
        # No referent set, any team user can edit
        self.assertTrue(famille.can_edit(user_1))
        interv = IntervenantSOF.objects.create(
            suivi=famille.suivisof, intervenant=user_2, role=Role.objects.get(nom='Educ'),
            date_debut=date.today() - timedelta(days=3)
        )
        # Only referent (or manager) can access
        self.assertFalse(famille.can_edit(user_1))
        self.assertTrue(famille.can_edit(user_2))
        self.assertTrue(famille.can_edit(manager))
        self.assertTrue(famille.can_edit(manager2))
        interv.date_fin = date.today() - timedelta(days=1)
        self.assertTrue(famille.can_edit(user_2))

    def test_famille_list(self):
        reg_montagnes = Region.objects.get_or_create(nom='Montagnes', secteurs__contains=['sof'])[0]
        reg_litt_ouest = Region.objects.get_or_create(nom='Littoral Ouest', secteurs__contains=['sof'])[0]
        fam1 = FamilleSOF.objects.get(nom='Haddock')
        fam2 = FamilleSOF.objects.create_famille(
            nom='Dupond', rue='Château 4', npa=2299, localite='Klow',
            region=reg_montagnes,
        )
        fam2.suivisof.date_debut_evaluation = date.today() - timedelta(days=3)
        fam2.suivisof.save()
        fam2.suivisof.intervenants.add(self.user_sof, through_defaults={'role': Role.objects.get(nom='Educ')})
        # famille sans équipe
        FamilleSOF.objects.create_famille(
            nom='Sprutz', rue='Fleur 12', npa=2299, localite='Klow',
            region=reg_montagnes,
        )
        FamilleSOF.objects.create_famille(
            nom='Tournesol', rue='Château1', npa=2000, localite='Moulinsart',
            region=reg_litt_ouest,
        )
        response = self.client.get(reverse('sof-famille-list'))
        self.assertEqual(len(response.context['object_list']), FamilleSOF.objects.count())
        self.assertContains(
            response,
            '<a href="%s" title="Suivi">Haddock</a>' % reverse('sof-famille-suivi', args=[fam1.pk]),
            html=True
        )
        # Apply filters
        response = self.client.get(
            reverse('sof-famille-list') + '?nom=du'
        )
        self.assertEqual(response.context['form'].errors, {})
        self.assertQuerySetEqual(response.context['object_list'], [fam2])
        # All
        response = self.client.get(reverse('sof-famille-list'))
        self.assertEqual(len(response.context['object_list']), 4)
        # Ressources
        hier = date.today() - timedelta(days=1)
        role_ase = Role.objects.get(nom='ASE')
        response = self.client.get(reverse('sof-famille-list') + f'?ressource={role_ase.pk}')
        self.assertNotContains(response, fam2.nom, html=False)

        IntervenantSOF.objects.create(suivi=fam2.suivisof, intervenant=self.user_sof2, role=role_ase, date_debut=hier)
        response = self.client.get(reverse('sof-famille-list') + f'?ressource={role_ase.pk}')
        self.assertContains(response, fam2.nom, html=False)

        # Liste d'attente
        response = self.client.get(reverse('sof-famille-attente'))
        self.assertEqual(len(response.context['object_list']), 4)
        self.assertContains(response, f'<td>{reg_montagnes.nom}</td>', html=True)
        self.assertContains(response, '<td class="red">À faire</td>', html=True)

    def test_famille_list_filtre_intervenant(self):
        fam1 = FamilleSOF.objects.get(nom='Haddock')
        # Ajout intervention passée
        fam1.suivisof.intervenants.add(
            self.user_sof, through_defaults={
                'role': Role.objects.get(nom='Educ'), 'date_fin': date.today() - timedelta(days=3)
            }
        )
        fam2 = FamilleSOF.objects.create_famille(
            nom='Dupond', rue='Château 4', npa=2299, localite='Klow',
        )
        fam2.suivisof.date_debut_evaluation = date.today() - timedelta(days=3)
        fam2.suivisof.save()
        fam2.suivisof.intervenants.add(
            self.user_sof, through_defaults={'role': Role.objects.get(nom='Educ')}
        )
        response = self.client.get(reverse('sof-famille-list') + f'?&interv={self.user_sof.pk}')
        self.assertEqual(response.context['form'].errors, {})
        self.assertQuerySetEqual(response.context['object_list'], [fam2])
        # Familles sans intervenant
        response = self.client.get(reverse('sof-famille-list') + '?interv=0')
        self.assertQuerySetEqual(response.context['object_list'], [fam1])
        self.assertEqual(response.context['object_list'][0].suivisof.intervenantsof_set.count(), 0)

    @expectedFailure
    # En attente de décision si la fonctionnalité est souhaitée
    def test_calcul_charge_liste_famille(self):
        fam1 = FamilleSOF.objects.get(nom='Haddock')
        fam1.suivisof.date_debut_suivi = date.today()
        fam1.suivisof.save()
        fam1.suivisof.intervenants.add(self.user_sof, through_defaults={'role': Role.objects.get(nom='Educ')})

        fam2 = FamilleSOF.objects.create_famille(
            nom='Dupond', rue='Château 4', npa=2299, localite='Klow',
        )
        fam2.suivisof.date_debut_evaluation = date.today() - timedelta(days=3)
        fam2.suivisof.save()
        fam2.suivisof.intervenants.add(self.user_sof, through_defaults={'role': Role.objects.get(nom='Educ')})

        fam3 = FamilleSOF.objects.create_famille(
            nom='Dupond2', rue='Château 4', npa=2299, localite='Klow',
        )
        fam3.suivisof.date_debut_evaluation = date.today() - timedelta(days=3)
        fam3.suivisof.save()
        fam3.suivisof.intervenants.add(self.user_sof, through_defaults={'role': Role.objects.get(nom='Educ')})

        # Ne doit pas compter car intervention terminée
        fam4 = FamilleSOF.objects.create_famille(
            nom='Dupond3', rue='Château 6', npa=2299, localite='Klow',
        )
        fam4.suivisof.date_debut_evaluation = date.today() - timedelta(days=3)
        fam4.suivisof.save()
        fam4.suivisof.intervenants.add(
            self.user_sof, through_defaults={
                'role': Role.objects.get(nom='Educ'), 'date_fin': date.today() - timedelta(days=1),
            }
        )

        response = self.client.get(reverse('sof-famille-list'))
        self.assertEqual(
            response.context['ma_charge'],
            {'heures': 9, 'nbre_eval': 2, 'nbre_suivi': 1, 'charge_diff': 7}
        )

        self.client.force_login(self.user_admin)
        response = self.client.get(reverse('charge-utilisateurs'))
        self.assertEqual(response.context['utilisateurs'], [self.user_sof, self.user_sof2])
        self.assertEqual(response.context['utilisateurs'][0].charge, 8)
        self.assertEqual(response.context['utilisateurs'][0].heures, 9)
        self.assertEqual(response.context['utilisateurs'][0].nbre_suivi, 1)
        self.assertEqual(response.context['utilisateurs'][0].nbre_eval, 2)
        self.assertEqual(response.context['utilisateurs'][1].charge, 0)
        self.assertEqual(response.context['utilisateurs'][1].heures, 0)
        self.assertEqual(response.context['utilisateurs'][1].nbre_suivi, 0)
        self.assertEqual(response.context['utilisateurs'][1].nbre_eval, 0)

    def test_delete_enfant_suivi(self):
        famille = FamilleSOF.objects.first()
        pers = Personne.objects.create_personne(
            famille=famille, prenom='Gaston', nom='Lagaffe',
            role=Role.objects.get(nom='Enfant suivi')
        )
        with self.assertLogs('django.request', level='WARNING'):
            response = self.client.post(reverse('personne-delete', args=[famille.pk, pers.pk]), follow=True)
        self.assertTrue(response.status_code, 403)

    def test_affichage_familles_libres_pour_tous_les_educs(self):
        famille = FamilleSOF.objects.create_famille(
            nom='Simpson', rue='Château1', npa=2000, localite='Moulinsart',
        )
        self.assertEqual(famille.suivisof.intervenants.count(), 0)
        response = self.client.get(reverse('sof-famille-list'))
        self.assertContains(response, 'Simpson')

    def test_affichage_adresse(self):
        famille = FamilleSOF(nom='Simpson', rue='Château1', npa=2000, localite='Moulinsart')
        self.assertEqual(famille.adresse, 'Château1, 2000 Moulinsart')
        famille.rue = ''
        self.assertEqual(famille.adresse, '2000 Moulinsart')
        famille.npa = ''
        self.assertEqual(famille.adresse, 'Moulinsart')

    def test_acces_lecture_seule_famille_archivee(self):
        famille = FamilleSOF.objects.get(nom='Haddock')
        famille.suivi.date_fin_suivi = '2020-04-15'
        famille.suivi.save()
        response = self.client.get(reverse('sof-famille-suivi', args=[famille.pk]))
        self.assertFalse(response.context['can_edit'])
        response = self.client.get(reverse('sof-famille-edit', args=[famille.pk]))
        self.assertFalse(response.context['can_edit'])
        response = self.client.get(reverse('personne-edit', args=[famille.pk, famille.membres.first().pk]))
        self.assertTrue(response.context['form'].fields['nom'].disabled)
        # Changement adresse impossible
        response = self.client.get(reverse('famille-adresse-change', args=[famille.pk]))
        self.assertEqual(response.status_code, 403)

    def test_affichage_bouton_changement_adresse(self):
        famille = FamilleSOF.objects.get(nom='Haddock')
        response = self.client.get(reverse('sof-famille-edit', args=[famille.pk]))
        url = reverse('famille-adresse-change', args=[famille.pk])
        self.assertContains(
            response,
            f'<a class="btn btn-sm btn-outline-primary" href="{url}">Changer l’adresse</a>',
            html=True
        )

    def test_membres_famille_dans_form_changement_adresse(self):
        famille = FamilleSOF.objects.get(nom='Haddock')
        pers = Personne.objects.create_personne(
            famille=famille, prenom='Gaston', nom='Lagaffe', rue='Fleurs 44',
            role=Role.objects.get(nom='Enfant suivi')
        )
        edit_url = reverse('famille-adresse-change', args=[famille.pk])
        response = self.client.get(edit_url)
        self.assertContains(response, 'Lagaffe Gaston (Enfant suivi)')
        self.assertContains(response, 'Haddock Archibald (Père)')

        data = {'rue': 'Champs-Elysées 1', 'npa': '7500', 'localite': 'Paris', 'membres': [pers.pk]}
        response = self.client.post(edit_url, data=data)
        self.assertRedirects(response, reverse('sof-famille-edit', args=[famille.pk]))
        pers.refresh_from_db()
        self.assertEqual(pers.rue, "Champs-Elysées 1")
        for parent in famille.parents():
            self.assertEqual(parent.rue, 'Château1')
        # Test changement adresse sans changer pour les membres
        data = {'rue': 'Champs-Elysées 2', 'npa': '7500', 'localite': 'Paris', 'membres': []}
        response = self.client.post(edit_url, data=data)
        self.assertRedirects(response, reverse('sof-famille-edit', args=[famille.pk]))

    def test_nom_famille_formatage(self):
        form = FamilleCreateForm(data={'nom': 'dubois'})
        self.assertTrue(form.is_valid())
        form.save()
        self.assertTrue(FamilleSOF.objects.filter(nom='Dubois').exists())

        form = FamilleCreateForm(data={'nom': 'de Gaule'})
        self.assertTrue(form.is_valid())
        form.save()
        self.assertTrue(FamilleSOF.objects.filter(nom='de Gaule').exists())

    def test_nom_personne_formatage(self):
        famille = FamilleSOF.objects.get(nom='Haddock')
        pers = Personne.objects.get(nom='Haddock', prenom='Archibald')
        edit_url = reverse('personne-edit', args=[famille.pk, pers.pk])
        self.client.post(
            edit_url,
            data={'nom': 'doe', 'prenom': 'john', 'role': pers.role.pk, 'genre': 'M'},
            follow=True
        )
        doe = Personne.objects.get(nom='Doe', prenom='John')
        self.assertEqual(pers.pk, doe.pk)

    def test_interventions_actives(self):
        famille = FamilleSOF.objects.get(nom='Haddock')
        educ = Role.objects.get(nom='Educ')
        today = date.today()
        date_passe = today - timedelta(days=30)
        date_futur = today + timedelta(days=30)
        int1, int2, int3, int4 = IntervenantSOF.objects.bulk_create([
            IntervenantSOF(
                suivi=famille.suivi, intervenant=self.user_sof, role=educ,
                date_debut=debut, date_fin=fin
            ) for debut, fin in [
                (date_passe, date_passe + timedelta(days=5)),  # complètement passé
                (date_futur, None),  # futur
                (date_passe, date_futur),
                (date_passe, None),
            ]
        ])
        self.assertQuerySetEqual(famille.interventions_actives().order_by('pk'), [int3, int4])
        self.assertQuerySetEqual(famille.interventions_actives(today - timedelta(days=31)), [])

    def test_ajout_intervenant(self):
        famille = FamilleSOF.objects.get(nom='Haddock')
        intervenant = self.user_sof
        role = Role.objects.get(nom='Educ')
        date_debut = date.today()
        response = self.client.post(
            reverse('sof-intervenant-add', args=[famille.pk]),
            data={'intervenant': intervenant.pk, 'role': role.pk, 'date_debut': date_debut}
        )
        self.assertRedirects(response, reverse('sof-famille-suivi', args=[famille.pk]))
        self.assertTrue(
            IntervenantSOF.objects.filter(
                suivi=famille.suivisof, intervenant=intervenant, role=role, date_debut=date_debut
            ).exists()
        )

    def test_modif_intervenant(self):
        famille = FamilleSOF.objects.get(nom='Haddock')
        intervenant = self.user_sof
        role = Role.objects.get(nom='Educ')
        date_debut = date.today()
        intervention = IntervenantSOF.objects.create(
            suivi=famille.suivi, intervenant=intervenant, role=role, date_debut=date_debut
        )
        suivi_url = reverse('sof-famille-suivi', args=[famille.pk])
        add_url = reverse("sof-intervenant-add", args=[famille.pk])
        edit_url = reverse("sof-intervenant-edit", args=[famille.pk, intervention.pk])
        response = self.client.get(suivi_url)
        # Test boutons ajouter/modifier
        self.assertContains(
            response,
            ('<a href="" class="btn btn-sm btn-mini btn-primary js-add" '
             f'data-url="{add_url}" role="button" title="Ajouter un intervenant">+</a>'),
            html=True
        )
        self.assertContains(
            response,
            ('<a href="" class="btn btn-sm btn-mini js-edit"'
             f'data-url="{edit_url}" role="button" title="Modifier une intervention">'
             '<img src="/static/admin/img/icon-changelink.svg"></a>'),
            html=True
        )
        response = self.client.post(
            reverse('sof-intervenant-edit', args=[famille.pk, intervention.pk]), data={
                'intervenant': intervenant.pk, 'role': role.pk,
                'date_debut': date_debut, 'date_fin': date.today()
            }
        )
        self.assertRedirects(response, reverse('sof-famille-suivi', args=[famille.pk]))
        intervention.refresh_from_db()
        self.assertEqual(intervention.date_fin, date.today())


class SuiviTests(SOFDataMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.client.force_login(self.user_sof)

    def test_suivi_dossier(self):
        famille = FamilleSOF.objects.get(nom='Haddock')
        suivi_url = reverse('sof-famille-suivi', args=[famille.pk])
        response = self.client.get(suivi_url)
        self.assertContains(
            response,
            '<button class="btn btn-sm btn-success" name="save" type="submit">Enregistrer</button>',
            html=True
        )
        response = self.client.post(suivi_url, data={
            'ope_referent': Contact.objects.get(nom='Rathfeld').pk,
            'date_demande': '2018-10-24',
            'etape_suivi': 'demande',
        })
        self.assertRedirects(response, suivi_url)

    def test_suivi_lecture_seule(self):
        user_other = Utilisateur.objects.create_user(
            'user_other', 'user_other@example.org', nom='Spe2', prenom='Prénom', sigle='SP2'
        )
        user_other.groups.add(Group.objects.get(name='sof'))
        famille = FamilleSOF.objects.get(nom='Haddock')
        famille.suivi.intervenants.add(
            self.user_sof, through_defaults={'role': Role.objects.get(nom='Educ')}
        )
        self.client.force_login(user_other)
        suivi_url = reverse('sof-famille-suivi', args=[famille.pk])
        response = self.client.get(suivi_url + '?confirm=1')
        self.assertFalse(response.context['can_edit'])
        self.assertNotContains(response, 'Enregistrer')

    # Temporairement désactivé en attendant la finalisation de l'archivage.
    def xxtest_effacement_famille_impossible_en_janvier(self):
        self.user_sof.user_permissions.add(Permission.objects.get(codename='delete_famillesof'))
        # Recharger pour réinitialiser cache des permissions
        user_sof = Utilisateur.objects.get(pk=self.user_sof.pk)
        self.client.force_login(user_sof)
        famille = FamilleSOF.objects.get(nom='Haddock')
        famille.date_fin_suivi = date(2017, 2, 1)
        famille.motif_fin_suivi = 'autres'
        famille.date_bilan_final = date(2015, 1, 1)
        famille.save()

        response = self.client.get(reverse('sof-suivis-termines'))
        self.assertContains(response, '<th>Suppr.</th>', html=True)

        with patch('sof.models.date') as mock_date:
            mock_date.today.return_value = date(2019, 1, 15)
            response = self.client.get(reverse('sof-suivis-termines'))
            self.assertNotIn('<th>Suppr.</th>', response)

    def test_permission_effacement_definitif_saisie_par_erreur(self):
        famille = FamilleSOF.objects.create_famille(
            nom='Simpson', rue='Château1', npa=2000, localite='Moulinsart',
        )
        famille.suivisof.date_fin_suivi = date.today() - timedelta(days=500)
        famille.suivisof.save()
        self.assertFalse(famille.can_be_deleted(self.user_sof))

        self.user_sof.user_permissions.add(Permission.objects.get(codename='change_famillesof'))
        self.user_sof.user_permissions.add(Permission.objects.get(codename='can_archivesof'))
        # Recharger pour réinitialiser cache des permissions
        user_mont = Utilisateur.objects.get(pk=self.user_sof.pk)
        self.client.force_login(user_mont)
        self.assertTrue(famille.can_be_archived(user_mont))

        famille.suivisof.date_fin_suivi = None
        famille.suivisof.save()
        response = self.client.post(
            reverse('sof-famille-agenda', args=[famille.pk]),
            data={'date_demande': date.today(),
                  'date_debut_evaluation': date.today(),
                  'date_fin_evaluation': date.today(),
                  'motif_fin_suivi': 'erreur'}
        )
        # Lorsque le dossier est fermé, la redirection se fait sur la liste des familles
        self.assertRedirects(response, reverse('sof-famille-list'))

        famille.refresh_from_db()
        self.assertEqual(famille.suivisof.date_fin_suivi, date.today())
        self.assertEqual(famille.suivisof.motif_fin_suivi, 'erreur')
        self.assertTrue(famille.can_be_deleted(user_mont))

    def test_date_suivante_bilan(self):
        famille = FamilleSOF.objects.create_famille(
            nom='Simpson', rue='Château1', npa=2000, localite='Moulinsart',
        )
        famille.suivisof.date_debut_suivi = date.today() - timedelta(days=90)
        famille.suivisof.save()
        self.assertEqual(famille.suivisof.etape.code, 'debut_suivi')
        self.assertEqual(famille.suivisof.date_suivante(), date.today())
        Bilan.objects.create(
            date=date.today() - timedelta(days=5),
            auteur=self.user,
            famille=famille,
        )
        self.assertEqual(famille.suivisof.date_suivante(), date.today() + timedelta(days=90 - 5))

    def test_date_fin_suivi_prevue(self):
        famille = FamilleSOF.objects.create_famille(
            nom='Simpson', rue='Château1', npa=2000, localite='Moulinsart',
        )
        famille.suivisof.date_debut_suivi = date.today() - timedelta(days=90)
        famille.suivisof.save()
        self.assertEqual(
            SuiviSOF.WORKFLOW['fin_suivi'].delai_depuis(famille.suivisof),
            famille.suivisof.date_debut_suivi + timedelta(days=EtapeFin.delai_standard)
        )

    def test_bilan_form(self):
        famille = FamilleSOF.objects.create_famille(
            nom='Simpson', rue='Château1', npa=2000, localite='Moulinsart',
        )
        famille.suivisof.intervenants.add(
            self.user_sof, through_defaults={'role': Role.objects.get(nom='Educ')}
        )
        famille.suivisof.intervenants.add(
            self.user_sof2, through_defaults={
                'role': Role.objects.get(nom='Educ'), 'date_fin': date.today() - timedelta(days=3)
            }
        )

        self.client.force_login(self.user_sof)
        response = self.client.get(reverse('famille-bilansof-add', args=[famille.pk]))
        # Seule l'intervention active est visible
        self.assertQuerySetEqual(response.context['form']['sig_interv'].field.queryset, [self.user_sof])

        response = self.client.post(reverse('famille-bilansof-add', args=[famille.pk]), data={
            'date': '2020-08-30',
            'objectifs': '<p class="msoNormal"><span>Objectifs</span></p>',
            'rythme': '<p class="msoNormal"><span>Rythme</span></p>',
        })
        self.assertRedirects(response, reverse('sof-famille-agenda', args=[famille.pk]))
        bilan = famille.bilans_sof.first()
        self.assertEqual(bilan.objectifs, '<p>Objectifs</p>')
        self.assertEqual(bilan.rythme, '<p>Rythme</p>')


class AgendaTests(SOFDataMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.client.force_login(self.user_sof)
        self.request = RequestFactory().get('/')
        self.request.user = self.user_sof

    def test_affichage_agenda(self):
        famille = FamilleSOF.objects.get(nom='Haddock')
        self.client.force_login(self.user_sof)
        suivi_url = reverse('sof-famille-agenda', args=[famille.pk])
        response = self.client.get(suivi_url)
        self.assertContains(
            response,
            '<input type="text" name="date_demande" value="{}" '
            'class="vDateField vDateField-rounded" size="10" id="id_date_demande">'.format(format_d_m_Y(date.today())),
            html=True
        )

    def test_dates_obligatoires(self):
        today = date.today()
        form_data = {
            field_name: today for field_name in AgendaForm._meta.fields
            if field_name not in ['date_fin_suivi', 'motif_fin_suivi']
        }
        for field, etape in SuiviSOF.WORKFLOW.items():
            data2 = dict(form_data)
            if etape.oblig is True:
                etape_preced_oblig = SuiviSOF.WORKFLOW[etape.preced_oblig]
                data2[etape_preced_oblig.date_nom()] = ''
                if etape.code == 'fin_suivi':
                    data2['date_fin_suivi'] = today
                    data2['motif_fin_suivi'] = 'evol_positive'
                form = AgendaForm(data2, request=self.request)
                self.assertFalse(form.is_valid())
                self.assertEqual(form.errors, {'__all__': ['La date «{}» est obligatoire'.format(etape_preced_oblig)]})
            else:
                form = AgendaForm(data2, request=self.request)
                self.assertTrue(form.is_valid())

    def test_fin_suivi(self):
        famille = FamilleSOF.objects.create_famille(
            nom='Loiseau', rue='Château1', npa=2000, localite='Moulinsart',
        )
        today = date.today()
        suivi = famille.suivisof
        form_data = {}
        suivi.date_demande = form_data['date_demande'] = today - timedelta(days=360)
        suivi.date_debut_evaluation = form_data['date_debut_evaluation'] = today - timedelta(days=290)
        suivi.date_fin_evaluation = form_data['date_fin_evaluation'] = today - timedelta(days=250)
        suivi.date_debut_suivi = form_data['date_debut_suivi'] = today - timedelta(days=120)
        suivi.save()
        form = AgendaForm(
            data={**form_data, 'date_fin_suivi': today - timedelta(days=5),
                  'motif_fin_suivi': 'evol_positive'},
            instance=suivi,
            request=self.request,
        )
        self.assertTrue(form.is_valid(), msg=form.errors)
        suivi = form.save()
        self.assertEqual(suivi.date_fin_suivi, today - timedelta(days=5))

    def test_motif_fin_suivi_sans_date(self):
        data = {'date_demande': date.today(),
                'date_debut_evaluation': date.today(),
                'date_fin_evaluation': date.today(),
                'date_debut_suivi': date.today(),
                'motif_fin_suivi': 'evol_positive'}
        form = AgendaForm(data, request=self.request)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {'__all__': ["Les champs «Fin de l'accompagnement», «Motif de fin» et «Destination» "
                         "sont obligatoires pour fermer le dossier."]}
        )
        # Test avec date_fin_suivi non valide
        data['date_fin_suivi'] = '2019-01-32'
        form = AgendaForm(data, request=self.request)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {'date_fin_suivi': ['Saisissez une date valide.'],
             '__all__': ["Les champs «Fin de l'accompagnement», «Motif de fin» et «Destination» "
                         "sont obligatoires pour fermer le dossier."]
             }
        )

    def test_dates_non_chronologiques(self):
        data = {
            field_name: '2019-01-15' for field_name in AgendaForm._meta.fields
            if field_name not in ['date_fin_suivi', 'motif_fin_suivi']
        }
        date_field_preced = None
        for field_name in AgendaForm._meta.fields:
            if field_name in ['date_demande', 'motif_fin_suivi']:
                date_field_preced = field_name
                continue
            elif field_name == 'date_fin_suivi':
                data = dict(
                    data, date_fin_suivi='2019-01-15', motif_fin_suivi='evol_positive',
                )
            form = AgendaForm(dict(data, **{date_field_preced: '2019-01-16'}), request=self.request)
            self.assertFalse(form.is_valid())
            self.assertEqual(
                form.errors,
                {'__all__':
                    ["La date «{}» ne respecte pas l’ordre chronologique!".format(form.fields[field_name].label)]}
            )
            date_field_preced = field_name
        # Test avec trou dans les valeurs de dates
        data = {
            'date_demande': '2019-01-15',
            'date_fin_evaluation': '2019-01-01',  # Ordre chronologique non resofcté
            'date_debut_suivi': '2019-02-01'
        }
        form = AgendaForm(data, request=self.request)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {'__all__': ["La date «Fin de l’évaluation le» ne respecte pas l’ordre chronologique!"]}
        )

    def test_saisie_date_invalide(self):
        form = AgendaForm({
            'date_demande': '20.8',
        }, request=self.request)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {'date_demande': ['Saisissez une date valide.']})

    def test_saisie_par_erreur_OK(self):
        data = {'motif_fin_suivi': 'erreur'}
        form = AgendaForm(data, request=self.request)
        self.assertTrue(form.is_valid())
        self.assertEqual(form.cleaned_data['date_fin_suivi'], date.today())

    def test_demande_non_aboutie(self):
        data = {'motif_fin_suivi': 'non_aboutie'}
        form = AgendaForm(data, request=self.request)
        self.assertTrue(form.is_valid())
        self.assertEqual(form.cleaned_data['date_fin_suivi'], date.today())

    def test_abandon_durant_evaluation(self):
        data = {'date_demande': date.today(), 'motif_fin_suivi': 'evol_positive'}
        form = AgendaForm(data, request=self.request)
        self.assertTrue(form.is_valid(), form.errors)

    def test_dates_tardives(self):
        date_passee = date.today() - timedelta(days=31 + PrestationSOF.DELAI_SAISIE_SUPPL)
        form_data = {}
        suivi = SuiviSOF()
        for date_field in AgendaForm._meta.fields:
            if not date_field.startswith('date'):
                continue
            if date_field == 'date_fin_suivi':
                form_data['motif_fin_suivi'] = 'evol_positive'
            form_data[date_field] = date_passee
            form = AgendaForm(data=form_data, instance=suivi, request=self.request)
            self.assertFalse(form.is_valid())
            if date_field == 'date_fin_suivi':
                self.assertEqual(
                    form.errors,
                    {date_field: ["La saisie de dates pour le mois précédent n’est pas permise !"],
                     '__all__': ["Les champs «Fin de l'accompagnement», «Motif de fin» et «Destination» "
                                 "sont obligatoires pour fermer le dossier."]}
                )
            else:
                self.assertEqual(
                    form.errors,
                    {date_field: ["La saisie de dates pour le mois précédent n’est pas permise !"]}
                )
            setattr(suivi, date_field, date.today() - timedelta(days=90))
            form_data[date_field] = date.today() - timedelta(days=90)

    def test_date_demande_anticipee(self):
        date_demande = date.today() + timedelta(days=1)
        data = {'date_demande': date_demande}
        form = AgendaForm(data, request=self.request)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {'date_demande': ["La saisie anticipée est impossible !"]}
        )

    def test_date_debut_suivi_anticipe_ok(self):
        data = {'date_demande': date.today(),
                'date_debut_evaluation': date.today() + timedelta(days=ANTICIPATION_POUR_DEBUT_SUIVI),
                'date_fin_evaluation': date.today() + timedelta(days=ANTICIPATION_POUR_DEBUT_SUIVI),
                'date_debut_suivi': date.today() + timedelta(days=ANTICIPATION_POUR_DEBUT_SUIVI)}
        form = AgendaForm(data, request=self.request)
        self.assertTrue(form.is_valid())

    def test_date_debut_suivi_anticipe_error(self):
        data = {'date_demande': date.today(),
                'date_debut_evaluation': date.today() + timedelta(days=ANTICIPATION_POUR_DEBUT_SUIVI + 1),
                'date_fin_evaluation': date.today() + timedelta(days=ANTICIPATION_POUR_DEBUT_SUIVI + 1),
                'date_debut_suivi': date.today() + timedelta(days=ANTICIPATION_POUR_DEBUT_SUIVI + 1)}
        form = AgendaForm(data, request=self.request)
        self.assertFalse(form.is_valid())

    def test_date_anticipee_pour_groupe_admin(self):
        famille = FamilleSOF.objects.get(nom='Haddock')
        self.client.force_login(self.user_admin)
        response = self.client.post(
            reverse('sof-famille-agenda', args=[famille.pk]),
            data={'date_demande': '2022-09-01'},
            follow=True
        )
        self.assertContains(
            response,
            '<li class="alert alert-warning">Les dates saisies peuvent affecter les statistiques '
            'déjà communiquées !</li>',
            html=True
        )
        self.assertContains(
            response,
            '<li class="alert alert-success">Les modifications ont été enregistrées avec succès</li>',
            html=True
        )

    def test_affichage_intervention_temporaire(self):
        famille = FamilleSOF.objects.get(nom='Haddock')
        intervenant = self.user_sof
        role = Role.objects.get(nom='Educ')
        date_debut = date.today()
        intervention = IntervenantSOF.objects.create(
            suivi=famille.suivi, intervenant=intervenant, role=role, date_debut=date_debut, date_fin=date.today()
        )
        self.client.force_login(self.user_sof)
        suivi_url = reverse('sof-famille-agenda', args=[famille.pk])
        response = self.client.get(suivi_url)
        self.assertContains(
            response,
            (
                f"<div>Sof Prénom (Educ) ({format_d_m_Y(intervention.date_debut)} - "
                f"{format_d_m_Y(intervention.date_fin)})</div>"
            ),
            html=True
        )


class JournalisationTests(SOFDataMixin, TestCase):
    def test_acces_famille_journalise(self):
        famille = FamilleSOF.objects.get(nom='Haddock')
        IntervenantSOF.objects.create(
            suivi=famille.suivi, intervenant=self.user_sof,
            role=Role.objects.get(nom='Educ'),
            date_debut=date.today(),
        )
        IntervenantSOF.objects.create(
            suivi=famille.suivi, intervenant=self.user_sof2,
            role=Role.objects.get(nom='Educ'),
            date_debut=date.today() - timedelta(days=3),
            date_fin=date.today() - timedelta(days=1),
        )
        self.client.force_login(self.user_sof)
        suivi_url = reverse('sof-famille-suivi', args=[famille.pk])
        response = self.client.get(suivi_url)
        self.assertTemplateUsed(response, 'sof/suivi_edit.html')

        self.client.force_login(self.user_sof2)
        response = self.client.get(suivi_url)
        self.assertTemplateUsed(response, 'croixrouge/acces_famille.html')
        response = self.client.get(suivi_url + '?confirm=1')
        self.assertTemplateUsed(response, 'sof/suivi_edit.html')
        line = JournalAcces.objects.get(utilisateur=self.user_sof)
        self.assertTrue(line.ordinaire)
        line = JournalAcces.objects.get(utilisateur=self.user_sof2)
        self.assertFalse(line.ordinaire)


@tag('pdf')
class PdfTests(SOFDataMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.client.force_login(self.user_sof)

    def _test_print_pdf(self, url, filename):
        self.client.force_login(self.user_sof)
        response = self.client.get(url)
        self.assertEqual(
            response['content-disposition'],
            'attachment; filename="{}"'.format(filename)
        )
        self.assertEqual(response['content-type'], 'application/pdf')
        self.assertGreater(len(response.getvalue()), 200)

    def test_print_bilan(self):
        fam = FamilleSOF.objects.get(nom='Haddock')
        bilan = Bilan.objects.create(
            date=date(2020, 11, 3),
            auteur=self.user,
            famille=fam,
            objectifs="<p>Para 1<br><br>Para 2</p>",
            rythme="<p>Para 1<br><br>Para 2</p>",
        )
        self._test_print_pdf(
            reverse('sof-print-bilan', args=(bilan.pk,)),
            'haddock_bilan_20201103.pdf'
        )

    def test_print_coord_famille_un_enfant_un_parent(self):
        fam = FamilleSOF.objects.get(nom='Haddock')
        Personne.objects.create_personne(
            famille=fam, prenom='Gaston', nom='Lagaffe',
            role=Role.objects.get(nom='Enfant suivi')
        )
        self._test_print_pdf(
            reverse('sof-print-coord-famille', args=(fam.pk,)),
            'haddock_coordonnees.pdf'
        )

    def test_print_coord_un_enfant_deux_parents(self):
        fam = FamilleSOF.objects.get(nom='Haddock')
        Personne.objects.create_personne(
            famille=fam, prenom='Gaston', nom='Lagaffe',
            role=Role.objects.get(nom='Enfant suivi')
        )
        Personne.objects.create_personne(
            famille=fam, prenom='Maude', nom='Zarella',
            role=Role.objects.get(nom='Mère')
        )
        self._test_print_pdf(
            reverse('sof-print-coord-famille', args=(fam.pk,)),
            'haddock_coordonnees.pdf'
        )

    def test_print_coord_pere_mere_beaupere(self):
        fam = FamilleSOF.objects.get(nom='Haddock')
        Personne.objects.create_personne(
            famille=fam, prenom='Gaston', nom='Lagaffe',
            role=Role.objects.get(nom='Enfant suivi')
        )
        Personne.objects.create_personne(
            famille=fam, prenom='Maude', nom='Zarella',
            role=Role.objects.get(nom='Mère')
        )
        Personne.objects.create_personne(
            famille=fam, prenom='Aloïs', nom='Silalune',
            role=Role.objects.get(nom='Beau-père')
        )
        self._test_print_pdf(
            reverse('sof-print-coord-famille', args=(fam.pk,)),
            'haddock_coordonnees.pdf'
        )

    def test_print_sans_famille(self):
        famille = FamilleSOF.objects.create_famille(
            nom='Haddock', rue='Château1', npa=2000, localite='Moulinsart',
            autorite_parentale='conjointe', statut_marital='divorce',
            monoparentale=False
        )
        Personne.objects.create_personne(
            famille=famille, prenom='Gaston', nom='Lagaffe',
            role=Role.objects.get(nom='Enfant suivi')
        )
        self._test_print_pdf(
            reverse('sof-print-coord-famille', args=(famille.pk,)),
            'haddock_coordonnees.pdf'
        )


class PrestationTests(SOFDataMixin, TestCase):

    def setUp(self):
        super().setUp()
        self.famille = FamilleSOF.objects.get(nom='Haddock')
        Personne.objects.create_personne(
            famille=self.famille, role=Role.objects.get(nom='Enfant suivi'),
            nom='Haddock', prenom='Paulet', genre='M', date_naissance=date(1956, 2, 16),
            rue='Château1', npa=2000, localite='Moulinsart',
        )
        self.famille.suivisof.intervenants.add(
            self.user_sof, through_defaults={'role': Role.objects.get(nom='Educ')}
        )
        self.prest_fam = LibellePrestation.objects.get(code='sof01')  # Évaluation
        self.prest_gen = LibellePrestation.objects.get(code='sof03')

    def create_prestation_fam(self, texte=""):
        prest = PrestationSOF.objects.create(
            auteur=self.user_sof, date_prestation=date.today(), duree=timedelta(hours=8),
            famille=self.famille, lib_prestation=self.prest_fam, texte=texte
        )
        prest.intervenants.add(self.user_sof, self.user_sof2)
        return prest

    def create_prestation_gen(self):
        data = dict(
            auteur=self.user_sof, date_prestation=date.today(), duree=timedelta(hours=8),
            famille=None, lib_prestation=self.prest_gen
        )
        prest = PrestationSOF.objects.create(**data)
        prest.intervenants.add(self.user_sof)
        return prest

    def test_prestation_texte_clean(self):
        """Leading and trailing empty paragraphs are stripped."""
        texte = (
            '<p></p>\n'
            '<p>Echanges mail fin de suivi, au revoir à la famille<br></p>\n'
            '<p> </p>'
        )
        data = dict(
            duree='3:40', date_prestation=date.today(), lib_prestation=self.prest_fam.pk,
            intervenants=[self.user_sof.pk], texte=texte
        )
        form = PrestationSOFForm(famille=self.famille, user=self.user_sof, data=data)
        self.assertTrue(form.is_valid())
        self.assertEqual(
            form.cleaned_data['texte'],
            '<p>Echanges mail fin de suivi, au revoir à la famille<br></p>'
        )

    def test_prestation_date_prestation_dans_mois_courant(self):
        data = dict(
            duree='3:40', date_prestation=date.today(), lib_prestation=self.prest_fam.pk,
            intervenants=[self.user_sof.pk]
        )
        form = PrestationSOFForm(famille=self.famille, user=self.user_sof, data=data)
        self.assertTrue(form.is_valid(), msg=form.errors)

    def test_prestation_saisie_anticipee(self):
        data = dict(
            duree='3:40', date_prestation=date.today() + timedelta(days=1),
            lib_prestation=self.prest_fam.pk, intervenants=[self.user_sof.pk]
        )
        form = PrestationSOFForm(famille=self.famille, user=self.user_sof, data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {'date_prestation': ['La saisie anticipée est impossible !']})

    def test_prestation_saisie_tardive(self):
        data = dict(
            duree='3:40', date_prestation=date.today() - timedelta(days=31 + PrestationSOF.DELAI_SAISIE_SUPPL),
            lib_prestation=self.prest_fam.pk, intervenants=[self.user_sof.pk]
        )
        form = PrestationSOFForm(famille=self.famille, user=self.user_sof, data=data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {'date_prestation': ['La saisie des prestations des mois précédents est close !']}
        )

    def test_prestation_edit_perm_speciale(self):
        prest = self.create_prestation_fam()
        prest.date_prestation = date.today() - timedelta(days=31 + PrestationSOF.DELAI_SAISIE_SUPPL)
        prest.save()
        self.user_admin.user_permissions.add(
            Permission.objects.get(codename='edit_prest_prev_month', content_type__app_label='sof')
        )
        self.client.force_login(self.user_admin)
        response = self.client.post(reverse('sof-prestation-edit', args=[self.famille.pk, prest.pk]), data={
            'date_prestation': prest.date_prestation,
            'duree': '00:45',
            'intervenants': [self.user_sof.pk],
            'texte': 'foo',
        })
        self.assertRedirects(response, reverse('sof-journal-list', args=[self.famille.pk]))
        prest.refresh_from_db()
        self.assertEqual(prest.texte, 'foo')

    def test_ase_duree_toujours_0(self):
        user_ase = Utilisateur.objects.create_user(
            'user_ase', 'user_ase@example.org',
            nom='Ase1'
        )
        user_ase.roles.add(Role.objects.get(nom='ASE'))
        user_ase.groups.add(Group.objects.get(name='sof'))
        IntervenantSOF.objects.create(
            suivi=self.famille.suivisof, intervenant=user_ase, role=Role.objects.get(nom="Educ")
        )
        self.client.force_login(user_ase)
        response = self.client.get(reverse('sof-prestation-famille-add', args=[self.famille.pk]))
        self.assertNotContains(response, "Durée")
        response = self.client.post(reverse('sof-prestation-famille-add', args=[self.famille.pk]), data={
            'date_prestation': date.today(),
            'intervenants': [user_ase.pk],
            'texte': "Un commentaire",
        })
        self.assertRedirects(response, reverse('sof-journal-list', args=[self.famille.pk]))
        prestation = user_ase.prestations('sof')[0]
        self.assertEqual(prestation.duree, timedelta())

    def test_add_prestation_fam(self):
        add_url = reverse('sof-prestation-famille-add', args=[self.famille.pk])
        data = dict(
            duree='3:40', date_prestation=date.today() - timedelta(days=3), famille=self.famille.pk,
            intervenants=[self.user_sof.pk]
        )
        self.client.force_login(self.user_sof)
        response = self.client.post(add_url, data)
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        self.assertRedirects(response, reverse('sof-journal-list', args=[self.famille.pk]))
        prestation = self.famille.prestations.get(duree="3:40")
        # Évaluation SOF choisie auto car pas de date de début de suivi
        self.assertEqual(prestation.lib_prestation.code, 'sof01')

        # Le type de prestation dépend de la date exacte de début de suivi.
        self.famille.suivi.date_debut_suivi = date.today() - timedelta(days=1)
        self.famille.suivi.save()
        data.update({'date_prestation': date.today(), 'duree': '1:00'})
        response = self.client.post(add_url, data)
        prestation = self.famille.prestations.get(duree="1:00")
        self.assertEqual(prestation.lib_prestation.code, 'sof02')
        data.update({'date_prestation': date.today() - timedelta(days=2), 'duree': '1:30'})
        response = self.client.post(add_url, data)
        prestation = self.famille.prestations.get(duree="1:30")
        self.assertEqual(prestation.lib_prestation.code, 'sof01')

    def test_affichage_prestation_menu_fam(self):
        self.create_prestation_fam()
        menu_url = reverse('sof-prestation-menu')
        self.client.force_login(self.user_sof)
        response = self.client.get(menu_url)
        self.assertContains(
            response,
            '<td class="total">16:00</td>',
            html=True
        )
        self.assertContains(
            response,
            '<td class="mesprest">08:00</td>',
            html=True
        )

    def test_add_prestation_gen(self):
        add_url = reverse('sof-prestation-gen-add')
        data = dict(
            duree='3:40', famille='',
            date_prestation=date.today() - timedelta(days=360),  # Too old!
            intervenants=[self.user_sof.pk]
        )
        self.client.force_login(self.user_sof)
        response = self.client.post(add_url, data)
        self.assertEqual(
            response.context['form'].errors,
            {'date_prestation': ['La saisie des prestations des mois précédents est close !']}
        )
        data['date_prestation'] = date.today()
        response = self.client.post(add_url, data)
        self.assertRedirects(response, reverse('sof-prestation-gen-list'))
        self.assertEqual(self.user_sof.prestations_sof.first().lib_prestation.code, 'sof03')

    def test_update_prestation_fam(self):
        prest = self.create_prestation_fam()
        url = reverse('sof-prestation-edit', args=[self.famille.pk, prest.pk])
        self.client.force_login(self.user_sof)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        data = model_to_dict(prest)
        del data['medlink_date']
        data['fichier'] = ''
        data['intervenants'] = [str(interv.pk) for interv in data['intervenants']]
        data['duree'] = '12:00'
        response = self.client.post(url, data=data)
        prest.refresh_from_db()
        self.assertEqual(prest.duree, timedelta(hours=12))

    def test_correction_prestation_debut_suivi_passe(self):
        """
        Quand un début de suivi est saisi dans le passé, les prestations depuis
        cette date sont passées en accompagnement.
        """
        prest = self.create_prestation_fam()
        self.assertEqual(prest.lib_prestation.code, 'sof01')
        self.assertIsNone(self.famille.suivi.date_debut_suivi)
        self.famille.suivisof.intervenants.add(
            self.user, through_defaults={'role': Role.objects.get(nom='Educ')}
        )
        self.client.force_login(self.user_sof)
        response = self.client.post(
            reverse('sof-famille-agenda', args=[self.famille.pk]),
            data={
                'date_demande': date.today() - timedelta(days=4),
                'date_debut_evaluation': date.today() - timedelta(days=4),
                'date_fin_evaluation': date.today() - timedelta(days=2),
                'date_debut_suivi': date.today() - timedelta(days=2),
            },
        )
        self.assertEqual(response.status_code, 302)
        self.famille.refresh_from_db()
        self.assertIsNotNone(self.famille.suivi.date_debut_suivi)
        prest.refresh_from_db()
        self.assertEqual(prest.lib_prestation.code, 'sof02')

    def test_droit_modification_prestation_familiale(self):
        prest = PrestationSOF.objects.create(
            famille=self.famille,
            auteur=self.user_admin,
            lib_prestation=LibellePrestation.objects.get(code='sof01'),
            date_prestation=date.today(),
            duree='1:10'
        )
        prest.intervenants.set([self.user_sof])

        self.client.force_login(self.user_sof2)
        with self.assertLogs('django.request', level='WARNING'):
            response = self.client.get(reverse('sof-prestation-edit', args=[self.famille.pk, prest.pk]))
            self.assertEqual(response.status_code, 403)
        self.assertTrue(prest.can_edit(self.user_sof))

        self.client.force_login(self.user_admin)
        response = self.client.get(reverse('sof-prestation-edit', args=[self.famille.pk, prest.pk]))
        self.assertEqual(response.status_code, 200)

    def test_affichage_prestation_personnelle(self):
        self.create_prestation_fam()
        self.create_prestation_gen()
        self.client.force_login(self.user_sof)
        response = self.client.get(reverse('prestation-personnelle', args=['sof']))
        self.assertContains(
            response,
            '<tr><th>Total prestations sof01</th><td align="right">08:00</td></tr>',
            html=True
        )
        self.assertContains(
            response,
            '<tr><th>Total prestations sof03</th><td align="right">08:00</td></tr>',
            html=True
        )
        self.assertContains(
            response,
            '<tr class="border-top"><th>Total</th><th class ="text-end">16:00</th></tr>',
            html=True
        )

    def test_affichage_prestation_generale(self):
        self.create_prestation_fam()
        self.create_prestation_gen()
        self.client.force_login(self.user_sof)
        response = self.client.get(reverse('prestation-generale', args=['sof']))
        self.assertContains(
            response,
            '<td>08:00</td>',
            html=True,
            count=1
        )

    def test_suppression_prestation_famille(self):
        prest_fam = self.create_prestation_fam()
        self.client.force_login(self.user_sof)
        response = self.client.post(reverse('sof-prestation-delete', args=[prest_fam.famille_id, prest_fam.pk]))
        self.assertRedirects(response, reverse('sof-journal-list', args=[prest_fam.famille_id]))
        self.assertFalse(PrestationSOF.objects.filter(pk=prest_fam.pk).exists())

    def test_recherche_prestations(self):
        texte1 = "La famille Lucky va bien"
        texte2 = "En été, Luke tire bien plus vite que son ombre!"
        prest1 = self.create_prestation_fam(texte1)
        prest2 = self.create_prestation_fam(texte2)
        self.client.force_login(self.user_sof)
        url = reverse('sof-journal-list', args=[self.famille.pk])

        # Note: les chaînes à rechercher sont en majuscule pour s'assurer que
        # la recherche n'est pas sensible à la casse

        # mot contenu dans les deux enregistrements
        response = self.client.get(url, {"recherche": "BIEN"})
        self.assertQuerySetEqual(response.context["object_list"], [prest1, prest2])

        # mot contenu dans le premier enregistrement
        response = self.client.get(url, {"recherche": "FAMILLE"})
        self.assertQuerySetEqual(response.context["object_list"], [prest1])

        # mot contenu dans le deuxième enregistrement
        response = self.client.get(url, {"recherche": "OMBRE"})
        self.assertQuerySetEqual(response.context["object_list"], [prest2])

        # mot contenu dans aucun enregistrement
        response = self.client.get(url, {"recherche": "TINTIN"})
        self.assertQuerySetEqual(response.context["object_list"], [])

        # deux mots contenus chacun dans un enregistrement différent
        response = self.client.get(url, {"recherche": "FAMILLE OMBRE"})
        self.assertQuerySetEqual(response.context["object_list"], [])

        # deux mots contenus dans le même enregistrement
        response = self.client.get(url, {"recherche": "BIEN OMBRE"})
        self.assertQuerySetEqual(response.context["object_list"], [prest2])

        # Recherche d'un mot avec accent (mot clé sans accent)
        response = self.client.get(url, {"recherche": "ETE"})
        self.assertQuerySetEqual(response.context["object_list"], [prest2])

        # Recherche d'un mot sans accent (mot clé avec accent)
        response = self.client.get(url, {"recherche": "FÂMÌLLÉ"})
        self.assertQuerySetEqual(response.context["object_list"], [prest1])

    def test_message_recherche_prestations_sans_resultat(self):
        pas_de_prestation = "Aucune prestation saisie"
        recherche_vide = "Pas de résultat pour votre recherche."
        self.client.force_login(self.user_sof)

        # Message pour contenu vide sans recherche
        response = self.client.get(reverse('sof-journal-list', args=[self.famille.pk]))
        self.assertContains(response, pas_de_prestation)
        self.assertNotContains(response, recherche_vide)

        # Message pour contenu vide lors d'une recherche
        response = self.client.get(reverse('sof-journal-list', args=[self.famille.pk]), {"recherche": "VIDE"})
        self.assertContains(response, recherche_vide)
        self.assertNotContains(response, pas_de_prestation)


class RapportSOFTests(SOFDataMixin, TestCase):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.famille = FamilleSOF.objects.create_famille(nom='Doe', typ='sof')
        cls.educ = Utilisateur.objects.create_user('Educ', 'educ@exemple.org', '123')
        cls.educ.user_permissions.add(Permission.objects.get(codename='change_famillesof'))

    def setUp(self) -> None:
        self.create_kwargs = {
            'famille': self.famille,
            'auteur': self.educ,
            'date': date.today(),
            'situation': 'Situation initiale',
            'evolutions': 'Evolutions',
            'evaluation': 'Evaluation',
            'observations': 'Observation'
        }

    def test_create_model(self):
        rapport = RapportSOF.objects.create(**self.create_kwargs)
        self.assertIsInstance(rapport, RapportSOF)
        self.assertEqual(str(rapport), f"Résumé du {format_d_m_Y(date.today())} pour la famille Doe - ")

    def test_create_view_with_observations(self):
        # New rapports always with observations field
        self.client.force_login(self.educ)
        response = self.client.get(reverse('famille-rapport-add', args=[self.famille.pk]))
        self.assertNotContains(response, "Observations IPE")
        self.assertNotContains(response, "Observations SOF")
        self.assertNotContains(response, "Observations APA")
        self.assertContains(
            response,
            '<label for="id_observations">Observations, évolution et hypothèses :</label>',
            html=True
        )
        self.assertNotIn('Évaluation / Hypothèses', response)
        self.assertNotIn('Évolutions et observations', response)

    def test_display_old_rapport_without_observations(self):
        # evolutions and evaluation not empty
        rapport = RapportSOF.objects.create(**self.create_kwargs)
        self.client.force_login(self.educ)
        response = self.client.get(reverse('famille-rapport-view', args=[self.famille.pk, rapport.pk]))

        self.assertContains(response, '<h3>Évaluation / Hypothèses</h3>', html=True)
        self.assertContains(response, '<h3>Évolutions et observations</h3>', html=True)
        self.assertNotIn('Observations', response)

    def test_display_new_rapport_with_observations(self):
        # evolutions and evaluation are empty
        self.create_kwargs['evolutions'] = ''
        self.create_kwargs['evaluation'] = ''
        rapport = RapportSOF.objects.create(**self.create_kwargs)
        self.client.force_login(self.educ)
        response = self.client.get(reverse('famille-rapport-view', args=[self.famille.pk, rapport.pk]))

        self.assertNotContains(response, '<h3>Évaluation / Hypothèses</h3>', html=True)
        self.assertNotContains(response, '<h3>Évolutions et observations</h3>', html=True)
        self.assertContains(response, '<h3>Observations, évolution et hypothèses</h3>', html=True)

    def test_edit_rapport_by_ipe(self):
        rapport = RapportSOF.objects.create(**self.create_kwargs)
        user_ipe = Utilisateur.objects.create_user('IPE', 'ipe@exemple.org', '123')
        user_ipe.roles.add(Role.objects.get(nom='IPE'))
        user_ipe.groups.add(Group.objects.get(name='sof'))
        self.famille.suivisof.intervenants.add(self.educ, through_defaults={'role': Role.objects.get(nom='Educ')})
        self.famille.suivisof.intervenants.add(user_ipe, through_defaults={'role': Role.objects.get(nom='IPE')})
        self.assertFalse(rapport.can_delete(user_ipe))
        self.client.force_login(user_ipe)
        response = self.client.get(reverse('famille-rapport-view', args=[self.famille.pk, rapport.pk]))
        self.assertContains(response, "Modifier")
        self.assertNotContains(response, "Supprimer")
        url = reverse('famille-rapport-edit', args=[self.famille.pk, rapport.pk])
        response = self.client.get(url)
        self.assertTrue(response.context['form'].fields['situation'].disabled)
        ipe_content = "<p>Des observations de la part d’un-e IPE</p>"
        response = self.client.post(url, data={
            'observ_ipe': ipe_content,
        })
        self.assertRedirects(response, reverse('sof-famille-agenda', args=[self.famille.pk]))
        rapport.refresh_from_db()
        self.assertEqual(rapport.observ_ipe, ipe_content)

    def test_edit_rapport_by_sof(self):
        self.famille.suivisof.intervenants.add(self.educ, through_defaults={'role': Role.objects.get(nom='Educ')})
        rapport = RapportSOF.objects.create(**self.create_kwargs)
        rapport.pres_interv.set([self.educ])
        rapport.sig_interv.set([self.educ])
        user_sof = Utilisateur.objects.create_user('IPE', 'ipe@exemple.org', '123')
        user_sof.roles.add(Role.objects.get(nom='Assistant-e social-e'))
        user_sof.groups.add(Group.objects.get(name='sof'))
        self.client.force_login(user_sof)
        url = reverse('famille-rapport-edit', args=[self.famille.pk, rapport.pk])
        response = self.client.get(url)
        self.assertTrue(response.context['form'].fields['situation'].disabled)
        sof_content = "<p>Des observations de la part d’un-e assistant-e social-e</p>"
        response = self.client.post(url, data={
            'observ_sof': sof_content,
        })
        self.assertRedirects(response, reverse('sof-famille-agenda', args=[self.famille.pk]))
        rapport.refresh_from_db()
        self.assertEqual(rapport.observ_sof, sof_content)
