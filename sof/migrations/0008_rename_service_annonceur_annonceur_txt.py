from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sof', '0007_remove_suivisof_motif_detail'),
    ]

    operations = [
        migrations.RenameField(
            model_name='suivisof',
            old_name='service_annonceur',
            new_name='annonceur_txt',
        ),
    ]
