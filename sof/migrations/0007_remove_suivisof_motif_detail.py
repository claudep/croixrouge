from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sof', '0006_remove_suivisof_unused_fields'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='suivisof',
            name='motif_detail',
        ),
    ]
