import django.db.models.deletion
import django.utils.timezone
from django.conf import settings
from django.db import migrations, models

import common.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('croixrouge', '__first__'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='FamilleSOF',
            fields=[],
            options={
                'permissions': {('can_archivesof', 'Archiver les dossiers SOF'), ('can_manage_waiting_list', "Gérer la liste d'attente")},
                'proxy': True,
                'indexes': [],
                'constraints': [],
            },
            bases=('croixrouge.famille',),
        ),
        migrations.CreateModel(
            name='IntervenantSOF',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_debut', models.DateField(default=django.utils.timezone.now, verbose_name='Date début')),
                ('date_fin', models.DateField(blank=True, null=True, verbose_name='Date fin')),
                ('intervenant', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('role', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='croixrouge.role')),
            ],
        ),
        migrations.CreateModel(
            name='SuiviSOF',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('heure_coord', models.BooleanField(default=False, verbose_name='Heure de coordination')),
                ('difficultes', models.TextField(blank=True, verbose_name='Difficultés')),
                ('aides', models.TextField(blank=True, verbose_name='Aides souhaitées')),
                ('competences', models.TextField(blank=True, verbose_name='Ressources/Compétences')),
                ('dates_demande', models.CharField(blank=True, max_length=128, verbose_name='Dates')),
                ('autres_contacts', models.TextField(blank=True, verbose_name='Autres services contactés')),
                ('disponibilites', models.TextField(blank=True, verbose_name='Disponibilités')),
                ('remarque', models.TextField(blank=True)),
                ('remarque_privee', models.TextField(blank=True, verbose_name='Remarque privée')),
                ('service_orienteur', models.CharField(blank=True, choices=[('famille', 'Famille'), ('ope', 'OPE'), ('spe', 'SPE'), ('cnpea', 'CNPea'), ('ecole', 'École'), ('res_prim', 'Réseau primaire'), ('res_sec', 'Réseau secondaire'), ('pediatre', 'Pédiatre'), ('autre', 'Autre')], max_length=15, verbose_name='Orienté vers le SOF par')),
                ('service_annonceur', models.CharField(blank=True, max_length=60, verbose_name='Service annonceur')),
                ('motif_demande', common.fields.ChoiceArrayField(base_field=models.CharField(choices=[
                    ("soutien_adm", "Soutien administratif"),
                    ("integr_enf", "Intégration sociale des enfants"),
                    ("integr_par", "Intégration sociale des parents"),
                    ("linguist", "Développement des compétences linguistiques"),
                    ("soutien_edu", "Soutien éducatif"),
                    ("soutien_par", "Soutien parental"),
                    ("violence", "Violence/maltraitance"),
                ], max_length=60), blank=True, null=True, size=None, verbose_name='Motif de la demande')),
                ('motif_detail', models.TextField(blank=True, verbose_name='Motif')),
                ('referent_note', models.TextField(blank=True, verbose_name='Autres contacts')),
                ('collaboration', models.TextField(blank=True, verbose_name='Collaboration')),
                ('ressource', models.TextField(blank=True, verbose_name='Ressource')),
                ('crise', models.TextField(blank=True, verbose_name='Gestion de crise')),
                ('date_demande', models.DateField(blank=True, default=None, null=True, verbose_name='Demande déposée le')),
                ('date_debut_evaluation', models.DateField(blank=True, default=None, null=True, verbose_name='Début de l’évaluation le')),
                ('date_fin_evaluation', models.DateField(blank=True, default=None, null=True, verbose_name='Fin de l’évaluation le')),
                ('date_debut_suivi', models.DateField(blank=True, default=None, null=True, verbose_name='Début du suivi le')),
                ('date_fin_suivi', models.DateField(blank=True, default=None, null=True, verbose_name='Fin du suivi le')),
                ('demande_prioritaire', models.BooleanField(default=False, verbose_name='Demande prioritaire')),
                ('demarche', common.fields.ChoiceArrayField(base_field=models.CharField(blank=True, choices=[('volontaire', 'Volontaire'), ('contrainte', 'Contrainte'), ('post_placement', 'Post placement'), ('non_placement', 'Eviter placement')], max_length=60), blank=True, null=True, size=None, verbose_name='Démarche')),
                ('pers_famille_presentes', models.CharField(blank=True, max_length=200, verbose_name='Membres famille présents')),
                ('ref_presents', models.CharField(blank=True, max_length=250, verbose_name='Intervenants présents')),
                ('autres_pers_presentes', models.CharField(blank=True, max_length=100, verbose_name='Autres pers. présentes')),
                ('motif_fin_suivi', models.CharField(blank=True, choices=[('desengagement', 'Désengagement'), ('evol_positive', 'Autonomie familiale'), ('non_aboutie', 'Demande non aboutie'), ('non_dispo', 'Pas de disponibilités/place'), ('erreur', 'Erreur de saisie'), ('curatelle', 'Demande de curatelle'), ('spe', 'Suivi poursuivi sous SPE'), ('autre_svc', 'Dirigé vers un autre service')], max_length=20, verbose_name='Motif de fin de suivi')),
                ('intervenants', models.ManyToManyField(blank=True, related_name='interventions_sof', through='sof.IntervenantSOF', to=settings.AUTH_USER_MODEL)),
                ('sse_referent', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='croixrouge.contact', verbose_name='SSE')),
                ('famille', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='sof.famillesof')),
            ],
            options={
                'verbose_name': 'Suivi SOF',
                'verbose_name_plural': 'Suivis SOF',
            },
        ),
        migrations.AddField(
            model_name='intervenantsof',
            name='suivi',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='sof.suivisof'),
        ),
        migrations.CreateModel(
            name='RapportSOF',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(verbose_name='Date du résumé')),
                ('situation', models.TextField(blank=True, verbose_name='Situation / contexte familial')),
                ('evolutions', models.TextField(blank=True, verbose_name='Évolutions et observations')),
                ('evaluation', models.TextField(blank=True, verbose_name='Évaluation / Hypothèses')),
                ('observ_ipe', models.TextField(blank=True, verbose_name='Observations IPE')),
                ('observ_sof', models.TextField(blank=True, verbose_name='Observations SOF')),
                ('observ_apa', models.TextField(blank=True, verbose_name='Observations APA')),
                ('projet', models.TextField(blank=True, verbose_name='Perspectives d’avenir')),
                ('observations', models.TextField(blank=True, verbose_name='Observations, évolution et hypothèses')),
                ('auteur', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL)),
                ('pres_interv', models.ManyToManyField(blank=True, related_name='+', to=settings.AUTH_USER_MODEL, verbose_name='Intervenants cités dans le résumé')),
                ('sig_interv', models.ManyToManyField(blank=True, related_name='+', to=settings.AUTH_USER_MODEL, verbose_name='Signature des intervenants')),
                ('famille', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='rapports_sof', to='sof.famillesof')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PrestationSOF',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_prestation', models.DateField(verbose_name='date de l’intervention')),
                ('duree', models.DurationField(verbose_name='durée')),
                ('familles_actives', models.PositiveSmallIntegerField(blank=True, default=0)),
                ('texte', models.TextField(blank=True, verbose_name='Contenu')),
                ('manque', models.BooleanField(default=False, verbose_name='Rendez-vous manqué')),
                ('fichier', models.FileField(blank=True, upload_to='prestations', verbose_name='Fichier/image')),
                ('medlink_date', models.DateTimeField(blank=True, null=True, verbose_name='Date import. Medlink')),
                ('auteur', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='auteur')),
                ('intervenants', models.ManyToManyField(related_name='prestations_sof', to=settings.AUTH_USER_MODEL)),
                ('lib_prestation', models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='prestations_%(app_label)s', to='croixrouge.libelleprestation')),
                ('famille', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='prestations_sof', to='sof.famillesof', verbose_name='Famille')),
            ],
            options={
                'ordering': ('-date_prestation',),
                'permissions': (('edit_prest_prev_month', 'Modifier prestations du mois précédent'),),
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Bilan',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(verbose_name='Date du bilan')),
                ('objectifs', models.TextField(verbose_name='Objectifs')),
                ('rythme', models.TextField(verbose_name='Rythme et fréquence')),
                ('sig_famille', models.BooleanField(default=True, verbose_name='Apposer signature de la famille')),
                ('fichier', models.FileField(blank=True, upload_to='bilans', verbose_name='Fichier/image')),
                ('auteur', models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='bilans_sof', to=settings.AUTH_USER_MODEL)),
                ('sig_interv', models.ManyToManyField(blank=True, related_name='+', to=settings.AUTH_USER_MODEL, verbose_name='Signature des intervenants')),
                ('famille', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='bilans_sof', to='sof.famillesof')),
            ],
        ),
    ]
